//
//  EligibilityView.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "EligibilityView.h"
#import "IndexAppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "CheckEligibility.h"
#import "InsurenceXMLData.h"
#import "NSString+validation.h"
#import "eligibilityRequest.h"
#import "eligiblePayers.h"
#import "eligibleServices.h"
#import "SHKActivityIndicator.h"

@interface EligibilityView ()

@end

@implementation EligibilityView

@synthesize patientContact,patientEmail,patientName,patientOrigin,ssnHeading,ssnValue,patientImage;
@synthesize landscape_patientContact,landscape_patientEmail,landscape_patientImage,landscape_patientName,landscape_patientOrigin,landscape_ssnHeading,landscape_ssnValue,landscapeView,portraitView,landscapebackImg,port_btn_insurance,land_btn_insurance;
@synthesize land_lbl_insurance,port_lbl_insurance,land_tbl_eligibility,port_tbl_eligibility,port_table_eligible,land_table_eligible,port_circle;
@synthesize port_btn_payer,port_btn_service,land_btn_payer,land_btn_service,port_lbl_payer,land_lbl_payer;

@synthesize isFiltered,filteredTableData,searchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initwithArray:(NSMutableDictionary *)array
{   
    arr=array;
    return self;
}
-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    
}

-(void)myOrientation
{
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        [self textValue];
        
        if ([arr objectForKey:@"FullName"]) {
             landscape_patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            landscape_patientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            landscape_patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            landscape_patientEmail.text=@"";
        }
        if ([arr objectForKey:@"ssn"]) {
            landscape_ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            landscape_ssnValue.text=@"";
        }
        if ([arr objectForKey:@"ethnicOriginName"]) {
            landscape_patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            landscape_patientOrigin.text=@"";
        }
        
        
        // landscapepatientContact.text=[arr objectForKey:@"contactNo"];
        NSString *string = [arr objectForKey:@"contactNo"];
        
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            landscape_patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
           
            landscape_patientContact.text=nil;
        }
        
        if ([arr objectForKey:@"imageName"]!=nil) {
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [landscape_patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        }
        else
        {
            landscape_patientImage.image=[UIImage imageNamed:@"defaultpatient.jpeg"];
        }
   
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 660, 1024, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.landscapeView addSubview:footerView];
        
        land_tbl_eligibility.layer.borderWidth=2.0;
        land_tbl_eligibility.layer.cornerRadius=15.0;
        land_tbl_eligibility.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        [self textValue];
      
        if ([arr objectForKey:@"FullName"]) {
            patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
           patientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
           patientEmail.text=@"";
        }
        if ([arr objectForKey:@"ssn"]) {
            ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
           ssnValue.text=@"";
        }
        if ([arr objectForKey:@"ethnicOriginName"]) {
          patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
          patientOrigin.text=@"";
        }
        
        
               //  patientContact.text=[arr objectForKey:@"contactNo"];
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
          
            patientContact.text=nil;
        }
        
        NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 910, 770, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.portraitView addSubview:footerView];
        
        port_tbl_eligibility.layer.borderWidth=2.0;
        port_tbl_eligibility.layer.cornerRadius=15.0;
        port_tbl_eligibility.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
    }
}

-(void)listXML
{
 
    if (!payerLists) {
           eligiblePayers *payer=[[eligiblePayers alloc]init];
    payerLists=[payer PayerList];
   
       payers=[self payerList];
    }
    else
        {
            if (!payers) {
                payers=[self payerList];
            }
            
        }


    if (!serviceList) {

        eligibleServices *services=[[eligibleServices alloc]init];
    serviceList=[services ServiceList];
        service=[self serviceList];
    }
    else
    {
        if (!service) {
             service=[self serviceList];
        }
       
    }
}

-(void) clearCurrentView {

	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.navigationItem.title=@"Eligibility";
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc]
                                initWithTitle:@"Logout"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:@selector(logout:)];
    self.navigationItem.rightBarButtonItem=btnSave;
    self.backImg.layer.borderWidth=2.0;
    self.backImg.layer.cornerRadius=15.0;
    self.backImg.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.landscapebackImg.layer.borderWidth=2.0;
    self.landscapebackImg.layer.cornerRadius=15.0;
    self.landscapebackImg.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    self.port_circle.layer.borderWidth=2.0;
    self.port_circle.layer.cornerRadius=15.0;
    self.port_circle.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.land_circle.layer.borderWidth=2.0;
    self.land_circle.layer.cornerRadius=15.0;
    self.land_circle.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
   
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	orientation = [[UIDevice currentDevice] orientation];
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
  
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    eligibilityRequest *eligibilityReq=[[eligibilityRequest alloc]init];
    
    requestData=[eligibilityReq patientEligibilityRequestByXML:appdalegate.patientId];
   
    
    InsurenceXMLData *patientData=[[InsurenceXMLData alloc]init];
    
    InsurenceData=[patientData patientInsurenceDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
    
    company=[[NSMutableArray alloc]init];
    
         [self myOrientation];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    eligibilityRequest *eligibilityReq=[[eligibilityRequest alloc]init];
    
    requestData=[eligibilityReq patientEligibilityRequestByXML:appdalegate.patientId];
   
    [port_table_eligible reloadData];
    [land_table_eligible reloadData];
    [self listXML];
    [port_lbl_insurance setTextColor:[UIColor blackColor]];
    [port_lbl_payer setTextColor:[UIColor blackColor]];
    [land_lbl_insurance setTextColor:[UIColor blackColor]];
    [land_lbl_payer setTextColor:[UIColor blackColor]];
    [port_btn_payer setTitle:@"Select one" forState:UIControlStateNormal];
    [land_btn_payer setTitle:@"Select one" forState:UIControlStateNormal];
    if (InsurenceData) {
        [port_btn_insurance setTitle:@"Select one" forState:UIControlStateNormal];
             [land_btn_insurance setTitle:@"Select one" forState:UIControlStateNormal];
    }
          [port_btn_service setTitle:@"Select one" forState:UIControlStateNormal];
           [land_btn_service setTitle:@"Select one" forState:UIControlStateNormal];
    
    if (InsurenceData) {
        
      
        company=[self convertString:InsurenceData];
     
        
    }
    else
    {
        [port_btn_insurance setTitle:@"Insurance information does not exist" forState:UIControlStateNormal];
        [land_btn_insurance setTitle:@"Insurance information does not exist" forState:UIControlStateNormal];
    }

}

-(NSMutableArray*)convertString:(NSMutableArray *)Mutarr

{
    NSMutableArray *array=[[NSMutableArray alloc]init];
    for (int i=0;i<=[InsurenceData count]-1;++i) {
        NSDictionary *dictionary=[InsurenceData objectAtIndex:i];
      
        NSString *str=[dictionary objectForKey:@"subscriberID"];
        
        [array addObject:str];
        dictionary=nil;
        str=nil;
      
    }
    
    return array;
}

- (IBAction)logout:(id)sender {
    
    UIAlertView *log=[[UIAlertView alloc]initWithTitle:@"Do you really want to log out?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [log show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
        }
            
        default:
            break;
    }

}

-(NSArray*)payerList
{

    NSMutableArray *array=[[NSMutableArray alloc]init];
    for (int i=0;i<=[payerLists count]-1;++i) {
        NSMutableDictionary *dictionary=[payerLists objectAtIndex:i];
      
        NSString *str=[dictionary objectForKey:@"payerName"];
        
        
        [array addObject:str];
        dictionary=nil;
        str=nil;
       
    }
    
    return array;
 }

-(NSArray*)serviceList
{

    NSMutableArray *array=[[NSMutableArray alloc]init];
    for (int i=0;i<=[serviceList count]-1;++i) {
        NSMutableDictionary *dictionary=[serviceList objectAtIndex:i];
   
        NSString *str=[dictionary objectForKey:@"serviceName"];
        [array addObject:str];
        dictionary=nil;
        str=nil;
       
    }
    
    return array;
    
}

- (IBAction)selectClicked:(id)sender {
   btr=(UIButton *)sender;

    if (btr.tag==1) {
       
        if(dropDown == nil) {
           
            CGFloat f ;
            if ([company count]<=5) {
              f=30*[company count];
                
            }
            else
            {
                f=30*5;
            }
            
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:company IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    if (btr.tag==2) {
        
        if(dropDown == nil) {
           
            butt=btr;
            CGFloat f = 30*10;
            [self addsearchBarOnButton:btr.frame];
            temper=service;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:service IA:nil D:@"down"];
           
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    if (btr.tag==3) {
      
        if(dropDown == nil) {
            butt=btr;
            CGFloat f = 30*10;
            [self addsearchBarOnButton:btr.frame];
            temper=payers;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:payers IA:nil D:@"down"];
            
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    
    [self rel];
    
    }

-(void)rel
    {
        [searchBar removeFromSuperview];
        dropDown = nil;
    }


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    UITouch *touch = [[event allTouches] anyObject];
//    CGPoint touchLocation;
//    
//    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
//        touchLocation = [touch locationInView:self.landscapeView];
//        
//        
//    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
//        
//      touchLocation = [touch locationInView:self.portraitView];
//    }

    [searchBar removeFromSuperview];
    [dropDown hideDropDown:butt];
    [self rel];
    
}


-(void)addsearchBarOnButton:(CGRect)button
{
  
    searchBar=[[UISearchBar alloc]initWithFrame:button];
    [searchBar setDelegate:self];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        [landscapeView addSubview:searchBar];
        
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        [portraitView addSubview:searchBar];
    }
    
    
}

-(IBAction)checkEligibility:(id)sender
{
    NSThread *actThread=[[NSThread alloc]initWithTarget:self selector:@selector(threadMethod) object:nil];
    [actThread start];
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
        if ([[land_btn_payer titleForState:UIControlStateNormal] isEqualToString:@"Select one"]) {
      
            eligibilitypayerID=@"";
        }
        else
        {
            eligibilityPayerName=[land_btn_payer titleForState:UIControlStateNormal];
            eligibilitypayerID=[self eligiblePayerID:[land_btn_payer titleForState:UIControlStateNormal]];
        }
        if ([[land_btn_service titleForState:UIControlStateNormal] isEqualToString:@"Select one"]) {
            
            eligibilityServiceID=@"";
        }
        else
        {
            eligibilityServiceID=[self eligibleServiceID:[land_btn_service titleForState:UIControlStateNormal]];
            
        }
        
        if ([[land_btn_insurance titleForState:UIControlStateNormal] isEqualToString:@"Select one"]) {
            
            insurenceID=@"";
        }
        else
        {
            insurenceID=[land_btn_insurance titleForState:UIControlStateNormal];
            
        }
        
        
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        if ([[port_btn_payer titleForState:UIControlStateNormal] isEqualToString:@"Select one"]) {
            
            eligibilitypayerID=@"";
            
        }
        else
        {
            eligibilityPayerName=[port_btn_payer titleForState:UIControlStateNormal];
            eligibilitypayerID=[self eligiblePayerID:[port_btn_payer titleForState:UIControlStateNormal]];
            [eligibilityPayerName stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
           
            
        }
        if ([[port_btn_service titleForState:UIControlStateNormal] isEqualToString:@"Select one"]) {
             eligibilityServiceID=@"";
        }
        else
        {
            eligibilityServiceID=[self eligibleServiceID:[port_btn_service titleForState:UIControlStateNormal]];
            
        }
        if ([[port_btn_insurance titleForState:UIControlStateNormal] isEqualToString:@"Select one"]) {
            
            insurenceID=@"";
        }
        else
        {
            insurenceID=[port_btn_insurance titleForState:UIControlStateNormal];
            
        }

    
    }
   
    if ([eligibilitypayerID isEqualToString:@""]) {
        [port_lbl_payer setTextColor:[UIColor redColor]];
        [land_lbl_payer setTextColor:[UIColor redColor]];
    }
    else
    {
        [port_lbl_payer setTextColor:[UIColor blackColor]];
        [land_lbl_payer setTextColor:[UIColor blackColor]];
    }
 
    if([insurenceID isEqualToString:@""])
    {
        [port_lbl_insurance setTextColor:[UIColor redColor]];
        [land_lbl_insurance setTextColor:[UIColor redColor]];
    }
    else
    {
        [port_lbl_insurance setTextColor:[UIColor blackColor]];
        [land_lbl_insurance setTextColor:[UIColor blackColor]];
    }
    
        
    if([insurenceID isEqualToString:@""]||[eligibilitypayerID isEqualToString:@""])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
                       
            CheckEligibility *eligibilityView=[[CheckEligibility alloc]initwithArray:arr withinsuranceID:insurenceID eligibilityID:nil eligiblePayerName:eligibilityPayerName eligiblePayerID:eligibilitypayerID eligibleServiceID:eligibilityServiceID];
            NSLog(@"%@",eligibilityView);
            [self.navigationController pushViewController:eligibilityView animated:YES];
        }
        
    
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
          
            break;
        case MFMailComposeResultSaved:
          
            [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail has been saved successfully!" delegate:self cancelButtonTitle:@"OK,thank you" otherButtonTitles:nil, nil] show];
            break;
        case MFMailComposeResultSent:
           
            [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail has been sent successfully!" delegate:self cancelButtonTitle:@"OK,thank you" otherButtonTitles:nil, nil] show];
            
            break;
            
        case MFMailComposeResultFailed:
            [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail sent failure" delegate:self cancelButtonTitle:@"OK,thank you" otherButtonTitles:nil, nil] show];
         
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textValue
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
        
        [land_btn_insurance setTitle:[port_btn_insurance titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        [land_btn_payer setTitle:[port_btn_payer titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [land_btn_service setTitle:[port_btn_service titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        [port_btn_insurance setTitle:[land_btn_insurance titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [port_btn_payer setTitle:[land_btn_payer titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [port_btn_service setTitle:[land_btn_service titleForState:UIControlStateNormal] forState:UIControlStateNormal];
	}
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (requestData) {
       return [requestData count]; 
    }
    else return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     static NSString *CellIdentifierPortrait = @"intouchemr";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierPortrait];
    }
    
    if (requestData) {
        
        NSDictionary *temp=[requestData objectAtIndex:indexPath.row];
     //   NSString *requestDate=[self TimeDifference:[temp objectForKey:@"requestDate"]];
        cell.textLabel.text=[temp objectForKey:@"requestDate"];
        cell.accessoryType=UITableViewCellAccessoryDetailDisclosureButton;
       cell.textLabel.font=[UIFont fontWithName:@"Arial" size:14];
    }
        return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict =[requestData objectAtIndex:indexPath.row];
    NSString *str_id=[NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
   
    CheckEligibility *eligibilityView=[[CheckEligibility alloc]initwithArray:arr withinsuranceID:nil eligibilityID:str_id eligiblePayerName:nil eligiblePayerID:nil eligibleServiceID:nil];
    [self.navigationController pushViewController:eligibilityView animated:YES];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict =[requestData objectAtIndex:indexPath.row];
    NSString *str_id=[NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
   
    CheckEligibility *eligibilityView=[[CheckEligibility alloc]initwithArray:arr withinsuranceID:nil eligibilityID:str_id eligiblePayerName:nil eligiblePayerID:nil eligibleServiceID:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:eligibilityView animated:YES];
}


-(NSString *)eligiblePayerID:(NSString *)payerName
{
    NSString *payerID=@"";
    for (NSMutableDictionary* patientObj in payerLists)
    {
        if ([[patientObj objectForKey:@"payerName"]isEqualToString:payerName]) {
            payerID=[patientObj objectForKey:@"payerID"];
          
        }
        
    }
    return payerID;
}

-(NSString *)eligibleServiceID:(NSString *)serviceName
{
    NSString *serviceID=@"";
    for (NSMutableDictionary* patientObj in serviceList)
    {
        if ([[patientObj objectForKey:@"serviceName"]isEqualToString:serviceName]) {
            serviceID=[patientObj objectForKey:@"serviceID"];
           
        }
        
    }
    return serviceID;
}

-(void)threadMethod
{
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
}


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    
    if(text.length == 0)
    {
        isFiltered = FALSE;
        dropDown.list=(NSMutableArray *)temper;
        [dropDown.table reloadData];
    }
    else
    {
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        for (NSDictionary* patientObj in temper)
        {
            
            NSRange descriptionRange = [patientObj.description rangeOfString:text options:NSCaseInsensitiveSearch];
            if(descriptionRange.location != NSNotFound)
            {
                [filteredTableData addObject:patientObj];
            }
            dropDown.list=filteredTableData;
            
        }
       [dropDown.table reloadData];
    }

    
    
}




@end
