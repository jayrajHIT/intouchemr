//
//  patientContactXMLData.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 23/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface patientContactXMLData : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)patientContactDataByXML:(NSString *)patientID registationNO:(NSString *)registationID;
@end
