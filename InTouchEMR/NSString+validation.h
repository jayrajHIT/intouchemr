//
//  NSString+validation.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 08/06/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (validation)
+ (BOOL ) stringIsEmpty:(NSString *) aStringl;
+ (BOOL ) stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace;
@end
