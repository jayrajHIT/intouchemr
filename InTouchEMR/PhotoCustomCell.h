//
//  PhotoCustomCell.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 20/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *Name;
@property (strong, nonatomic) IBOutlet UILabel *Type;
@property (strong, nonatomic) IBOutlet UILabel *Date;
@property (strong, nonatomic) IBOutlet UIButton *accessoryButton;
@end
