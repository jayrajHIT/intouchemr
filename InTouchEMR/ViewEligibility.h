//
//  ViewEligibility.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 06/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewEligibility : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr,*arr1;
}
-(NSMutableArray *)patientEligibilityViewByXML:(NSString *)elibilityID;


@end
