//
//  EligibilityXMLDate.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 29/06/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "EligibilityXMLDate.h"
#import "IndexAppDelegate.h"
#import "NSMutableString+xmlForm.h"

@implementation EligibilityXMLDate

-(NSMutableArray *)patientEligibilityDataByXML:(NSString *)patientID registationNO:(NSString *)registationID insuranceID:(NSString *)insuranceID eligiblePayerName:(NSString *)payer eligiblePayerID:(NSString *)payerID eligibleServiceID:(NSString *)serviceID username:(NSString *)uname
{
    
 IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSURL *url = [NSURL URLWithString:globalDele.eligibilityUrl];
    NSString *xmlText;

   
    NSMutableString *newpayer=[NSMutableString stringWithString:payer];
    [newpayer xmlSimpleEscape];
       
    if(![serviceID isEqualToString:@""])
    {
        xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><EligibleDTO><patientID>%@</patientID><registerID>%@</registerID><userName>%@</userName><subscriberMemberID>%@</subscriberMemberID><payerID>%@</payerID><payerName>%@</payerName><serviceTypeCode>%@</serviceTypeCode></EligibleDTO>",patientID,registationID,uname,insuranceID,payerID,newpayer,serviceID];
    }
    else
    {
        xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><EligibleDTO><patientID>%@</patientID><registerID>%@</registerID><userName>%@</userName><subscriberMemberID>%@</subscriberMemberID><payerID>%@</payerID><payerName>%@</payerName></EligibleDTO>",patientID,registationID,uname,insuranceID,payerID,newpayer];
    }
     
    NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse* response;
    NSError *requestError = NULL;
    
    
    NSLog(@"Request Data: %@",request);
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    
  
    
    
//      NSString *myString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
//      NSLog(@"Response Data %@",myString);
    

    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:responseData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
      //  NSLog(@"success");
        NSLog(@"%@",xmlparsing);
    }
    else
    {
      //  NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"eligibilityStatus"]||[myelement isEqualToString:@"copaymentList"]||[myelement isEqualToString:@"id"]||[myelement isEqualToString:@"coverageStatus"]||[myelement isEqualToString:@"coverageStatusLabel"]||[myelement isEqualToString:@"serviceType"]||[myelement isEqualToString:@"typeLabel"]||[myelement isEqualToString:@"requestDate"]||[myelement isEqualToString:@"insuranceCompanyName"]||[myelement isEqualToString:@"insuranceCompanyID"]||[myelement isEqualToString:@"subscriberFirstName"]||[myelement isEqualToString:@"subscriberLastName"]||[myelement isEqualToString:@"subscriberMemberID"]||[myelement isEqualToString:@"subscriberDOB"]||[myelement isEqualToString:@"subscriberGender"]||[myelement isEqualToString:@"subscriberAdd1"]||[myelement isEqualToString:@"subscriberAdd2"]||[myelement isEqualToString:@"subscriberCity"]||[myelement isEqualToString:@"subscriberState"]||[myelement isEqualToString:@"subscriberZip"]||[myelement isEqualToString:@"subscriberGroupID"]||[myelement isEqualToString:@"subscriberGroupName"]||[myelement isEqualToString:@"planType"]||[myelement isEqualToString:@"planCoverageStatus"]||[myelement isEqualToString:@"planCoverageStatusName"]||[myelement isEqualToString:@"planName"]||[myelement isEqualToString:@"planGroupName"]||[myelement isEqualToString:@"planBegin"]||[myelement isEqualToString:@"planEligibilityBegin"]||[myelement isEqualToString:@"planEligibilityEnd"]||[myelement isEqualToString:@"planEnd"]||[myelement isEqualToString:@"planNumber"]||[myelement isEqualToString:@"dependentFirstName"]||[myelement isEqualToString:@"dependentLastName"]||[myelement isEqualToString:@"dependentMemberID"]||[myelement isEqualToString:@"dependentGroupID"]||[myelement isEqualToString:@"dependentGroupName"]||[myelement isEqualToString:@"dependentAdd1"]||[myelement isEqualToString:@"dependentAdd2"]||[myelement isEqualToString:@"dependentDOB"]||[myelement isEqualToString:@"dependentGender"]||[myelement isEqualToString:@"dependentCity"]||[myelement isEqualToString:@"dependentState"]||[myelement isEqualToString:@"dependentZip"]|[myelement isEqualToString:@"responseCode"]||[myelement isEqualToString:@"agencyQualifierCode"]||[myelement isEqualToString:@"rejectReasonCode"]||[myelement isEqualToString:@"followUpActionCode"]||[myelement isEqualToString:@"rejectReasonDescription"]||[myelement isEqualToString:@"followUpActionDescription"]||[myelement isEqualToString:@"agencyQualifierDesciption"]||[myelement isEqualToString:@"dateType"]||[myelement isEqualToString:@"dateValue"]||[myelement isEqualToString:@"details"])
                        
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
        }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
   
    if ([elementName isEqualToString:@"demographicList"]||[elementName isEqualToString:@"dependentList"]||[elementName isEqualToString:@"copaymentList"])
    {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
        
    }
    else if([elementName isEqualToString:@"planDates2s"]||[elementName isEqualToString:@"planList"])
    {
                            if (!arr1) {
            arr1=[[NSMutableArray alloc]init];
        }
        [arr1 addObject:dict];
        dict=nil;
        
    }
    else if([elementName isEqualToString:@"EligibleDTO"])
    {
        if (arr1) {
            [arr addObject:arr1];
        }
        else
        {
            if (!arr) {
                arr=[[NSMutableArray alloc]init];
            }
            [arr addObject:dict];
            dict=nil;

        }
    
    }

}

@end
