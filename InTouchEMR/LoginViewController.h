//
//  LoginViewController.h
//  InTouchEMR
//
//  Created by DSC on 12/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<NSXMLParserDelegate,UINavigationBarDelegate,UIWebViewDelegate>
{
    NSString *myelement,*temp;
    NSMutableArray *arr;
    NSMutableDictionary *dict;
    UISwitch *rememberSwitch;
    NSUserDefaults *pref;
    UIImageView *footerView;
     
}
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *loginbutton;
@property (strong, nonatomic) IBOutlet UIImageView *LogoImg;
@property (strong, nonatomic) IBOutlet UIButton *buttontryit;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBorder;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;
@property (strong, nonatomic) IBOutlet UILabel *lblPassword;
@property (strong, nonatomic) IBOutlet UILabel *lblRememberMe;

@property (strong, nonatomic) IBOutlet UIImageView *loginMainBg;
@property (strong, nonatomic) IBOutlet UILabel *loginMainHeading;
//@property (strong, nonatomic) IBOutlet UIImageView *footerView;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property(nonatomic,retain) NSUserDefaults *pref;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong,nonatomic) IBOutlet UISwitch *rememberSwitch;
@property (strong, nonatomic) IBOutlet UILabel *lblNotaMember;
- (IBAction)LoginButton:(id)sender;
- (IBAction)signUp:(id)sender;
@end
