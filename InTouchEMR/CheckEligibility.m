//
//  CheckEligibility.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "CheckEligibility.h"
#import "IndexAppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "EligibilityXMLDate.h"
#import "eligibilityServiceCell.h"
#import "ViewEligibility.h"
#import "NSString+validation.h"
#import "SHKActivityIndicator.h"
#import "SVModalWebViewController.h"

@interface CheckEligibility ()

@end

@implementation CheckEligibility

@synthesize landscapeView,portraitView;
@synthesize port_coverageType,port_dateofbirth,port_ddateofbirth,port_dgender,port_dgroupName,port_dgroupNo,port_dmemberID,port_dname,port_eligibilityStart,port_gender,port_groupName,port_groupNo,port_memberId,port_patientName,port_payerName,port_planName,port_ssn,port_timePeriod,title,port_adress,sc1,port_dadress;
@synthesize land_adress,land_coverageType,land_dateofbirth,land_ddateofbirth,land_dgender,land_dgroupName,land_dgroupNo,land_dmemberID,land_dname,land_eligibilityStart,land_gender,land_groupName,land_groupNo,land_memberId,land_patientName,land_payerName,land_planName,land_ssn,land_timePeriod,land_dadress;

@synthesize port_eligibilityStatus,port_errorCode1,port_errorCode2,port_responseCode,port_errorView;
@synthesize land_eligibilityStatus,land_errorCode1,land_errorCode2,land_errorView,land_responseCode;
@synthesize patientContact,patientEmail,patientName,patientOrigin,ssnValue,landscape_patientContact,landscape_patientEmail,landscape_patientName,landscape_patientOrigin,landscape_ssnValue,patientImage,landscape_patientImage;
@synthesize port_error1_discribtion,port_error2_discribtion,land_error1_discribtion,land_error2_discribtion,port_sub_errorView,land_sub_errorView,port_services,land_services,IsDependent;


@synthesize land_details,port_details;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initwithArray:(NSMutableDictionary *)array withinsuranceID:(NSString *)insuID eligibilityID:(NSString *)eligibility eligiblePayerName:(NSString *)payer eligiblePayerID:(NSString *)payerID eligibleServiceID:(NSString *)serviceID
{
    insuranceID=insuID;
    arr=array;
    eligibilityID=eligibility;
    eligibilitypayerName=payer;
    eligibilitypayerID=payerID;
    eligibilityServiceID=serviceID;
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)orientationChanged:(NSNotification *)notification{
//    
//    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
//	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
//		if (newOrientation==orientation) {
//            
//        }
//        else
//        {
//            orientation=newOrientation;
//            [self testOrientation];
//        }
//        
////        else if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
////            orientation=newOrientation;
////            // Clear the current view and insert the orientation specific view.
////            [self clearCurrentView];
////            if ([arr1 count]==1) {
////                
////                [self.view insertSubview:land_errorView atIndex:0];
////                [self myOrientation1];
////            }
////            else
////            {
////                [self.view insertSubview:landscapeView atIndex:0];
////                [self myOrientation];
////            }
////            
////        }
////        else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
////            orientation=newOrientation;
////            // Clear the current view and insert the orientation specific view.
////            [self clearCurrentView];
////            if ([arr1 count]==1)
////            {
////                [self.view insertSubview:port_errorView atIndex:0];
////                [self myOrientation1];
////            }
////            else
////            {
////                [self.view insertSubview:portraitView atIndex:0];
////                [self myOrientation];
////            }
////        }
//
//        
//       	}
//    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
//    {
//        
//    }
//    // Do your orientation logic here
//	}

-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self testOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    
}
-(void)testOrientation
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
       
        // Clear the current view and insert the orientation specific view.
        [self clearCurrentView];
        if ([arr1 count]==1) {
            
            [self myOrientation1];
        }
        else if([arr1 count]>1)
        {
           
            [self myOrientation];
        }
        else
        {
            
        }
        
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
       
        // Clear the current view and insert the orientation specific view.
        [self clearCurrentView];
        if ([arr1 count]==1)
        {
          
            [self myOrientation1];
        }
        else if([arr1 count]>1)
        {
            
            [self myOrientation];
        }
        else
        {
            
        }
    }

}

-(void)myOrientation
{
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
        
        [self.view insertSubview:landscapeView atIndex:0];
             
        [sc1 setFrame:CGRectMake(0,20,1024, 748)];
        [sc1 setContentSize:CGSizeMake(1024,sc1.bounds.size.height+400)];
        [self.landscapeView addSubview:sc1];
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 660, 1024, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.landscapeView addSubview:footerView];
        
        land_services.layer.borderWidth=2.0;
        land_services.layer.cornerRadius=15.0;
        land_services.layer.borderColor=[UIColor lightGrayColor].CGColor;
  
        
        if ([arr objectForKey:@"ssn"]) {
            land_ssn.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            land_ssn.text=@"";
        }
        
        NSArray *array=[arr1 lastObject];
        
        land_timePeriod.text=[self timepriend:array];
        NSDictionary *dict=[array firstObjectCommonWithArray:array];
      
        
        if (dict) {
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"insuranceCompanyName"]]) {
                land_payerName.text=@"";
            }
            else
            {
                land_payerName.text=[dict objectForKey:@"insuranceCompanyName"];
            }
          
            
            for (int i=0; i<[array count]; i++) {
                NSDictionary *tempdict=[array objectAtIndex:i];
                
                if ([[tempdict objectForKey:@"dateType"] isEqualToString:@"eligibility_begin"]) {
                    land_eligibilityStart.text=[tempdict objectForKey:@"dateValue"];
                    break;
                }
                else
                {
                    land_eligibilityStart.text=@"";
                }
            }
            
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"planCoverageStatusName"]]) {
                land_coverageType.text=@"";
            }
            else
            {
                land_coverageType.text=[dict objectForKey:@"planCoverageStatusName"];
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"planName"]]) {
                land_planName.text=@"";
            }
            else
            {
                land_planName.text=[dict objectForKey:@"planName"];
            }
            
        }
       
        NSDictionary *dict2=[arr1 objectAtIndex:0];
        if (dict2) {
            
     
        land_patientName.text=[NSString stringWithFormat:@"%@ %@",[dict2 objectForKey:@"subscriberFirstName"],[dict2 objectForKey:@"subscriberLastName"]];
     
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberGroupID"]]) {
                land_groupNo.text=@"";
            }
            else
            {
                land_groupNo.text=[dict2 objectForKey:@"subscriberGroupID"];
            }
           
            
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberMemberID"]]) {
                land_memberId.text=@"";
            }
            else
            {
                land_memberId.text=[dict2 objectForKey:@"subscriberMemberID"];
            }
        
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberDOB"]]) {
                land_dateofbirth.text=@"";
            }
            else
            {
                land_dateofbirth.text=[dict2 objectForKey:@"subscriberDOB"];
            }
            
        if ([[dict2 objectForKey:@"subscriberGender"] isEqualToString:@"F"]) {
            land_gender.text=@"Female";
        }
        else
        {
            land_gender.text=@"Male";
        }
        
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberGroupID"]]) {
                land_groupNo.text=@"";
            }
            else
            {
                land_groupNo.text=[dict2 objectForKey:@"subscriberGroupID"];
            }
           
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberGroupName"]]) {
                land_groupName.text=@"";
            }
            else
            {
                land_groupName.text=[dict2 objectForKey:@"subscriberGroupName"];
            }
        
      
            NSString *add1,*city,*state,*zip;
            
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberAdd1"]])
                add1=@"";
            else
                add1=[dict2 objectForKey:@"subscriberAdd1"];
            
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberCity"]])
                city=@"";
            else
                city=[dict2 objectForKey:@"subscriberCity"];
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberState"]])
                state=@"";
            else
                state=[dict2 objectForKey:@"subscriberState"];
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberZip"]])
                zip=@"";
            else
                zip=[dict2 objectForKey:@"subscriberZip"];
            
            
            land_adress.text=[NSString stringWithFormat:@"%@ %@ %@ %@",add1,city,state,zip];
        }
        
        if (IsDependent==YES) {
            
        
        NSDictionary *dict3=[arr1 objectAtIndex:1];
      
        if (dict3) {
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentFirstName"]]&&[NSString stringIsEmpty:[dict3 objectForKey:@"dependentLastName"]]) {
                land_dname.text=@"";
            }
            else
            {
               land_dname.text=[NSString stringWithFormat:@"%@ %@",[dict3 objectForKey:@"dependentFirstName"],[dict3 objectForKey:@"dependentLastName"]];
            }
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentMemberID"]]) {
                land_dmemberID.text=@"";
            }
            else
            {
                land_dmemberID.text=[dict3 objectForKey:@"dependentMemberID"];
            }
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentGroupID"]]) {
                land_dgroupNo.text=@"";
            }
            else
            {
                land_dgroupNo.text=[dict3 objectForKey:@"dependentGroupID"];
            }
       
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentGroupName"]]) {
                land_dgroupName.text=@"";
            }
            else
            {
                land_dgroupName.text=[dict3 objectForKey:@"dependentGroupName"];
            }
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentDOB"]]) {
                land_ddateofbirth.text=@"";
            }
            else
            {
                land_ddateofbirth.text=[dict3 objectForKey:@"dependentDOB"];
            }
       
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentGender"]]) {
                land_dgender.text=@"";
            }
            else
            {
                if ([[dict3 objectForKey:@"dependentGender"] isEqualToString:@"F"]) {
                    land_dgender.text=@"Female";
                }
                else
                {
                    land_dgender.text=@"Male";
                }
            }
            
            NSString *dadd1,*dcity,*dstate,*dzip;
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentAdd1"]])
                dadd1=@"";
            else
                dadd1=[dict3 objectForKey:@"dependentAdd1"];
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentCity"]])
                dcity=@"";
            else
                dcity=[dict3 objectForKey:@"dependentCity"];
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentState"]])
                dstate=@"";
            else
                dstate=[dict3 objectForKey:@"dependentState"];
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentZip"]])
                dzip=@"";
            else
                dzip=[dict3 objectForKey:@"dependentZip"];
            
            
            land_dadress.text=[NSString stringWithFormat:@"%@ %@ %@ %@",dadd1,dcity,dstate,dzip];
            
                    }
       
        }
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		
      
        [self.view insertSubview:portraitView atIndex:0];
        
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 910, 770, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.portraitView addSubview:footerView];
        
        port_services.layer.borderWidth=2.0;
        port_services.layer.cornerRadius=15.0;
        port_services.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        if ([arr objectForKey:@"ssn"]) {
            port_ssn.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            port_ssn.text=@"";
        }
        
        NSArray *array=[arr1 lastObject];
        port_timePeriod.text=[self timepriend:array];
       
        NSDictionary *dict=[array objectAtIndex:0];
        
        if (dict) {
         
            if ([NSString stringIsEmpty:[dict objectForKey:@"insuranceCompanyName"]]) {
                port_payerName.text=@"";
            }
            else
            {
                port_payerName.text=[dict objectForKey:@"insuranceCompanyName"];
            }
            
            for (int i=0; i<[array count]; i++) {
                NSDictionary *tempdict=[array objectAtIndex:i];
            
            if ([[tempdict objectForKey:@"dateType"] isEqualToString:@"eligibility_begin"]) {
                port_eligibilityStart.text=[tempdict objectForKey:@"dateValue"];
                break;
            }
            else
            {
                port_eligibilityStart.text=@"";
            }
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"planCoverageStatusName"]]) {
                port_coverageType.text=@"";
            }
            else
            {
                port_coverageType.text=[dict objectForKey:@"planCoverageStatusName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"planName"]]) {
                port_planName.text=@"";
            }
            else
            {
                port_planName.text=[dict objectForKey:@"planName"];
            }
            
        }
        
        NSDictionary *dict2=[arr1 objectAtIndex:0];
        if (dict2) {
           
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberFirstName"]]&&[NSString stringIsEmpty:[dict2 objectForKey:@"subscriberLastName"]]) {
                port_patientName.text=@"";
            }
            else
            {
                 port_patientName.text=[NSString stringWithFormat:@"%@ %@",[dict2 objectForKey:@"subscriberFirstName"],[dict2 objectForKey:@"subscriberLastName"]];
            }
        
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberGroupID"]]) {
                port_groupNo.text=@"";
            }
            else
            {
                port_groupNo.text=[dict2 objectForKey:@"subscriberGroupID"];
            }
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberMemberID"]]) {
                port_memberId.text=@"";
            }
            else
            {
                port_memberId.text=[dict2 objectForKey:@"subscriberMemberID"];
            }
            
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberDOB"]]) {
                port_dateofbirth.text=@"";
            }
            else
            {
                port_dateofbirth.text=[dict2 objectForKey:@"subscriberDOB"];
            }
       
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberGender"]]) {
                port_gender.text=@"";
            }
            else
            {
                if ([[dict2 objectForKey:@"subscriberGender"] isEqualToString:@"F"]) {
                    port_gender.text=@"Female";
                }
                else
                {
                    port_gender.text=@"Male";
                }

            }
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberGroupID"]]) {
                port_groupNo.text=@"";
            }
            else
            {
                port_groupNo.text=[dict2 objectForKey:@"subscriberGroupID"];
            }
            
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberGroupName"]]) {
                port_groupName.text=@"";
            }
            else
            {
                port_groupName.text=[dict2 objectForKey:@"subscriberGroupName"];
            }
       
            NSString *add1,*city,*state,*zip;
            
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberAdd1"]])
                add1=@"";
            else
                add1=[dict2 objectForKey:@"subscriberAdd1"];
            
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberCity"]])
                city=@"";
            else
                city=[dict2 objectForKey:@"subscriberCity"];
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberState"]])
                state=@"";
            else
                state=[dict2 objectForKey:@"subscriberState"];
            if ([NSString stringIsEmpty:[dict2 objectForKey:@"subscriberZip"]])
                zip=@"";
            else
                zip=[dict2 objectForKey:@"subscriberZip"];
            
               port_adress.text=[NSString stringWithFormat:@"%@ %@ %@ %@",add1,city,state,zip];
                 
        }
       
        if (IsDependent==YES) {
              
        NSDictionary *dict3=[arr1 objectAtIndex:1];
      
        if ([dict3 objectForKey:@"dependentFirstName"]||[dict3 objectForKey:@"dependentLastName"]) {
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentFirstName"]]&&[NSString stringIsEmpty:[dict3 objectForKey:@"dependentLastName"]]) {
                port_dname.text=@"";
            }
            else
            {
                 port_dname.text=[NSString stringWithFormat:@"%@ %@",[dict3 objectForKey:@"dependentFirstName"],[dict3 objectForKey:@"dependentLastName"]];
            }
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentMemberID"]]) {
                port_dmemberID.text=@"";
            }
            else
            {
                port_dmemberID.text=[dict3 objectForKey:@"dependentMemberID"];
            }
        
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentGroupID"]]) {
                port_dgroupNo.text=@"";
            }
            else
            {
                port_dgroupNo.text=[dict3 objectForKey:@"dependentGroupID"];
            }

            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentGroupName"]]) {
                port_dgroupName.text=@"";
            }
            else
            {
                port_dgroupName.text=[dict3 objectForKey:@"dependentGroupName"];
            }
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentDOB"]]) {
                port_ddateofbirth.text=@"";
            }
            else
            {
                port_ddateofbirth.text=[dict3 objectForKey:@"dependentDOB"];
            }
       
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentGender"]]) {
                port_dgender.text=@"";
            }
            else
            {
                if ([[dict3 objectForKey:@"dependentGender"] isEqualToString:@"F"]) {
                    port_dgender.text=@"Female";
                }
                else
                {
                    port_dgender.text=@"Male";
                }
            }
        
            
            NSString *dadd1,*dcity,*dstate,*dzip;
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentAdd1"]])
                dadd1=@"";
            else
                dadd1=[dict3 objectForKey:@"dependentAdd1"];
            
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentCity"]])
                dcity=@"";
            else
                dcity=[dict3 objectForKey:@"dependentCity"];
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentState"]])
                dstate=@"";
            else
                dstate=[dict3 objectForKey:@"dependentState"];
            if ([NSString stringIsEmpty:[dict3 objectForKey:@"dependentZip"]])
                dzip=@"";
            else
                dzip=[dict3 objectForKey:@"dependentZip"];
            
            port_dadress.text=[NSString stringWithFormat:@"%@ %@ %@ %@",dadd1,dcity,dstate,dzip];
        }
        
        }
    }
     [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}

-(void)myOrientation1
{
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:land_errorView atIndex:0];
          
        land_sub_errorView .layer.borderWidth=2.0;
        land_sub_errorView.layer.cornerRadius=15.0;
        land_sub_errorView.layer.borderColor=[UIColor blackColor].CGColor;
       
        
        if ([arr objectForKey:@"FullName"]) {
             landscape_patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            landscape_patientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            landscape_patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            landscape_patientEmail.text=@"";
        }
        if ([arr objectForKey:@"ssn"]) {
            landscape_ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            landscape_ssnValue.text=@"";
        }
        if ([arr objectForKey:@"ethnicOriginName"]) {
            landscape_patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            landscape_patientOrigin.text=@"";
        }

   
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            landscape_patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
           
            landscape_patientContact.text=nil;
        }
        if ([arr objectForKey:@"imageName"]!=nil) {
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [landscape_patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        }
        else
        {
            landscape_patientImage.image=[UIImage imageNamed:@"defaultpatient.jpeg"];
        }

        NSDictionary *tempdict=[arr1 objectAtIndex:0];
        land_eligibilityStatus.text=[tempdict objectForKey:@"eligibilityStatus"];
        land_errorCode1.text=[tempdict objectForKey:@"followUpActionCode"];
        land_errorCode2.text=[tempdict objectForKey:@"rejectReasonCode"];
        land_responseCode.text=[tempdict objectForKey:@"responseCode"];
        land_details.text = [tempdict objectForKey:@"details"];
        
        land_error1_discribtion.text=[tempdict objectForKey:@"rejectReasonDescription"];
        land_error2_discribtion.text=[tempdict objectForKey:@"followUpActionDescription"];
        
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 660, 1024, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.land_errorView addSubview:footerView];
        
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:port_errorView atIndex:0];
        
        port_sub_errorView .layer.borderWidth=2.0;
        port_sub_errorView.layer.cornerRadius=15.0;
        port_sub_errorView.layer.borderColor=[UIColor blackColor].CGColor;
       
        
        if ([arr objectForKey:@"FullName"]) {
            patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            patientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            patientEmail.text=@"";
        }
        if ([arr objectForKey:@"ssn"]) {
            ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            ssnValue.text=@"";
        }
        if ([arr objectForKey:@"ethnicOriginName"]) {
            patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            patientOrigin.text=@"";
        }
        
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
           
            patientContact.text=nil;
        }
        NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
               
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 910, 770, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.port_errorView addSubview:footerView];
        NSDictionary *tempdict=[arr1 objectAtIndex:0];
        port_eligibilityStatus.text=[tempdict objectForKey:@"eligibilityStatus"];
        port_errorCode1.text=[tempdict objectForKey:@"followUpActionCode"];
        port_errorCode2.text=[tempdict objectForKey:@"rejectReasonCode"];
        port_responseCode.text=[tempdict objectForKey:@"responseCode"];
        port_details.text = [tempdict objectForKey:@"details"];
        port_error1_discribtion.text=[tempdict objectForKey:@"rejectReasonDescription"];
        port_error2_discribtion.text=[tempdict objectForKey:@"followUpActionDescription"];
        
    }
    
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}

-(void) clearCurrentView {
    
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
    else if (port_errorView.superview)
    {
        [port_errorView removeFromSuperview];
    }
    else if (land_errorView.superview)
    {
        [land_errorView removeFromSuperview];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.navigationItem.title=@"Eligibility";
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc]
                                initWithTitle:@"Logout"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:@selector(logout:)];
    self.navigationItem.rightBarButtonItem=btnSave;
     IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSURL *url = [NSURL URLWithString:appdalegate.eligibilityUrl];
    NSLog(@"%@",url);

    
    if (insuranceID) {
    
    EligibilityXMLDate *eligibilityDate=[[EligibilityXMLDate alloc]init];
    arr1=[eligibilityDate patientEligibilityDataByXML:appdalegate.patientId registationNO:appdalegate.registationID insuranceID:insuranceID eligiblePayerName:eligibilitypayerName eligiblePayerID:eligibilitypayerID eligibleServiceID:eligibilityServiceID username:appdalegate.username];
        NSLog(@"%@",arr1);
       
    }
    else
    {
        if (eligibilityID) {
        
        ViewEligibility *eligibilityView=[[ViewEligibility alloc]init];
        arr1=[eligibilityView patientEligibilityViewByXML:eligibilityID];
        }
}
    
    if ([arr1 count]>3) {
        
        NSDictionary *tempDict=[arr1 objectAtIndex:1];
        
        if ([tempDict objectForKey:@"dependentFirstName"]||[tempDict objectForKey:@"dependentLastName"]) {
            IsDependent=YES;
        }
        else
        {
            IsDependent=NO;
        }
    }

    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	orientation = [[UIDevice currentDevice] orientation];
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
  
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading..."];
    
    if ([arr1 count]==1) {
        
         NSDictionary *tempdict=[arr1 objectAtIndex:0];
        if ([tempdict objectForKey:@"eligibilityStatus"]||[tempdict objectForKey:@"rejectReasonCode"]) {
            [self myOrientation1];
        }
        else
        {
            [[SHKActivityIndicator currentIndicator]hide];
            av=[[UIAlertView alloc]initWithTitle:@"Patient eligibility information not found." message:@" For more information, please visit \n \n www.intouchemr.com/eligibilityinfo/ " delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK" ,nil];
//            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//            [av addSubview:button];
            [av show];
//            button.titleLabel.font = [UIFont boldSystemFontOfSize:15];
//            
//            [button setTitleColor:[UIColor colorWithRed:51/255.0 green:102/255.0 blue:187/255.0 alpha:1 ] forState:UIControlStateNormal];
//            [button setTitle:@"www.intouchemr.com/eligibilityinfo/" forState:UIControlStateNormal];
//            [button addTarget:self action:@selector(someAction:) forControlEvents:UIControlEventAllEvents];
//            [button setFrame:CGRectMake(-55, 50, 400, 100)];
            
            
         //   [av show];
            
            av.tag=1;

            
        }
    
    }
    else if ([arr1 count]==0)
    {
        [[SHKActivityIndicator currentIndicator]hide];
//        UIAlertView *log=[[UIAlertView alloc]initWithTitle:@"Patient eligibility information not found." message:@" For more information, please visit www.intouchemr.com/eligibilityinfo/" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"More information",nil];

        av=[[UIAlertView alloc]initWithTitle:@"Patient eligibility information not found." message:@" For more information, please visit \n \n www.intouchemr.com/eligibilityinfo/" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK" ,nil];
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [av addSubview:button];
        [av show];
//        button.titleLabel.font = [UIFont boldSystemFontOfSize:15];
//        
//        [button setTitleColor:[UIColor colorWithRed:51/255.0 green:102/255.0 blue:187/255.0 alpha:1 ] forState:UIControlStateNormal];
//        [button setTitle:@"www.intouchemr.com/eligibilityinfo/" forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(someAction:) forControlEvents:UIControlEventAllEvents];
//        [button setFrame:CGRectMake(-55, 50, 400, 100)];
//
//        
//        [av show];
        
        av.tag=1;
        
    }
        else
    {
    [self myOrientation];
    }  // Do any additional setup after loading the view from its nib.
}

-(IBAction)someAction:(id)sender
{
    
[av dismissWithClickedButtonIndex:-1 animated:YES];
 
    NSURL *URL = [NSURL URLWithString:@"http://www.intouchemr.com/eligibilityinfo/"];
 
    
    if ([[UIApplication sharedApplication] openURL:URL])
    {
        NSLog(@"%@%@",@"success to open url:",[URL description]);
    }
        else
    {
        NSLog(@"%@%@",@"Failed to open url::",[URL description]);
    }

   [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)logout:(id)sender {
    
    UIAlertView *log=[[UIAlertView alloc]initWithTitle:@"Do you really want to log out?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [log show];
    log.tag=0;
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==0) {
        
    switch (buttonIndex) {
        case 1:
        {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
        }
            
        default:
            break;
    }
        
        
        
    }
    
    else
    {
        switch (buttonIndex) {
            case 0:
            {
                [self.navigationController popViewControllerAnimated:YES];
                break;
               
            }
           
                
            default:
                break;
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (IsDependent==YES) {
        
    if ([arr1 count]>3) {
        temp=[arr1 subarrayWithRange:NSMakeRange(2, [arr1 count]-3)];
        
    }
    }
    else
    {
        temp=[arr1 subarrayWithRange:NSMakeRange(1, [arr1 count]-2)];
    }
       return [temp count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifierPortrait = @"portrait";

    NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"eligibilityServiceCell" owner:self options:nil];
    
    eligibilityServiceCell * cell=(eligibilityServiceCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
    
    if (cell==nil) {
        cell=[[ eligibilityServiceCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierPortrait];
        cell=[nib objectAtIndex:0];
    }
    
    NSDictionary *tempdict=[temp objectAtIndex:indexPath.row];
    
    cell.serviceType.text=[tempdict objectForKey:@"typeLabel"];
    cell.serviceValue.text=[tempdict objectForKey:@"coverageStatusLabel"];
 
       return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(NSString *)timepriend:(NSArray *)array
{
    NSString *abc=@"";
    NSString *abc1=@"";
    NSString *abc2=@"";
    for (int i=0; i<[array count]; i++) {
        NSMutableDictionary *dict=[array objectAtIndex:i];
       
        if ([[dict objectForKey:@"dateType"] isEqualToString:@"plan_begin"]) {
            abc1=[NSString stringWithFormat:@"%@",[dict objectForKey:@"dateValue"]];
        }
        if ([[dict objectForKey:@"dateType"] isEqualToString:@"plan_end"]) {
            abc2=[NSString stringWithFormat:@"%@",[dict objectForKey:@"dateValue"]];
        }
    
    }
    abc=[NSString stringWithFormat:@"%@ - %@",abc1,abc2];
    
    return abc;
}

@end
