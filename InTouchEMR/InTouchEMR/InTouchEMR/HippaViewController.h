//
//  HippaViewController.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 31/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSiOSSDK/S3/AmazonS3Client.h>
#import <AWSiOSSDK/AmazonEndpoints.h>
@interface HippaViewController : UIViewController
{
    NSMutableString *hippatext;
    CGPoint point;
    UIImageView *image;
    BOOL mouseSwiped;
    int mouseMoved;
    NSString *sign_Name;
    NSString *ACCESS_KEY_ID;
    NSString *SECRET_KEY;
    NSString *PICTURE_BUCKET;
    NSString *isagree;
     NSData *imageData;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    
    BOOL checkboxSelected;
	IBOutlet UIButton *checkboxButton;
  IBOutlet UIButton *landscapecheckboxButton;
    }

- (IBAction)checkboxButton:(id)sender;
- (IBAction)agree:(id)sender;
- (IBAction)clear:(id)sender;
@property (nonatomic, retain) AmazonS3Client *s3;
@property (strong, nonatomic)IBOutlet UILabel *signHere;
@property (strong, nonatomic)IBOutlet UILabel *landscapesignHere;
@property (strong, nonatomic)IBOutlet UIImageView *image;
@property (strong, nonatomic)IBOutlet UISwitch *agree;
@property (strong, nonatomic)IBOutlet UIImageView *land_image;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UIWebView *webView1;
@property (strong, nonatomic) IBOutlet UIWebView *webView2;
@property (strong, nonatomic) IBOutlet UIButton *port_agree;
@property (strong, nonatomic) IBOutlet UIButton *land_agree;
@end
