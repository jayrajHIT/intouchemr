//
//  IndexAppDelegate.h
//  InTouchEMR
//
//  Created by DSC on 11/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IndexViewController;

@interface IndexAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IndexViewController *viewController;
@property(strong,nonatomic) NSString *userID;
@property(strong,nonatomic) NSString *fullname;
@property(strong,nonatomic) NSString *registationID;
@property(strong,nonatomic) NSString *username;
@property(strong,nonatomic) NSString *email;
@property(strong,nonatomic) NSString *loginUrl;
@property(strong,nonatomic) NSString *patientlistUrl;
@property(strong,nonatomic) NSString *patientId;
@property(strong,nonatomic) NSString *isExternal;
@property(strong,nonatomic) NSString *addPatientUrl;
@property(strong,nonatomic) NSString *emailIdCheckUrl;
@property(strong,nonatomic) NSString *demographicsUrl;
@property(strong,nonatomic) NSString *identifierUrl;
@property(strong,nonatomic) NSString *addressUrl;
@property(strong,nonatomic) NSString *insuranceUrl;
@property(strong,nonatomic) NSString *contactUrl;
@property(strong,nonatomic) NSString *uploadUrl;
@property(strong,nonatomic) NSString *insuranceCompanyUrl;
@property(strong,nonatomic) NSString *demogrphicsUpdateUrl;
@property(strong,nonatomic) NSString *addressSavedUrl;
@property(strong,nonatomic) NSString *contactSavedUrl;
@property(strong,nonatomic) NSString *insuranceSavedUrl;
@property(strong,nonatomic) NSString *identifierSavedUrl;
@property(strong,nonatomic) NSString *documentSavedUrl;
@property(strong,nonatomic) NSString *profileImageSavedUrl;
@property(strong,nonatomic) NSString *eligibilityUrl;
@property(strong,nonatomic) NSString *eligibilityRequest;
@property(strong,nonatomic) NSString *vieweligibility;
@property(strong,nonatomic) NSString *stateList;
@property(strong,nonatomic) NSString *physicianList;
@property(strong,nonatomic) NSString *hipaaText;
@property(strong,nonatomic) NSString *hipaaSavedURL;
@property(strong,nonatomic) NSString *intouchURL;
@property(strong,nonatomic) NSString *newsletterUpdateURL;
@property(strong,nonatomic) NSString *appointmentUpdateURL;
@property(strong,nonatomic) NSString *eligiblepayersURL;
@property(strong,nonatomic) NSString *eligibleServicesURL;
@property(strong,nonatomic) NSString *appointmentStatus;
@property(strong,nonatomic) NSString *newsletterStatus;


@property(strong,nonatomic)NSString *smokingStatus;
@property(strong,nonatomic)NSString *employmentType;
@property(strong,nonatomic)NSString *identifierType;

@property(strong,nonatomic)NSString *titleType;
@property(strong,nonatomic)NSString *genderType;
@property(strong,nonatomic)NSString *documentType;
@property(strong,nonatomic)NSString *reminderList;
@property(strong,nonatomic)NSString *meritalStatus;
@property(strong,nonatomic)NSString *ethinicOrigin;
@property(strong,nonatomic)NSString *AddressTypeList;
@property(strong,nonatomic)NSString *contactType;
@property(strong,nonatomic)NSString *contactCategoryType;
@property(strong,nonatomic)NSString *appointmentReminderType;
@property(strong,nonatomic)NSString *insurePartyType;
@property(strong,nonatomic)NSString *studentType;
@property (nonatomic, assign) BOOL flag;

@end
