//
//  IndexAppDelegate.m
//  InTouchEMR
//
//  Created by DSC on 11/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//


#import "IndexAppDelegate.h"
#import "IndexViewController.h"
#import <AWSiOSSDK/AmazonLogger.h>
#import <AWSiOSSDK/AmazonErrorHandler.h>

@implementation IndexAppDelegate
@synthesize userID,registationID,username,fullname,email,loginUrl ,patientlistUrl,addPatientUrl,emailIdCheckUrl,demographicsUrl,patientId,addressSavedUrl,identifierUrl,addressUrl,insuranceUrl,uploadUrl,contactUrl,insuranceCompanyUrl,demogrphicsUpdateUrl,contactSavedUrl,insuranceSavedUrl,identifierSavedUrl,documentSavedUrl,profileImageSavedUrl,eligibilityUrl,stateList,eligibilityRequest,vieweligibility,physicianList,hipaaText,hipaaSavedURL,intouchURL,newsletterUpdateURL,appointmentUpdateURL,eligiblepayersURL,eligibleServicesURL,flag,newsletterStatus,appointmentStatus;

@synthesize smokingStatus,employmentType,identifierType;

@synthesize genderType,titleType,documentType,reminderList,meritalStatus,ethinicOrigin,AddressTypeList,contactType,contactCategoryType,appointmentReminderType,insurePartyType,studentType;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[IndexViewController alloc] initWithNibName:@"IndexViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];

   loginUrl = @"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/login/";
    patientlistUrl = @"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getPatientListByRegistrationId/";
   addPatientUrl = @"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/addPatient";
    emailIdCheckUrl = @"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/checkUserEmail";
    demographicsUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getIntakePatientDemographicsInformation/";
    identifierUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getIntakePatientIdentities";
    addressUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getIntakePatientAddresses";
    insuranceUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getIntakePatientInsurances";
    contactUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getIntakePatientContacts";
    uploadUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getPatientDocuments";
  insuranceCompanyUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getInsurances";
    demogrphicsUpdateUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/updateorsaveIntakePatientDemographicsInformation";
   addressSavedUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/saveIntakePatientAddress/";
    contactSavedUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/saveIntakePatientContact";
    insuranceSavedUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/saveIntakePatientInsurances";
    identifierSavedUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/saveIntakePatientIdentities";
    documentSavedUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/savePatientDocument";
    profileImageSavedUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/savePatientImage";
 
    eligibilityUrl=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/checkEligibility";
    eligibilityRequest=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getEligibilityRequestDate/";
   vieweligibility=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getEligibility/";
  stateList=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getUsaStates";
    physicianList=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getPhysicianByRegisterID/";
    hipaaText=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getHippaAggrement";
    hipaaSavedURL=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/saveHipaaAgreement";
 
 intouchURL=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/callRemaining";
 newsletterUpdateURL=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/updateTherapyNewsLetter";
 appointmentUpdateURL=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/updateAppointmentReminderStatus";

 eligibleServicesURL=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getEligibleMasterServiceTypeList";
    
    eligiblepayersURL=@"https://eligibleapi.com/resources/information-sources.xml";
   
   
// add these webservices on 01-03-2014

   smokingStatus=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getAllSmokingStatus";
   employmentType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getAllEmploymentTypes";
   identifierType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getIdentityTypes";
   
   
   titleType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getTitleList";
   genderType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getGenderList";
   documentType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getDocumentTypeList";
   reminderList=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getAppointmentReminderList";
   meritalStatus=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getMaritalStatusList";
   ethinicOrigin=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getAllEthnicOrigin";
   AddressTypeList=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getAddressTypes";
   contactType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getContactTypes";
   contactCategoryType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getPhoneTypes";
   appointmentReminderType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getAppointmentReminderList";
   insurePartyType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getInsuredParties";
   studentType=@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/DropDown/getStudentList";


   
   flag=NO;

/*
 loginUrl = @"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/login/";
   patientlistUrl = @"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getPatientListByRegistrationId/";
    addPatientUrl = @"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/addPatient";
    emailIdCheckUrl = @"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/checkUserEmail";
    demographicsUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getIntakePatientDemographicsInformation/";
    identifierUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getIntakePatientIdentities";
    addressUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getIntakePatientAddresses";
    insuranceUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getIntakePatientInsurances";
    contactUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getIntakePatientContacts";
    uploadUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getPatientDocuments";
    insuranceCompanyUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getInsurances";
    demogrphicsUpdateUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/updateorsaveIntakePatientDemographicsInformation";
    addressSavedUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/saveIntakePatientAddress/";
   contactSavedUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/saveIntakePatientContact";
    insuranceSavedUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/saveIntakePatientInsurances";
    identifierSavedUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/saveIntakePatientIdentities";
    documentSavedUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/savePatientDocument";
    profileImageSavedUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/savePatientImage";
   eligibilityUrl=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/checkEligibility";
    eligibilityRequest=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getEligibilityRequestDate/";
    vieweligibility=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getEligibility/";
   stateList=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getUsaStates";
    physicianList=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getPhysicianByRegisterID/";
 hipaaText=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getHippaAggrement";
hipaaSavedURL=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/saveHipaaAgreement";

 intouchURL=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/callRemaining";
 newsletterUpdateURL=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/updateTherapyNewsLetter";
    appointmentUpdateURL=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/updateAppointmentReminderStatus";
   eligiblepayersURL=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getEligibleMasterPayerList";
    eligibleServicesURL=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/getEligibleMasterServiceTypeList";
 
    
    
    eligiblepayersURL=@"https://eligibleapi.com/resources/information-sources.xml";
flag=NO;

    // add these webservices on 01-03-2014
    
    smokingStatus=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getAllSmokingStatus";
    employmentType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getAllEmploymentTypes";
    identifierType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getIdentityTypes";
   
    
    titleType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getTitleList";
genderType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getGenderList";
    documentType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getDocumentTypeList";
    reminderList=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getAppointmentReminderList";
    meritalStatus=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getMaritalStatusList";
    ethinicOrigin=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getAllEthnicOrigin";
    AddressTypeList=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getAddressTypes";
    contactType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getContactTypes";
    contactCategoryType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getPhoneTypes";
    appointmentReminderType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getAppointmentReminderList";
    insurePartyType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getInsuredParties";
    studentType=@"http://10.0.1.246:8080/InTouchEmrWebService/services/intouchEMR/DropDown/getStudentList";
 */
    
#ifdef DEBUG
    [AmazonLogger verboseLogging];
#else
    [AmazonLogger turnLoggingOff];
#endif
    
    [AmazonErrorHandler shouldNotThrowExceptions];
    
    [_window makeKeyAndVisible];
 
    return YES;
}

- (NSUInteger)application:(UIApplication*)application
supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    return UIInterfaceOrientationMaskAll;
}
- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
