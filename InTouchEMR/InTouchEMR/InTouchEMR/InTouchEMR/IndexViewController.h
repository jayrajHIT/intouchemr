//
//  IndexViewController.h
//  InTouchEMR
//
//  Created by DSC on 11/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndexViewController : UIViewController
{
     UIImageView *footerView;
}
- (IBAction)click_on_login:(id)sender;
- (IBAction)signUp:(id)sender ;

@property (strong, nonatomic) IBOutlet UIButton *loginbutton;
@property (strong, nonatomic) IBOutlet UIButton *regisbutton;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewlogo;


@end
