//
//  eligibleServices.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 28/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface eligibleServices : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)ServiceList;

@end
