//
//  AddressViewController.h
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "addressCell.h"

@interface AddressViewController : UIViewController<NIDropDownDelegate,UITextViewDelegate>
{
     NSMutableArray *AddressData,*states,*state1;
    NSMutableDictionary *arr;
    NSMutableDictionary *dict;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    UIImage *image;
    UIView *vi;
    NIDropDown *dropDown;
    addressCell *cell;
    NSString *str_addressType;
    NSString *address_id;
    NSString *isHealthFusion;
    NSString *add1;
    NSString *add2;
    NSString *temp_city;
    NSString *temp_state;
    int countryID;
    NSString *AddressId;
    NSString *temp_pinCode;
    NSString *addinfo;
    NSString *isPrimay;
    UIButton *button;
    
    NSMutableArray *addressTypeArr;
    NSArray *addressTypeName, *addressTypeID;
    
    
}

- (IBAction)selectClicked:(id)sender;

-(void)rel;

- (IBAction)next:(id)sender;
-(void)clearCurrentView;
- (IBAction)update:(id)sender;
-(IBAction)saveAddress:(id)sender;
@property (nonatomic, assign) BOOL working;
@property (nonatomic, assign) BOOL orientchange;
@property (nonatomic, assign) BOOL error;

@property (nonatomic, assign) NSInteger index;
@property (strong, nonatomic) IBOutlet UIView *addView;
@property (strong, nonatomic) IBOutlet UIButton *statebtn;
@property (strong, nonatomic) IBOutlet UIImageView *landscapePatientImage;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UILabel *patientContact;
@property (strong, nonatomic) IBOutlet UILabel *patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *patientName;



@property (strong, nonatomic) IBOutlet UILabel *landscapepatientContact;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnHeading;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientName;



@property (strong, nonatomic) IBOutlet UISwitch *Isprimary;

@property (strong, nonatomic) IBOutlet UITableView *addressTable;
@property (strong, nonatomic) IBOutlet UITableView *landscapeaddressTable;


@property (strong, nonatomic) IBOutlet UITextField *edit_city;
@property (strong, nonatomic) IBOutlet UITextField *edit_state;
@property (strong, nonatomic) IBOutlet UITextField *edit_pin;
@property (strong, nonatomic) IBOutlet UITextField *edit_country;
@property (strong, nonatomic) IBOutlet UITextView *edit_address;
@property (strong, nonatomic) IBOutlet UITextView *edit_address2;
@property (strong, nonatomic) IBOutlet UITextView *edit_addInfo;
@property (strong, nonatomic) IBOutlet UIButton *edit_addresstype;
@property (strong, nonatomic) IBOutlet UIButton *edit_Country;
@property (strong, nonatomic) IBOutlet UILabel *addressType;
@property (strong, nonatomic) IBOutlet UILabel *Address1;
@property (strong, nonatomic) IBOutlet UILabel *city;
@property (strong, nonatomic) IBOutlet UILabel *state;
@property (strong, nonatomic) IBOutlet UILabel *Country;
@property (strong, nonatomic) IBOutlet UILabel *pincode;
@property (strong, nonatomic) IBOutlet UIButton *Save_update;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_Adress;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_Address;
-(id)initwithArray:(NSMutableDictionary *)array;
@end
