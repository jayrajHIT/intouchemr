//
//  EligibilityXMLDate.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 29/06/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EligibilityXMLDate : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr,*arr1;
}
-(NSMutableArray *)patientEligibilityDataByXML:(NSString *)patientID registationNO:(NSString *)registationID insuranceID:(NSString *)insuranceID eligiblePayerName:(NSString *)payer eligiblePayerID:(NSString *)payerID eligibleServiceID:(NSString *)serviceID username:(NSString *)uname;

@end
