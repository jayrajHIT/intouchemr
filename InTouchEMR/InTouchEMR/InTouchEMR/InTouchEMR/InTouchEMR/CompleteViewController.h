//
//  HippaViewController.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 31/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompleteViewController : UIViewController<UIAlertViewDelegate>
{
    CGPoint point;
    UIImageView *image;
    BOOL mouseSwiped;
    int mouseMoved;
    NSString *cpwd;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
   
}
- (IBAction)login:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic)IBOutlet UIImageView *image;
@end
