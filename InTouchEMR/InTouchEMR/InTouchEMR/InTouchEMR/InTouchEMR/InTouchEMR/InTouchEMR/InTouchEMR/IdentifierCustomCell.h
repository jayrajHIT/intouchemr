//
//  IdentifierCustomCell.h
//  InTouchEMR
//
//  Created by Ghanshyam on 08/03/14.
//  Copyright (c) 2014 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IdentifierCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *identifierName;
@property (strong, nonatomic) IBOutlet UILabel *identifierID;
@property (strong, nonatomic) IBOutlet UILabel *issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *EffectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *ExpireDate;
@property (strong, nonatomic) IBOutlet UIButton *edit;
@end
