//
//  IndexViewController.m
//  InTouchEMR
//
//  Created by DSC on 11/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "IndexViewController.h"
#import "LoginViewController.h"
#import "IndexAppDelegate.h"
#import "SVWebViewController.h"


@interface IndexViewController ()

@end

@implementation IndexViewController
@synthesize imgViewlogo,regisbutton,loginbutton;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
    self.navigationController.navigationBar.translucent = NO;
    
    imgViewlogo=[[UIImageView alloc]init];
    [imgViewlogo setImage:[UIImage imageNamed:@"logo_intouchemr.jpeg"]];
    [self.view addSubview:imgViewlogo];
    
    footerView=[[UIImageView alloc]init];
    [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
    [self.view addSubview:footerView];
    
    loginbutton=[UIButton buttonWithType:UIButtonTypeCustom];
    [loginbutton setImage:[UIImage imageNamed:@"login_btn.png"] forState:UIControlStateNormal];
    
    [loginbutton addTarget:self action:@selector(click_on_login:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginbutton];
    
    regisbutton=[UIButton buttonWithType:UIButtonTypeCustom];
    [regisbutton setImage:[UIImage imageNamed:@"register_btn.png"] forState:UIControlStateNormal];
   
    [regisbutton addTarget:self action:@selector(signUp:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:regisbutton];
    
    
    [self doLayoutForOrientation:currentOrientation];
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)click_on_login:(id)sender {
        LoginViewController *loginView=[[LoginViewController alloc]init];
IndexAppDelegate *appdelegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:loginView];
[appdelegate.window setRootViewController:nav];

}
- (IBAction)signUp:(id)sender {
//	NSURL *URL = [NSURL URLWithString:@"http://www.intouchemr.com/ipad"];
//	SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
//	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
//    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsOpenInChrome | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
//	
//    [self presentViewController:webViewController animated:YES completion:nil];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self doLayoutForOrientation:toInterfaceOrientation];
}

-(void)doLayoutForOrientation:(UIInterfaceOrientation)orientation
{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    int ver = [version intValue];
    if(UIInterfaceOrientationIsPortrait(orientation))
    {
        
       
        if (ver < 7){
            //iOS 6 work
             [footerView setFrame:CGRectMake(0, 954, 770, 50)];
        }
        else{
            //iOS 7 related work
             [footerView setFrame:CGRectMake(0, 974, 770, 50)];
        }
        
        [imgViewlogo setFrame:CGRectMake(60,120,650,330)];
        [loginbutton setFrame:CGRectMake(180,650,422,105)];
//        [regisbutton setFrame:CGRectMake(180,680,422,105)];
       
                
    } else {
        
        if (ver < 7){
            //iOS 6 work
            [footerView setFrame:CGRectMake(0, 698, 1024, 50)];
        }
        else{
            //iOS 7 related work
           [footerView setFrame:CGRectMake(0, 718, 1024, 50)];
        }
        
        [imgViewlogo setFrame:CGRectMake(195,20,650,330)];
        [loginbutton setFrame:CGRectMake(300,480,422,105)];
     //   [regisbutton setFrame:CGRectMake(300,550,422,105)];
        
    
    }
}


@end
