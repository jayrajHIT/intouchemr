//
//  HippaViewController.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 31/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.


#import "CompleteViewController.h"
#import "QuartzCore/QuartzCore.h"
@interface CompleteViewController ()

@end

@implementation CompleteViewController
@synthesize portraitView,landscapeView,image;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
    
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
   
    
    self.navigationItem.hidesBackButton=YES;
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    [self myOrientation];
 
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    cpwd = [pref objectForKey:@"tempPass"];
    // Do any additional setup after loading the view from its nib.
}

-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    
    
}
-(void)myOrientation
{
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
        [self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        [self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
    }

}

-(void) clearCurrentView {
    
    
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
        
        
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
        
	}
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password "
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Submit", nil];
    
    [message setAlertViewStyle:UIAlertViewStyleSecureTextInput];
    message.delegate=self;
    [message show];
    
}


- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if( [inputText length] >= cpwd.length )
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            NSString *inputText = [[alertView textFieldAtIndex:0] text];
            if ([inputText isEqualToString:cpwd]) {
                
               [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
                
                
            }
            else
            {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Incorrect password "
                                                                  message:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil, nil];
                [message show];
            }
            break;
        }
        default:
            break;
    }
    
}


@end
