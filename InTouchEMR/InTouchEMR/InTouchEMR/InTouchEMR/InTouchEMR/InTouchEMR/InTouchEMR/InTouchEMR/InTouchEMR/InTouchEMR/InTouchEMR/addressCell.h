//
//  addressCell.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 25/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface addressCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *addressType;
@property (strong, nonatomic) IBOutlet UILabel *city;
@property (strong, nonatomic) IBOutlet UILabel *State;
@property (strong, nonatomic) IBOutlet UILabel *pincode;
@property (strong, nonatomic) IBOutlet UILabel *country;
@property (strong, nonatomic) IBOutlet UITextView *address;
@property (strong, nonatomic) IBOutlet UIButton *edit;

//@property (strong, nonatomic) IBOutlet UILabel *landscapeaddressType;
//@property (strong, nonatomic) IBOutlet UILabel *landscapecity;
//@property (strong, nonatomic) IBOutlet UILabel *landscapeState;
//@property (strong, nonatomic) IBOutlet UILabel *landscapepincode;
//@property (strong, nonatomic) IBOutlet UITextView *landscapeaddress;
//@property (strong, nonatomic) IBOutlet UIButton *landscapeedit;
@end
