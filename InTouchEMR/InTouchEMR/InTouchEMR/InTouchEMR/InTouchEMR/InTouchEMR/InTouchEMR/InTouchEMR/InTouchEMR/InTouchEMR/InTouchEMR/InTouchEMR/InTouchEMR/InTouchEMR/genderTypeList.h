//
//  genderTypeList.h
//  InTouchEMR
//
//  Created by Ghanshyam on 08/03/14.
//  Copyright (c) 2014 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface genderTypeList : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)genderTypeListData;

@end
