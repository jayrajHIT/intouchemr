//
//  eligiblePayers.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 28/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface eligiblePayers : NSObject<NSXMLParserDelegate>
{
    NSString *myelement,*payerId;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
    NSMutableString *currentNodeContent;
    NSSortDescriptor *sortDescriptor;
}
-(NSMutableArray *)PayerList;


@end
