//
//  contactCell.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 27/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface contactCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *ContactType;
@property (strong, nonatomic) IBOutlet UILabel *Contact;
@property (strong, nonatomic) IBOutlet UILabel *Usage;
@property (strong, nonatomic) IBOutlet UILabel *primary;
@property (strong, nonatomic) IBOutlet UILabel *emergency;
@property (strong, nonatomic) IBOutlet UIButton *edit;

@end
