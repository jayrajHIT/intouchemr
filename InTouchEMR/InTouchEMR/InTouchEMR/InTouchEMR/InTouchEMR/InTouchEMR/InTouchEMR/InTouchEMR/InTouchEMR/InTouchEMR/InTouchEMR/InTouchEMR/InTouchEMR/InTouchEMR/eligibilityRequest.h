//
//  eligibilityRequest.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 06/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface eligibilityRequest : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr,*arr1;
}
-(NSMutableArray *)patientEligibilityRequestByXML:(NSString *)patientID;
@end
