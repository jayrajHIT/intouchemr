//
//  ContactsViewController.h
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "contactCell.h"
@interface ContactsViewController : UIViewController<NIDropDownDelegate>
{
    NSMutableDictionary *arr;
     NSMutableArray *ContactData;
    NSMutableDictionary *dict;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    UIImage *image;
     UIView *vi;
    NIDropDown *dropDown;
    contactCell *cell;
    NSString *ContTypeID;
    NSString *PhoneTypeID;
    NSString *contact_id;
    NSString *str_PhoneNo;
    NSString *str_ext;
    int EmrgCont;
    int isprimary;
    NSString *str_additionInfo;
    NSString *appointReminderID;
    NSString *str_emerCont;
    int appointStatus;
     NSString *isHealthFusion;
    
    NSMutableArray *contactList,*contactCategoryXMLList,*appointmentreminderList;
    NSArray *contactTypeName,*contactTypeID,*contactCategoryTypeName,*contactCategorytypeID,*appointmentReminderTypeName,*appointmentReminderTypeID;
    
}



- (IBAction)selectClicked:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)update:(id)sender;
- (IBAction)cancelView:(id)sender;
-(id)initwithArray:(NSMutableDictionary *)array withImage:(UIImage *)img;
-(void)rel;
-(IBAction)saveContact:(id)sender;
-(IBAction)emailContact:(id)sender;
@property (nonatomic, assign) BOOL orientchange;
@property (strong, nonatomic) IBOutlet UITableView *contactTable;
@property (strong, nonatomic) IBOutlet UITableView *landscapecontactTable;
@property (strong, nonatomic) IBOutlet UITextField *extNo;
@property (strong, nonatomic) IBOutlet UITextField *emrgncy;
@property (strong, nonatomic) IBOutlet UISwitch *IsEmergency;
@property (strong, nonatomic) IBOutlet UISwitch *Isprime;
@property (strong, nonatomic) IBOutlet UISwitch *appointRemind;
@property (strong, nonatomic) IBOutlet UITextView *AdditionalInfo;
@property (strong, nonatomic) IBOutlet UIButton *appointReminder;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) BOOL working;
@property (strong, nonatomic) IBOutlet UIView *addView;
@property (strong, nonatomic) IBOutlet UIImageView *landscapePatientImage;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;
@property (strong, nonatomic) IBOutlet UILabel *patientContact;
@property (strong, nonatomic) IBOutlet UILabel *patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *patientName;



@property (strong, nonatomic) IBOutlet UILabel *landscapepatientContact;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnHeading;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientName;


@property (strong, nonatomic) IBOutlet UIButton *edit_contactType;
@property (strong, nonatomic) IBOutlet UITextField *edit_contact;
@property (strong, nonatomic) IBOutlet UIButton *edit_usage;

@property (strong, nonatomic) IBOutlet UITextField *edit_phoneType;
@property (strong, nonatomic) IBOutlet UITextField *edit_appointReminder;

@property (strong, nonatomic) IBOutlet UILabel *ContactType;
@property (strong, nonatomic) IBOutlet UILabel *Phonetype;
@property (strong, nonatomic) IBOutlet UILabel *ContactNo;
@property (strong, nonatomic) IBOutlet UILabel *IsEmrgency;
@property (strong, nonatomic) IBOutlet UILabel *IsPrimary;
@property (strong, nonatomic) IBOutlet UILabel *ext;
@property (strong, nonatomic) IBOutlet UILabel *additnInfo;
@property (strong, nonatomic) IBOutlet UILabel *appoint;
@property (strong, nonatomic) IBOutlet UIButton *edit_Save;
@property (strong, nonatomic) IBOutlet UIButton *edit_Cancel;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_Contact;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_Contact;

@end
