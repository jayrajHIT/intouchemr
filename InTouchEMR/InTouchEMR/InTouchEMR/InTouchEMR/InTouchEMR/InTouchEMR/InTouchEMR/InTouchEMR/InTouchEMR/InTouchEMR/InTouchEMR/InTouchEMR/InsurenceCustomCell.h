//
//  InsurenceCustomCell.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 18/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsurenceCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *insuranceName;
@property (strong, nonatomic) IBOutlet UILabel *subscriberID;
@property (strong, nonatomic) IBOutlet UILabel *insuredParty;
@property (strong, nonatomic) IBOutlet UILabel *policyNo;
@property (strong, nonatomic) IBOutlet UILabel *effectiveDate;
@property (strong, nonatomic) IBOutlet UIButton *edit;
@end
