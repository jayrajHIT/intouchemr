//
//  hipaaTextXML.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 09/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface hipaaTextXML : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableString *arr;
}
-(NSMutableString *)getHipaaTextXML;

@end
