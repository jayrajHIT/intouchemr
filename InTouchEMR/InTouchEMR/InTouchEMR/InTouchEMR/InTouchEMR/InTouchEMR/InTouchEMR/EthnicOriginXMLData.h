//
//  EthnicOriginXMLData.h
//  InTouchEMR
//
//  Created by Ghanshyam on 09/03/14.
//  Copyright (c) 2014 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EthnicOriginXMLData : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)EthnicOriginStatusListData;

@end
