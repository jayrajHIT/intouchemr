//
//  EligibilityView.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "LoginViewController.h"
#import "NIDropDown.h"

@interface EligibilityView : UIViewController<NIDropDownDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    UIImage *image;
    NSMutableDictionary *arr;
    UIImageView *footerView;
    UIDeviceOrientation orientation;
    NSMutableArray *InsurenceData,*company,*requestData,*payerLists,*serviceList;
    NIDropDown *dropDown;
    NSString *insurenceID,*eligibilitypayerID,*eligibilityServiceID,*eligibilityPayerName;
    UIButton *btr;
    NSArray *payers,*service,*temper;
    
    id butt;
}
@property (strong,nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (nonatomic, assign) bool isFiltered;


@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UILabel *patientContact;
@property (strong, nonatomic) IBOutlet UILabel *patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *patientName;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;

@property (strong, nonatomic) IBOutlet UILabel *landscape_patientContact;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *landscape_ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscape_ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientName;
@property (strong, nonatomic) IBOutlet UIImageView *landscape_patientImage;
@property (strong, nonatomic) IBOutlet UIImageView *backImg;
@property (strong, nonatomic) IBOutlet UIImageView *landscapebackImg;
@property (strong, nonatomic) IBOutlet UIButton *port_btn_insurance;
@property (strong, nonatomic) IBOutlet UIButton *land_btn_insurance;
@property (strong, nonatomic) IBOutlet UILabel *port_lbl_insurance;
@property (strong, nonatomic) IBOutlet UILabel *land_lbl_insurance;
@property (strong, nonatomic) IBOutlet UIView *port_tbl_eligibility;
@property (strong, nonatomic) IBOutlet UIView *land_tbl_eligibility;
@property (strong, nonatomic) IBOutlet UITableView *port_table_eligible;
@property (strong, nonatomic) IBOutlet UITableView *land_table_eligible;
@property (strong, nonatomic) IBOutlet UIImageView *port_circle;
@property (strong, nonatomic) IBOutlet UIImageView *land_circle;

@property (strong, nonatomic) IBOutlet UIButton *port_btn_payer;
@property (strong, nonatomic) IBOutlet UIButton *land_btn_payer;
@property (strong, nonatomic) IBOutlet UIButton *port_btn_service;
@property (strong, nonatomic) IBOutlet UIButton *land_btn_service;
@property (strong, nonatomic) IBOutlet UILabel *port_lbl_payer;
@property (strong, nonatomic) IBOutlet UILabel *land_lbl_payer;


-(id)initwithArray:(NSMutableDictionary *)array;

-(IBAction)checkEligibility:(id)sender;
- (IBAction)selectClicked:(id)sender;
@end
