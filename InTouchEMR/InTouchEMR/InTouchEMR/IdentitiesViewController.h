//
//  IdentitiesViewController.h
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "IdentifierCustomCell.h"

@interface IdentitiesViewController : UIViewController<NIDropDownDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate>
{
    IdentifierCustomCell *cell;
    
    NSMutableArray *IdentityData;
    NSMutableDictionary *dict,*arr;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    UIImage *image;
    UIView *vi;
    NIDropDown *dropDown;
    UIBarButtonItem *doneButton;
    UIDatePicker *datePicker1;
   UIDatePicker *datePicker2;
    UIPopoverController *popoverController;
    NSString *str_identityTypeID;
    NSString *str_id;
    NSString *str_issuingSource;
    NSString *str_effictiveDate;
    NSString *str_expireDate;
    NSString *identity_id;
    NSString *identity_id1,*identityid2,*identityid3;
    int isprime,isprime1,isprime2,isprime3;
     NSString *patientDOB;
    NSString *dl_primary,*sys_primary,*ss_primary,*primary;
    
    NSMutableArray *identifierType;
    NSArray* identifierID,*identiferName;
    
}

- (IBAction)selectClicked:(id)sender;

-(void)rel;

-(IBAction)saveIdentity:(id)sender;
- (IBAction)cancelView:(id)sender;
- (IBAction)update:(id)sender;
@property (nonatomic, assign) BOOL orientchange;
@property (strong, nonatomic) IBOutlet UITableView *idetifierTable;
@property (strong, nonatomic) IBOutlet UITableView *landscapeIdentifierTable;
@property (nonatomic, assign) BOOL working;
@property (nonatomic, assign) NSInteger index;
@property (strong, nonatomic) IBOutlet UIView *addView;
@property (nonatomic, strong) UIPopoverController *popoverController;
@property (strong, nonatomic) IBOutlet UIImageView *landscapePatientImage;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UILabel *patientContact;
@property (strong, nonatomic) IBOutlet UILabel *patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *patientName;
@property (strong, nonatomic) IBOutlet UILabel *DL_id;
@property (strong, nonatomic) IBOutlet UILabel *DL_issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *DL_effectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *DL_expirationDate;
@property (strong, nonatomic) IBOutlet UILabel *sysID;
@property (strong, nonatomic) IBOutlet UILabel *sys_issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *sys_effectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *sys_expirationDate;
@property (strong, nonatomic) IBOutlet UILabel *SS_id;
@property (strong, nonatomic) IBOutlet UILabel *ss_issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *ss_effectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *ss_expirationDate;


@property (strong, nonatomic) IBOutlet UILabel *landscapepatientContact;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnHeading;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeDL_id;
@property (strong, nonatomic) IBOutlet UILabel *landscapeDL_issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *landscapeDL_effectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *landscapeDL_expirationDate;
@property (strong, nonatomic) IBOutlet UILabel *landscapesysID;
@property (strong, nonatomic) IBOutlet UILabel *landscapesys_issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *landscapesys_effectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *landscapesys_expirationDate;
@property (strong, nonatomic) IBOutlet UILabel *landscapeSS_id;
@property (strong, nonatomic) IBOutlet UILabel *landscapess_issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *landscapess_effectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *landscapess_expirationDate;

@property (strong, nonatomic) IBOutlet UITextField *edit_issuingSource;
@property (strong, nonatomic) IBOutlet UITextField *identityType;
@property (strong, nonatomic) IBOutlet UITextField *edit_id;
@property (strong, nonatomic) IBOutlet UITextField *edit_effectivedate;
@property (strong, nonatomic) IBOutlet UITextField *edit_expiredate;
@property (strong, nonatomic) IBOutlet UIButton *edit_identityType;
@property (strong, nonatomic) IBOutlet UISwitch *edit_isprimary;
@property (strong, nonatomic) IBOutlet UILabel *lbl_identityType;
@property (strong, nonatomic) IBOutlet UILabel *lbl_id;
@property (strong, nonatomic) IBOutlet UILabel *lbl_issuingSource;
@property (strong, nonatomic) IBOutlet UILabel *lbl_effectiveDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_expirationDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_isPrimary;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_identity;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_identity;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_identity1;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_identity1;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_identity2;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_identity2;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_identity3;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_identity3;
@property (strong, nonatomic) IBOutlet UIButton *btn_port_next;
@property (strong, nonatomic) IBOutlet UIButton *btn_land_next;
@property (strong, nonatomic) IBOutlet UIButton *btn_save;




- (IBAction)next:(id)sender;
-(id)initwithArray:(NSMutableDictionary *)array withImage:(UIImage *)img;
@end
