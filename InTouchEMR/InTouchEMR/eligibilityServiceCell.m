//
//  eligibilityServiceCell.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "eligibilityServiceCell.h"

@implementation eligibilityServiceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
