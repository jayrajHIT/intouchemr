//
//  IdentitiesViewController.m
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "IdentitiesViewController.h"
#import "patientIdentiesXMLData.h"
#import "IndexAppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "SHKActivityIndicator.h"
#import "NSString+validation.h"
#import "identifierTypeList.h"

@interface IdentitiesViewController ()

@end

@implementation IdentitiesViewController
@synthesize DL_effectiveDate,DL_expirationDate,DL_id,DL_issuingSource,SS_id,ss_issuingSource,ss_effectiveDate,ss_expirationDate,sysID,sys_issuingSource,sys_effectiveDate,sys_expirationDate,patientContact,patientEmail,patientName,patientOrigin,ssnHeading,ssnValue;

@synthesize landscapessnValue,landscapessnHeading,landscapepatientOrigin,landscapepatientName,landscapepatientEmail,landscapepatientContact,landscapeDL_effectiveDate,landscapeDL_expirationDate,landscapeDL_id,landscapeDL_issuingSource,landscapess_effectiveDate,landscapess_expirationDate,landscapeSS_id,landscapess_issuingSource,landscapesys_effectiveDate,landscapesys_expirationDate,landscapesys_issuingSource,landscapesysID;
@synthesize portraitView,landscapeView;
@synthesize patientImage,landscapePatientImage,index,edit_effectivedate,edit_expiredate,edit_id,edit_identityType,edit_issuingSource,identityType,popoverController,edit_isprimary;
@synthesize lbl_identityType,lbl_id,lbl_issuingSource,lbl_effectiveDate,lbl_expirationDate,lbl_isPrimary,addView;

@synthesize btn_edit_identity,btn_edit_identity1,btn_edit_identity2,btn_edit_identity3,btn_edit_land_identity,btn_edit_land_identity1,btn_edit_land_identity2,btn_edit_land_identity3,btn_land_next,btn_port_next,btn_save;


@synthesize idetifierTable,landscapeIdentifierTable,orientchange;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initwithArray:(NSMutableDictionary *)array withImage:(UIImage *)img
{
    arr=array;
    image=img;
    return self;
    
}

-(void)orientationChanged:(NSNotification *)notification{
    
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown)  {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    // Do your orientation logic here
	
}

-(void)myOrientation
{
    [self setOrientchange:NO];
    [self performSelectorInBackground:@selector(mythread)
                           withObject:nil];

    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
		[self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        
        if ([arr objectForKey:@"FullName"]) {
            landscapepatientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            landscapepatientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            landscapepatientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            landscapepatientEmail.text=@"";
        }
       
        if ([arr objectForKey:@"ssn"]) {
            landscapessnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            landscapessnValue.text=@"";
        }
        if ([arr objectForKey:@"ethnicOriginName"]) {
            landscapepatientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            landscapepatientOrigin.text=@"";
        }

        if ([arr objectForKey:@"birth"]) {
            patientDOB=[arr objectForKey:@"birth"];
        }
        else
        {
            patientDOB=@"";
        }
       
     
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            landscapepatientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                     landscapepatientContact.text=nil;
        }
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [landscapePatientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
       

        if (IdentityData!=nil) {
            [self afterthread];
        }
        if (self.working==YES) {

            [self editView];
                 
        }
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
				[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
       
        if ([arr objectForKey:@"FullName"]) {
            patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            patientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            patientEmail.text=@"";
        }

        if ([arr objectForKey:@"ssn"]) {
            ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            ssnValue.text=@"";
        }
        if ([arr objectForKey:@"ethnicOriginName"]) {
            patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            patientOrigin.text=@"";
        }
        if ([arr objectForKey:@"birth"]) {
            patientDOB=[arr objectForKey:@"birth"];
        }
        else
        {
            patientDOB=@"";
        }
       
    
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                      patientContact.text=nil;
        }

            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        
        if (IdentityData!=nil) {
            [self afterthread];
        }
        
        if (self.working==YES) {
            [self editView];
            
        }

	}
}

-(void)mythread
{
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    patientIdentiesXMLData *patientData=[[patientIdentiesXMLData alloc]init];
    IdentityData=[patientData patientIdentityDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
  
    [self addDropDown];
    [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
}
-(void)afterthread
{
    
    [ idetifierTable reloadData];
    [landscapeIdentifierTable reloadData];
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}

-(void)editView
{
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024,670)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [landscapeView addSubview:vi];
        self.addView.frame=CGRectMake(250, 100, 492, 409);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [landscapeView addSubview:self.addView];
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 930)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [portraitView addSubview:vi];
        self.addView.frame=CGRectMake(125, 250, 492, 409);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [portraitView addSubview:self.addView];
    }
    
    
    
    if ([self orientchange]) {
        
        if ([self index]==1001) {
            
            
            [edit_identityType setEnabled:YES];
            [btn_save setTitle:@"Save" forState:UIControlStateNormal];
            [edit_identityType setTitle:@" select one" forState:UIControlStateNormal];
            
            edit_id.text=@"";
            edit_issuingSource.text=@"";
            edit_effectivedate.text=@"";
            edit_expiredate.text=@"";
            
            [self setOrientchange:NO];
            
        }
        else
        {
            
            NSDictionary *editdict=[IdentityData objectAtIndex:index];
            [btn_save setTitle:@"Update" forState:UIControlStateNormal];
           
           
           
            if ([editdict objectForKey:@"identityTypeID"]) {
                if ([identifierID indexOfObject:[editdict objectForKey:@"identityTypeID"]]!=NSNotFound) {
                    
                int a=[identifierID indexOfObject:[editdict objectForKey:@"identityTypeID"]];
                
                [edit_identityType setTitle:[identiferName objectAtIndex:a] forState:UIControlStateNormal];
            }
                else{
                    [edit_identityType setTitle:@"Select One" forState:UIControlStateNormal];
                }
            }
            else
            {
               [edit_identityType setTitle:@"Select One" forState:UIControlStateNormal];
            }
            
            
            if ([editdict objectForKey:@"idNumber"]) {
                edit_id.text=[editdict objectForKey:@"idNumber"];
            }
            else
            {
                edit_id.text=@"";
            }
            if ([editdict objectForKey:@"issuingSource"]) {
                edit_issuingSource.text=[editdict objectForKey:@"issuingSource"];
            }
            else
            {
                edit_issuingSource.text=@"";
            }
            
            if ([editdict objectForKey:@"startDate"]) {
                edit_effectivedate.text=[self viewDate:[editdict objectForKey:@"startDate"]];
            }
            else
            {
                edit_effectivedate.text=@"";
            }
            if ([editdict objectForKey:@"endDate"]) {
                edit_expiredate.text=[self viewDate:[editdict objectForKey:@"endDate"]];
            }
            else
            {
                edit_expiredate.text=@"";
            }
            
            if ([editdict objectForKey:@"isPrimary"]) {
                NSString *a=[editdict objectForKey:@"isPrimary"];
                if ([a isEqualToString:@"1"]) {
                    [edit_isprimary setOn:YES];
                }
                else
                {
                    [edit_isprimary setOn:NO];
                }
                
            }
            else
            {
                 [edit_isprimary setOn:NO];
            }
            if ([editdict objectForKey:@"id"]) {
                identity_id=[editdict objectForKey:@"id"];
            }
            else
            {
                identity_id=@"";
            }
            
            
            [self setOrientchange:NO];
        }
        
        
    }

    
    
    
    
    
    
    
    
//    if ([self index]==0) {
//
//        [edit_identityType setEnabled:YES];
//        [btn_save setTitle:@"Save" forState:UIControlStateNormal];
//       [edit_identityType setTitle:@" select one" forState:UIControlStateNormal];
//        
//        edit_id.text=@"";
//        edit_issuingSource.text=@"";
//        edit_effectivedate.text=@"";
//        edit_expiredate.text=@"";
//         
//       }
//    else
    
//    if([self index]==1)
//    {
//        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
//            [btn_save setTitle:@"Update" forState:UIControlStateNormal];
//            [edit_identityType setTitle:@"Driver's License" forState:UIControlStateNormal]; // To set the title
//            [edit_identityType setEnabled:NO];
//            
//             edit_id.text=landscapeDL_id.text;
//            edit_issuingSource.text=landscapeDL_issuingSource.text;
//           
//            edit_effectivedate.text=landscapeDL_effectiveDate.text;
//            
//            edit_expiredate.text=landscapeDL_expirationDate.text;
//            identity_id=identity_id1;
//            if ([dl_primary isEqualToString:@"1"]) {
//                [edit_isprimary setOn:YES];
//            }
//            else
//            {
//                [edit_isprimary setOn:NO];
//            }
//        }
//        else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
//            
//            [edit_identityType setTitle:@"Driver's License" forState:UIControlStateNormal]; // To set the title
//            [edit_identityType setEnabled:NO];
//           
//            [btn_save setTitle:@"Update" forState:UIControlStateNormal];
//           
//            edit_id.text=DL_id.text;
//            edit_issuingSource.text=DL_issuingSource.text;
//           
//            edit_effectivedate.text=DL_effectiveDate.text;
//            edit_expiredate.text=DL_expirationDate.text;
//            identity_id=identity_id1;
//           
//            if ([dl_primary isEqualToString:@"1"]) {
//                [edit_isprimary setOn:YES];
//            }
//            else
//            {
//                [edit_isprimary setOn:NO];
//            }
//
//            
//        }
//        
//    }
//    else if([self index]==2)
//    {
//
//        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
//            
//            edit_identityType.titleLabel.text=@"Social Security";
//            [edit_identityType setTitle:@"Social Security" forState:UIControlStateNormal]; // To set the title
//            [edit_identityType setEnabled:NO];
//            [btn_save setTitle:@"Update" forState:UIControlStateNormal];
//            edit_id.text=SS_id.text;
//            edit_issuingSource.text=landscapess_issuingSource.text;
//            edit_effectivedate.text=landscapess_effectiveDate.text;
//            edit_expiredate.text=landscapess_expirationDate.text;
//                        identity_id= identityid2;
//            
//            if ([ss_primary isEqualToString:@"1"]) {
//                [edit_isprimary setOn:YES];
//            }
//            else
//            {
//                [edit_isprimary setOn:NO];
//            }
//
//            
//        }
//        else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
//            
//        edit_identityType.titleLabel.text=@"Social Security";
//        [edit_identityType setTitle:@"Social Security" forState:UIControlStateNormal]; // To set the title
//        [edit_identityType setEnabled:NO];
//            [btn_save setTitle:@"Update" forState:UIControlStateNormal];
//        edit_id.text=SS_id.text;
//        edit_issuingSource.text=ss_issuingSource.text;
//        edit_effectivedate.text=ss_effectiveDate.text;
//        edit_expiredate.text=ss_expirationDate.text;
//            
//        identity_id= identityid2;
//            
//            if ([ss_primary isEqualToString:@"1"]) {
//                [edit_isprimary setOn:YES];
//            }
//            else
//            {
//                [edit_isprimary setOn:NO];
//            }
//
//    }
//    }
//    else if([self index]==3)
//    {
//        
//        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
//            [edit_identityType setTitle:@"System ID" forState:UIControlStateNormal]; // To set the title
//            [edit_identityType setEnabled:NO];
//            [btn_save setTitle:@"Update" forState:UIControlStateNormal];
//            // edit_identityType.titleLabel.text=@"System ID";
//            edit_id.text=sysID.text;
//            edit_issuingSource.text=landscapesys_issuingSource.text;
//            edit_effectivedate.text=landscapesys_effectiveDate.text;
//            edit_expiredate.text=landscapesys_expirationDate.text;
//           
//            identity_id=identityid3;
//            
//            if ([sys_primary isEqualToString:@"1"]) {
//                [edit_isprimary setOn:YES];
//            }
//            else
//            {
//                [edit_isprimary setOn:NO];
//            }
//
//
//        }
//        else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
//        
//        
//        [edit_identityType setTitle:@"System ID" forState:UIControlStateNormal]; // To set the title
//        [edit_identityType setEnabled:NO];
//        [btn_save setTitle:@"Update" forState:UIControlStateNormal];
//       // edit_identityType.titleLabel.text=@"System ID";
//        edit_id.text=sysID.text;
//        edit_issuingSource.text=sys_issuingSource.text;
//        edit_effectivedate.text=sys_effectiveDate.text;
//        edit_expiredate.text=sys_expirationDate.text;
//                    identity_id=identityid3;
//            if ([sys_primary isEqualToString:@"1"]) {
//                [edit_isprimary setOn:YES];
//            }
//            else
//            {
//                [edit_isprimary setOn:NO];
//            }
//
//    }
//    }
}


-(void) clearCurrentView {
    [self.addView removeFromSuperview];
    [vi removeFromSuperview];
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setWorking:NO];
    
    // Do any additional setup after loading the view from its nib.
   
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	orientation = [[UIDevice currentDevice] orientation];
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
    
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    [edit_isprimary setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [edit_isprimary setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    

   
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
    [self myOrientation];
    
    datePicker1=[[UIDatePicker alloc]init];//Date picker
    
    datePicker1.frame=CGRectMake(0,0,320, 216);
     datePicker1.backgroundColor = [UIColor whiteColor];
    NSDate *date1=[NSDate date];
    [datePicker1 setMaximumDate:date1];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *DOB = [dateFormat dateFromString:patientDOB];
    [datePicker1 setMinimumDate:DOB];
    
    datePicker2=[[UIDatePicker alloc]init];//Date picker
    
    datePicker2.frame=CGRectMake(0,0,320, 216);
     datePicker2.backgroundColor = [UIColor whiteColor];
    
    [datePicker2 setMinimumDate:date1];
    
    identity_id=[NSString new];
    
   }


- (IBAction)cancelView:(id)sender {
    [self.addView removeFromSuperview];
    [vi removeFromSuperview];
    self.working=NO;
    [self setOrientchange:NO];
    lbl_identityType.textColor=[UIColor blackColor];
    lbl_id.textColor=[UIColor blackColor];
    lbl_issuingSource.textColor=[UIColor blackColor];
    lbl_effectiveDate.textColor=[UIColor blackColor];
    lbl_expirationDate.textColor=[UIColor blackColor];
     
   // [edit_identityType setEnabled:YES];
}
- (IBAction)update:(id)sender {
   
    [self setIndex:1001];
    [self setOrientchange:YES];
    [self setWorking:YES];
    
    [self editView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return  [IdentityData count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==idetifierTable) {
        
        static NSString *CellIdentifierPortrait = @"portrait";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"IdentifierCustomCell" owner:self options:nil];
        
        
        cell=(IdentifierCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
        
        if (cell==nil) {
            cell=[[ IdentifierCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierPortrait];
            cell=[nib objectAtIndex:0];
        }
        
        
        NSMutableDictionary *tempdict=[IdentityData objectAtIndex:indexPath.row];
        if ([tempdict objectForKey:@"identityTypeID"]) {
          
            if ([identifierID indexOfObject:[tempdict objectForKey:@"identityTypeID"]]!=NSNotFound) {
                
                int a=[identifierID indexOfObject:[tempdict objectForKey:@"identityTypeID"]];
                
                cell.identifierName.text=[identiferName objectAtIndex:a];
            }
            else{
                 cell.identifierName.text=@"";
            }
            
            
        }
        else
        {
            cell.identifierName.text=@"";
        }
        if ([tempdict objectForKey:@"idNumber"]) {
            cell.identifierID.text=[tempdict objectForKey:@"idNumber"];
        }
        else
        {
            cell.identifierID.text=@"";
        }
        if ([tempdict objectForKey:@"issuingSource"]) {
            cell.issuingSource.text=[tempdict objectForKey:@"issuingSource"];
        }
        else
        {
            cell.issuingSource.text=@"";
        }
       
        if ([tempdict objectForKey:@"startDate"]) {
            cell.EffectiveDate.text=[self viewDate:[tempdict objectForKey:@"startDate"]];
        }
        else
        {
            cell.EffectiveDate.text=@"";
        }
        
        if ([tempdict objectForKey:@"endDate"]) {
            cell.ExpireDate.text=[self viewDate:[tempdict objectForKey:@"endDate"]];
        }
        else
        {
            cell.ExpireDate.text=@"";
        }
        IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        if ([appDele.isExternal isEqualToString:@"1"]) {
            [cell.edit setHidden:YES];
        }
        else if ([appDele.isExternal isEqualToString:@"0"])
        {
            [cell.edit setHidden:NO];
        }
        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if (tableView==landscapeIdentifierTable) {
        
        static NSString *CellIdentifier = @"landscape";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"IdentifierCustomCell" owner:self options:nil];
        
        
        cell=(IdentifierCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil) {
            cell=[[ IdentifierCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell=[nib objectAtIndex:1];
        }
        
        
        NSMutableDictionary *tempdict=[IdentityData objectAtIndex:indexPath.row];
        
        if ([tempdict objectForKey:@"identityTypeID"]) {
            if ([identifierID indexOfObject:[tempdict objectForKey:@"identityTypeID"]]!=NSNotFound) {
                
                int a=[identifierID indexOfObject:[tempdict objectForKey:@"identityTypeID"]];
                
                cell.identifierName.text=[identiferName objectAtIndex:a];
            }
            else{
                cell.identifierName.text=@"";
            }
            
        }
        else
        {
            cell.identifierName.text=@"";
        }
        if ([tempdict objectForKey:@"idNumber"]) {
            cell.identifierID.text=[tempdict objectForKey:@"idNumber"];
        }
        else
        {
            cell.identifierID.text=@"";
        }
        if ([tempdict objectForKey:@"issuingSource"]) {
            cell.issuingSource.text=[tempdict objectForKey:@"issuingSource"];
        }
        else
        {
            cell.issuingSource.text=@"";
        }
        
        if ([tempdict objectForKey:@"startDate"]) {
            cell.EffectiveDate.text=[self viewDate:[tempdict objectForKey:@"startDate"]];
        }
        else
        {
            cell.EffectiveDate.text=@"";
        }
        
        if ([tempdict objectForKey:@"endDate"]) {
            cell.ExpireDate.text=[self viewDate:[tempdict objectForKey:@"endDate"]];
        }
        else
        {
            cell.ExpireDate.text=@"";
        }
        IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        if ([appDele.isExternal isEqualToString:@"1"]) {
            [cell.edit setHidden:YES];
        }
        else if ([appDele.isExternal isEqualToString:@"0"])
        {
            [cell.edit setHidden:NO];
        }
        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
    
}

- (void)accessoryButtonTapped:(id)sender event:(id)event
{
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition;
    NSIndexPath *indexPath;
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        currentTouchPosition = [touch locationInView:landscapeIdentifierTable];
        indexPath = [landscapeIdentifierTable indexPathForRowAtPoint: currentTouchPosition];
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        currentTouchPosition = [touch locationInView:idetifierTable];
        indexPath = [idetifierTable indexPathForRowAtPoint: currentTouchPosition];
    }
    
    if (indexPath != nil)
    {
        [self tableView: idetifierTable accessoryButtonTappedForRowWithIndexPath: indexPath];
        
    }
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    [self setIndex:indexPath.row];
    self.working=YES;
    [self setOrientchange:YES];
    [self editView];
    
}





-(IBAction)saveIdentity:(id)sender
{
    BOOL flag=NO;
    
    if ([[edit_identityType titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
        str_identityTypeID=nil;
    }
    else
    {
    if ([identiferName indexOfObject:[edit_identityType titleForState:UIControlStateNormal]]!=NSNotFound) {
        
        int a=[identiferName indexOfObject:[edit_identityType titleForState:UIControlStateNormal]];
        
        str_identityTypeID=[identifierID objectAtIndex:a];
    }
    else{
         str_identityTypeID=nil;
    }
    }
//    int a=[identiferName indexOfObject:[edit_identityType titleForState:UIControlStateNormal]];
//    
//    
//    
//    str_identityTypeID=[identifierID objectAtIndex:a];
     str_id=edit_id.text;
    str_issuingSource=edit_issuingSource.text;
    
   str_effictiveDate= [self saveDate:edit_effectivedate.text];
  
    
  str_expireDate=[self saveDate:edit_expiredate.text];
    
   
    if ([edit_isprimary isOn]) {
        isprime=1;
    }
    else
    {
        isprime=0;
    }
    
    
    if([NSString stringIsEmpty:str_identityTypeID])
    {

        lbl_identityType.textColor=[UIColor redColor];
        flag=YES;
        //  [self setError:YES];
    }
    else
    {
        lbl_identityType.textColor=[UIColor blackColor];
        //[self setError:NO];
    }
    if([NSString stringIsEmpty:str_id])
    {
        lbl_id.textColor=[UIColor redColor];
        flag=YES;
    }
    else{
        lbl_id.textColor=[UIColor blackColor];
        //   [self setError:NO];
    }
    
    if([NSString stringIsEmpty:str_issuingSource])
    {
        lbl_issuingSource.textColor=[UIColor redColor];
        flag=YES;
    }
    else
    {
        lbl_issuingSource.textColor=[UIColor blackColor];
        //   [self setError:NO];
    }
    
    if([NSString stringIsEmpty:str_effictiveDate])
    {
        lbl_effectiveDate.textColor=[UIColor redColor];
        flag=YES;
    }
    else
    {
        lbl_effectiveDate.textColor=[UIColor blackColor];
        //  [self setError:NO];
    }
    if([NSString stringIsEmpty:str_expireDate])
    {
        lbl_expirationDate.textColor=[UIColor redColor];
        flag=YES;
    }
    else
    {
        lbl_expirationDate.textColor=[UIColor blackColor];
        //    [self setError:NO];
    }
    if (flag==YES) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    else
    {
        IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        NSString *xmlText;
        
       
     
        NSURL *url = [NSURL URLWithString:appdalegate.identifierSavedUrl];

        
        if ([self index]==1001) {
            xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientIdentity><identityTypeID>%@</identityTypeID><isPrimary>%i</isPrimary><idNumber>%@</idNumber><issuingSource>%@</issuingSource><effectiveDate>%@</effectiveDate><expirationDate>%@</expirationDate><patientID>%@</patientID></PatientIdentity>",str_identityTypeID,isprime,str_id,str_issuingSource,str_effictiveDate,str_expireDate,appdalegate.patientId];
        }
        else{
            xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientIdentity><id>%@</id><identityTypeID>%@</identityTypeID><isPrimary>%i</isPrimary><idNumber>%@</idNumber><issuingSource>%@</issuingSource><effectiveDate>%@</effectiveDate><expirationDate>%@</expirationDate><patientID>%@</patientID></PatientIdentity>",identity_id,str_identityTypeID,isprime,str_id,str_issuingSource,str_effictiveDate,str_expireDate,appdalegate.patientId];
            
        }
            NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
             NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        [request setURL:url];
        
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLResponse* response;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        if ([self index]==1001) {
            if ([responseString isEqualToString:@"success"]) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your identifier information has been successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your identifier information has not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        
        else{
            if ([responseString isEqualToString:@"success"]) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your identifier information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your identifier information has not updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        
                    
        [self reloadTable];
    }
    
    
}
-(void)reloadTable
{
    [addView removeFromSuperview];
    [vi removeFromSuperview];
    self.working=NO;
    
    [self myOrientation];
  
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	
	[textField resignFirstResponder];
	return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        
        if (textField.tag==5){
            const int movementDistance = 160; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            self.addView.frame=CGRectOffset(self.addView.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        
    }
}

- (IBAction)next:(id)sender {
    self.tabBarController.selectedIndex=5;
}

- (IBAction)selectClicked:(id)sender {
    UIButton *btr=(UIButton *)sender;
    if (btr.tag==1) {
        
//        NSArray * arr1 = [[NSArray alloc]initWithObjects:@"Driver's License",@"Social Security",@"System ID",nil];
        
        if(dropDown == nil) {
            CGFloat f = 90;
             dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:identiferName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    }
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    
    dropDown = nil;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    BOOL a;
    
    if (textField.tag==10) {
        
        
        UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
        
        UIView *popoverView = [[UIView alloc] init];   //view
        popoverView.backgroundColor = [UIColor blackColor];
        
        
        datePicker1.datePickerMode = UIDatePickerModeDate;
        [datePicker1 setMinuteInterval:5];
        [datePicker1 setTag:10];
        
        
           [datePicker1 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
        [popoverView addSubview:datePicker1];
        
        popoverContent.view = popoverView;
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.delegate=self;
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:navigationController];
        popoverController.delegate=self;
        
        
        [popoverController setPopoverContentSize:CGSizeMake(320, 220) animated:NO];
        
       
            [popoverController presentPopoverFromRect:textField.frame inView:self.addView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
              
        a=NO;
        
    }
    else if (textField.tag==11)
    {
        
        UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
        
        UIView *popoverView = [[UIView alloc] init];   //view
        popoverView.backgroundColor = [UIColor blackColor];
        
        
        datePicker2.datePickerMode = UIDatePickerModeDate;
        [datePicker2 setMinuteInterval:5];
        [datePicker2 setTag:11];
        
  
        [datePicker2 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
        [popoverView addSubview:datePicker2];
        
        popoverContent.view = popoverView;
        
       
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.delegate=self;
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:navigationController];
         popoverController.delegate=self;
        
        [popoverController setPopoverContentSize:CGSizeMake(320, 220) animated:NO];
        
        
        [popoverController presentPopoverFromRect:textField.frame inView:self.addView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        a=NO;
    }
    else
    {
        a=YES;
    }
    return a;
}
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if (!doneButton) {
        doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                      style:UIBarButtonItemStylePlain
                                                     target:self action:@selector(done:)];
    }
    
    viewController.navigationItem.rightBarButtonItem = doneButton;
}
-(IBAction)done:(id)sender
{
    [popoverController dismissPopoverAnimated:YES];
}

-(void)Result:(UIDatePicker *)picker{
  
    if (picker.tag==10) {
        
       NSDateFormatter *formDay = [[NSDateFormatter alloc] init];
     formDay.dateFormat=@"MM-dd-yyyy";
    NSString *day = [formDay stringFromDate:[datePicker1 date]];
    self.edit_effectivedate.text=day;
   
    }
    else if(picker.tag==11)
    {
        NSDateFormatter *formDay = [[NSDateFormatter alloc] init];
        formDay.dateFormat=@"MM-dd-yyyy";
        NSString *day = [formDay stringFromDate:[datePicker2 date]];
               self.edit_expiredate.text=day;
    }
       
    
}
-(int)IdentityId:(NSString *)str
{
    int i=0;
    if ([str isEqualToString:@"Driver's License"]) {
        i=1;
    }
    else if ([str isEqualToString:@"Social Security"])
    {
        i=2;
    }
    else if ([str isEqualToString:@"System ID"])
    {
        i=3;
    }
    return i;
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    BOOL temp=YES;
    if (textField==edit_id) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if ((character >= 32 && character<=127)||character==8||character==9)
            {
                temp=YES; // 48 unichar for 0
            }
            else
            {
                temp=NO;
            }
        }
 
    }
    else if(textField==edit_issuingSource)
    {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if ((character >= 65 && character<=90)||(character>=97 && character<=122)||character==32||character==8||character==9)
            {
                temp=YES; // 48 unichar for 0
            }
            else
            {
                temp=NO;
            }
        }
    }
    else
    {
        temp=YES;
    }
    return temp;
}

-(NSString *) saveDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSString *result = [formater stringFromDate:date2];
   
    return result;
    
}
-(NSString *)viewDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSString *result = [formater stringFromDate:date2];
      return result;
    
}


-(void)addDropDown
{
    identifierType=[[NSMutableArray alloc]init];
    identifierTypeList *identifier=[[identifierTypeList alloc]init];
    identifierType=[identifier identifierTypeList];
    
    identiferName=[identifierType valueForKey:@"name"];
    identifierID=[identifierType valueForKey:@"id"];
    
}




@end
