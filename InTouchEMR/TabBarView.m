//
//  TabBarView.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 24/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "TabBarView.h"
#import "IndexAppDelegate.h"
#import "PatientInformationViewController.h"
@interface TabBarView ()

@end

@implementation TabBarView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initwithArray:(NSMutableDictionary *)array
{
    //image=img;
    arr=array;
    return self;
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc]
                                initWithTitle:@"Logout"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:@selector(logout:)];
    self.navigationItem.rightBarButtonItem=btnSave;
     self.navigationController.navigationBar.translucent = NO;
     [self.navigationController.navigationBar setTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navigationBar"]]];
    
    [self.navigationController setNavigationBarHidden:NO];
    IndexAppDelegate *appDelegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.patientId=[arr objectForKey:@"patientID"];
    
    
  
    NSUInteger numberOfViewControllersOnStack = [self.navigationController.viewControllers count];
    UIViewController *parentViewController = self.navigationController.viewControllers[numberOfViewControllersOnStack - 2];
    Class parentVCClass = [parentViewController class];
    NSString *className = NSStringFromClass(parentVCClass);
    if (![className isEqualToString:@"PatientInformationViewController"]) {
        [self.navigationItem setHidesBackButton:YES];
        [demographicsView.btnemail setHidden:YES];
        [demographicsView.landscapebtnemail setHidden:YES];
    }
    else
    {
        UIBarButtonItem *barBtnItem = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Patient Information"
                                       style:UIBarButtonItemStyleBordered
                                       target:self
                                       action:@selector(pop:)];
        
        self.navigationItem.leftBarButtonItem = barBtnItem;

        self.navigationItem.hidesBackButton=NO;
    }
    
    
    
    
        
    tabBar=[[UITabBarController alloc]init];
   
   
    demographicsView=[[DemographicsViewController alloc]initwithArray:arr];
    addressView=[[AddressViewController alloc]initwithArray:arr];
    contactsView=[[ContactsViewController alloc]initwithArray:arr withImage:image];
    InsuranceView=[[InsuranceViewController alloc]initwithArray:arr withImage:image];
    identitiesView=[[IdentitiesViewController alloc]initwithArray:arr withImage:image];
    patientphotosView=[[PatientPhotosViewController alloc]initwithArray:arr];
    
    demographicsView.tabBarItem.title=@"Essentials";
    addressView.tabBarItem.title=@"Addresses";
    contactsView.tabBarItem.title=@"Touch Points";
    InsuranceView.tabBarItem.title=@"Insurance";
    identitiesView.tabBarItem.title=@"Designators";
    patientphotosView.tabBarItem.title=@"Uploads";
  
    
    
    [demographicsView.tabBarItem setImage:[UIImage imageNamed:@"medical-sign.png"]];
    addressView.tabBarItem.image=[UIImage imageNamed:@"newspaper.png"];
    contactsView.tabBarItem.image=[UIImage imageNamed:@"diary.png"];
    InsuranceView.tabBarItem.image=[UIImage imageNamed:@"hierarchy.png"];
    identitiesView.tabBarItem.image=[UIImage imageNamed:@"id-card.png"];
    patientphotosView.tabBarItem.image=[UIImage imageNamed:@"camera.png"];
    tabBar.delegate=self;
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
    NSString *version = [[UIDevice currentDevice] systemVersion];
    int ver = [version intValue];
    

    
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        if (ver < 7){
            //iOS 6 work
            [tabBar.tabBar setFrame:CGRectMake(0, 950, 770, 50)];
        }
        else{
            //iOS 7 related work
            [tabBar.tabBar setFrame:CGRectMake(0, 970, 770, 50)];
        }
        
        
    }
    else{
    
        if (ver < 7){
            //iOS 6 work
            [tabBar.tabBar setFrame:CGRectMake(0, 718, 1024, 50)];
//            [self.view setBackgroundColor:[UIColor blackColor]];
        }
        else{
            //iOS 7 related work
            [tabBar.tabBar setFrame:CGRectMake(0, 718, 1024, 50)];
            
//            [self.view setBackgroundColor:[UIColor blackColor]];
            /*
            self.view.autoresizesSubviews = YES;
            self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
             
             */
        }
        
        
       // [tabBar.tabBar setFrame:CGRectMake(0, 970, 768, 50)];
    }

           tabBar.viewControllers=[NSArray arrayWithObjects:demographicsView,addressView,contactsView,identitiesView,InsuranceView,patientphotosView, nil];
 
    [self addChildViewController:tabBar];
    [self.view addSubview:tabBar.view];
	// Do any additional setup after loading the view.
}

- (IBAction)pop:(id)sender {

       arr=demographicsView.arr;
       
 [self.navigationController popViewControllerAnimated:YES];
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logout:(id)sender {
    UIAlertView *log=[[UIAlertView alloc]initWithTitle:@"Do you really want to log out?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [log show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
        }
            
        default:
            break;
    }

}


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
 
    if (viewController==demographicsView) {
        [demographicsView viewDidLoad];
        
    }
    else if(viewController==addressView)
    {
        [addressView viewDidLoad];
    }
    else if(viewController==contactsView)
    {    [contactsView viewDidLoad];
    }
    else if(viewController==InsuranceView)
    {
        [InsuranceView viewDidLoad];
    }
    else if (viewController==identitiesView)
    {
        [identitiesView viewDidLoad];
    }
    else if (viewController==patientphotosView)
    {
        [patientphotosView viewDidLoad];
    }
    
}


@end
