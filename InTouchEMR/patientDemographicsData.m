//
//  patientDemographicsData.m
//  InTouchEMR
//
//  Created by Apple on 15/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "patientDemographicsData.h"
#import "IndexAppDelegate.h"

@implementation patientDemographicsData

-(NSMutableArray *)patientDataByXML:(NSString *)patientID registationNO:(NSString *)registationID
{
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString *postString = [[NSString alloc]initWithFormat:@"%@%@/%@",globalDele.demographicsUrl,patientID,registationID];

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
   
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
   
    
    
    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
        NSLog(@"success");
        NSLog(@"%@",xmlparsing);

    }
    else
    {
      //  NSLog(@"error");
    }
    
 
   
    
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"firstName"]||[myelement isEqualToString:@"lastName"]||[myelement isEqualToString:@"middleName"]||[myelement isEqualToString:@"titleID"]||[myelement isEqualToString:@"gender"]||[myelement isEqualToString:@"maritalStatusID"]||[myelement isEqualToString:@"email"]||[myelement isEqualToString:@"employerName"]||[myelement isEqualToString:@"nickName"]||[myelement isEqualToString:@"otherInformation"]||[myelement isEqualToString:@"studentTypeID"]||[myelement isEqualToString:@"ssn"]||[myelement isEqualToString:@"smokingStatusID"]||[myelement isEqualToString:@"jobTypeID"]||[myelement isEqualToString:@"isWorking"]  ||  [myelement isEqualToString:@"employerAddress"]||[myelement isEqualToString:@"employerName"]||[myelement isEqualToString:@"therapy_newsletter_subscription"]||[myelement isEqualToString:@"hippaaAgreementSigned"]||[myelement isEqualToString:@"pcpRelease"]||[myelement isEqualToString:@"statusName"]||[myelement isEqualToString:@"paymentStatus"]||[myelement isEqualToString:@"ethnicOriginID"]||[myelement isEqualToString:@"isMinor"]||[myelement isEqualToString:@"minor_mother_name"]||[myelement isEqualToString:@"minor_father_name"]||[myelement isEqualToString:@"guarantorFirstName"]||[myelement isEqualToString:@"guarantorLastName"]||[myelement isEqualToString:@"guarantorMiddleName"]||[myelement isEqualToString:@"guarantorAddress"]||[myelement isEqualToString:@"guarantorCity"]||[myelement isEqualToString:@"guarantorState"]||[myelement isEqualToString:@"guarantorZip"]||[myelement isEqualToString:@"guarantorPhone"]||[myelement isEqualToString:@"imageName"]||[myelement isEqualToString:@"birth"]||[myelement isEqualToString:@"userName"]||[myelement isEqualToString:@"referralPhysicianID"])
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Patient"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
}

@end
