//
//  PhotoViewController.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 27/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import "AFDownloadRequestOperation.h"
#import "IndexAppDelegate.h"

@interface PhotoViewController : UIViewController<UIWebViewDelegate,NSURLConnectionDelegate>
{
    NSString *ACCESS_KEY_ID;
    NSString *SECRET_KEY;
    NSString *PICTURE_BUCKET;
    NSDictionary *imgdata;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    UIDeviceOrientation orientation;
    UIImage *image;
    NSString *contentType;
    NSString *currentURL;
    AFDownloadRequestOperation *operation;
    IndexAppDelegate *globleData;
    
    NSMutableArray *DocumentTypeData;
    NSArray *documentTypeName,*DocumentTypeID;
}
-(void)clearCurrentView;
@property (nonatomic, assign) BOOL flag;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UIWebView *webView1;
@property (strong, nonatomic) IBOutlet UIWebView *webView2;

@property (strong, nonatomic) IBOutlet UILabel *DocumentType;
@property (strong, nonatomic) IBOutlet UILabel *DocumentName;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *loading;
@property (strong, nonatomic) IBOutlet UILabel *loadingText;
@property (strong, nonatomic) IBOutlet UILabel *land_loading;
@property (strong, nonatomic) IBOutlet UILabel *land_loadingText;

@property (strong, nonatomic) IBOutlet UIImageView *myImage;
@property (strong, nonatomic) IBOutlet UIProgressView *progress;
@property (strong, nonatomic) IBOutlet UIProgressView *landscapeprogress;


@property (strong, nonatomic) IBOutlet UILabel *landscapeDocumentType;
@property (strong, nonatomic) IBOutlet UILabel *landscapeDocumentName;
@property (strong, nonatomic) IBOutlet UILabel *landscapedate;

@property (strong, nonatomic) IBOutlet UIImageView *landscapemyImage;
-(id)initwithImageName:(NSDictionary *)img ;
@end
