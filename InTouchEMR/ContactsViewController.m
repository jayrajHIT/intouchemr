//
//  ContactsViewController.m
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "ContactsViewController.h"
#import "IndexAppDelegate.h"
#import "patientContactXMLData.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "SHKActivityIndicator.h"
#import "NSString+validation.h"

#import "contactTypeXMLList.h"
#import "contactCategoryList.h"
#import "AppointmentReminderXMLList.h"

@interface ContactsViewController ()

@end

@implementation ContactsViewController
@synthesize patientContact,patientEmail,patientName,patientOrigin,ssnHeading,ssnValue;

@synthesize landscapessnValue,landscapessnHeading,landscapepatientOrigin,landscapepatientName,landscapepatientEmail,landscapepatientContact;
@synthesize patientImage,landscapePatientImage,orientchange,addView;
@synthesize extNo,IsEmergency,AdditionalInfo,appointReminder;

@synthesize contactTable,landscapecontactTable,index;
@synthesize edit_contact,edit_contactType,edit_phoneType,edit_appointReminder,edit_usage;
@synthesize ContactType,Phonetype,ContactNo,IsEmrgency,IsPrimary;
@synthesize ext,appoint,additnInfo,edit_Cancel,edit_Save,Isprime,emrgncy,appointRemind;
@synthesize btn_edit_Contact,btn_edit_land_Contact;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initwithArray:(NSMutableDictionary *)array withImage:(UIImage *)img
{
    arr=array;
    image=img;
    return self;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setWorking:NO];
   [self setOrientchange:NO];
      [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];

	orientation = [[UIDevice currentDevice] orientation];
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    [IsEmergency setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [IsEmergency setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    [Isprime setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [Isprime setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
    [self myOrientation];
   
 }

-(void)orientationChanged:(NSNotification *)notification{
    
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown)  {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    // Do your orientation logic here
	
}

-(void)myOrientation
{
    [self performSelectorInBackground:@selector(mythread)
                           withObject:nil];
    
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
		[self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        
        if ([arr objectForKey:@"FullName"]) {
            landscapepatientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            landscapepatientName.text=@"";
        }
        
        if ([arr objectForKey:@"email"]) {
            landscapepatientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            landscapepatientEmail.text=@"";
        }
        if ([arr objectForKey:@"ssn"]) {
            landscapessnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            landscapessnValue.text=@"";
        }
        
        if ([arr objectForKey:@"ethnicOriginName"]) {
            landscapepatientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            landscapepatientOrigin.text=@"";
        }

    
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            landscapepatientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                     landscapepatientContact.text=nil;
        }
       
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [landscapePatientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        [landscapecontactTable reloadData];
        
        if (self.working==YES) {
            
            [self editView];
        }

        
         } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		
		[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
      
             if ([arr objectForKey:@"FullName"]) {
                 patientName.text=[arr objectForKey:@"FullName"];
             }
             else
             {
                 patientName.text=@"";
             }
             
             if ([arr objectForKey:@"email"]) {
                 patientEmail.text=[arr objectForKey:@"email"];
             }
             else
             {
                 patientEmail.text=@"";
             }
             if ([arr objectForKey:@"ssn"]) {
                 ssnValue.text=[arr objectForKey:@"ssn"];
             }
             else
             {
                 ssnValue.text=@"";
             }
             
             if ([arr objectForKey:@"ethnicOriginName"]) {
              patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
             }
             else
             {
                patientOrigin.text=@"";
             }

        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                      patientContact.text=nil;
        }
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
       
        [contactTable reloadData];
        if (self.working==YES) {
            
            [self editView];
        }
        
	}
}
-(void)mythread
{
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    patientContactXMLData *patientData=[[patientContactXMLData alloc]init];
    ContactData=[patientData patientContactDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
    [self addDropDown];
    [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
}
-(void)afterthread
{
    [self updateContactInHeading];
    [contactTable reloadData];
    [landscapecontactTable reloadData];
   
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}


-(void)updateContactInHeading
{
    for (int i=0; i<[ContactData count]; i++) {
        NSDictionary *tempDic=[ContactData objectAtIndex:i];
        if ([[tempDic objectForKey:@"isPrimary"]isEqualToString:@"1"]) {
            if ([tempDic objectForKey:@"contact"]) {
                [arr setObject:[tempDic objectForKey:@"contact"] forKey:@"contactNo"];
           
            
  
            NSString *string = [tempDic objectForKey:@"contact"];
            if ([string rangeOfString:@"@"].location == NSNotFound) {
                landscapepatientContact.text=[tempDic objectForKey:@"contact"];
                 patientContact.text=[tempDic objectForKey:@"contact"];
            }
            
            else {
               
                landscapepatientContact.text=@"";
                 patientContact.text=@"";
            }
            }
        }
    }
    
}


-(void)editView
{
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024,670)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [landscapeView addSubview:vi];
        self.addView.frame=CGRectMake(250, 60, 525, 520);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        AdditionalInfo.layer.cornerRadius=5.0;
        AdditionalInfo.layer.borderWidth=1.0;
        AdditionalInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        [landscapeView addSubview:self.addView];
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 930)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [portraitView addSubview:vi];
        self.addView.frame=CGRectMake(125, 170, 525, 520);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        AdditionalInfo.layer.cornerRadius=5.0;
        AdditionalInfo.layer.borderWidth=1.0;
        AdditionalInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [portraitView addSubview:self.addView];
    }
    
    if ([self orientchange]) {
       
       
    if ([self index]==1001) {
               
        [self unselectEmail];
        [edit_Save setTitle:@"Save" forState:UIControlStateNormal];
        [edit_contactType setTitle:@"select one" forState:UIControlStateNormal];
        [edit_usage setTitle:@"select one" forState:UIControlStateNormal];
        edit_contact.text=@"";
         AdditionalInfo.text=@"Additional Information";
        [extNo setText:@""];
        [appointReminder setTitle:@"select one" forState:UIControlStateNormal];
          //      edit_emergency.text=@"";
        [IsEmergency setOn:NO];
        [Isprime setOn:NO];
         [self setOrientchange:NO];
         isHealthFusion=@"0";
    }
    else
    {
    
        
        NSDictionary *editdict=[ContactData objectAtIndex:index];
      
        if ([editdict objectForKey:@"id"]) {
            contact_id=[editdict objectForKey:@"id"];
        }
        else
        {
            contact_id=@"";
        }
        
        
        if ([editdict objectForKey:@"contactTypeID"]) {
             if ([contactTypeID indexOfObject:[editdict objectForKey:@"contactTypeID"]] != NSNotFound) {
            
          int a= [contactTypeID indexOfObject:[editdict objectForKey:@"contactTypeID"]];
            [edit_contactType setTitle:[contactTypeName objectAtIndex:a] forState:UIControlStateNormal];
             }
            else
            {
                [edit_contactType setTitle:@"select one" forState:UIControlStateNormal];
                
            }
        }
        else
        {
             [edit_contactType setTitle:@"select one" forState:UIControlStateNormal];
        }
        
        
       [edit_Save setTitle:@"Update" forState:UIControlStateNormal];
         isHealthFusion=[editdict objectForKey:@"isHealthFusion"];
        
        if ([[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"Email"]) {
            [self selectEmail];
        }
        else
        {
            [self unselectEmail];
        }
        
        if ([editdict objectForKey:@"contact"]) {
             edit_contact.text=[editdict objectForKey:@"contact"];
        }
        else
        {
            edit_contact.text=@"";
        }
        
        if ([editdict objectForKey:@"contactUsageID"]) {
            
             if ([contactCategorytypeID indexOfObject:[editdict objectForKey:@"contactUsageID"]] != NSNotFound) {
            int a= [contactCategorytypeID indexOfObject:[editdict objectForKey:@"contactUsageID"]];
            [edit_usage setTitle:[contactCategoryTypeName objectAtIndex:a] forState:UIControlStateNormal];
             }
            else
            {
                [edit_usage setTitle:@"select one" forState:UIControlStateNormal];
            }
        }
        else
        {
           [edit_usage setTitle:@"select one" forState:UIControlStateNormal];
        }

        if ([editdict objectForKey:@"phoneExtension"]) {
            extNo.text=[editdict objectForKey:@"phoneExtension"];
        }
        else
        {
            extNo.text=@"";
        }

        if ([editdict objectForKey:@"additionalInformation"]) {
            AdditionalInfo.text=[editdict objectForKey:@"additionalInformation"];
        }
        else
        {
            AdditionalInfo.text=@"";
        }

        if ([editdict objectForKey:@"appointmentReminderTypeID"]) {
           
            
            if ([appointmentReminderTypeID indexOfObject:[editdict objectForKey:@"appointmentReminderTypeID"]] != NSNotFound) {
            int a=[appointmentReminderTypeID indexOfObject:[editdict objectForKey:@"appointmentReminderTypeID"]];
        
            [appointReminder setTitle:[appointmentReminderTypeName objectAtIndex:a] forState:UIControlStateNormal];
        }
            else
            {
                [appointReminder setTitle:@"select one" forState:UIControlStateNormal];
            }
        }
        else
        {
            [appointReminder setTitle:@"select one" forState:UIControlStateNormal];
        }
        
       
       
        if ([[editdict objectForKey:@"emergencyContact"]isEqualToString:@"1"]) {
            [IsEmergency setOn:YES];
        }
        else
        {
            [IsEmergency setOn:NO];
        }
        if ([[editdict objectForKey:@"isPrimary"] isEqualToString:@"1"]) {
            [Isprime setOn:YES];
        }
        else
        {
            [Isprime setOn:NO];
        }
        
        [self setOrientchange:NO];
    }
        if ([isHealthFusion isEqualToString:@"1"]) {
            [edit_contact setUserInteractionEnabled:NO];
            [edit_contactType setUserInteractionEnabled:NO];
            [edit_usage setUserInteractionEnabled:NO];
 
        }
        else
        {
            [edit_contact setUserInteractionEnabled:YES];
            [edit_contactType setUserInteractionEnabled:YES];
            [edit_usage setUserInteractionEnabled:YES];
           
        }
}
}


-(void) clearCurrentView {
    [self.addView removeFromSuperview];
    [vi removeFromSuperview];
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}
- (IBAction)cancelView:(id)sender {
    [self.addView removeFromSuperview];
    [vi removeFromSuperview];
    self.working=NO;
     [self setOrientchange:NO];
    ContactType.textColor=[UIColor blackColor];
    Phonetype.textColor=[UIColor blackColor];
    ContactNo.textColor=[UIColor blackColor];
    
}
- (IBAction)update:(id)sender {
    [self setWorking:YES];
    [self setIndex:1001];
     [self setOrientchange:YES];
    [self editView];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        
        if (textField.tag==5||textField.tag==1||textField.tag==2){
            const int movementDistance = 160; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            self.addView.frame=CGRectOffset(self.addView.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag==5 && [textView.text isEqualToString:@"Additional Information"]) {
        textView.text=@"";
    }
    
    
    [self animateTextView: textView up: YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView: textView up: NO];
    
    if (textView.tag==5 && textView.text.length==0 ) {
        
        textView.text=@"Additional Information";
    }
    

}
- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        
        if (textView.tag==5){
            const int movementDistance = 150; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            self.addView.frame=CGRectOffset(self.addView.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [ContactData count];
    // return [arr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==contactTable) {
        
        static NSString *CellIdentifierPortrait = @"portrait";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"contactCell" owner:self options:nil];
        
        
        cell=( contactCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
        
        if (cell==nil) {
            cell=[[ contactCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierPortrait];
            cell=[nib objectAtIndex:0];
        }
        
        
        NSDictionary *tempdict=[ContactData objectAtIndex:indexPath.row];
        //  NSString *add=[NSString stringWithFormat:@"%@ ( %@ )",[tempdict objectForKey:@"group"],[tempdict objectForKey:@"insuredPartyName"]];
        if ([tempdict objectForKey:@"contactTypeID"]) {
 if ([contactTypeID indexOfObject:[tempdict objectForKey:@"contactTypeID"]] != NSNotFound) {
            int a=[contactTypeID indexOfObject:[tempdict objectForKey:@"contactTypeID"]];
            
        cell.ContactType.text=[contactTypeName objectAtIndex:a];
        }
            else
            {
                cell.ContactType.text=@"";
            }
        }
        else
        {
             cell.ContactType.text=@"";
        }
        
        if ([tempdict objectForKey:@"contact"]) {
            
            cell.Contact.text=[tempdict objectForKey:@"contact"];
        }
        else
        {
            cell.Contact.text=@"";
        }
        
        if ([tempdict objectForKey:@"contactUsageID"]) {
            
             if ([contactCategorytypeID indexOfObject:[tempdict objectForKey:@"contactUsageID"]] != NSNotFound) {
            int a=[contactCategorytypeID indexOfObject:[tempdict objectForKey:@"contactUsageID"]];
            
            cell.Usage.text=[contactCategoryTypeName objectAtIndex:a];
             }
            else
            {
                cell.Usage.text=@"";
            }
        }
        else
        {
            cell.Usage.text=@"";
        }
        
       
        int a=[contactTypeID indexOfObject:[tempdict objectForKey:@"contactTypeID"]];
        
        if ([[contactTypeName objectAtIndex:a]isEqualToString:@"Email"]) {
            
            if ([tempdict objectForKey:@"emergencyContact"]) {
                
                cell.emergency.text=[tempdict objectForKey:@"emergencyContact"];
            }
            else
            {
                cell.emergency.text=@"";
            }

        }
        else
        {
            if ([[tempdict objectForKey:@"emergencyContact"] isEqualToString:@"1"]) {
                cell.emergency.text=@"Yes";
            }
            else
            {
                cell.emergency.text=@"No";
            }
        }
        if ([[tempdict objectForKey:@"isPrimary"] isEqualToString:@"1"]) {
            cell.primary.text=@"Yes";
        }
        else
        {
            cell.primary.text=@"No";
        }
        
 
        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (tableView==landscapecontactTable) {
        
        static NSString *CellIdentifier = @"landscape";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"contactCell" owner:self options:nil];
        
        cell=(contactCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil) {
            cell=[[ contactCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell=[nib objectAtIndex:1];
        }
        
        
        NSDictionary *tempdict=[ContactData objectAtIndex:indexPath.row];
        
        if ([tempdict objectForKey:@"contactTypeID"]) {
            
            if ([contactTypeID indexOfObject:[tempdict objectForKey:@"contactTypeID"]] != NSNotFound) {
            int a=[contactTypeID indexOfObject:[tempdict objectForKey:@"contactTypeID"]];
            
            cell.ContactType.text=[contactTypeName objectAtIndex:a];
            }
            else
            {
                cell.ContactType.text=@"";
            }
        }
        else
        {
            cell.ContactType.text=@"";
        }
        
        if ([tempdict objectForKey:@"contact"]) {
            
            cell.Contact.text=[tempdict objectForKey:@"contact"];
        }
        else
        {
            cell.Contact.text=@"";
        }
        if ([tempdict objectForKey:@"contactUsageID"]) {
            
            if ([contactCategorytypeID indexOfObject:[tempdict objectForKey:@"contactUsageID"]] != NSNotFound) {
            int a=[contactCategorytypeID indexOfObject:[tempdict objectForKey:@"contactUsageID"]];
            
            cell.Usage.text=[contactCategoryTypeName objectAtIndex:a];
        }
            else
            {
                 cell.Usage.text=@"";
            }
        }
        else
        {
            cell.Usage.text=@"";
        }
        
        int a=[contactTypeID indexOfObject:[tempdict objectForKey:@"contactTypeID"]];
        
        if ([[contactTypeName objectAtIndex:a]isEqualToString:@"Email"]) {
       
            if ([tempdict objectForKey:@"emergencyContact"]) {
                
                cell.emergency.text=[tempdict objectForKey:@"emergencyContact"];
            }
            else
            {
                cell.emergency.text=@"";
            }
            
        }
        else
        {
            if ([[tempdict objectForKey:@"emergencyContact"] isEqualToString:@"1"]) {
                cell.emergency.text=@"Yes";
            }
            else
            {
                cell.emergency.text=@"No";
            }
        }
        if ([[tempdict objectForKey:@"isPrimary"] isEqualToString:@"1"]) {
            cell.primary.text=@"Yes";
        }
        else
        {
            cell.primary.text=@"No";
        }
        
    

        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)accessoryButtonTapped:(id)sender event:(id)event
{
    
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition;
    NSIndexPath *indexPath;
	if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        currentTouchPosition = [touch locationInView:landscapecontactTable];
        indexPath = [landscapecontactTable indexPathForRowAtPoint: currentTouchPosition];
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        currentTouchPosition = [touch locationInView:contactTable];
        indexPath = [contactTable indexPathForRowAtPoint: currentTouchPosition];
	}
    
    
  	if (indexPath != nil)
	{
        [self tableView: contactTable accessoryButtonTappedForRowWithIndexPath: indexPath];
        
	}
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    [self setIndex:indexPath.row];
    self.working=YES;
    
    [self setOrientchange:YES];
    [self editView];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setIndex:indexPath.row];
    self.working=YES;
    
    [self setOrientchange:YES];
    [self editView];
    
    
}
- (IBAction)next:(id)sender {
    self.tabBarController.selectedIndex=3;
}

-(IBAction)emailContact:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if ([[btn titleForState:UIControlStateNormal]isEqualToString:@"Email"] ) {
        
    }
    
}

-(IBAction)saveContact:(id)sender
{
       BOOL flag=NO;
    
    if (![[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
      
        int a=[contactTypeName indexOfObject:[edit_contactType titleForState:UIControlStateNormal]];
        ContTypeID=[contactTypeID objectAtIndex:a];
    }
    
    if (![[edit_usage titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
        int b=[contactCategoryTypeName indexOfObject:[edit_usage titleForState:UIControlStateNormal]];
        PhoneTypeID=[contactCategorytypeID objectAtIndex:b];

    }
    
    str_PhoneNo=edit_contact.text;
    str_ext=extNo.text;
    str_emerCont=emrgncy.text;
    
    if ([NSString stringIsEmpty:str_emerCont] ) {
        str_emerCont=@"";
    }
    if ([NSString stringIsEmpty:str_ext]) {
        str_ext=@"";
    }
    
    if ([IsEmergency isOn]) {
        EmrgCont=1;
    }
    else
    {
        EmrgCont=0;
    }
    if ([Isprime isOn]) {
        isprimary=1;
    }
    else
    {
        isprimary=0;
    }
    if ([[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"Email"]) {
     
    if (appointRemind.isOn) {
       appointStatus=1;
    }
    else
    {
        appointStatus=0;
    }
        
    }
    
    str_additionInfo=AdditionalInfo.text;
    if (!([[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"Email"])) {
     
         if (![[appointReminder titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
        
        int a=[appointmentReminderTypeName indexOfObject:[appointReminder titleForState:UIControlStateNormal]];
        appointReminderID=[appointmentReminderTypeID objectAtIndex:a];
         }
        else
        {
            appointReminderID=@"0";
        }
  
    }
    
   
     if ([isHealthFusion isEqualToString:@"0"]) {
   
         if ([[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
        ContactType.textColor=[UIColor redColor];
        flag=YES;
    }
    else
    {
        ContactType.textColor=[UIColor blackColor];
        
    }
         
    if (!([[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"Email"])) {
        
   
    if ([[edit_usage titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
        Phonetype.textColor=[UIColor redColor];
        flag=YES;
    }
    else
    {
        Phonetype.textColor=[UIColor blackColor];
        
    }

    }
        if([NSString stringIsEmpty:str_PhoneNo])
    {
        ContactNo.textColor=[UIColor redColor];
        flag=YES;
    }
    else{
        ContactNo.textColor=[UIColor blackColor];
       
    }
    }
   
       
    if([NSString stringIsEmpty:str_ext])
    {
        str_ext=@"";
        
    }
   
   if (flag==YES && [isHealthFusion isEqualToString:@"0"])
   {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       
      
    }
    
   
    else
    {
        
        IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
         NSString *xmlText;
        if ([[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"Email"]) {
            
        if (![self NSStringIsValidEmail:str_PhoneNo]) {
             ContactNo.textColor=[UIColor redColor];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email is an invalid format" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else
        {
             ContactNo.textColor=[UIColor blackColor];
            
            if ([self index]==1001) {
                xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Contact><contactTypeID>%@</contactTypeID><isPrimary>%i</isPrimary><contact>%@</contact><emergencyContact>%@</emergencyContact><additionalInformation>%@</additionalInformation><appointmentReminder>%i</appointmentReminder><patientID>%@</patientID><isHealthFusion>%@</isHealthFusion><userName>%@</userName></Contact>",ContTypeID,isprimary,str_PhoneNo,str_emerCont,str_additionInfo,appointStatus,appdalegate.patientId,isHealthFusion,appdalegate.username];
            }
            else{
                xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Contact><id>%@</id><contactTypeID>%@</contactTypeID><isPrimary>%i</isPrimary><contact>%@</contact><emergencyContact>%@</emergencyContact><additionalInformation>%@</additionalInformation><appointmentReminder>%i</appointmentReminder><patientID>%@</patientID><isHealthFusion>%@</isHealthFusion><userName>%@</userName></Contact>",contact_id,ContTypeID,isprimary,str_PhoneNo,str_emerCont,str_additionInfo,appointStatus,appdalegate.patientId,isHealthFusion,appdalegate.username];
            }
           
                  [self saveData:xmlText];
        }
        }
    
        else
        {
            int length = [self getLength:str_PhoneNo];
                if (length!=10) {
                    ContactNo.textColor=[UIColor redColor];
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Contact Number must be 10 digits" message:@"The required information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    ContactNo.textColor=[UIColor blackColor];
                         
            
            
            if ([self index]==1001) {
                xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Contact><contactTypeID>%@</contactTypeID><contactUsageID>%@</contactUsageID><isPrimary>%i</isPrimary><contact>%@</contact><phoneExtension>%@</phoneExtension><emergencyContact>%i</emergencyContact><additionalInformation>%@</additionalInformation><appointmentReminderTypeID>%@</appointmentReminderTypeID><patientID>%@</patientID><isHealthFusion>%@</isHealthFusion><userName>%@</userName></Contact>",ContTypeID,PhoneTypeID,isprimary,str_PhoneNo,str_ext,EmrgCont,str_additionInfo,appointReminderID,appdalegate.patientId,isHealthFusion,appdalegate.username];
            }
            else{
                xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Contact><id>%@</id><contactTypeID>%@</contactTypeID><contactUsageID>%@</contactUsageID><isPrimary>%i</isPrimary><contact>%@</contact><phoneExtension>%@</phoneExtension><emergencyContact>%i</emergencyContact><additionalInformation>%@</additionalInformation><appointmentReminderTypeID>%@</appointmentReminderTypeID><patientID>%@</patientID><isHealthFusion>%@</isHealthFusion><userName>%@</userName></Contact>",contact_id,ContTypeID,PhoneTypeID,isprimary,str_PhoneNo,str_ext,EmrgCont,str_additionInfo,appointReminderID,appdalegate.patientId,isHealthFusion,appdalegate.username];
            }
        
        
            [self saveData:xmlText];
        }
    }
       }
    
}


-(void)saveData:(NSString *)str
{
 
    
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString *xmlText;
    
      NSURL *url = [NSURL URLWithString:appdalegate.contactSavedUrl];
  
    xmlText=str;
     NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse* response;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if ([self index]==1001) {
        if ([responseString isEqualToString:@"success"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your touch point information has been successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your touch point information has not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    else{
        if ([responseString isEqualToString:@"success"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your touch point information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your touch point information has not updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    [self reloadTable];
}

   
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)selectClicked:(id)sender {
    UIButton *btr=(UIButton *)sender;
    if (btr.tag==1) {
        
            
        if(dropDown == nil) {
            CGFloat f = [contactTypeName count]*30;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:contactTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            
            [self rel];
        }
        
        
    }
    if (btr.tag==2) {
        
      
        if(dropDown == nil) {
            CGFloat f = [contactCategoryTypeName count]*30;
             dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:contactCategoryTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    if (btr.tag==3) {
        
        
        if(dropDown == nil) {
            CGFloat f;
            if ([appointmentReminderTypeName count]>5) {
                f=150;
            }
            else
            {
                f=[appointmentReminderTypeName count]*30;
            }
            
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:appointmentReminderTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    
}
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    if ([[edit_contactType titleForState:UIControlStateNormal] isEqualToString:@"Email"]) {
        [self selectEmail];
    }
    else
    {
        [self unselectEmail];
    }
    
}

-(void)rel{
    
    dropDown = nil;
}

-(void)selectEmail
{
    [edit_contact setTag:5];
    [edit_contact setKeyboardType:UIKeyboardTypeEmailAddress];
    [ContactNo setText:@"Email id *:"];
    [edit_contact setPlaceholder:@"Email id"];
    
    [Phonetype setHidden:YES];
    [edit_usage setHidden:YES];
    [edit_phoneType setHidden:YES];
    [edit_contact setFrame:CGRectMake(220, 112, 224, 30)];
    [extNo setHidden:YES];
    [IsEmergency setHidden:YES];
    [AdditionalInfo setFrame:CGRectMake(220, 200,224,48)];
    [edit_appointReminder setHidden:YES];
    [appointReminder setHidden:YES];
    [ContactNo setFrame:CGRectMake(125, 116, 128, 21)];
    [ext setHidden:YES];
    [IsEmrgency setFrame:CGRectMake(42, 160, 166, 21)];
    [additnInfo setFrame:CGRectMake(33, 200, 174, 21)];
    [appoint setFrame:CGRectMake(25, 270, 183, 21)];
    
    [IsPrimary setFrame:CGRectMake(122, 320, 92, 21)];
    [Isprime setFrame:CGRectMake(225, 320, 79, 27)];
    [appointRemind setHidden:NO];
    [emrgncy setHidden:NO];
    [emrgncy setFrame:CGRectMake(220, 156, 224,30)];
    [emrgncy setKeyboardType:UIKeyboardTypeNumberPad];

    [appointRemind setFrame:CGRectMake(220, 270, 79, 27)];
  //  [addView addSubview:appointRemind];
   
    
    
    [addView setBounds:CGRectMake(0, 0, 492, 430)];
    
    [edit_Save setFrame:CGRectMake(260, 370, 100, 38)];
    [edit_Cancel setFrame:CGRectMake(132, 370, 100, 38)];
}
-(void)unselectEmail
{
    
    [edit_contact setTag:2];
    [edit_contact setKeyboardType:UIKeyboardTypeNumberPad];
    [ContactNo setText:@"Phone Number *:"];
    [edit_contact setPlaceholder:@"Phone Number"];
    [Phonetype setHidden:NO];
    [edit_usage setHidden:NO];
    [edit_phoneType setHidden:NO];
    [extNo setHidden:NO];
    [IsEmergency setHidden:NO];
    [edit_appointReminder setHidden:NO];
    [appointReminder setHidden:NO];
    [ext setHidden:NO];
    [appointRemind setHidden:YES];
    [emrgncy setHidden:YES];
   
   // [edit_usage setFrame:CGRectMake(220, 112, 224, 30)];
  //  [edit_phoneType setFrame:CGRectMake(220, 112, 224, 30)];
    [edit_contact setFrame:CGRectMake(220, 156, 224, 30)];
    
    [extNo setFrame:CGRectMake(220, 200, 224, 30)];

    
    
    [IsEmergency setFrame:CGRectMake(220, 241, 79, 27)];
    [AdditionalInfo setFrame:CGRectMake(220, 283,224,48)];
    [edit_appointReminder setFrame:CGRectMake(220, 342, 224, 30)];
    [appointReminder setFrame:CGRectMake(220, 342, 224, 31)];
    
  //  [Phonetype setFrame:CGRectMake(100, 116, 104, 21)];
    [ContactNo setFrame:CGRectMake(72, 160, 140, 21)];
    [ext setFrame:CGRectMake(161, 203, 51, 21)];
    [IsEmrgency setFrame:CGRectMake(42, 241, 166, 21)];
    [additnInfo setFrame:CGRectMake(32, 283, 174, 21)];
    [appoint setFrame:CGRectMake(25, 348, 183, 21)];
    [IsPrimary setFrame:CGRectMake(125, 400, 92, 21)];
    [Isprime setFrame:CGRectMake(230, 400, 79, 27)];
    [edit_Save setFrame:CGRectMake(260, 450, 100, 38)];
    [edit_Cancel setFrame:CGRectMake(132, 450, 100, 38)];
    [addView setBounds:CGRectMake(0, 0, 492, 520)];
}


-(void)reloadTable
{
    [addView removeFromSuperview];
    [vi removeFromSuperview];
    self.working=NO;
    [self setOrientchange:NO];
    [self myOrientation];
    
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag==1) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48) return NO; // 48 unichar for 0
            if (character > 57) return NO; // 57 unichar for 9
        }
        
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 4) return NO;
        return YES;
    }

    else if (textField.tag==2||textField==emrgncy)  {
        
        
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48) return NO; // 48 unichar for 0
            if (character > 57) return NO; // 57 unichar for 9
        }
        
        int length = [self getLength:textField.text];
        
        if(length == 10) {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3) {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@-",num];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6) {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    
           return YES;
   }

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

//-(int)PhoneId:(NSString *)str
//{
//   
//    int i=0;
//    if ([str isEqualToString:@"Home"]) {
//        i=1;
//    }
//    else if ([str isEqualToString:@"Other"])
//    {
//        i=2;
//    }
//    else if ([str isEqualToString:@"Work"])
//    {
//        i=3;
//    }
//    return i;
//    
//}
//-(int)ContactId:(NSString *)str
//{
//    int i=0;
//    if ([str isEqualToString:@"Phone"]) {
//        i=1;
//    }
//    else if ([str isEqualToString:@"Mobile"])
//    {
//        i=2;
//    }
//    else if ([str isEqualToString:@"Fax"])
//    {
//        i=3;
//    }
//    else if ([str isEqualToString:@"Email"])
//    {
//        i=4;
//    }
//   
//    return i;
//    
//}
//-(int)AppointReminder:(NSString *)str
//{
//    int i=1;
//    if ([str isEqualToString:@"None"]) {
//        i=1;
//    }
//    else if ([str isEqualToString:@"Voice Call"])
//    {
//        i=2;
//    }
//    else if ([str isEqualToString:@"Text Message"])
//    {
//        i=3;
//    }
//    else if ([str isEqualToString:@"Both"])
//    {
//        i=4;
//    }
//    return i;
//    
//}

-(NSString*)formatNumber:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    if(length > 10) {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    return mobileNumber;
}

-(int)getLength:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    return length;
}


-(void)addDropDown
{
    contactList=[[NSMutableArray alloc]init];
    contactTypeXMLList *contact=[[contactTypeXMLList alloc]init];
    contactList=[contact contactTypeList];
    
    contactTypeName=[contactList valueForKey:@"name"];
    contactTypeID=[contactList valueForKey:@"id"];
    
    contactCategoryXMLList=[[NSMutableArray alloc]init];
     contactCategoryList *contactCategoryTypesStatus=[[contactCategoryList alloc]init];
    contactCategoryXMLList=[contactCategoryTypesStatus contactCategoryTypeList];
    
    contactCategoryTypeName=[contactCategoryXMLList valueForKey:@"name"];
    contactCategorytypeID=[contactCategoryXMLList valueForKey:@"id"];
    
    
    appointmentreminderList=[[NSMutableArray alloc]init];
    AppointmentReminderXMLList *appointList=[[AppointmentReminderXMLList alloc]init];
   
    appointmentreminderList=[appointList AppointmentReminderTypeList];
    appointmentReminderTypeName=[appointmentreminderList valueForKey:@"name"];
    appointmentReminderTypeID=[appointmentreminderList valueForKey:@"id"];
    
}



@end
