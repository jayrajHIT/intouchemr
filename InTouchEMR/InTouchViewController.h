//
//  InTouchViewController.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 22/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface InTouchViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    UIImage *image;
    NSMutableDictionary *arr;
    UIImageView *footerView;
     UIDeviceOrientation orientation;
    NSMutableArray *requestData,*addressData,*contactData;
    UIAlertView *log;
    
}
@property (strong, nonatomic) IBOutlet UISwitch *port_newsletter;
@property (strong, nonatomic) IBOutlet UISwitch *port_appointment;
@property (strong, nonatomic) IBOutlet UISwitch *land_newsletter;
@property (strong, nonatomic) IBOutlet UISwitch *land_appointment;

@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UILabel *patientContact;
@property (strong, nonatomic) IBOutlet UILabel *patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *patientName;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;

@property (strong, nonatomic) IBOutlet UILabel *landscape_patientContact;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *landscape_ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscape_ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientName;
@property (strong, nonatomic) IBOutlet UIImageView *landscape_patientImage;
@property (strong, nonatomic) IBOutlet UIImageView *backImg;
@property (strong, nonatomic) IBOutlet UIImageView *landscapebackImg;

@property (strong, nonatomic) IBOutlet UILabel *port_callremaining;
@property (strong, nonatomic) IBOutlet UILabel *port_smsremaining;

@property (strong, nonatomic) IBOutlet UILabel *land_callremaining;
@property (strong, nonatomic) IBOutlet UILabel *land_smsremaining;


-(id)initwithArray:(NSMutableDictionary *)array;

-(IBAction)updatenewsletter:(id)sender;
-(IBAction)updateappointment:(id)sender;

@end
