//
//  PatientInformationViewController.h
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "LoginViewController.h"
@class LoginViewController;
@interface PatientInformationViewController : UIViewController<MFMailComposeViewControllerDelegate,UIAlertViewDelegate>
{
    NSMutableDictionary *PatientData;
    UIImageView *footerView;
    UIImage *img;
     NSString *patientFullname ;
    NSMutableDictionary *info;
    NSString *cpwd;
    LoginViewController *login;
    NSMutableArray *EthnicOriginList;
    NSArray *EthnicOriginName,*EthnicOriginID;
}
@property(strong ,nonatomic) NSMutableDictionary *PatientData;
@property (strong, nonatomic) IBOutlet UIImageView *Img_backgroung;
@property (strong, nonatomic) IBOutlet UIImageView *patient_img;
@property (strong, nonatomic) IBOutlet UILabel *patientName;
@property (strong, nonatomic) IBOutlet UILabel *patientEmailId;
@property (strong, nonatomic) IBOutlet UILabel *patientSSN;

@property (strong, nonatomic) IBOutlet UILabel *patientNatinality;


@property (strong, nonatomic) IBOutlet UILabel *lblSSN;
@property (strong, nonatomic) IBOutlet UIButton *btnIntake;
@property (strong, nonatomic) IBOutlet UIButton *btnEligibility;
@property (strong, nonatomic) IBOutlet UIButton *btnIntouch;
@property (strong,nonatomic)  IBOutlet UITabBarController *tabBar;


@property (strong,nonatomic)  IBOutlet UIButton *emailsend;

@property (strong, nonatomic) IBOutlet UILabel *patientDOB;
@property (strong, nonatomic) IBOutlet UILabel *lblDOB;

-(IBAction)sendemail:(id)sender;
- (IBAction)click_on_intake:(id)sender;
//- (IBAction)click_on_intouch:(id)sender;
- (IBAction)click_on_eligibility:(id)sender;
-(id)initwithPatientInformation:(NSDictionary *)dict ;
@end
