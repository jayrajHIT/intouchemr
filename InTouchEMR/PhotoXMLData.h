//
//  PhotoXMLData.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 20/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoXMLData : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)patientPhotoDataByXML:(NSString *)patientID registationNO:(NSString *)registationID;
@end
