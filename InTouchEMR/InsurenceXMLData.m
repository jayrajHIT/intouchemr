//
//  InsurenceXMLData.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 18/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "InsurenceXMLData.h"
#import "IndexAppDelegate.h"
@implementation InsurenceXMLData

-(NSMutableArray *)patientInsurenceDataByXML:(NSString *)patientID registationNO:(NSString *)registationID
{
  
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString *postString = [[NSString alloc]initWithFormat:@"%@/%@/%@",globalDele.insuranceUrl,patientID,registationID];
 
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
   
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
      //  NSLog(@"success");
    }
    else
    {
     //   NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"insuranceName"]||[myelement isEqualToString:@"subscriberID"]||[myelement isEqualToString:@"coPay"]||[myelement isEqualToString:@"group"]||[myelement isEqualToString:@"insuredPartyID"]||[myelement isEqualToString:@"id"]||[myelement isEqualToString:@"startDate"]||[myelement isEqualToString:@"endDate"]||[myelement isEqualToString:@"visitsAllowed"]||[myelement isEqualToString:@"additionalInformation"]||[myelement isEqualToString:@"contactNo"]||[myelement isEqualToString:@"subscriberMemberID"]||[myelement isEqualToString:@"subscriberFirstName"]||[myelement isEqualToString:@"subscriberLastName"]||[myelement isEqualToString:@"subscriberGender"]||[myelement isEqualToString:@"subscriberBirth"]||[myelement isEqualToString:@"insurancePolicyNo"]||[myelement isEqualToString:@"insurancePlanName"]||[myelement isEqualToString:@"insuranceID"])
            
            
            
            
            
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"PatientInsurance"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
