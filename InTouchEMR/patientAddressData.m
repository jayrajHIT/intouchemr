//
//  patientAddressData.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 18/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "patientAddressData.h"
#import "IndexAppDelegate.h"
@implementation patientAddressData


-(NSMutableArray *)patientAddressDataByXML:(NSString *)patientID registationNO:(NSString *)registationID
{
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString *postString = [[NSString alloc]initWithFormat:@"%@/%@/%@",globalDele.addressUrl,patientID,registationID];
    
      NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
     //   NSLog(@"success");
    }
    else
    {
      //  NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
    currentNodeContent=[NSMutableString stringWithString:@""];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"city"]||[myelement isEqualToString:@"state"]||[myelement isEqualToString:@"countryName"]||[myelement isEqualToString:@"contactNo"]||[myelement isEqualToString:@"postalCode"]||[myelement isEqualToString:@"addressTypeID"]||[myelement isEqualToString:@"id"]||[myelement isEqualToString:@"addressLine2"]||[myelement isEqualToString:@"isPrimary"]||[myelement isEqualToString:@"isHealthFusion"])
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
        if ([myelement isEqualToString:@"addressLine1"])
        {
            [currentNodeContent appendString:string];
        }
        
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([myelement isEqualToString:@"addressLine1"])
    {
        if(!dict)
        {
            dict=[[NSMutableDictionary alloc]init];
        }
        [dict setObject:currentNodeContent forKey:myelement];
    }
    
    if ([elementName isEqualToString:@"Address"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
