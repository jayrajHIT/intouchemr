//
//  PatientInformationViewController.m
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "PatientInformationViewController.h"
#import "IndexAppDelegate.h"
#import "TabBarView.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "InTouchViewController.h"
#import "SHKActivityIndicator.h"
#import "EligibilityView.h"
#import "EthnicOriginXMLData.h"
@interface PatientInformationViewController ()

@end

@implementation PatientInformationViewController
@synthesize PatientData,tabBar,btnEligibility,btnIntouch,btnIntake,patientName,patientNatinality,patientSSN,patientEmailId,Img_backgroung,patient_img,lblSSN,emailsend,lblDOB,patientDOB;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton=NO;
    [self.navigationController setNavigationBarHidden:NO];
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
   
    
    
//    CALayer *cellImageLayer = patient_img.layer;
//    [cellImageLayer setCornerRadius:67];
//    [cellImageLayer setMasksToBounds:YES];
    
    
    NSString *docName=[NSString stringWithFormat:@"%@",[PatientData objectForKey:@"imageName"]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
    [patient_img setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];

    NSString *patientfname=[PatientData objectForKey:@"firstName"];
    NSString *patientlname = [PatientData objectForKey:@"lastName"];
    
    if (patientfname != nil && patientfname.length > 0)
    {
        patientFullname = [NSString stringWithFormat:@"%@ ",patientfname];
    }
    if (patientlname != nil && patientlname.length > 0) {
        patientFullname = [patientFullname stringByAppendingString:patientlname];
        NSString *firstletter = [[patientFullname substringToIndex:1] capitalizedString];
        patientFullname = [patientFullname stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstletter];
       
    }

    
    self.patientName.text= patientFullname;
    if ([PatientData objectForKey:@"email"] != nil && [[PatientData objectForKey:@"email"]length] > 0) {
        self.patientEmailId.text=[PatientData objectForKey:@"email"];
    }
    else
    {
        self.patientEmailId.text=nil;
    }
    if ([PatientData objectForKey:@"birth"] != nil && [[PatientData objectForKey:@"birth"]length] > 0) {
       
             self.patientDOB.text=[self viewDate:[PatientData objectForKey:@"birth"]];
    }
    else
    {
        self.patientDOB.text=nil;
    }
    
    if ([PatientData objectForKey:@"ssn"] != nil && [[PatientData objectForKey:@"ssn"]length] > 0) {
        self.patientSSN.text=[PatientData objectForKey:@"ssn"];
    }
    else
    {
        self.patientSSN.text=nil;
    }
    if ([PatientData objectForKey:@"ethnicOriginName"] != nil && [[PatientData objectForKey:@"ethnicOriginName"]length] > 0) {
       
        self.patientNatinality.text=[PatientData objectForKey:@"ethnicOriginName"];
    }
    else
    {
        self.patientNatinality.text=nil;
    }
   
    
    IndexAppDelegate *appDelegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.patientId=[PatientData objectForKey:@"patientID"];
 
    [self doLayoutForOrientation:currentOrientation];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   
     
    
       self.navigationItem.title=@"Patient Information";
    
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc]
                                initWithTitle:@"Logout"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:@selector(logout:)];
    self.navigationItem.rightBarButtonItem=btnSave;
    UIBarButtonItem *barBtnItem = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Patient List"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(pop:)];
    
    self.navigationItem.leftBarButtonItem = barBtnItem;
    
       
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
  
        if(UIInterfaceOrientationIsPortrait(currentOrientation))
        {
            footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 910, 770, 50)];
            [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
            [self.view addSubview:footerView];
    
        }
        else{
            footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 650, 1024, 50)];
            [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
            [self.view addSubview:footerView];
        }
    
    [self doLayoutForOrientation:currentOrientation];
  
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
  cpwd  = [pref objectForKey:@"tempPass"];
 }
-(UIImage*)imageWithImage:(UIImage*)image
             scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)click_on_intake:(id)sender {
  
  
    [PatientData setObject:patientFullname forKey:@"FullName"];
    
   TabBarView *tabBarView=[[TabBarView alloc]initwithArray:PatientData];
    [self.navigationController pushViewController:tabBarView animated:YES];
   
}

-(id)initwithPatientInformation:(NSMutableDictionary *)dict
{
    PatientData=dict;
  [self addDropDown];
    return self;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self doLayoutForOrientation:toInterfaceOrientation];
}

-(void)doLayoutForOrientation:(UIInterfaceOrientation)orientation
{
    if(UIInterfaceOrientationIsPortrait(orientation))
    {
        [Img_backgroung setFrame:CGRectMake(6,81,780,635)];
        [patientName setFrame:CGRectMake(268,165,300,27)];
        [patientEmailId setFrame:CGRectMake(268,190,230,24)];
        [lblDOB setFrame:CGRectMake(268,215,60,25)];
        [patientDOB setFrame:CGRectMake(320,215,92,25)];
        [lblSSN setFrame:CGRectMake(268,240,60,25)];
        [patientSSN setFrame:CGRectMake(320,240,92,25)];
        
        [patientNatinality setFrame:CGRectMake(268, 270, 203, 21)];
        
        [emailsend setFrame:CGRectMake(559, 230, 105, 46)];
       
        [btnIntouch setFrame:CGRectMake(257,575,307,64)];
        [btnIntake setFrame:CGRectMake(257,412,307,64)];
        [btnEligibility setFrame:CGRectMake(257, 495, 307, 64)];
        [patient_img setFrame:CGRectMake(92,160,135,135)];
        [footerView setFrame:CGRectMake(0, 910, 770, 50)];
       
//        [phoneIcon setFrame:CGRectMake(550, 180, 32, 32)];
//        [lblphone setFrame:CGRectMake(600, 180, 127, 32)];
        
            
    } else {
      
        [footerView setFrame:CGRectMake(0, 660, 1024, 50)];
        [Img_backgroung setFrame:CGRectMake(113,12,780,635)];
        [patient_img setFrame:CGRectMake(184,84,135,135)];
        [patientName setFrame:CGRectMake(375,90,300,27)];
        [patientEmailId setFrame:CGRectMake(375,115,230,24)];
        [lblDOB setFrame:CGRectMake(376,140,54,25)];
        [patientDOB setFrame:CGRectMake(415,140,92,25)];
        [lblSSN setFrame:CGRectMake(376,165,54,25)];
        [patientSSN setFrame:CGRectMake(415,165,92,25)];
        [patientNatinality setFrame:CGRectMake(376, 195, 203, 21)];
        [emailsend setFrame:CGRectMake(650, 170, 107, 46)];
        [btnIntake setFrame:CGRectMake(364,320,307,64)];
        [btnEligibility setFrame:CGRectMake(364, 415, 307, 64)];
        [btnIntouch setFrame:CGRectMake(364,513,307,64)];
//        [phoneIcon setFrame:CGRectMake(650, 110, 35, 32)];
//        [lblphone setFrame:CGRectMake(705, 110, 127, 32)];
    }
}


- (IBAction)logout:(id)sender {
    
    
  //  [self.view removeFromSuperview];
    
    UIAlertView *log=[[UIAlertView alloc]initWithTitle:@"Do you really want to log out?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [log setTag:2];
    [log show];
    
}

- (IBAction)pop:(id)sender {
   
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please enter your password "
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Submit", nil];
    
    [message setAlertViewStyle:UIAlertViewStyleSecureTextInput];
    [message setTag:1];
    
    [message show];
    
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
   BOOL flag=YES;
    if (alertView.tag==1) {

    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if( [inputText length] >= cpwd.length )
    {
        flag=YES;
    }
        else
        {
            flag=NO;
        }
    }
        return flag;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1) {
        
    
    switch (buttonIndex) {
        case 1:
        {
            NSString *inputText = [[alertView textFieldAtIndex:0] text];
            if ([inputText isEqualToString:cpwd]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Incorrect password"
                                                                  message:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil, nil];
                [message show];
            }
            break;
        }
        default:
            break;
    }
    }
    else
    {
        switch (buttonIndex) {
            case 1:
            {
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
            }
                
            default:
                break;
        }

    }
    
    }
-(IBAction)sendemail:(id)sender
{
    
    if ([MFMailComposeViewController canSendMail]) {
        if (patientEmailId.text != nil && patientEmailId.text.length > 0) {
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        NSString *emailTitle = @"";
        NSString *messageBody = @"";
        NSArray *toRecipents =[NSArray arrayWithObject:patientEmailId.text];
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
    
       [self presentViewController:mc animated:YES completion:nil];
        }
        else{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Patient email id not found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Before sending email, please configure your default email account under settings." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
 
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
         
                      break;
        case MFMailComposeResultSaved:
           
            [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail has been saved successfully!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            break;
        case MFMailComposeResultSent:
           
           [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail has been sent successfully!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
            break;
            
        case MFMailComposeResultFailed:
           [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail sent failure" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
         
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)click_on_intouch:(id)sender
{
    NSThread *actThread=[[NSThread alloc]initWithTarget:self selector:@selector(threadMethod) object:nil];
    [actThread start];
    

    [PatientData setObject:patientFullname forKey:@"FullName"];
    
    InTouchViewController *inTouchView=[[InTouchViewController alloc]initwithArray:PatientData];
    [self.navigationController pushViewController:inTouchView animated:YES];
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}
- (IBAction)click_on_eligibility:(id)sender
{
       
  
    NSThread *actThread=[[NSThread alloc]initWithTarget:self selector:@selector(threadMethod) object:nil];
    [actThread start];

    [PatientData setObject:patientFullname forKey:@"FullName"];
    
    EligibilityView *eligibility=[[EligibilityView alloc]initwithArray:PatientData];
    [self.navigationController pushViewController:eligibility animated:YES];
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}

-(void)threadMethod
{
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
}

-(NSString *)viewDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSString *result = [formater stringFromDate:date2];
   
    return result;
    
}

-(void)addDropDown
{
    EthnicOriginList=[[NSMutableArray alloc]init];
    EthnicOriginXMLData *ethnicoriginList=[[EthnicOriginXMLData alloc]init];
    EthnicOriginList=[ethnicoriginList EthnicOriginStatusListData];
    EthnicOriginName=[EthnicOriginList valueForKey:@"ethnicOriginName"];
    EthnicOriginID=[EthnicOriginList valueForKey:@"id"];
  
}



@end
