//
//  HippaViewController.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 31/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "HippaViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "CompleteViewController.h"
#import "hipaaTextXML.h"
#import "IndexAppDelegate.h"
#import "SHKActivityIndicator.h"

@interface HippaViewController ()

@end

@implementation HippaViewController
@synthesize image,land_image,portraitView,landscapeView,webView1,webView2,signHere,landscapesignHere;
@synthesize s3,port_agree,land_agree;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    mouseMoved = 0;
    image.layer.borderWidth=2.0;
    image.layer.borderColor=[UIColor blackColor].CGColor;
    land_image.layer.borderWidth=2.0;
    land_image.layer.borderColor=[UIColor blackColor].CGColor;
    checkboxSelected=NO;
    isagree=[NSString stringWithFormat:@"0"];
    self.navigationItem.title=@"Please Review This HIPAA Agreement";
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
    
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
  
    
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    [port_agree setUserInteractionEnabled:NO];
    [land_agree setUserInteractionEnabled:NO];
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
    [self myOrientation];

     [NSThread detachNewThreadSelector:@selector(clientCreate) toTarget:self withObject:nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)clientCreate
{
    ACCESS_KEY_ID=[[NSBundle mainBundle]objectForInfoDictionaryKey:@"ACCESS_KEY_ID"];
    SECRET_KEY=[[NSBundle mainBundle]objectForInfoDictionaryKey:@"SECRET_KEY"];
    PICTURE_BUCKET=[[NSBundle mainBundle]objectForInfoDictionaryKey:@"PICTURE_BUCKET"];
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSWest2 identityPoolId:@"YourIdentityPoolId"];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSWest2 credentialsProvider:credentialsProvider];
    
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
}



-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    
    
}
-(void)myOrientation
{
    [self performSelectorInBackground:@selector(mythread)
                           withObject:nil];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
        [self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        [self orientationChange];

        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        [self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        [self orientationChange];
       
    }
    
}

-(void)mythread
{
    if (!hippatext) {
        hipaaTextXML *hipaaTextFile=[[hipaaTextXML alloc]init];
        hippatext=[hipaaTextFile getHipaaTextXML];
        }
    
    [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
}
-(void)afterthread
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
       
        [self loadDocument:webView1];
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        [self loadDocument:webView2];
    }
    
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
    
}


-(void) clearCurrentView {
    
    
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
        
        
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
        
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    mouseSwiped = NO;
    UITouch *touch = [touches anyObject];
    [signHere setHidden:YES];
    [landscapesignHere setHidden:YES];
//    if ([touch tapCount] == 2) {
//        image.image = nil;
//        land_image.image=nil;
//        return;
//    }
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
        point = [touch locationInView:land_image];
      //  point.y -= 20;
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        point = [touch locationInView:image];
      //  point.y -= 20;
    }
    

   
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    mouseSwiped = YES;
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint;
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        currentPoint = [touch locationInView:land_image];
      //  currentPoint.y -= 20;
        UIGraphicsBeginImageContext(land_image.frame.size);
        [land_image.image drawInRect:CGRectMake(0, 0, land_image.frame.size.width,
                                           land_image.frame.size.height)];
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
       currentPoint = [touch locationInView:image];
        
        UIGraphicsBeginImageContext(image.frame.size);
        [image.image drawInRect:CGRectMake(0, 0, image.frame.size.width,
                                           image.frame.size.height)];
    }

   
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(),1.5);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), point.x, point.y);
   CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
       land_image.image = UIGraphicsGetImageFromCurrentImageContext();
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
       image.image = UIGraphicsGetImageFromCurrentImageContext();
    }
    
    UIGraphicsEndImageContext();
    point = currentPoint;
    mouseMoved++;
    if (mouseMoved == 10) {
        mouseMoved = 0;
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
 
    if(!mouseSwiped) {
        
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
            UIGraphicsBeginImageContext(land_image.frame.size);
            [land_image.image drawInRect:CGRectMake(0, 0, land_image.frame.size.width,
                                               land_image.frame.size.height)];
            
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
            
            UIGraphicsBeginImageContext(image.frame.size);
            [image.image drawInRect:CGRectMake(0, 0, image.frame.size.width,
                                               image.frame.size.height)];
        }

        
       
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 1.5);
       // CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 0.0, 0.0, 1.0);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(),0,0,0,1.0);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), point.x, point.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), point.x, point.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
            land_image.image = UIGraphicsGetImageFromCurrentImageContext();
            
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
            
             image.image = UIGraphicsGetImageFromCurrentImageContext();
        }
       
        UIGraphicsEndImageContext();
    }
}
-(void)orientationChange
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
       land_image.image=image.image;
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        image.image=land_image.image;
    }

    
    
}


-(void)loadDocument:(UIWebView*)webView
{
    

    [webView loadHTMLString:hippatext baseURL:nil];
    [port_agree setUserInteractionEnabled:YES];
    [land_agree setUserInteractionEnabled:YES];
    
}



- (IBAction)agree:(id)sender {
   
    
    [self signUpload];
    
    
}
-(void)signUpload
{
    UIImage *signImg=[[UIImage alloc]init];
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
        signImg=land_image.image;
        
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        signImg=image.image;
    }
    
    CGImageRef cgref = [signImg CGImage];
    CIImage *cim = [signImg CIImage];
    
    if (cim == nil && cgref == NULL)
    {
             if ([isagree isEqualToString:@"0"]) {
           [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading..."];
        [self imageSave];
       }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"If you want to agree Hipaa agreement so please sign in box" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
            
    }
    else
    {
       if ([isagree isEqualToString:@"1"]) {
           
        [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading..."];
        imageData = UIImageJPEGRepresentation(signImg, 1.0);
        [self processBackgroundThreadUpload:imageData];
    }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"If you want to agree Hipaa agreement so please click on check box" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }
}
- (IBAction)clear:(id)sender {
    image.image = nil;
    land_image.image=nil;
    [signHere setHidden:NO];
    [landscapesignHere setHidden:NO];
}


- (IBAction)checkboxButton:(id)sender{
	if (checkboxSelected == 0){
		[checkboxButton setSelected:YES];
        [landscapecheckboxButton setSelected:YES];
		checkboxSelected = 1;
        isagree=@"1";
	} else {
		[checkboxButton setSelected:NO];
        [landscapecheckboxButton setSelected:NO];
		checkboxSelected = 0;
        isagree=@"0";
	}
}


- (void)processBackgroundThreadUpload:(NSData *)imagepatientData
{
    [self performSelectorInBackground:@selector(processBackgroundThreadUploadInBackground:)
                           withObject:imagepatientData];
}

- (void)processBackgroundThreadUploadInBackground:(NSData *)imagepatientData
{
   
    
    NSDate *date=[NSDate date];
    NSDateFormatter *form=[[NSDateFormatter alloc]init];
    [form setDateFormat:@"ddMMHHmmss"];
    NSString *currentDate = [form stringFromDate:date];
    IndexAppDelegate *globleView=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
   // sign_Name=[sign_Name stringByAppendingString:currentDate];
    sign_Name = [NSString stringWithFormat:@"%@%@.png",globleView.patientId,currentDate];
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = PICTURE_BUCKET;
    uploadRequest.key = sign_Name;
    uploadRequest.contentType = @"image/png";
    uploadRequest.serverSideEncryption = AWSS3ServerSideEncryptionAES256;
    uploadRequest.body = [NSURL URLWithString:sign_Name];
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        [self showCheckErrorMessage:task.error];
        return nil;
    }];
}

- (void)showCheckErrorMessage:(NSError *)error
{
    if(error != nil)
    {
            [self showAlertMessage:[error.userInfo objectForKey:@"message"] withTitle:@"Upload Error"];
    }
    else
    {
        
        [self imageSave];
        
    }
    
}

- (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}


-(void)imageSave
{
    NSURL *url;
    NSData *postData;
    NSString *xmlText;
    
    IndexAppDelegate *globleView=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSDate *date=[NSDate date];
    NSDateFormatter *form=[[NSDateFormatter alloc]init];
    [form setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDate = [form stringFromDate:date];
        url = [NSURL URLWithString:globleView.hipaaSavedURL];
    
   
    
    if (sign_Name) {
     
    xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Patient><patientID>%@</patientID><hippaaAgreementSigned>%@</hippaaAgreementSigned><signatureURL>%@</signatureURL><signatureDateTime>%@</signatureDateTime></Patient>",globleView.patientId,isagree,sign_Name,currentDate];
    }
    else
    {
      xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Patient><patientID>%@</patientID><hippaaAgreementSigned>%@</hippaaAgreementSigned></Patient>",globleView.patientId,isagree];   
    }
            postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        [request setURL:url];
        
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLResponse* response;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
      
    if ([responseString isEqualToString:@"HIPAA Agreement Saved"]||[responseString isEqualToString:@"success"]) {
        CompleteViewController *lastView=[[CompleteViewController alloc]init];
        [self.navigationController pushViewController:lastView animated:YES];
    }
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
       
    }
    

@end
