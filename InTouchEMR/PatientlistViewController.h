//
//  PatientlistViewController.h
//  InTouchEMR
//
//  Created by DSC on 12/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatientlistCustomCell.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>

@interface PatientlistViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSMutableArray *arr,*arr1;
    PatientlistCustomCell *cell;
     UIImageView *footerView;
    NSArray *imgarr;
    AWSS3 *s3;
    NSString *ACCESS_KEY_ID;
    NSString *SECRET_KEY;
    NSString *PICTURE_BUCKET;
    NSOperationQueue *imgqueue;
    UIBarButtonItem *btnNext;
    UIButton *btnNext1;
    
}
//@property (strong,nonatomic) PatientlistCustomCell *cell;
@property (strong, nonatomic) IBOutlet UITableView *patientList;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (nonatomic, assign) bool isFiltered;
@property (nonatomic, retain) AWSS3 *s3;
    @end
