//
//  NSMutableString+xmlForm.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 22/08/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableString (xmlForm)
- (NSMutableString *)xmlSimpleUnescape;
- (NSMutableString *)xmlSimpleEscape;
@end
