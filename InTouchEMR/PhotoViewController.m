//
//  PhotoViewController.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 27/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "PhotoViewController.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import "SHKActivityIndicator.h"
#import "AFURLConnectionOperation.h"
#import "AFHTTPRequestOperation.h"
#import "AFDownloadRequestOperation.h"
#import "documentTypeList.h"

@interface PhotoViewController ()

@end

@implementation PhotoViewController
@synthesize myImage,DocumentName,DocumentType,date,progress,loading;
@synthesize landscapedate,landscapeDocumentName,landscapeDocumentType,landscapemyImage,landscapeView,portraitView,webView1,webView2,landscapeprogress,flag,land_loading,land_loadingText,loadingText;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initwithImageName:(NSDictionary *)img 
{
    imgdata=img;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self addDropDown];
    globleData=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
  
   self.navigationItem.title=@"My Upload";
   
    
    if ([imgdata objectForKey:@"contentType"]) {
        contentType=[imgdata objectForKey:@"contentType"];
        [loading setHidden:YES];

    NSArray *contentArray =[contentType componentsSeparatedByString:@"/"];
    contentType =[contentArray objectAtIndex:0];
  
    }
    
    DocumentName.text=[imgdata objectForKey:@"documentName"];
   
    if ([imgdata objectForKey:@"documentTypeID"]) {
        int a=[DocumentTypeID indexOfObject:[imgdata objectForKey:@"documentTypeID"]];
        DocumentType.text=[documentTypeName objectAtIndex:a];
    }
    
    date.text=[imgdata objectForKey:@"receiptDate"];
    
    UIBarButtonItem *barBtnItem = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Back"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(pop:)];
    
    self.navigationItem.leftBarButtonItem = barBtnItem;
   
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
      if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }

    [self myOrientation];
      
}
- (IBAction)pop:(id)sender {

    [operation cancel];
    globleData.flag =NO;
    [self.navigationController popViewControllerAnimated:YES];
    
   
}

-(void)orientationChanged:(NSNotification *)notification{
    
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown)  {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    // Do your orientation logic here
	
}


-(void)myOrientation
{

    
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        landscapeDocumentName.text=[imgdata objectForKey:@"documentName"];
       
        if ([imgdata objectForKey:@"documentTypeID"]) {
            int a=[DocumentTypeID indexOfObject:[imgdata objectForKey:@"documentTypeID"]];
            landscapeDocumentType.text=[documentTypeName objectAtIndex:a];
            
          
        }
        
        landscapedate.text=[self viewDate:[imgdata objectForKey:@"receiptDate"]];
        [self performSelectorInBackground:@selector(mythread)
                               withObject:nil];
        
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        DocumentName.text=[imgdata objectForKey:@"documentName"];
       
        if ([imgdata objectForKey:@"documentTypeID"]) {
            int a=[DocumentTypeID indexOfObject:[imgdata objectForKey:@"documentTypeID"]];
            DocumentType.text=[documentTypeName objectAtIndex:a];
        }
       
        date.text=[self viewDate:[imgdata objectForKey:@"receiptDate"]];

        [self performSelectorInBackground:@selector(mythread)
                               withObject:nil];
    }
}

-(void)mythread
{
    [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
}

-(void)afterthread
{
      
     NSString *docName=[NSString stringWithFormat:@"%@",[imgdata objectForKey:@"documentUrl"]];
   
    
    if ([contentType isEqualToString:@"image"]) {
        
        [myImage setHidden:NO];
        [landscapemyImage setHidden:NO];
        [webView1 setHidden:YES];
        [webView2 setHidden:YES];
        
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {

            [self loadImage:docName inView:landscapemyImage];
            
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {

            [self loadImage:docName inView:myImage];
        }
         
      }
    else
    {
        [webView1 setHidden:NO];
        [webView2 setHidden:NO];
        [myImage setHidden:YES];
        [landscapemyImage setHidden:YES];
        
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
            [self loadDocument:docName inView:webView2];
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
             [self loadDocument:docName inView:webView1];
         
        }
        
     }
 }

-(void) clearCurrentView {
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}

-(void)loadDocument:(NSString*)documentName inView:(UIWebView*)webView
{
    
 
    NSString *stringPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"/File"];

    NSError *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:stringPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:stringPath withIntermediateDirectories:NO attributes:nil error:&error];
    stringPath = [stringPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",documentName]];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:stringPath])
    {
        
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {[landscapeprogress setHidden:NO];
            if (globleData.flag==NO) {
                landscapeprogress.progress=0.0;
            }
            
            
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
            
            [progress setHidden:NO];
            if (globleData.flag==NO) {
                progress.progress = 0.0;
            }
            [loading setHidden:NO];
            [land_loading setHidden:NO];
            
            [land_loadingText setHidden:NO];
            [loadingText setHidden:NO];
            
        }
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",documentName]];
        
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        if (globleData.flag==NO) {
            
            
            operation = [[AFDownloadRequestOperation alloc] initWithRequest:request targetPath:stringPath shouldResume:YES];
            
            __weak typeof(self) weakSelf = self;
            __weak typeof(globleData) weakgloble= globleData;
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [weakSelf.progress setHidden:YES];
                [weakSelf.landscapeprogress setHidden:YES];
                [weakSelf.loading setHidden:YES];
                [weakSelf.land_loading setHidden:YES];
                
                [weakSelf.land_loadingText setHidden:YES];
                [weakSelf.loadingText setHidden:YES];
                
                NSURL *targetURL = [NSURL fileURLWithPath:stringPath];
                 NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
                [webView loadRequest:request];
                
                [weakSelf.webView1 loadRequest:request];
                [weakSelf.webView2 loadRequest:request];
                
                weakgloble.flag=NO;
                
               
                
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in fetching document" delegate:weakSelf cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                [weakSelf.progress setHidden:YES];
                [weakSelf.landscapeprogress setHidden:YES];
                [weakSelf.loading setHidden:YES];
                [weakSelf.land_loading setHidden:YES];
                
                [weakSelf.land_loadingText setHidden:YES];
                [weakSelf.loadingText setHidden:YES];
              
                
//                [imageView setImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
                
                weakgloble.flag=NO;
                
                
            }];
            
        }
        
        __weak typeof(self) weakSelf = self;
        __weak typeof(globleData) weakgloble= globleData;
        [operation setProgressiveDownloadProgressBlock:^(NSInteger bytesRead, long long totalBytesRead, long long totalBytesExpected, long long totalBytesReadForFile, long long totalBytesExpectedToReadForFile) {
            float percentDone = totalBytesReadForFile/(float)totalBytesExpectedToReadForFile;
            
       
            
            weakSelf.loadingText.text = [NSString stringWithFormat:@"%.0f%%",percentDone*100];
            weakSelf.land_loadingText.text = [NSString stringWithFormat:@"%.0f%%",percentDone*100];
            
            weakgloble.flag=YES;
            
            weakSelf.landscapeprogress.progress = (float)totalBytesRead / totalBytesExpected;
            
            weakSelf.progress.progress = (float)totalBytesRead / totalBytesExpected;
            
            
            
            
            
        }];
        
              if (globleData.flag==NO) {
            [operation start];
            
        }
        
    }
    
    else
    {
        [progress setHidden:YES];
        [landscapeprogress setHidden:YES];
        [loading setHidden:YES];
        [land_loading setHidden:YES];
        [land_loadingText setHidden:YES];
        [loadingText setHidden:YES];
        
        NSURL *targetURL = [NSURL fileURLWithPath:stringPath];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [webView loadRequest:request];
        
       
    }
    
    
    
}

    
 
   



-(void)loadImage:(NSString*)imageName inView:(UIImageView*)imageView
{
  
    
    NSString *stringPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"/images"];
      NSError *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:stringPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:stringPath withIntermediateDirectories:NO attributes:nil error:&error];
    stringPath = [stringPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",imageName]];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:stringPath])
    {
   
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {[landscapeprogress setHidden:NO];
            if (globleData.flag==NO) {
                landscapeprogress.progress=0.0;
            }
            
            
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
            
            [progress setHidden:NO];
            if (globleData.flag==NO) {
                progress.progress = 0.0;
            }
            [loading setHidden:NO];
            [land_loading setHidden:NO];
            
            [land_loadingText setHidden:NO];
            [loadingText setHidden:NO];
            
        }
      
           
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",imageName]];
        
              
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
     
        if (globleData.flag==NO) {
            
        
        operation = [[AFDownloadRequestOperation alloc] initWithRequest:request targetPath:stringPath shouldResume:YES];
        
        __weak typeof(self) weakSelf = self;
        __weak typeof(globleData) weakgloble= globleData;
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [weakSelf.progress setHidden:YES];
            [weakSelf.landscapeprogress setHidden:YES];
            [weakSelf.loading setHidden:YES];
            [weakSelf.land_loading setHidden:YES];
           
            [weakSelf.land_loadingText setHidden:YES];
            [weakSelf.loadingText setHidden:YES];
            
            NSURL *targetURL = [NSURL fileURLWithPath:stringPath];
            
            NSData *data1=[NSData dataWithContentsOfURL:targetURL];
            
            [imageView setImage:[UIImage imageWithData:data1]];
            [weakSelf.myImage setImage:[UIImage imageWithData:data1]];
            [weakSelf.landscapemyImage setImage:[UIImage imageWithData:data1]];
            
           weakgloble.flag=NO;
            
       
            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in fetching Image" delegate:weakSelf cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            [weakSelf.progress setHidden:YES];
            [weakSelf.landscapeprogress setHidden:YES];
            [weakSelf.loading setHidden:YES];
            [weakSelf.land_loading setHidden:YES];
            
            [weakSelf.land_loadingText setHidden:YES];
            [weakSelf.loadingText setHidden:YES];
          
            
            [imageView setImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
           
            weakgloble.flag=NO;
            
            
        }];
            
        }
        
        __weak typeof(self) weakSelf = self;
        __weak typeof(globleData) weakgloble= globleData;
        [operation setProgressiveDownloadProgressBlock:^(NSInteger bytesRead, long long totalBytesRead, long long totalBytesExpected, long long totalBytesReadForFile, long long totalBytesExpectedToReadForFile) {
            float percentDone = totalBytesReadForFile/(float)totalBytesExpectedToReadForFile;
           
            
            weakSelf.loadingText.text = [NSString stringWithFormat:@"%.0f%%",percentDone*100];
            weakSelf.land_loadingText.text = [NSString stringWithFormat:@"%.0f%%",percentDone*100];
            
            weakgloble.flag=YES;
           
            
               weakSelf.landscapeprogress.progress = (float)totalBytesRead / totalBytesExpected;
                               
                 weakSelf.progress.progress = (float)totalBytesRead / totalBytesExpected;
          
            
            
           
            
        }];
     
            if (globleData.flag==NO) {
            [operation start];
           
        }
        
    }
    
    else
    {
        [progress setHidden:YES];
        [landscapeprogress setHidden:YES];
        [loading setHidden:YES];
        [land_loading setHidden:YES];
        [land_loadingText setHidden:YES];
        [loadingText setHidden:YES];
        
        NSURL *targetURL = [NSURL fileURLWithPath:stringPath];
        
        NSData *data1=[NSData dataWithContentsOfURL:targetURL];
        
        [imageView setImage:[UIImage imageWithData:data1]];
        
          }
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)viewDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSString *result = [formater stringFromDate:date2];
  
    return result;
    
}



-(void)addDropDown
{
    DocumentTypeData=[[NSMutableArray alloc]init];
    documentTypeList *documentList=[[documentTypeList alloc]init];
    DocumentTypeData=[documentList DocumentTypeListData];
    
    NSDictionary *abc=[[NSDictionary alloc]initWithObjectsAndKeys:@"1",@"id",@"Profile Image",@"name", nil];
    [DocumentTypeData insertObject:abc atIndex:0];
    
    documentTypeName=[DocumentTypeData valueForKey:@"name"];
    DocumentTypeID=[DocumentTypeData valueForKey:@"id"];
  
    
}





@end
