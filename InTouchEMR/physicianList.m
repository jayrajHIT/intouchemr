//
//  physicianList.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 08/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "physicianList.h"
#import "IndexAppDelegate.h"

@implementation physicianList
-(NSMutableArray *)physicianListXML:(NSString *)registationID
{
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString *postUrl=[NSString stringWithFormat:@"%@%@",globalDele.physicianList,registationID];
   
    NSURL *url = [NSURL URLWithString:postUrl];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
      //  NSLog(@"success");
    }
    else
    {
      //  NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"physicianName"]||[myelement isEqualToString:@"id"])
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Physician"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        NSLog(@"%@",myelement);
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
