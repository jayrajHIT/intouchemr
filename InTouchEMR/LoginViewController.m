//
//  LoginViewController.m
//  InTouchEMR
//
//  Created by DSC on 12/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "LoginViewController.h"
#import "IndexAppDelegate.h"
#import "PatientlistViewController.h"
#import "SHKActivityIndicator.h"
#import "SVWebViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize usernameTextField,passwordTextField,rememberSwitch,pref,lblUsername,lblNotaMember,lblPassword,lblRememberMe,loginMainBg,loginMainHeading,imgViewBorder,loginbutton,buttontryit,activityIndicator,LogoImg;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
    
   [self.navigationController setNavigationBarHidden:YES];
     self.navigationController.navigationBar.translucent = NO;
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
    
    passwordTextField.secureTextEntry = YES;
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        
        
       footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-70, 770, 50)];
       
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.view addSubview:footerView];
      
    }
    else{
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-70, 1024, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.view addSubview:footerView];
    }
    [rememberSwitch setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [rememberSwitch setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    [LogoImg setImage:[UIImage imageNamed:@"logo_intouchemr.jpeg"]];
       [self doLayoutForOrientation:currentOrientation];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES];

    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
   
    //    if(UIInterfaceOrientationIsPortrait(currentOrientation))
//    {
//        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 954, 770, 50)];
//        
//        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
//        [self.view addSubview:footerView];
//        
//    }
//    else{
//        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 698, 1024, 50)];
//        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
//        [self.view addSubview:footerView];
//    }
    [self doLayoutForOrientation:currentOrientation];


    pref = [NSUserDefaults standardUserDefaults];
    
    NSString *clogin = [pref objectForKey:@"clogin"];
    NSString *cpwd  = [pref objectForKey:@"cpwd"];
    
    usernameTextField.text = clogin;
    passwordTextField.text = cpwd;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)LoginButton:(id)sender {
       
    if (self.usernameTextField.text == NULL || self.usernameTextField.text.length == 0
        || self.passwordTextField.text ==NULL || self.passwordTextField.text.length == 0)
    {
        UIAlertView *notice = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please enter a valid username and password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [notice show];
        
    }
    else {
        NSThread *actThread=[[NSThread alloc]initWithTarget:self selector:@selector(threadMethod) object:nil];
        [actThread start];

        NSString *username=[usernameTextField text];
        NSString *password=[passwordTextField text];
         pref =[NSUserDefaults standardUserDefaults];
        [pref setObject:password forKey:@"tempPass"];
        if([rememberSwitch isOn])
        {
            
            [pref setObject:username forKey:@"clogin"];
            [pref setObject:password forKey:@"cpwd"];
        }
        else {
            [pref setObject:@"" forKey:@"clogin"];
            [pref setObject:@"" forKey:@"cpwd"];
        }
        IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        NSString *postString = [[NSString alloc]initWithFormat:@"%@%@/%@", globalDele.loginUrl,username, password];
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
        NSURLResponse *response;
        NSError *error;
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
        temp = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
       
        if ([temp isEqualToString:@"InValid Username or Password."]||temp==NULL){
            
            UIAlertView *notice = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please enter a valid username and password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
            [notice show];
        } else {
            
                     NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
            xmlparsing.delegate=self;
            if ([xmlparsing parse]) {                
                NSMutableDictionary *tempdict=[arr objectAtIndex:0];
                IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
                appDele.userID=[tempdict objectForKey:@"userID"];
                appDele.registationID=[tempdict objectForKey:@"registrationID"];
                appDele.username=[tempdict objectForKey:@"username"];
                appDele.fullname=[tempdict objectForKey:@"fullName"];
                appDele.email=[tempdict objectForKey:@"email"];
                appDele.isExternal=[tempdict objectForKey:@"isExternal"];
                appDele.appointmentStatus=[tempdict objectForKey:@"appointmentServiceSelected"];
                appDele.newsletterStatus=[tempdict objectForKey:@"therapyNewsLetterSelected"];
       
                arr=nil;
PatientlistViewController *patientlistView =[[PatientlistViewController alloc]initWithNibName:@"PatientlistViewController" bundle:nil];
           
                
                [self.navigationController pushViewController:patientlistView animated:YES];
           
               
                
                
            }
            else{
                UIAlertView *notice = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please re-enter credentials and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil,nil];
                [notice show];
                
            }
        }
       [[SHKActivityIndicator currentIndicator]hideAfterDelay];
 
        usernameTextField.text=@"";
        passwordTextField.text=@"";
      
    }

    }

-(void)threadMethod
{
      [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"userID"]||[myelement isEqualToString:@"fullName"]||[myelement isEqualToString:@"registrationID"]||[myelement isEqualToString:@"username"]||[myelement isEqualToString:@"email"]||[myelement isEqualToString:@"isExternal"]||[myelement isEqualToString:@"appointmentServiceSelected"]||[myelement isEqualToString:@"therapyNewsLetterSelected"]) {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"LoginUser"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
}

- (IBAction)signUp:(id)sender {
	NSURL *URL = [NSURL URLWithString:@"http://www.intouchemr.com/ipad"];
	SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:URL];
	webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    webViewController.availableActions = SVWebViewControllerAvailableActionsOpenInSafari | SVWebViewControllerAvailableActionsOpenInChrome | SVWebViewControllerAvailableActionsCopyLink | SVWebViewControllerAvailableActionsMailLink;
	
    [self presentViewController:webViewController animated:YES completion:nil];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self doLayoutForOrientation:toInterfaceOrientation];
}

-(void)doLayoutForOrientation:(UIInterfaceOrientation)orientation
{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    int ver = [version intValue];
    
    if(UIInterfaceOrientationIsPortrait(orientation))
    {
      
        if (ver < 7){
            //iOS 6 work
             [footerView setFrame:CGRectMake(0, 954, 770, 50)];
        }
        else{
            //iOS 7 related work
             [footerView setFrame:CGRectMake(0, 974, 770, 50)];
        }
        
        
        
       
        [LogoImg setFrame:CGRectMake(200, 10, 400,180)];
        [loginMainBg setFrame:CGRectMake(111, 207, 586, 473)];
//        [imgViewBorder setFrame:CGRectMake(171, 503, 466, 86)];
        [loginMainHeading setFrame:CGRectMake(293, 250, 271, 51)];
        [lblUsername setFrame:CGRectMake(179, 346, 83, 23)];
        [lblPassword setFrame:CGRectMake(178, 391, 74, 21)];
        [lblRememberMe setFrame:CGRectMake(179, 444, 110, 21)];
//        [lblNotaMember setFrame:CGRectMake(203, 529, 163, 34)];
        [loginbutton setFrame:CGRectMake(308, 500, 180, 44)];
//        [buttontryit setFrame:CGRectMake(415, 525, 159, 44)];
        [usernameTextField setFrame:CGRectMake(308, 343, 266, 30)];
        [passwordTextField setFrame:CGRectMake(308, 389, 266, 30)];
        [rememberSwitch setFrame:CGRectMake(308, 443, 79, 27)];
        
        
    } else {
        if (ver < 7){
            //iOS 6 work
            [footerView setFrame:CGRectMake(0, 698, 1024, 50)];
        }
        else{
            //iOS 7 related work
          [footerView setFrame:CGRectMake(0, 718, 1024, 50)];
        }

        
        
        [LogoImg setFrame:CGRectMake(300, 0, 400, 135)];
        [loginMainBg setFrame:CGRectMake(220, 138, 586, 473)];
//        [imgViewBorder setFrame:CGRectMake(280, 434, 466, 86)];
        [loginMainHeading setFrame:CGRectMake(402, 181, 271, 51)];
        [lblUsername setFrame:CGRectMake(288, 277, 83, 23)];
        [lblPassword setFrame:CGRectMake(286, 322, 74, 21)];
        [lblRememberMe setFrame:CGRectMake(288, 375, 110, 21)];
//        [lblNotaMember setFrame:CGRectMake(312, 460, 163, 34)];
        [loginbutton setFrame:CGRectMake(417, 430, 180, 44)];
//        [buttontryit setFrame:CGRectMake(524, 456, 159, 44)];
        [usernameTextField setFrame:CGRectMake(417, 274, 266, 30)];
        [passwordTextField setFrame:CGRectMake(417, 320, 266, 30)];
        [rememberSwitch setFrame:CGRectMake(417, 374, 79, 27)];
    }
}
@end
