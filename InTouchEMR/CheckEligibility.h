//
//  CheckEligibility.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface CheckEligibility : UIViewController<MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSString *insuranceID,*eligibilityID,*eligibilitypayerName,*eligibilityServiceID,*eligibilitypayerID;
    UIImage *image;
    NSMutableDictionary *arr;
    NSMutableArray * arr1;
    NSArray *temp;
    UIImageView *footerView;
    UIDeviceOrientation orientation;
    UIAlertView *av;
}
@property (nonatomic, assign) BOOL IsDependent;
@property (strong, nonatomic) IBOutlet UIScrollView *sc1;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UIView *port_errorView;
@property (strong, nonatomic) IBOutlet UIView *land_errorView;
@property (strong, nonatomic) IBOutlet UIView *port_sub_errorView;
@property (strong, nonatomic) IBOutlet UIView *land_sub_errorView;
@property (strong, nonatomic) IBOutlet UILabel *patientContact;
@property (strong, nonatomic) IBOutlet UILabel *patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *patientName;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientContact;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *landscape_ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscape_patientName;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;
@property (strong, nonatomic) IBOutlet UIImageView *landscape_patientImage;


@property (strong, nonatomic) IBOutlet UILabel *port_payerName;
@property (strong, nonatomic) IBOutlet UILabel *port_patientName;
@property (strong, nonatomic) IBOutlet UILabel *port_ssn;
@property (strong, nonatomic) IBOutlet UILabel *port_memberId;
@property (strong, nonatomic) IBOutlet UILabel *port_groupNo;
@property (strong, nonatomic) IBOutlet UILabel *port_dateofbirth;
@property (strong, nonatomic) IBOutlet UITextView *port_adress;
@property (strong, nonatomic) IBOutlet UILabel *port_groupName;
@property (strong, nonatomic) IBOutlet UILabel *port_gender;
@property (strong, nonatomic) IBOutlet UILabel *port_eligibilityStart;
@property (strong, nonatomic) IBOutlet UILabel *port_timePeriod;
@property (strong, nonatomic) IBOutlet UILabel *port_planName;
@property (strong, nonatomic) IBOutlet UILabel *port_dname;
@property (strong, nonatomic) IBOutlet UILabel *port_dmemberID;
@property (strong, nonatomic) IBOutlet UITextView *port_dadress;
@property (strong, nonatomic) IBOutlet UILabel *port_dgroupNo;
@property (strong, nonatomic) IBOutlet UILabel *port_ddateofbirth;
@property (strong, nonatomic) IBOutlet UILabel *port_dgroupName;
@property (strong, nonatomic) IBOutlet UILabel *port_dgender;
@property (strong, nonatomic) IBOutlet UILabel *port_coverageType;


@property (strong, nonatomic) IBOutlet UILabel *land_payerName;
@property (strong, nonatomic) IBOutlet UILabel *land_patientName;
@property (strong, nonatomic) IBOutlet UILabel *land_ssn;
@property (strong, nonatomic) IBOutlet UILabel *land_memberId;
@property (strong, nonatomic) IBOutlet UILabel *land_groupNo;
@property (strong, nonatomic) IBOutlet UILabel *land_dateofbirth;
@property (strong, nonatomic) IBOutlet UITextView *land_adress;
@property (strong, nonatomic) IBOutlet UILabel *land_groupName;
@property (strong, nonatomic) IBOutlet UILabel *land_gender;
@property (strong, nonatomic) IBOutlet UILabel *land_eligibilityStart;
@property (strong, nonatomic) IBOutlet UILabel *land_timePeriod;
@property (strong, nonatomic) IBOutlet UILabel *land_planName;
@property (strong, nonatomic) IBOutlet UILabel *land_dname;
@property (strong, nonatomic) IBOutlet UILabel *land_dmemberID;
@property (strong, nonatomic) IBOutlet UITextView *land_dadress;
@property (strong, nonatomic) IBOutlet UILabel *land_dgroupNo;
@property (strong, nonatomic) IBOutlet UILabel *land_ddateofbirth;
@property (strong, nonatomic) IBOutlet UILabel *land_dgroupName;
@property (strong, nonatomic) IBOutlet UILabel *land_dgender;
@property (strong, nonatomic) IBOutlet UILabel *land_coverageType;

@property (strong, nonatomic) IBOutlet UILabel *port_eligibilityStatus;
@property (strong, nonatomic) IBOutlet UILabel *port_errorCode1;
@property (strong, nonatomic) IBOutlet UILabel *port_errorCode2;
@property (strong, nonatomic) IBOutlet UILabel *port_responseCode;
@property (strong, nonatomic) IBOutlet UILabel *port_error1_discribtion;
@property (strong, nonatomic) IBOutlet UILabel *port_error2_discribtion;

@property (strong, nonatomic) IBOutlet UILabel *land_eligibilityStatus;
@property (strong, nonatomic) IBOutlet UILabel *land_errorCode1;
@property (strong, nonatomic) IBOutlet UILabel *land_errorCode2;
@property (strong, nonatomic) IBOutlet UILabel *land_responseCode;
@property (strong, nonatomic) IBOutlet UILabel *land_error1_discribtion;
@property (strong, nonatomic) IBOutlet UILabel *land_error2_discribtion;

@property (strong, nonatomic) IBOutlet UIView *land_services;
@property (strong, nonatomic) IBOutlet UIView *port_services;


// New Add

@property (strong, nonatomic) IBOutlet UILabel *port_details;

@property (strong, nonatomic) IBOutlet UILabel *land_details;



-(id)initwithArray:(NSMutableDictionary *)array withinsuranceID:(NSString *)insuID eligibilityID:(NSString *)eligibility eligiblePayerName:(NSString *)payer eligiblePayerID:(NSString *)payerID eligibleServiceID:(NSString *)serviceID;
@end
