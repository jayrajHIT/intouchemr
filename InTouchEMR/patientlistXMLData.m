/*
 
 @file: patientlistXMLData.h
 @dated: 12 April 2013
 This file is used for handle the xml response.
 
 */

#import "patientlistXMLData.h"
#import "IndexAppDelegate.h"
@implementation patientlistXMLData

-(NSMutableArray *)patientListByXML:(NSString *)registationID
{
    
IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
NSString *postString = [[NSString alloc]initWithFormat:@"%@%@",globalDele.patientlistUrl,registationID];
    
NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
   
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
     //   NSLog(@"success");
    }
    else
    {
      //  NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"patientID"]||[myelement isEqualToString:@"firstName"]||[myelement isEqualToString:@"lastName"]||[myelement isEqualToString:@"middleName"]||[myelement isEqualToString:@"gender"]||[myelement isEqualToString:@"contactNo"]||[myelement isEqualToString:@"email"]||[myelement isEqualToString:@"ssn"] || [myelement isEqualToString:@"birth"] || [myelement isEqualToString:@"ethnicOriginID"]||[myelement isEqualToString:@"imageName"]||[myelement isEqualToString:@"ethnicOriginName"] )
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Patient"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
