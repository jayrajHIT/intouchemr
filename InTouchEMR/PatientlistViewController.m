//
//  PatientlistViewController.m
//  InTouchEMR
//
//  Created by DSC on 12/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "PatientlistViewController.h"
#import "PatientlistCustomCell.h"
#import "patientlistXMLData.h"
#import "IndexAppDelegate.h"
#import "PatientInformationViewController.h"
#import "LoginViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NewPatientViewController.h"


@interface PatientlistViewController ()

@end

@implementation PatientlistViewController
@synthesize searchBar,isFiltered,filteredTableData,patientList,s3;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 [self.navigationController setNavigationBarHidden:NO];
 self.navigationController.navigationBar.translucent = NO;
    // [self doLayoutForOrientation:currentOrientation];
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
   
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 910, 770, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.view addSubview:footerView];
    }
    else{
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 660, 1024, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.view addSubview:footerView];
    }
    IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    patientlistXMLData *patients=[[patientlistXMLData alloc]init];
    arr=[patients patientListByXML:appDele.registationID];

    if (arr == nil || [arr count] == 0) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Problem to Fetch Patients Data From Database" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
    
  //  UIImageView *navigaionImg
   [self.navigationController.navigationBar setTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navigationBar"]]];
  //  [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:0.0 green:100.0 blue:200.0 alpha:1]];
    self.navigationItem.title=@"Patient List ";
    self.navigationItem.hidesBackButton=YES;
   
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc]
                                initWithTitle:@"Logout"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:@selector(logout:)];
    self.navigationItem.leftBarButtonItem=btnSave;

    btnNext1 =[[UIButton alloc] init];
    [btnNext1 setBackgroundImage:[UIImage imageNamed:@"addpatient.png"] forState:UIControlStateNormal];
    
    btnNext1.frame = CGRectMake(0,0, 40, 40);
    btnNext =[[UIBarButtonItem alloc] initWithCustomView:btnNext1];
    [btnNext1 addTarget:self action:@selector(AddPatient:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = btnNext;
    
   
    
    [self doLayoutForOrientation:currentOrientation];
    [patientList reloadData];
   
}


    // Do any additional setup after loading the view from its nib.
-(void)viewWillAppear:(BOOL)animated
{
     IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([appDele.isExternal isEqualToString:@"1"]) {
        [btnNext1 setHidden:YES];
    }
    else if ([appDele.isExternal isEqualToString:@"0"])
    {
        [btnNext1 setHidden:NO];
    }
    self.navigationItem.hidesBackButton=YES;
        [self.navigationController setNavigationBarHidden:NO];
    UIApplication *app = [UIApplication sharedApplication];
    
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
   
    [self doLayoutForOrientation:currentOrientation];
    [patientList reloadData];
     [NSThread detachNewThreadSelector:@selector(threadData) toTarget:self withObject:nil];
   
}
-(void)threadData
{
    IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    patientlistXMLData *patients=[[patientlistXMLData alloc]init];
    arr=[patients patientListByXML:appDele.registationID];
   
    [patientList reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount;
    if(self.isFiltered)
        rowCount = filteredTableData.count;
    else
        rowCount = arr.count;
    
    return rowCount;
   
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierPortrait = @"portrait";
    static NSString *CellIdentifierLandscape = @"landscape";
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
   
    NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PatientlistCustomCell" owner:self options:nil];
   
    NSMutableDictionary *tempdict;
   
    
    
  
    if (UIInterfaceOrientationIsLandscape(currentOrientation)) {
        cell=(PatientlistCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierLandscape];
       
    }
    else
    {
        cell=(PatientlistCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
       
    }
    cell.patientImgView.image=[UIImage imageNamed:@"defaultpatient.jpeg"];
        if (cell==nil) {
            if (UIInterfaceOrientationIsLandscape(currentOrientation)) {
                cell=[[PatientlistCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierLandscape];
                cell=[nib objectAtIndex:1];
               
                
            }
            else
            {
                cell=[[PatientlistCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierPortrait];
                cell=[nib objectAtIndex:0];
                            }
            
            CALayer *cellImageLayer = cell.patientImgView.layer;
            [cellImageLayer setCornerRadius:30];
            [cellImageLayer setMasksToBounds:YES];

                          }
    if(isFiltered)
        tempdict = [filteredTableData objectAtIndex:indexPath.row];
    else
        tempdict = [arr objectAtIndex:indexPath.row];
   
    
        NSString *docName=[NSString stringWithFormat:@"%@",[tempdict objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [cell.patientImgView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
       
   
    NSString *patientfname = [tempdict objectForKey:@"firstName"];
    NSString *patientlname = [tempdict objectForKey:@"lastName"];
    NSString *patientFullname;
    NSString *email = [tempdict objectForKey:@"email"];
    NSString *gender = [tempdict objectForKey:@"gender"];
    NSString *ssn = [tempdict objectForKey:@"ssn"];
    NSString *tempdob = [tempdict objectForKey:@"birth"];
    
    NSDateFormatter *dateFormatObj = [[NSDateFormatter alloc]init];
    [dateFormatObj setDateFormat:@"yyyy-MM-dd"];
    NSDate *birthday = [dateFormatObj dateFromString:tempdob];
        
    NSDate* now = [NSDate date];

    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
         
    if (patientfname != nil && patientfname.length > 0)
    {
        patientFullname = [NSString stringWithFormat:@"%@ ",patientfname];
    }
    if (patientlname != nil && patientlname.length > 0) {
        patientFullname = [patientFullname stringByAppendingString:patientlname];
        NSString *firstletter = [[patientFullname substringToIndex:1] capitalizedString];
        patientFullname = [patientFullname stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstletter];
      //  cell.patientName.text= patientFullname;
    }
    if(age > 0 )
    {
        NSString *inStr = [NSString stringWithFormat:@" (%d yr)", age];
        patientFullname = [patientFullname stringByAppendingString:inStr];
    }
    
     cell.patientName.text = patientFullname;
    
    if (email != nil && email.length > 0 ) {
        cell.emailId.text = email;
    }
    else
    {
        cell.emailId.text=nil;
    }
    if (gender != nil && gender.length > 0) {
        cell.gender.text = gender;
    }
    else
    {
        cell.gender.text=nil;
    }
    if(ssn != nil && ssn.length > 0 )
    {
        cell.SsnNo.text = ssn;
    }
    else
    {
        cell.SsnNo.text=nil;
    }
    
    [cell.accessoryButton addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    tempdict=nil;

    return cell;
}

-(UIImage*)imageWithImage:(UIImage*)image
             scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 77;
}
- (void)accessoryButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.patientList];
	NSIndexPath *indexPath = [self.patientList indexPathForRowAtPoint: currentTouchPosition];
 
	if (indexPath != nil)
	{
        [self tableView: self.patientList accessoryButtonTappedForRowWithIndexPath: indexPath];
        
	}
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
   
    NSMutableDictionary *temp;
    if(isFiltered)
        temp = [filteredTableData objectAtIndex:indexPath.row];
    else
        temp= [arr objectAtIndex:indexPath.row];

    PatientInformationViewController *patientinformationObj= [[PatientInformationViewController alloc]initwithPatientInformation:temp];
    [self.navigationController pushViewController:patientinformationObj animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSMutableDictionary *temp;
    if(isFiltered)
        temp = [filteredTableData objectAtIndex:indexPath.row];
    else
        temp= [arr objectAtIndex:indexPath.row];
    PatientInformationViewController *patientinformationObj= [[PatientInformationViewController alloc]initwithPatientInformation:temp];
    [self.navigationController pushViewController:patientinformationObj animated:YES];
    
}
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
   
    if(text.length == 0)
    {
        isFiltered = FALSE;
        [self.patientList reloadData];
    }
    else
    {
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        for (NSMutableDictionary* patientObj in arr)
        {
            NSString *patientfname = [patientObj objectForKey:@"firstName"];
            NSString *patientlname = [patientObj objectForKey:@"lastName"];
            NSString *patientFullname;
            
            if (patientfname != nil && patientfname.length > 0)
            {
                patientFullname = [NSString stringWithFormat:@"%@ ",patientfname];
          
            }
            
            if (patientlname != nil && patientlname.length > 0) {
                patientFullname = [patientFullname stringByAppendingString:patientlname];
            }
            
            [patientObj setObject:patientFullname forKey:@"fullName"];
            
        
            NSRange descriptionRange = [patientObj.description rangeOfString:text options:NSCaseInsensitiveSearch];
            if(descriptionRange.location != NSNotFound)
            {
                [filteredTableData addObject:patientObj];
            }
             [self.patientList reloadData];
        }
           }
    [self.patientList reloadData];
   
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    [patientList reloadData];
    [self doLayoutForOrientation:toInterfaceOrientation];
   
}
-(void)doLayoutForOrientation:(UIInterfaceOrientation)orientation
{
    if(UIInterfaceOrientationIsPortrait(orientation))
    {
        
        [footerView setFrame:CGRectMake(0, 910, 770, 50)];
    }
    else{
        [footerView setFrame:CGRectMake(0, 660, 1024, 50)];
    }
}

- (IBAction)logout:(id)sender {
    
    UIAlertView *log=[[UIAlertView alloc]initWithTitle:@"Do you really want to log out?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [log show];

    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];        
        }
       
        default:
            break;
    }

        
}
- (IBAction)AddPatient:(id)sender {
    
    NewPatientViewController *newPatient=[[NewPatientViewController alloc]init];
  
    [self.navigationController pushViewController:newPatient animated:YES];
   
}
@end
