//
//  NewPatientViewController.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/06/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"


@interface NewPatientViewController : UIViewController<NIDropDownDelegate,UITextFieldDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate>
{
   
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    NIDropDown *dropDown;
    UIDatePicker *datePicker;
    UIPopoverController *popoverController;
    UIBarButtonItem *doneButton;
    
    NSString *fullname;
    NSString *fname;
    NSString *Mname;
    NSString *lname;
    NSString *uname;
    NSString *Pass;
    NSString *RePass;
    NSString *Emailid;
    NSString *date;
    NSString *gender;
    NSString *Sstatus;
    NSString *age;
    NSString *physicianValue;
    NSString *title;
    NSString *mstatus;
    
    NSString *nickName;
    NSString *ssnno;
    int status;
    NSString *student;
    NSString *paymentStatus;
    NSString *raceOrigin;
    NSString *jobtype;
    int currentEmp;
    NSString *empAdd;
    NSString *empname;
    NSString *otherInfo;
    
    int hippa;
  //  NSString *PCPrelese;
    
    NSString *minor_fname;
    NSString *minor_mname;
    NSString *Gurantor_fname;
    NSString *Gurantor_mname;
    NSString *Gurantor_lname;
    NSString *Gurantor_add;
    NSString *Gurantor_City;
    NSString *Gurantor_State;
    NSString *Gurantor_Zip;
    NSString *Gurantor_Phone;
    int isminor;
    NSString *patientID;
    
    NSMutableArray *smokingStatus,*EmployerType,*physicianStatus,*titleList,*genderList,*maritalList,*EthnicOriginList,*studentList;
    NSArray* smokingID,*smokingName,*jobTypeID,*jobTypeName,*physicianID,*physicianTypeName ,*titleTypeName,*titleTypeID,*genderTypeName,*genderTypeID,*maritalStatusName,*maritalStatusID,*EthnicOriginName,*EthnicOriginID,*studentTypeName,*studentTypeID;
    
}
- (IBAction)GoToStep2:(id)sender;
- (IBAction)GoToStep1:(id)sender;
- (IBAction)GoToStep3:(id)sender;
- (IBAction)GoBackStep2:(id)sender;
- (IBAction)selectClicked:(id)sender;
- (IBAction)Submit:(id)sender;
@property (nonatomic, strong) UIPopoverController *popoverController;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;

@property (strong, nonatomic) IBOutlet UIView *portrait_step2;
@property (strong, nonatomic) IBOutlet UIView *portrait_step3;
@property (strong, nonatomic) IBOutlet UIView *landscape_step2;
@property (strong, nonatomic) IBOutlet UIView *landscape_step3;
@property (nonatomic, assign) BOOL step1;
@property (nonatomic, assign) BOOL step2;
@property (nonatomic, assign) BOOL step3;
@property (nonatomic, assign) BOOL error;
@property (nonatomic, assign) BOOL phone_error;
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *MiddleName;
@property (strong, nonatomic) IBOutlet UITextField *LastName;
@property (strong, nonatomic) IBOutlet UITextField *UserName;
@property (strong, nonatomic) IBOutlet UITextField *Password;
@property (strong, nonatomic) IBOutlet UITextField *RePassword;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *dob;
@property (strong, nonatomic) IBOutlet UIButton *Title;
@property (strong, nonatomic) IBOutlet UIButton *Gender;
@property (strong, nonatomic) IBOutlet UIButton *matrialStatus;
@property (strong, nonatomic) IBOutlet UIButton *SmokingStatus;


@property (strong, nonatomic) IBOutlet UITextField *NickName;
@property (strong, nonatomic) IBOutlet UITextField *SsnNo;
@property (strong, nonatomic) IBOutlet UIButton *Status;
@property (strong, nonatomic) IBOutlet UIButton *Student;
@property (strong, nonatomic) IBOutlet UIButton *PaymentStatus;
@property (strong, nonatomic) IBOutlet UIButton *RaceOrigin;
@property (strong, nonatomic) IBOutlet UIButton *JobType;
@property (strong, nonatomic) IBOutlet UISwitch *CurrentEmp;
@property (strong, nonatomic) IBOutlet UIButton *physician;
@property (strong, nonatomic) IBOutlet UITextView *EmpAddress;

@property (strong, nonatomic) IBOutlet UITextField *empName;
@property (strong, nonatomic) IBOutlet UITextView *OtherInfo;


@property (strong, nonatomic) IBOutlet UIButton *PCPRelease;

@property (strong, nonatomic) IBOutlet UISwitch *isMinor;
@property (strong, nonatomic) IBOutlet UITextField *minor_fatherName;
@property (strong, nonatomic) IBOutlet UITextField *minor_motherName;
@property (strong, nonatomic) IBOutlet UITextField *gurantor_firstName;
@property (strong, nonatomic) IBOutlet UITextField *gurantor_middleName;
@property (strong, nonatomic) IBOutlet UITextField *gurantor_lastName;
@property (strong, nonatomic) IBOutlet UITextView *gurantor_address;
@property (strong, nonatomic) IBOutlet UITextField *gurantor_city;
@property (strong, nonatomic) IBOutlet UITextField *gurantor_state;
@property (strong, nonatomic) IBOutlet UITextField *gurantor_zip;
@property (strong, nonatomic) IBOutlet UITextField *gurantor_phone;




@property (strong, nonatomic) IBOutlet UITextField *landscapefirstName;
@property (strong, nonatomic) IBOutlet UITextField *landscapeMiddleName;
@property (strong, nonatomic) IBOutlet UITextField *landscapeLastName;
@property (strong, nonatomic) IBOutlet UITextField *landscapeUserName;
@property (strong, nonatomic) IBOutlet UITextField *landscapePassword;
@property (strong, nonatomic) IBOutlet UITextField *landscapeRePassword;
@property (strong, nonatomic) IBOutlet UITextField *landscapeemail;
@property (strong, nonatomic) IBOutlet UITextField *landscapedob;
@property (strong, nonatomic) IBOutlet UIButton *landscapeTitle;
@property (strong, nonatomic) IBOutlet UIButton *landscapeGender;
@property (strong, nonatomic) IBOutlet UIButton *landscapematrialStatus;
@property (strong, nonatomic) IBOutlet UIButton *landscapeSmokingStatus;


@property (strong, nonatomic) IBOutlet UITextField *landscapeNickName;
@property (strong, nonatomic) IBOutlet UITextField *landscapeSsnNo;
@property (strong, nonatomic) IBOutlet UIButton *landscapeStatus;
@property (strong, nonatomic) IBOutlet UIButton *landscapeStudent;
@property (strong, nonatomic) IBOutlet UIButton *landscapePaymentStatus;
@property (strong, nonatomic) IBOutlet UIButton *landscapeRaceOrigin;
@property (strong, nonatomic) IBOutlet UIButton *landscapeJobType;
@property (strong, nonatomic) IBOutlet UISwitch *landscapeCurrentEmp;
@property (strong, nonatomic) IBOutlet UITextView *landscapeEmpAddress;
@property (strong, nonatomic) IBOutlet UITextView *landscapeOtherInfo;
@property (strong, nonatomic) IBOutlet UIButton *landscapephysician;
@property (strong, nonatomic) IBOutlet UITextField *landscapeEmpName;


@property (strong, nonatomic) IBOutlet UIButton *landscapePCPRelease;

@property (strong, nonatomic) IBOutlet UISwitch *landscapeisMinor;
@property (strong, nonatomic) IBOutlet UITextField *landscapeminor_fatherName;
@property (strong, nonatomic) IBOutlet UITextField *landscapeminor_motherName;
@property (strong, nonatomic) IBOutlet UITextField *landscapegurantor_firstName;
@property (strong, nonatomic) IBOutlet UITextField *landscapegurantor_middleName;
@property (strong, nonatomic) IBOutlet UITextField *landscapegurantor_lastName;
@property (strong, nonatomic) IBOutlet UITextView *landscapegurantor_address;
@property (strong, nonatomic) IBOutlet UITextField *landscapegurantor_city;
@property (strong, nonatomic) IBOutlet UITextField *landscapegurantor_state;
@property (strong, nonatomic) IBOutlet UITextField *landscapegurantor_zip;
@property (strong, nonatomic) IBOutlet UITextField *landscapegurantor_phone;


@property (strong, nonatomic) IBOutlet UILabel *portFirstName;
@property (strong, nonatomic) IBOutlet UILabel *portLastName;
@property (strong, nonatomic) IBOutlet UILabel *portuserName;
@property (strong, nonatomic) IBOutlet UILabel *portpassword;
@property (strong, nonatomic) IBOutlet UILabel *portrepassword;
@property (strong, nonatomic) IBOutlet UILabel *portemail;
@property (strong, nonatomic) IBOutlet UILabel *porttitle;
@property (strong, nonatomic) IBOutlet UILabel *portGender;
@property (strong, nonatomic) IBOutlet UILabel *portDoB;
@property (strong, nonatomic) IBOutlet UILabel *portmstatus;
@property (strong, nonatomic) IBOutlet UILabel *portstatus;
@property (strong, nonatomic) IBOutlet UILabel *portPaymentStatus;
@property (strong, nonatomic) IBOutlet UILabel *portPCPrelease;


@property (strong, nonatomic) IBOutlet UILabel *landFirstName;
@property (strong, nonatomic) IBOutlet UILabel *landLastName;
@property (strong, nonatomic) IBOutlet UILabel *landuserName;
@property (strong, nonatomic) IBOutlet UILabel *landpassword;
@property (strong, nonatomic) IBOutlet UILabel *landrepassword;
@property (strong, nonatomic) IBOutlet UILabel *landemail;
@property (strong, nonatomic) IBOutlet UILabel *landtitle;
@property (strong, nonatomic) IBOutlet UILabel *landGender;
@property (strong, nonatomic) IBOutlet UILabel *landDoB;
@property (strong, nonatomic) IBOutlet UILabel *landmstatus;
@property (strong, nonatomic) IBOutlet UILabel *landstatus;
@property (strong, nonatomic) IBOutlet UILabel *landPaymentStatus;
@property (strong, nonatomic) IBOutlet UILabel *landPCPrelease;


@property (strong, nonatomic) IBOutlet UILabel *portminor_FatherName;
@property (strong, nonatomic) IBOutlet UILabel *portminor_MotherName;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_firstName;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_middleName;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_LastName;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_add;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_city;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_state;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_zip;
@property (strong, nonatomic) IBOutlet UILabel *portgurantor_phone;

@property (strong, nonatomic) IBOutlet UILabel *landminor_FatherName;
@property (strong, nonatomic) IBOutlet UILabel *landminor_MotherName;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_firstName;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_middleName;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_LastName;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_add;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_city;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_state;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_zip;
@property (strong, nonatomic) IBOutlet UILabel *landgurantor_phone;



@end
