//
//  SmokingStatusXMLData.h
//  InTouchEMR
//
//  Created by Ghanshyam on 01/03/14.
//  Copyright (c) 2014 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmokingStatusXMLData : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)SmokingStatusList;
@end
