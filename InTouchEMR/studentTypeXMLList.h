//
//  studentTypeXMLList.h
//  InTouchEMR
//
//  Created by Ghanshyam on 10/03/14.
//  Copyright (c) 2014 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface studentTypeXMLList : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)studentTypeList;


@end
