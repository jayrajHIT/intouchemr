 //
//  main.m
//  InTouchEMR
//
//  Created by DSC on 11/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IndexAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IndexAppDelegate class]));
    }
}
