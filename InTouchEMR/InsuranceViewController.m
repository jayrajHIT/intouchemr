//
//  InsuranceViewController.m
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "InsuranceViewController.h"
#import "InsurenceCustomCell.h"
#include "IndexAppDelegate.h"
#import "InsurenceXMLData.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "SHKActivityIndicator.h"
#import "patientInsurenceData.h"
#import "NSString+validation.h"
#import "insurePartyXMLList.h"

@interface InsuranceViewController ()

@end

@implementation InsuranceViewController
@synthesize patientContact,patientEmail,patientName,patientOrigin,ssnHeading,ssnValue,insurenceTable,portraitView;
@synthesize landscapeView,landscapepatientContact,landscapepatientEmail,landscapepatientName,landscapepatientOrigin,landscapessnHeading,landscapessnValue,landscapeinsurenceTable;
@synthesize patientImage,landscapePatientImage,index,sc1;
@synthesize lbl_edit_additionalInfo,lbl_edit_coPay,lbl_edit_DateOfBirth,lbl_edit_group,lbl_edit_InsuredParty,lbl_edit_insurence,lbl_edit_patient_MID,lbl_edit_planName,lbl_edit_policy_edate,lbl_edit_policy_sdate,lbl_edit_policyNo,lbl_edit_subscriber_fname,lbl_edit_subscriber_Gender,lbl_edit_subscriber_lname,lbl_edit_subscribID,lbl_edit_visitAllowed;
@synthesize edit_additionalInfo,edit_coPay,edit_DateOfBirth,edit_group,edit_InsuredParty,edit_insurence,edit_insurenceID,edit_patient_MID,edit_pdate,edit_planName,edit_policy_edate,edit_policy_sdate,edit_policyNo,edit_subscriber_fname,edit_subscriber_Gender,edit_subscriber_genderID,edit_subscriber_lname,edit_subscribID,edit_visitAllowed,edit_Cancel,edit_Save,companyList,edit_visitResetDate,popoverController;
@synthesize searchBar,isFiltered,filteredTableData,orientchange,btn_edit_insurence,btn_edit_land_insurence;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initwithArray:(NSMutableDictionary *)array withImage:(UIImage *)img
{
    arr=array;
    image=img;
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setWorking:NO];
 //   [self setOrientchange:NO];
    
    [self setListVisible:NO];
      [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
    
	orientation = [[UIDevice currentDevice] orientation];
    IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([appDele.isExternal isEqualToString:@"1"]) {
        [btn_edit_insurence setHidden:YES];
        [btn_edit_land_insurence setHidden:YES];
    }
    else if ([appDele.isExternal isEqualToString:@"0"])
    {
        [btn_edit_insurence setHidden:NO];
        [btn_edit_land_insurence setHidden:NO];
    }
    
    
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
      if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
    [self myOrientation];
    
    /*
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [txtFieldBranchYear setInputView:datePicker];
    */
    datePicker1=[[UIDatePicker alloc]init];//Date picker
    
    [datePicker1 setDate:[NSDate date]];
    datePicker1.frame=CGRectMake(0,0,320, 216);
     datePicker1.backgroundColor = [UIColor whiteColor];
    [datePicker1 setDatePickerMode:UIDatePickerModeDate];
    [datePicker1 setMaximumDate:[NSDate date]];
   
    if (![patientDOB isEqualToString:@""]) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];

            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *oneDayAgoComponents = [[NSDateComponents alloc] init];
            oneDayAgoComponents.year = -50;
            NSDate *YesrsAgo = [calendar dateByAddingComponents:oneDayAgoComponents
                                                          toDate:[NSDate date]
                                                         options:0];
            [datePicker1 setMinimumDate:YesrsAgo];
        
     
    }
   
    NSCalendar *calendar1 = [NSCalendar currentCalendar];
    NSDateComponents *oneDayAgoComponents1 = [[NSDateComponents alloc] init];
    oneDayAgoComponents1.year =50;
    NSDate *YesrsAfter = [calendar1 dateByAddingComponents:oneDayAgoComponents1
                                                 toDate:[NSDate date]
                                                options:0];
  
    
    datePicker2=[[UIDatePicker alloc]init];//Date picker
     [datePicker2 setDate:[NSDate date]];
    
    datePicker2.frame=CGRectMake(0,0,320, 216);
    datePicker2.backgroundColor = [UIColor whiteColor];
    [datePicker2 setDatePickerMode:UIDatePickerModeDate];
    [datePicker2 setMinimumDate:[NSDate date]];
    [datePicker2 setMaximumDate:YesrsAfter];
    
    
    
    datePicker3=[[UIDatePicker alloc]init];//Date picker
    [datePicker3 setDate:[NSDate date]];
    
    datePicker3.frame=CGRectMake(0,0,320, 216);
     datePicker3.backgroundColor = [UIColor whiteColor];
    [datePicker3 setDatePickerMode:UIDatePickerModeDate];
    [datePicker3 setMaximumDate:[NSDate date]];
    
    
    datePicker1.datePickerMode = UIDatePickerModeDate;
    [datePicker1 setMinuteInterval:5];
    [datePicker1 setTag:10];
    [datePicker1 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
    
    
    datePicker2.datePickerMode = UIDatePickerModeDate;
    [datePicker2 setMinuteInterval:5];
    [datePicker2 setTag:11];
    
    [datePicker2 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
    
    datePicker3.datePickerMode = UIDatePickerModeDate;
    [datePicker3 setMinuteInterval:5];
    [datePicker3 setTag:12];
    [datePicker3 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
    
    
    [edit_visitResetDate setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [edit_visitResetDate setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    

    
}
-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	
    if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
}

-(void)myOrientation
{
    [self setOrientchange:NO];
    
    [self performSelectorInBackground:@selector(mythread)
                           withObject:nil];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
       
        if ([arr objectForKey:@"FullName"]) {
            landscapepatientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            landscapepatientName.text=@"";
        }
        
        if ([arr objectForKey:@"email"]) {
            landscapepatientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            landscapepatientEmail.text=@"";
        }
        if ([arr objectForKey:@"ssn"]) {
            landscapessnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            landscapessnValue.text=@"";
        }
        
        if ([arr objectForKey:@"ethnicOriginName"]) {
            landscapepatientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            landscapepatientOrigin.text=@"";
        }
        if ([arr objectForKey:@"birth"]) {
             patientDOB=[arr objectForKey:@"birth"];
        }
        else
        {
            patientDOB=@"";
        }
        
        
       
       
        
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            landscapepatientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                      landscapepatientContact.text=nil;
        }
        
        [landscapeinsurenceTable reloadData];
        
        NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [landscapePatientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        
        if (InsurenceData!=nil) {
            [self afterthread];
        }
               
        if (self.working==YES) {
            [self editView];
            
        }
        
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		
		[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        
        
        if ([arr objectForKey:@"FullName"]) {
            patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            patientName.text=@"";
        }
        
        if ([arr objectForKey:@"email"]) {
            patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            patientEmail.text=@"";
        }
        if ([arr objectForKey:@"ssn"]) {
           ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
           ssnValue.text=@"";
        }
        
        if ([arr objectForKey:@"ethnicOriginName"]) {
            patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
           patientOrigin.text=@"";
        }
        if ([arr objectForKey:@"birth"]) {
            patientDOB=[arr objectForKey:@"birth"];
        }
        else
        {
            patientDOB=@"";
        }
        
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                      patientContact.text=nil;
        }
        
        
        
        NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        
        if (InsurenceData!=nil) {
            [self afterthread];
        }
        
        
        
        if (self.working==YES) {
            [self editView];
            
        }
	}
    
}

-(void)mythread
{
    
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    InsurenceXMLData *patientData=[[InsurenceXMLData alloc]init];
    
    
    InsurenceData=[patientData patientInsurenceDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
   
    [self addDropDown];
    [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
    
}
-(void)afterthread
{
    [insurenceTable reloadData];
    [landscapeinsurenceTable reloadData];
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}

-(void)editView
{
    
  //  [self companyName];
    
    [self registerForKeyboardNotifications];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024,670)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [landscapeView addSubview:vi];
        sc1.frame = CGRectMake(0,0,525, 620);
        [sc1 setContentSize:CGSizeMake(525,sc1.frame.size.height+300)];
        self.addView.frame=CGRectMake(250, 0, 525, 620);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        edit_additionalInfo.layer.cornerRadius=5.0;
        edit_additionalInfo.layer.borderWidth=1.0;
        edit_additionalInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [landscapeView addSubview:self.addView];
        [self.addView addSubview:sc1];
        
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 930)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        sc1.frame = CGRectMake(0,0,525, 700);
        [sc1 setContentSize:CGSizeMake(525,sc1.frame.size.height+200)];
        [portraitView addSubview:vi];
        self.addView.frame=CGRectMake(125, 50, 525, 700);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        edit_additionalInfo.layer.cornerRadius=5.0;
        edit_additionalInfo.layer.borderWidth=1.0;
        edit_additionalInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [portraitView addSubview:self.addView];
        [self.addView addSubview:sc1];
                
    }
    
    if ([self orientchange]) {
        
        if ([self index]==1001) {
            
            [self selectSelf];
            [edit_Save setTitle:@"Save" forState:UIControlStateNormal];
            str_companyID=nil;
            edit_group.text=@"";
            edit_subscribID.text=@"";
            edit_planName.text=@"";
            edit_policyNo.text=@"";
            edit_visitAllowed.text=@"";
            edit_pdate.text=@"";
            edit_policy_edate.text=@"";
            edit_coPay.text=@"";
            [edit_insurenceID setTitle:@"select one" forState:UIControlStateNormal];
            [edit_InsuredParty setTitle:@"select one" forState:UIControlStateNormal];
            edit_additionalInfo.text=@"Additional Information";
            edit_patient_MID.text=@"";
            edit_subscriber_fname.text=@"";
            edit_subscriber_lname.text=@"";
            edit_subscriber_Gender.text=@"";
            edit_DateOfBirth.text=@"";
          
//            if (InsurenceInfo) {
//                
//              [edit_insurenceID setTitle:@"select one" forState:UIControlStateNormal];                
//            }
//            else
//            {
//                [edit_insurenceID setTitle:@"Insurance companies does not exist" forState:UIControlStateNormal];
//                
//            }

            
            [self setOrientchange:NO];
            
        }
        else
        {
            
            NSDictionary *editdict=[InsurenceData objectAtIndex:index];
                      [edit_Save setTitle:@"Update" forState:UIControlStateNormal];
            
            int a=[insurePartyTypeID indexOfObject:[editdict objectForKey:@"insuredPartyID"]];
            
            if ([[insurePartyTypeName objectAtIndex:a] isEqualToString:@"Self"]) {
                [self selectSelf];
            }
            else
            {
                [self unselectSelf];
            }
            if ([editdict objectForKey:@"id"]) {
                id_insurence=[editdict objectForKey:@"id"];
            }
            else
            {
                id_insurence=@"";
            }
            if ([editdict objectForKey:@"insuranceID"]) {
                int a=[insurenceTypeID indexOfObject:[editdict objectForKey:@"insuranceID"]];
                
                str_companyID=[insurenceTypeArr objectAtIndex:a];
            }
            else
            {
                str_companyID=@"";
            }
            if ([editdict objectForKey:@"group"]) {
                edit_group.text=[editdict objectForKey:@"group"];
            }
            else
            {
                edit_group.text=@"";
            }
            
            if ([editdict objectForKey:@"subscriberID"]) {
                edit_subscribID.text=[editdict objectForKey:@"subscriberID"];
            }
            else
            {
                edit_subscribID.text=@"";
            }
            
            if ([editdict objectForKey:@"insurancePlanName"]) {
                edit_planName.text=[editdict objectForKey:@"insurancePlanName"];
            }
            else
            {
                edit_planName.text=@"";
            }
            if ([editdict objectForKey:@"insurancePolicyNo"]) {
                edit_policyNo.text=[editdict objectForKey:@"insurancePolicyNo"];
            }
            else
            {
                edit_policyNo.text=@"";
            }
           
            if ([editdict objectForKey:@"visitsAllowed"]) {
                edit_visitAllowed.text=[editdict objectForKey:@"visitsAllowed"];
            }
            else
            {
                edit_visitAllowed.text=@"";
            }

            if ([editdict objectForKey:@"startDate"]) {
                edit_pdate.text=[self viewDate:[editdict objectForKey:@"startDate"]];
            }
            else
            {
                edit_pdate.text=@"";
            }
            if ([editdict objectForKey:@"endDate"]) {
                edit_policy_edate.text=[self viewDate:[editdict objectForKey:@"endDate"]];
            }
            else
            {
                edit_policy_edate.text=@"";
            }
            
            if ([editdict objectForKey:@"coPay"]) {
                edit_coPay.text=[editdict objectForKey:@"coPay"];
            }
            else
            {
                edit_coPay.text=@"";
            }

            if ([editdict objectForKey:@"insuredPartyID"]) {
                  int a=[insurePartyTypeID indexOfObject:[editdict objectForKey:@"insuredPartyID"]];
                
                [edit_InsuredParty setTitle:[insurePartyTypeName objectAtIndex:a] forState:UIControlStateNormal];
            }
            else
            {
                [edit_InsuredParty setTitle:@"select one" forState:UIControlStateNormal];
               
            }
            if ([editdict objectForKey:@"additionalInformation"]) {
                edit_additionalInfo.text=[editdict objectForKey:@"additionalInformation"];
            }
            else
            {
                edit_additionalInfo.text=@"";
            }

            if ([editdict objectForKey:@"subscriberMemberID"]) {
                edit_patient_MID.text=[editdict objectForKey:@"subscriberMemberID"];
            }
            else
            {
                edit_patient_MID.text=@"";
            }
            if ([editdict objectForKey:@"subscriberFirstName"]) {
                edit_subscriber_fname.text=[editdict objectForKey:@"subscriberFirstName"];
            }
            else
            {
                edit_subscriber_fname.text=@"";
            }
            if ([editdict objectForKey:@"subscriberLastName"]) {
                edit_subscriber_lname.text=[editdict objectForKey:@"subscriberLastName"];
            }
            else
            {
                edit_subscriber_lname.text=@"";
            }
            if ([editdict objectForKey:@"subscriberGender"]) {
               [edit_subscriber_genderID setTitle:[editdict objectForKey:@"subscriberGender"] forState:UIControlStateNormal];
            }
            else
            {
                [edit_subscriber_genderID setTitle:@"select one" forState:UIControlStateNormal];
            }
           
            if ([editdict objectForKey:@"subscriberBirth"]) {
                edit_DateOfBirth.text=[self viewDate:[editdict objectForKey:@"subscriberBirth"]];
            }
            else
            {
                edit_DateOfBirth.text=@"";
            }
            if ([editdict objectForKey:@"insuranceID"]) {
                
                int a=[insurenceTypeID indexOfObject:[editdict objectForKey:@"insuranceID"]];
                
                [edit_insurenceID setTitle:[insurenceTypeName objectAtIndex:a] forState:UIControlStateNormal];
            }
            else
            {
                [edit_insurenceID setTitle:@"select one" forState:UIControlStateNormal];
            }

            
            
            [self setOrientchange:NO];
        }
        
        
    }
    
}



//-(void)companyName
//{
//    
//    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
//    if (!InsurenceInfo) {
//        
//        patientInsurenceData *patientInsurence=[[patientInsurenceData alloc]init];
//        InsurenceInfo=[patientInsurence patientInsurenceDataByXML:appdalegate.registationID];
//        
//            [self performSelectorOnMainThread:@selector(ShowCompany)
//                               withObject:nil
//                            waitUntilDone:NO];
//        
//    }
//    
//}
//-(void)ShowCompany
//{
//    [companyList reloadData];
//    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
//}
-(void) clearCurrentView {
    
    [self.addView removeFromSuperview];
    [sc1 removeFromSuperview];
    [vi removeFromSuperview];
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}

- (IBAction)cancelView:(id)sender {
    [self.addView removeFromSuperview];
    [vi removeFromSuperview];
    [sc1 removeFromSuperview];
    [self.InsurenceCompany removeFromSuperview];
    self.working=NO;
    [self setOrientchange:NO];
    [self setListVisible:NO];
    
    [[SHKActivityIndicator currentIndicator]hide];
    [lbl_edit_insurence setTextColor:[UIColor blackColor]];
    [lbl_edit_subscribID setTextColor:[UIColor blackColor]];
    [lbl_edit_planName setTextColor:[UIColor blackColor]];
    [lbl_edit_policyNo setTextColor:[UIColor blackColor]];
    [lbl_edit_policy_sdate setTextColor:[UIColor blackColor]];
    [lbl_edit_InsuredParty setTextColor:[UIColor blackColor]];
    [lbl_edit_patient_MID setTextColor:[UIColor blackColor]];
    [lbl_edit_subscriber_fname setTextColor:[UIColor blackColor]];
    [lbl_edit_subscriber_lname setTextColor:[UIColor blackColor]];
    [lbl_edit_subscriber_lname setTextColor:[UIColor blackColor]];
    [lbl_edit_DateOfBirth setTextColor:[UIColor blackColor]];
    [lbl_edit_subscriber_Gender setTextColor:[UIColor blackColor]];
    
    
}
- (IBAction)update:(id)sender {
    [self setWorking:YES];
    [self setIndex:1001];
    [self setOrientchange:YES];
    [self editView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	
	[textField resignFirstResponder];
	return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    // [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // [self animateTextField: textField up: NO];
    activeField=nil;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
   
      return  [InsurenceData count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==insurenceTable) {
        
        static NSString *CellIdentifierPortrait = @"portrait";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"InsurenceCustomCell" owner:self options:nil];
    
        
        cell=(InsurenceCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
        
        if (cell==nil) {
            cell=[[ InsurenceCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierPortrait];
            cell=[nib objectAtIndex:0];
        }
        
        
        NSMutableDictionary *tempdict=[InsurenceData objectAtIndex:indexPath.row];
       
        if ([tempdict objectForKey:@"insuranceID"]) {
            
            if ([insurenceTypeID count]>0) {
                
           
            if ([insurenceTypeID indexOfObject:[tempdict objectForKey:@"insuranceID"]] != NSNotFound) {
                
                int a=[insurenceTypeID indexOfObject:[tempdict objectForKey:@"insuranceID"]];
                
                cell.insuranceName.text=[insurenceTypeName objectAtIndex:a];
            }
            else {
                
                 cell.insuranceName.text=[tempdict objectForKey:@"insuranceName"];
            }
            
        }
        else
        {
           cell.insuranceName.text=[tempdict objectForKey:@"insuranceName"];
        }
        }
        else
        {
            cell.insuranceName.text=@"";
        }
       
        if ([tempdict objectForKey:@"subscriberID"]) {
            cell.subscriberID.text=[tempdict objectForKey:@"subscriberID"];
        }
        else
        {
            cell.subscriberID.text=@"";
        }
        if ([tempdict objectForKey:@"insuredPartyID"]) {
           
            
        //    int a=[insurePartyTypeID indexOfObject:[tempdict objectForKey:@"insuredPartyID"]];
            
          //  cell.insuredParty.text=[insurePartyTypeName objectAtIndex:a];
        }
        else
        {
           // cell.insuredParty.text=@"";
        }
        if ([tempdict objectForKey:@"insurancePolicyNo"]) {
           // cell.policyNo.text=[tempdict objectForKey:@"insurancePolicyNo"];
        }
        else
        {
         //   cell.policyNo.text=@"";
        }
        if ([tempdict objectForKey:@"startDate"]) {
            cell.effectiveDate.text=[self viewDate:[tempdict objectForKey:@"startDate"]];
        }
        else
        {
            cell.effectiveDate.text=@"";
        }
        
        
        IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        if ([appDele.isExternal isEqualToString:@"1"]) {
            [cell.edit setHidden:YES];
        }
        else if ([appDele.isExternal isEqualToString:@"0"])
        {
            [cell.edit setHidden:NO];
        }
        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if (tableView==landscapeinsurenceTable) {
        
        static NSString *CellIdentifier = @"landscape";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"InsurenceCustomCell" owner:self options:nil];
     
        
        cell=(InsurenceCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil) {
            cell=[[ InsurenceCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell=[nib objectAtIndex:1];
        }
               NSMutableDictionary *tempdict=[InsurenceData objectAtIndex:indexPath.row];
       
        
        if ([tempdict objectForKey:@"insuranceID"]) {
            
            if ([insurenceTypeID count]>0) {
                
                
                if ([insurenceTypeID indexOfObject:[tempdict objectForKey:@"insuranceID"]] != NSNotFound) {
                    
                    int a=[insurenceTypeID indexOfObject:[tempdict objectForKey:@"insuranceID"]];
                
                    cell.insuranceName.text=[insurenceTypeName objectAtIndex:a];
                }
                else {
                    
                    cell.insuranceName.text=[tempdict objectForKey:@"insuranceName"];
                    [cell.insuranceName setHighlightedTextColor:[UIColor redColor]];
                }
                
            }
            else
            {
                cell.insuranceName.text=[tempdict objectForKey:@"insuranceName"];
                [cell.insuranceName setHighlightedTextColor:[UIColor redColor]];

                
            }
        }
        else
        {
            cell.insuranceName.text=@"";
        }

        if ([tempdict objectForKey:@"subscriberID"]) {
         
            
            
             
            
            
            
            cell.subscriberID.text=[tempdict objectForKey:@"subscriberID"];
        }
        else
        {
            cell.subscriberID.text=@"";
        }
        if ([tempdict objectForKey:@"insuredPartyID"]) {
            
            //  int a=[insurePartyTypeID indexOfObject:[tempdict objectForKey:@"insuredPartyID"]];
            
            
           // cell.insuredParty.text=[insurePartyTypeName objectAtIndex:a];
        }
        else
        {
          //  cell.insuredParty.text=@"";
        }
        if ([tempdict objectForKey:@"insurancePolicyNo"]) {
         //   cell.policyNo.text=[tempdict objectForKey:@"insurancePolicyNo"];
        }
        else
        {
           // cell.policyNo.text=@"";
        }
        if ([tempdict objectForKey:@"startDate"]) {
            cell.effectiveDate.text=[self viewDate:[tempdict objectForKey:@"startDate"]];
        }
        else
        {
            cell.effectiveDate.text=@"";
        }
        
        
         IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        if ([appDele.isExternal isEqualToString:@"1"]) {
            [cell.edit setHidden:YES];
        }
        else if ([appDele.isExternal isEqualToString:@"0"])
        {
            [cell.edit setHidden:NO];
        }
        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
   return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    return 70;
    
}

- (void)accessoryButtonTapped:(id)sender event:(id)event
{
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition;
    NSIndexPath *indexPath;
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        currentTouchPosition = [touch locationInView:landscapeinsurenceTable];
        indexPath = [landscapeinsurenceTable indexPathForRowAtPoint: currentTouchPosition];
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        currentTouchPosition = [touch locationInView:insurenceTable];
        indexPath = [insurenceTable indexPathForRowAtPoint: currentTouchPosition];
    }
 
    if (indexPath != nil)
    {
        [self tableView: insurenceTable accessoryButtonTappedForRowWithIndexPath: indexPath];
        
    }
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    [self setIndex:indexPath.row];
    self.working=YES;
    [self setOrientchange:YES];
    [self editView];
    
}


-(IBAction)saveInsurence:(id)sender
{
    BOOL flag=NO;
  
    if ([[edit_insurenceID titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
        [lbl_edit_insurence setTextColor:[UIColor redColor]];
        flag=YES;
    }
    else
    {
        int a=[insurenceTypeName indexOfObject:[edit_insurenceID titleForState:UIControlStateNormal]];
        str_companyID=[insurenceTypeID objectAtIndex:a];
        [lbl_edit_insurence setTextColor:[UIColor blackColor]];
    }
   
//     str_group=edit_group.text; OLD
    str_group=@"";
    str_subscribeID=edit_subscribID.text;
    str_planName=edit_planName.text;
    str_policyNo=edit_policyNo.text;
//    str_visitAllowed=edit_visitAllowed.text; OLD
      str_visitAllowed=@"";
    
    str_policy_sdate=[self saveDate:edit_pdate.text];
    str_policy_edate=[self saveDate:edit_policy_edate.text];
//    str_coPay=edit_coPay.text; OLD
    str_coPay=@"";
    str_InsuredParty=[edit_InsuredParty titleForState:UIControlStateNormal];
    

    
//    insuredPartyID=[self InsuredParty:str_InsuredParty];
    str_additionalInfo=edit_additionalInfo.text;
    
    
    
    str_patient_MID=edit_patient_MID.text;
    str_subscriber_fname=edit_subscriber_fname.text;
    str_subscriber_lname=edit_subscriber_lname.text;
    str_subscriber_Gender=[edit_subscriber_genderID titleForState:UIControlStateNormal];
    str_DateOfBirth=[self saveDate:edit_DateOfBirth.text];
    
    
    
    if ([edit_visitResetDate isOn]) {
        VisitResetDate=1;
    }
    else
    {
        VisitResetDate=0;
    }
    
    
    
    if ([NSString stringIsEmpty:str_group]) {
        str_group=@"";
        
    }
    if ([NSString stringIsEmpty:str_visitAllowed]) {
        str_visitAllowed=@"";
    }
    if ([NSString stringIsEmpty:str_policy_edate]) {
        str_policy_edate=@"";
    }
    if ([NSString stringIsEmpty:str_coPay]) {
        str_coPay=@"0";
    }
    if ([str_additionalInfo isEqualToString:@"Additional Information"]) {
        str_additionalInfo=@"";
    }
    
    if ([NSString stringIsEmpty:str_subscribeID]) {
        [lbl_edit_subscribID setTextColor:[UIColor redColor]];
        flag=YES;
    }
    else
    {
        [lbl_edit_subscribID setTextColor:[UIColor blackColor]];
    }
    /*
    if ([NSString stringIsEmpty:str_planName]) {
        [lbl_edit_planName setTextColor:[UIColor redColor]];
        flag=YES;
    }
    else
    {
        [lbl_edit_planName setTextColor:[UIColor blackColor]];
    }
    if ([NSString stringIsEmpty:str_policyNo]) {
        [lbl_edit_policyNo setTextColor:[UIColor redColor]];
        flag=YES;
    }
    else
    {
        [lbl_edit_policyNo setTextColor:[UIColor blackColor]];
    }
    */
    if ([NSString stringIsEmpty:str_policy_sdate]) {
        [lbl_edit_policy_sdate setTextColor:[UIColor redColor]];
        flag=YES;
    }
    else
    {
        [lbl_edit_policy_sdate setTextColor:[UIColor blackColor]];
    }
    if ([str_InsuredParty isEqualToString:@"select one"]) {
        [lbl_edit_InsuredParty setTextColor:[UIColor redColor]];
        flag=YES;
        insuredPartyID=@"0";
    }
    else
    {
        [lbl_edit_InsuredParty setTextColor:[UIColor blackColor]];
        int a=[insurePartyTypeName indexOfObject:str_InsuredParty];
        
        insuredPartyID=[insurePartyTypeID objectAtIndex:a];
        
        
    }
    
    if (insuredPartyID.intValue>1) {
        
        if ([NSString stringIsEmpty:str_patient_MID]) {
            [lbl_edit_patient_MID setTextColor:[UIColor redColor]];
            flag=YES;
        }
        else
        {
            [lbl_edit_patient_MID setTextColor:[UIColor blackColor]];
        }
        if ([NSString stringIsEmpty:str_subscriber_fname]) {
            [lbl_edit_subscriber_fname setTextColor:[UIColor redColor]];
            flag=YES;
        }
        else
        {
            [lbl_edit_subscriber_fname setTextColor:[UIColor blackColor]];
        }
        if ([NSString stringIsEmpty:str_subscriber_lname]) {
            [lbl_edit_subscriber_lname setTextColor:[UIColor redColor]];
            flag=YES;
        }
        else
        {
            [lbl_edit_subscriber_lname setTextColor:[UIColor blackColor]];
        }
        if ([NSString stringIsEmpty:str_subscriber_lname]) {
            [lbl_edit_subscriber_lname setTextColor:[UIColor redColor]];
            flag=YES;
        }
        else
        {
            [lbl_edit_subscriber_lname setTextColor:[UIColor blackColor]];
        }
        
        
        if ([NSString stringIsEmpty:str_DateOfBirth]) {
            [lbl_edit_DateOfBirth setTextColor:[UIColor redColor]];
            flag=YES;
        }
        else
        {
            [lbl_edit_DateOfBirth setTextColor:[UIColor blackColor]];
        }
        
        if ([str_subscriber_Gender isEqualToString:@"Select one"]) {
            [lbl_edit_subscriber_Gender setTextColor:[UIColor redColor]];
            flag=YES;
        }
        else
        {
            [lbl_edit_subscriber_Gender setTextColor:[UIColor blackColor]];
        }
    }
    
    
    if(flag==YES)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
 
    }
    else
    {
        IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        NSString *xmlText;
        
        NSURL *url = [NSURL URLWithString:appdalegate.insuranceSavedUrl];
        NSLog(@"%@",url);
        
        
        if (insuredPartyID.intValue==1) {
            
            if ([self index]==1001) {
                
               
                 
                  xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><subscriberID>%@</subscriberID><effectiveDate>%@</effectiveDate><expiryDate>%@</expiryDate><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID></PatientInsurance> ",str_subscribeID,str_policy_sdate,str_policy_edate,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID];
                 
                
                /*
                OLD Line
                 
                  xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><subscriberID>%@</subscriberID><coPay>%@</coPay><insurancePlanName>%@</insurancePlanName><insurancePolicyNo>%@</insurancePolicyNo><effectiveDate>%@</effectiveDate><visitResetDate>%i</visitResetDate><expiryDate>%@</expiryDate><group>%@</group><visitsAllowed>%@</visitsAllowed><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID></PatientInsurance> ",str_subscribeID,str_coPay,str_planName,str_policyNo,str_policy_sdate,VisitResetDate,str_policy_edate,str_group,str_visitAllowed,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID];
                 
                 
                 */
                
                
              
            }
            else{
                xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><id>%@</id><subscriberID>%@</subscriberID><effectiveDate>%@</effectiveDate><expiryDate>%@</expiryDate><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID></PatientInsurance> ",id_insurence,str_subscribeID,str_policy_sdate,str_policy_edate,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID];
                
                /*
                 OLD
                 
                 xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><id>%@</id><subscriberID>%@</subscriberID><coPay>%@</coPay><insurancePlanName>%@</insurancePlanName><insurancePolicyNo>%@</insurancePolicyNo><effectiveDate>%@</effectiveDate><visitResetDate>%i</visitResetDate><expiryDate>%@</expiryDate><group>%@</group><visitsAllowed>%@</visitsAllowed><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID></PatientInsurance> ",id_insurence,str_subscribeID,str_coPay,str_planName,str_policyNo,str_policy_sdate,VisitResetDate,str_policy_edate,str_group,str_visitAllowed,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID];
                 
                 */
                
                
            }
            
            
                     NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
            [request setURL:url];
            
            
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            NSURLResponse* response;
            NSError *requestError = NULL;
            NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
            NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@",responseString);
           
            if ([self index]==1001) {
                if ([responseString isEqualToString:@"success"]) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your insurance information has been successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else{
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your insurance information has not updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            }
            else
            {
                
                if ([responseString isEqualToString:@"success"]) {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your insurance information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your insurance information has not updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }

                
            }

            
            [self reloadTable];
           
        }
        else
        {
            if ([self index]==1001) {
                
                xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><subscriberID>%@</subscriberID><effectiveDate>%@</effectiveDate><expiryDate>%@</expiryDate><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID><subscriberMemberID>%@</subscriberMemberID><subscriberFirstName>%@</subscriberFirstName><subscriberLastName>%@</subscriberLastName><subscriberGender>%@</subscriberGender><subscriberDateOfBirth>%@</subscriberDateOfBirth></PatientInsurance> ",str_subscribeID,str_policy_sdate,str_policy_edate,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID,str_patient_MID,str_subscriber_fname,str_subscriber_lname,str_subscriber_Gender,str_DateOfBirth];
                
                /*
                 OLD
                 
                  xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><subscriberID>%@</subscriberID><coPay>%@</coPay><insurancePlanName>%@</insurancePlanName><insurancePolicyNo>%@</insurancePolicyNo><effectiveDate>%@</effectiveDate><visitResetDate>%i</visitResetDate><expiryDate>%@</expiryDate><group>%@</group><visitsAllowed>%@</visitsAllowed><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID><subscriberMemberID>%@</subscriberMemberID><subscriberFirstName>%@</subscriberFirstName><subscriberLastName>%@</subscriberLastName><subscriberGender>%@</subscriberGender><subscriberDateOfBirth>%@</subscriberDateOfBirth></PatientInsurance> ",str_subscribeID,str_coPay,str_planName,str_policyNo,str_policy_sdate,VisitResetDate,str_policy_edate,str_group,str_visitAllowed,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID,str_patient_MID,str_subscriber_fname,str_subscriber_lname,str_subscriber_Gender,str_DateOfBirth];
                 
                 
                 */
                
                
                
            }
            else{
                xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><id>%@</id><subscriberID>%@</subscriberID><effectiveDate>%@</effectiveDate><expiryDate>%@</expiryDate><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID><subscriberMemberID>%@</subscriberMemberID><subscriberFirstName>%@</subscriberFirstName><subscriberLastName>%@</subscriberLastName><subscriberGender>%@</subscriberGender><subscriberDateOfBirth>%@</subscriberDateOfBirth></PatientInsurance> ",id_insurence,str_subscribeID,str_policy_sdate,str_policy_edate,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID,str_patient_MID,str_subscriber_fname,str_subscriber_lname,str_subscriber_Gender,str_DateOfBirth];
                
                
                /*
                 OLD
                 
                   xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientInsurance><id>%@</id><subscriberID>%@</subscriberID><coPay>%@</coPay><insurancePlanName>%@</insurancePlanName><insurancePolicyNo>%@</insurancePolicyNo><effectiveDate>%@</effectiveDate><visitResetDate>%i</visitResetDate><expiryDate>%@</expiryDate><group>%@</group><visitsAllowed>%@</visitsAllowed><insuredPartyID>%i</insuredPartyID><additionalInformation>%@</additionalInformation><patientID>%@</patientID><insuranceID>%@</insuranceID><subscriberMemberID>%@</subscriberMemberID><subscriberFirstName>%@</subscriberFirstName><subscriberLastName>%@</subscriberLastName><subscriberGender>%@</subscriberGender><subscriberDateOfBirth>%@</subscriberDateOfBirth></PatientInsurance> ",id_insurence,str_subscribeID,str_coPay,str_planName,str_policyNo,str_policy_sdate,VisitResetDate,str_policy_edate,str_group,str_visitAllowed,insuredPartyID.intValue,str_additionalInfo,appdalegate.patientId,str_companyID,str_patient_MID,str_subscriber_fname,str_subscriber_lname,str_subscriber_Gender,str_DateOfBirth];
                 
                 
                 
                 */
                
                
            }
  
               NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
              NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
            [request setURL:url];
     
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            NSURLResponse* response;
            NSError *requestError = NULL;
            NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
            NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
         
            
                if ([self index]==1001) {
                if ([responseString isEqualToString:@"success"]) {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your insurance information has been successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else
                {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your insurance information has not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            
            else
            {
                if ([responseString isEqualToString:@"success"]) {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your insurance information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else
                {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your insurance information has not updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }

                             
            [self reloadTable];
           
        }
        
        
    }
    
}

-(void)reloadTable
{
    [self.addView removeFromSuperview];
    [vi removeFromSuperview];
    self.working=NO;
    [self setOrientchange:NO];
    [self setListVisible:NO];
    [self myOrientation];
    
    
}

- (IBAction)next:(id)sender {
    self.tabBarController.selectedIndex=5;
}




- (IBAction)selectClicked:(id)sender {
    UIButton *btr=(UIButton *)sender;
    if (btr.tag==1) {
        
//        NSArray * arr1 = [[NSArray alloc] initWithObjects:@"Self",@"Spouse",@"Child",@"Mother",@"Father",@"Other",@"Unknown",nil];
        
        if(dropDown == nil) {
            CGFloat f = 120;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:insurePartyTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    if (btr.tag==3) {
        
        NSArray * arr1 = [[NSArray alloc] initWithObjects:@"Male",@"Female",nil];
        
        if(dropDown == nil) {
            CGFloat f = 60;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:arr1 IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    
    if (btr.tag==2) {
        
     /*
        
      if(dropDown == nil) {
      butt=btr;
      CGFloat f = 30*10;
      [self addsearchBarOnButton:btr.frame];
      temper=payers;
      dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:payers IA:nil D:@"down"];
      
      dropDown.delegate = self;
      }
      */
        
        if(dropDown == nil) {
            
            CGFloat f = 25*[insurenceTypeName count];
//            butt =btr;
           
            
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:insurenceTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
            
    }
}


- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    if ([[edit_InsuredParty titleForState:UIControlStateNormal] isEqualToString:@"Self"]||[[edit_InsuredParty titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
        [self selectSelf];
    }
    else
    {
        [self unselectSelf];
    }
    
}


-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

-(void) unselectSelf
{
    [lbl_edit_patient_MID setHidden:NO];
    [lbl_edit_subscriber_fname setHidden:NO];
    [lbl_edit_subscriber_lname setHidden:NO];
    [lbl_edit_subscriber_Gender setHidden:NO];
    [lbl_edit_DateOfBirth setHidden:NO];
    
    [edit_patient_MID setHidden:NO];
    [edit_subscriber_fname setHidden:NO];
    [edit_subscriber_lname setHidden:NO];
    [edit_subscriber_Gender setHidden:NO];
    [edit_subscriber_genderID setHidden:NO];
    [edit_DateOfBirth setHidden:NO];
    [edit_Save setFrame:CGRectMake(293, 640, 120, 38)];
    [edit_Cancel setFrame:CGRectMake(120, 640, 120, 38)];
    [sc1 setContentSize:CGSizeMake(525,sc1.frame.size.height+300)];
}
-(void)selectSelf
{
    [lbl_edit_patient_MID setHidden:YES];
    [lbl_edit_subscriber_fname setHidden:YES];
    [lbl_edit_subscriber_lname setHidden:YES];
    [lbl_edit_subscriber_Gender setHidden:YES];
    [lbl_edit_DateOfBirth setHidden:YES];
    
    [edit_patient_MID setHidden:YES];
    [edit_subscriber_fname setHidden:YES];
    [edit_subscriber_lname setHidden:YES];
    [edit_subscriber_Gender setHidden:YES];
    [edit_subscriber_genderID setHidden:YES];
    [edit_DateOfBirth setHidden:YES];
    [edit_Save setFrame:CGRectMake(293, 400, 120, 38)];
    [edit_Cancel setFrame:CGRectMake(120, 400, 120, 38)];
    
   }

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag==5 && [textView.text isEqualToString:@"Additional Information"]) {
        textView.text=@"";
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if (textView.tag==5 && textView.text.length==0 ) {
        
        textView.text=@"Additional Information";
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    BOOL a;
    
    if (textField.tag==10) {
        
        
        UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
        
        UIView *popoverView = [[UIView alloc] init];   //view
        popoverView.backgroundColor = [UIColor blackColor];
        
        /*
        datePicker1.datePickerMode = UIDatePickerModeDate;
        [datePicker1 setMinuteInterval:5];
        [datePicker1 setTag:10];
        
        
      
        [datePicker1 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
        
        */
        [popoverView addSubview:datePicker1];
        
        
        popoverContent.view = popoverView;
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.delegate=self;
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:navigationController];
        popoverController.delegate=self;
        
        
        [popoverController setPopoverContentSize:CGSizeMake(320, 220) animated:NO];
        
        
        [popoverController presentPopoverFromRect:textField.frame inView:sc1 permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        a=NO;
        
    }
    else if (textField.tag==11)
    {
        
        UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
        
        UIView *popoverView = [[UIView alloc] init];   //view
        popoverView.backgroundColor = [UIColor blackColor];
        
        /*
        datePicker2.datePickerMode = UIDatePickerModeDate;
        [datePicker2 setMinuteInterval:5];
        [datePicker2 setTag:11];
        
        [datePicker2 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
         
         */
        [popoverView addSubview:datePicker2];
        
        popoverContent.view = popoverView;
        
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.delegate=self;
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:navigationController];
        popoverController.delegate=self;
        
        [popoverController setPopoverContentSize:CGSizeMake(320, 220) animated:NO];
        
        
        [popoverController presentPopoverFromRect:textField.frame inView:sc1 permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        a=NO;
    }
    else if (textField.tag==12)
    {
        
        UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
        
        UIView *popoverView = [[UIView alloc] init];   //view
        popoverView.backgroundColor = [UIColor blackColor];
        
        /*
        
        datePicker3.datePickerMode = UIDatePickerModeDate;
        [datePicker3 setMinuteInterval:5];
        [datePicker3 setTag:12];
        [datePicker3 addTarget:self action:@selector(Result:) forControlEvents:UIControlEventValueChanged];
         
         */
        [popoverView addSubview:datePicker3];
        
        popoverContent.view = popoverView;
        
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.delegate=self;
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:navigationController];
        popoverController.delegate=self;
        
        [popoverController setPopoverContentSize:CGSizeMake(320, 220) animated:NO];
        
        
        [popoverController presentPopoverFromRect:textField.frame inView:sc1 permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        
        a=NO;
    }
    
    else
    {
        a=YES;
    }
    return a;
}
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if (!doneButton) {
        doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                      style:UIBarButtonItemStylePlain
                                                     target:self action:@selector(done:)];
    }
    
    viewController.navigationItem.rightBarButtonItem = doneButton;
}
-(IBAction)done:(id)sender
{
    [popoverController dismissPopoverAnimated:YES];
}

-(void)Result:(UIDatePicker *)picker{
    
    if (picker.tag==10) {
        
        NSDateFormatter *formDay = [[NSDateFormatter alloc] init];
        formDay.dateFormat=@"MM-dd-yyyy";
        NSString *day = [formDay stringFromDate:[datePicker1 date]];
        self.edit_pdate.text=day;
        
    }
    else if(picker.tag==11)
    {
        NSDateFormatter *formDay = [[NSDateFormatter alloc] init];
        formDay.dateFormat=@"MM-dd-yyyy";
        NSString *day = [formDay stringFromDate:[datePicker2 date]];
        self.edit_policy_edate.text=day;
    }
    else if(picker.tag==12)
    {
        NSDateFormatter *formDay = [[NSDateFormatter alloc] init];
        formDay.dateFormat=@"MM-dd-yyyy";
        NSString *day = [formDay stringFromDate:[datePicker3 date]];
        self.edit_DateOfBirth.text=day;
    }
    
}



-(NSString *)InsurenceID:(NSString *)insurenceName
{
    NSString *insurenceID=@"";
    for (NSMutableDictionary* patientObj in InsurenceInfo)
    {
        if ([[patientObj objectForKey:@"name"]isEqualToString:insurenceName]) {
            insurenceID=[patientObj objectForKey:@"id"];
            
          
        }
        
    }
    return insurenceID;
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGSize kbSize=CGSizeMake(0, 0);
    
    // NSDictionary* info = [aNotification userInfo];
    // CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        kbSize=CGSizeMake(1024, 280);
        
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        kbSize=CGSizeMake(768, 200);
    }
    
      
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    sc1.contentInset = contentInsets;
    sc1.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = sc1.frame;
    aRect.size.height -= kbSize.height;
    
    
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        kbSize.height += 200;
    }
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
        [sc1 setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    sc1.contentInset = contentInsets;
    sc1.scrollIndicatorInsets = contentInsets;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL temp=YES;
    if (textField.tag==1) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48) temp=NO; // 48 unichar for 0
            if (character > 57) temp=NO; // 57 unichar for 9
        }
        
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 10) temp=NO;
        
    }
    else if (textField.tag==2) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if ((character >= 65 && character<=90)||(character>=97 && character<=122)||character==32||character==8)
            {
                temp=YES; // 48 unichar for 0
            }
            else
            {
                temp=NO;
            }
        }
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 30) temp=NO;
    }
    else if (textField.tag==3) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if ((character >= 65 && character<=90)||(character>=97 && character<=122)||character==8||(character>=48 && character<=57))
            {
                temp=YES; // 48 unichar for 0
            }
            else
            {
                temp=NO;
            }
        }
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 30) temp=NO;
    }
    else
    {
        
        temp=YES;
    }
    return temp;
    
}


-(NSString *) saveDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSString *result = [formater stringFromDate:date2];
       return result;
    
}
-(NSString *)viewDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSString *result = [formater stringFromDate:date2];
      return result;
    
}


//-(NSMutableArray*)convertString:(NSMutableArray *)Mutarr
//
//{
//    NSMutableArray *array=[[NSMutableArray alloc]init];
//    for (int i=0;i<=[Mutarr count]-1;++i) {
//        NSMutableDictionary *dictionary=[Mutarr objectAtIndex:i];
//          NSString *str=[dictionary objectForKey:@"name"];
//        
//        
//        [array addObject:str];
//        dictionary=nil;
//        str=nil;
//      }
//    
//    return array;
//    
//}


-(void)addDropDown
{
    insurePartyTypeArr=[[NSMutableArray alloc]init];
    insurePartyXMLList *insureList=[[insurePartyXMLList alloc]init];
    insurePartyTypeArr=[insureList insurePartyTypeList];
    
    insurePartyTypeName=[insurePartyTypeArr valueForKey:@"name"];
    insurePartyTypeID=[insurePartyTypeArr valueForKey:@"id"];
    
    
    IndexAppDelegate *appdel=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    insurenceTypeArr=[[NSMutableArray alloc]init];
    patientInsurenceData *insurenceTypeList=[[patientInsurenceData alloc]init];
    insurenceTypeArr=[insurenceTypeList patientInsurenceDataByXML:appdel.registationID];
    
    insurenceTypeName=[insurenceTypeArr valueForKey:@"name"];
    insurenceTypeID=[insurenceTypeArr valueForKey:@"id"];
    
    
    
}

@end
