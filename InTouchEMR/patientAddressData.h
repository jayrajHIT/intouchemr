//
//  patientAddressData.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 18/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface patientAddressData : NSObject<NSXMLParserDelegate>
{
    
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
    NSMutableString *currentNodeContent;
}
-(NSMutableArray *)patientAddressDataByXML:(NSString *)patientID registationNO:(NSString *)registationID;
@end
