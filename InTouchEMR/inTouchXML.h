//
//  inTouchXML.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 27/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface inTouchXML : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)getnewsletterXML:(NSString *)registationNO patientID:(NSString *)patientID;

@end
