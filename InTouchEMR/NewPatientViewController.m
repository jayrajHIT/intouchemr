//
//  NewPatientViewController.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/06/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "NewPatientViewController.h"
#import "IndexAppDelegate.h"
#import "TabBarView.h"
#import "NSString+validation.h"
#import "TabBarView.h"
#import "QuartzCore/QuartzCore.h"
#import "physicianList.h"
#import "SmokingStatusXMLData.h"
#import "EmploymentTypeXMLData.h"
#import "TitleTypeList.h"
#import "genderTypeList.h"
#import "maritalStatusList.h"
#import "EthnicOriginXMLData.h"
#import "studentTypeXMLList.h"

@interface NewPatientViewController ()

@end

@implementation NewPatientViewController
@synthesize portrait_step2,portrait_step3,landscape_step2,landscape_step3,step2,step3;
@synthesize portraitView,landscapeView,error;
@synthesize firstName,MiddleName,LastName,UserName,Password,RePassword,email,Title,Gender,matrialStatus,dob,SmokingStatus;
@synthesize NickName,SsnNo,Status,Student,PaymentStatus,RaceOrigin,JobType,CurrentEmp,EmpAddress,OtherInfo,PCPRelease,empName,physician;
@synthesize isMinor,minor_fatherName,minor_motherName,gurantor_firstName,gurantor_middleName,gurantor_lastName,gurantor_address,gurantor_city,gurantor_state,gurantor_zip,gurantor_phone;

@synthesize landscapefirstName,landscapeMiddleName,landscapeLastName,landscapeUserName,landscapePassword,landscapeRePassword,landscapeemail,landscapeTitle,landscapeGender,landscapematrialStatus,landscapedob,landscapeSmokingStatus;
@synthesize landscapeNickName,landscapeSsnNo,landscapeStatus,landscapeStudent,landscapePaymentStatus,landscapeRaceOrigin,landscapeJobType,landscapeCurrentEmp,landscapeEmpAddress,landscapeOtherInfo,landscapePCPRelease,landscapeEmpName,landscapephysician;
@synthesize landscapeisMinor,landscapeminor_fatherName,landscapeminor_motherName,landscapegurantor_firstName,landscapegurantor_middleName,landscapegurantor_lastName,landscapegurantor_address,landscapegurantor_city,landscapegurantor_state,landscapegurantor_zip,landscapegurantor_phone;
@synthesize portFirstName,portLastName,portuserName,portpassword,portrepassword,porttitle,portDoB,portemail,portGender,portstatus,portmstatus,portPaymentStatus,portPCPrelease;
@synthesize landFirstName,landLastName,landuserName,landpassword,landrepassword,landtitle,landDoB,landemail,landGender,landstatus,landmstatus,landPaymentStatus,landPCPrelease;

@synthesize portminor_FatherName,portminor_MotherName,portgurantor_firstName,portgurantor_middleName,portgurantor_LastName,portgurantor_add,portgurantor_city,portgurantor_state,portgurantor_zip,portgurantor_phone;
@synthesize landminor_FatherName,landminor_MotherName,landgurantor_firstName,landgurantor_middleName,landgurantor_LastName,landgurantor_add,landgurantor_city,landgurantor_state,landgurantor_zip,landgurantor_phone,popoverController,phone_error;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title=@"Patient Registration";
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
   
    datePicker=[[UIDatePicker alloc]init];//Date picker
    datePicker.frame=CGRectMake(0,0,320, 216);
     datePicker.backgroundColor = [UIColor whiteColor];
    NSDate *date1=[NSDate date];
    [datePicker setMaximumDate:date1];
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMinuteInterval:5];
    [datePicker setTag:10];
    
    
    [datePicker addTarget:self action:@selector(Result) forControlEvents:UIControlEventValueChanged];
    
    
    EmpAddress.layer.cornerRadius=5.0;
    EmpAddress.layer.borderWidth=1.0;
    EmpAddress.layer.borderColor=[UIColor lightGrayColor].CGColor;
    OtherInfo.layer.cornerRadius=5.0;
    OtherInfo.layer.borderWidth=1.0;
    OtherInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
    gurantor_address.layer.cornerRadius=5.0;
    gurantor_address.layer.borderWidth=1.0;
    gurantor_address.layer.borderColor=[UIColor lightGrayColor].CGColor;
    landscapeEmpAddress.layer.cornerRadius=5.0;
    landscapeEmpAddress.layer.borderWidth=1.0;
    landscapeEmpAddress.layer.borderColor=[UIColor lightGrayColor].CGColor;
    landscapeOtherInfo.layer.cornerRadius=5.0;
    landscapeOtherInfo.layer.borderWidth=1.0;
    landscapeOtherInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
    landscapegurantor_address.layer.cornerRadius=5.0;
    landscapegurantor_address.layer.borderWidth=1.0;
    landscapegurantor_address.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    
    [isMinor setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [isMinor setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    [landscapeisMinor setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [landscapeisMinor setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    
    [CurrentEmp setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [CurrentEmp setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    [landscapeCurrentEmp setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [landscapeCurrentEmp setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];

     [self addDropDown];
    
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    [self myOrientation];
    
    // Do any additional setup after loading the view from its nib.
    
//    SmokingStatusXMLData *smokingStataus=[[SmokingStatusXMLData alloc]init];
//    NSMutableArray *arr=[smokingStataus SmokingStatusList];
//    NSLog(@"%@",arr);
//    
//    EmploymentTypeXMLData *empType=[[EmploymentTypeXMLData alloc]init];
//    NSMutableArray *arr1=[empType EmployerTypeList];
//    NSLog(@"%@",arr1);
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
   
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	
    if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
        orientation=newOrientation;
        
        [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
}

-(void)myOrientation
{
      
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
        [self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        [self textValue];
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        [self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
           [self textValue];        
	}

}


-(void) clearCurrentView {
    
   
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
        
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}


- (IBAction)GoToStep2:(id)sender {
 
   // [self setError:YES];
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		if ([NSString stringIsEmpty:landscapefirstName.text]==NO) {
           fname=landscapefirstName.text;
        }
        else
        {
            fname=@"";
        }
        if ([NSString stringIsEmpty:landscapeMiddleName.text]==NO) {
            Mname=landscapeMiddleName.text;
        }
        else
        {
            Mname=@"";
        }
        
        if ([NSString stringIsEmpty:landscapeLastName.text]==NO) {
            lname=landscapeLastName.text;
        }
        else
        {
            lname=@"";
        }
        
        if ([NSString stringIsEmpty:landscapeUserName.text]==NO) {
            uname=landscapeUserName.text;
        }
        else
        {
            uname=@"";
        }
        
        if ([NSString stringIsEmpty:landscapePassword.text]==NO) {
            Pass=landscapePassword.text;
        }
        else
        {
            Pass=@"";
        }
        if ([NSString stringIsEmpty:landscapeRePassword.text]==NO) {
            RePass=landscapeRePassword.text;
        }
        else
        {
            RePass=@"";
        }
        if ([NSString stringIsEmpty:landscapeemail.text]==NO) {
            Emailid=landscapeemail.text;
        }
        else
        {
            Emailid=@"";
        }
        if ([NSString stringIsEmpty:landscapedob.text]==NO) {
            date=[self saveDate:landscapedob.text];
        }
        else
        {
            date=@"";
        }
       
        
        
            gender=[landscapeGender titleForState:UIControlStateNormal];
        
        
        if([[landscapeSmokingStatus titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            Sstatus=@"";
        }
        else
        {
            int a=[smokingName indexOfObject:[landscapeSmokingStatus titleForState:UIControlStateNormal]];
            Sstatus=[smokingID objectAtIndex:a];
            
           
        }
       
        if([[landscapeTitle titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            title=@"";
        }
        else
        {
        int z=[titleTypeName indexOfObject:[landscapeTitle titleForState:UIControlStateNormal]];
            
        title=[titleTypeID objectAtIndex:z];
        }
        
        if([[landscapematrialStatus titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            mstatus=@"0";
        }
        else
        {
            
            int a=[maritalStatusName indexOfObject:[landscapematrialStatus titleForState:UIControlStateNormal]];
            mstatus=[maritalStatusID objectAtIndex:a];
            
        }
        
               
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        if ([NSString stringIsEmpty:firstName.text]==NO) {
            fname=firstName.text;
        }
        else
        {
            fname=@"";
        }
        if ([NSString stringIsEmpty:MiddleName.text]==NO) {
            Mname=MiddleName.text;
        }
        else
        {
            Mname=@"";
        }
        
        if ([NSString stringIsEmpty:LastName.text]==NO) {
            lname=LastName.text;
        }
        else
        {
            lname=@"";
        }
       
        if ([NSString stringIsEmpty:UserName.text]==NO) {
            uname=UserName.text;
        }
        else
        {
            uname=@"";
        }

        if ([NSString stringIsEmpty:Password.text]==NO) {
            Pass=Password.text;
        }
        else
        {
            Pass=@"";
        }
        if ([NSString stringIsEmpty:RePassword.text]==NO) {
           RePass=RePassword.text;
        }
        else
        {
            RePass=@"";
        }
        if ([NSString stringIsEmpty:email.text]==NO) {
            Emailid=email.text;
        }
        else
        {
            Emailid=@"";
        }
        if ([NSString stringIsEmpty:dob.text]==NO) {
           date=[self saveDate:dob.text];
        }
        else
        {
            date=@"";
        }
        
            gender=[Gender titleForState:UIControlStateNormal];
        
        if([[SmokingStatus titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            Sstatus=@"";
        }
        else
        {
            int a=[smokingName indexOfObject:[SmokingStatus titleForState:UIControlStateNormal]];
            Sstatus=[smokingID objectAtIndex:a];
        }
        
        
        if([[Title titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            title=@"";
        }
        else
        {
        int a=[titleTypeName indexOfObject:[Title titleForState:UIControlStateNormal]];
        
        title=[titleTypeID objectAtIndex:a];
        }
        
        if([[matrialStatus titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            mstatus=@"0";
        }
        else
        {
        
        int a=[maritalStatusName indexOfObject:[matrialStatus titleForState:UIControlStateNormal]];
        mstatus=[maritalStatusID objectAtIndex:a];
       
        }
  
        
	}
    
    
    if([NSString stringIsEmpty:fname])
    {
        portFirstName.textColor=[UIColor redColor];
        landFirstName.textColor=[UIColor redColor];
     //   [self setError:YES];
    }
    else{
       portFirstName.textColor=[UIColor blackColor];
        landFirstName.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    
    if([NSString stringIsEmpty:lname])
    {
        portLastName.textColor=[UIColor redColor];
        landLastName.textColor=[UIColor redColor];
     //   [self setError:YES];
    }
    else
    {
        portLastName.textColor=[UIColor blackColor];
        landLastName.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    /*
    if([NSString stringIsEmpty:uname])
    {
        portuserName.textColor=[UIColor redColor];
        landuserName.textColor=[UIColor redColor];
    //    [self setError:YES];
    }
    else
    {
        portuserName.textColor=[UIColor blackColor];
        landuserName.textColor=[UIColor blackColor];
      //  [self setError:NO];
    }
    */
    if([NSString stringIsEmpty:Pass])
    {
       portpassword.textColor=[UIColor redColor];
        landpassword.textColor=[UIColor redColor];
    //    [self setError:YES];
    }
    else
    {
        portpassword.textColor=[UIColor blackColor];
        landpassword.textColor=[UIColor blackColor];
     //   [self setError:NO];
    }
    if([NSString stringIsEmpty:RePass])
    {
        portrepassword.textColor=[UIColor redColor];
        landrepassword.textColor=[UIColor redColor];
    //    [self setError:YES];
    }
    else
    {
        portrepassword.textColor=[UIColor blackColor];
        landrepassword.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    if([NSString stringIsEmpty:Emailid])
    {
        portemail.textColor=[UIColor redColor];
        landemail.textColor=[UIColor redColor];
     //   [self setError:YES];
    }
    else
    {
        portemail.textColor=[UIColor blackColor];
        landemail.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    if([NSString stringIsEmpty:date])
    {
        portDoB.textColor=[UIColor redColor];
        landDoB.textColor=[UIColor redColor];
    //    [self setError:YES];
    }
    else
    {
        portDoB.textColor=[UIColor blackColor];
        landDoB.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    if([gender isEqualToString:@"select one"])
    {
        portGender.textColor=[UIColor redColor];
        landGender.textColor=[UIColor redColor];
    //    [self setError:YES];
    }
    else
    {
        portGender.textColor=[UIColor blackColor];
        landGender.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    
    if([title isEqualToString:@""])
    {
        porttitle.textColor=[UIColor redColor];
        landtitle.textColor=[UIColor redColor];
     //   [self setError:YES];
    }
    else
    {
        porttitle.textColor=[UIColor blackColor];
        landtitle.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    if([mstatus isEqualToString:@"0"])
    {
        portmstatus.textColor=[UIColor redColor];
        landmstatus.textColor=[UIColor redColor];
    //    [self setError:YES];
    }
    else
    {
        portmstatus.textColor=[UIColor blackColor];
        landmstatus.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    // [uname isEqualToString:@""]
    if ([fname isEqualToString:@""]||[lname isEqualToString:@""]||[Pass isEqualToString:@""]||[RePass isEqualToString:@""]||[Emailid isEqualToString:@""]||[date isEqualToString:@""]||[gender isEqualToString:@"select one"]||([title isEqualToString:@""])||([mstatus isEqualToString:@"0"])) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
                
        if (![self NSStringIsValidEmail:Emailid]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email is an invalid format" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            portemail.textColor=[UIColor redColor];
            landemail.textColor=[UIColor redColor];
        }
        else
        {
            portemail.textColor=[UIColor blackColor];
            landemail.textColor=[UIColor blackColor];
        if ([Pass isEqualToString:RePass]) {
            portpassword.textColor=[UIColor blackColor];
            landpassword.textColor=[UIColor blackColor];
            portrepassword.textColor=[UIColor blackColor];
            landrepassword.textColor=[UIColor blackColor];
          
            NSString *validation=[self ValidationUserName:uname withEmail:Emailid];
            
            NSLog(@"%@",validation);
            
            
            if ([validation isEqualToString:@"0"]) {
                /*
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Username already exsit" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                 */
                NSLog(@"InTouchEMR");
                 
                
            }
            else if([validation isEqualToString:@"00"])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email Id already exsit" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else if([validation isEqualToString:@"1"])
            {
            
            [landscapeView addSubview:landscape_step2];
            [portraitView addSubview:portrait_step2];
                

        }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
        {
            portpassword.textColor=[UIColor redColor];
            landpassword.textColor=[UIColor redColor];
            portrepassword.textColor=[UIColor redColor];
            landrepassword.textColor=[UIColor redColor];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Password fields do not match." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
         
        }
   
    }
}
- (IBAction)GoToStep1:(id)sender {
    [portrait_step2 removeFromSuperview];
    [landscape_step2 removeFromSuperview];
}

- (IBAction)GoToStep3:(id)sender {
  //  [self setError:NO];
  //  status=2;
  //  paymentStatus=@"error";
    NSString *job,*physician11;
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
        if([NSString stringIsEmpty:landscapeNickName.text]==NO)
        {
            nickName=landscapeNickName.text;
        }
        else
        {
            nickName=@"";
        }
        if([NSString stringIsEmpty:landscapeSsnNo.text]==NO)
        {
            ssnno=landscapeSsnNo.text;
        }
        else
        {
            ssnno=@"";
        }
        
        if ([[landscapeStatus titleForState:UIControlStateNormal] isEqualToString:@"ACTIVE"]) {
            status=1;
        }
        else if([[landscapeStatus titleForState:UIControlStateNormal] isEqualToString:@"INACTIVE"])
        {
            status=2;
        }
        else
        {
            status=0;
        }
        
        if([[landscapeStudent titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            student=@"0";
        }
        else
        {
            int x=[studentTypeName indexOfObject:[landscapeStudent titleForState:UIControlStateNormal]];
            student=[studentTypeID objectAtIndex:x];
        }
        
     
        
        if ([[landscapePaymentStatus titleForState:UIControlStateNormal] isEqualToString:@"Self Paid"]) {
            paymentStatus=@"SELF";
        }
        else if ([[landscapePaymentStatus titleForState:UIControlStateNormal] isEqualToString:@"Insurance Paid"])
        {
            paymentStatus=@"INSURANCE";
        }
        else
        {
            paymentStatus=@"error";
        }
      
        if ([[landscapeRaceOrigin titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
            
            raceOrigin=@"0";
        }
        else
        {
        int index=[EthnicOriginName indexOfObject:[landscapeRaceOrigin titleForState:UIControlStateNormal]];
        raceOrigin=[EthnicOriginID objectAtIndex:index];
            
        }
        
        
        job=[landscapeJobType titleForState:UIControlStateNormal];
        
        physician11=[landscapephysician titleForState:UIControlStateNormal];
        
       
        if([NSString stringIsEmpty:landscapeEmpName.text]==NO)
        {
            empname=landscapeEmpName.text;
        }
        else
        {
            empname=@"";
        }

        if ([landscapeCurrentEmp isOn]) {
            currentEmp=1;
        }
        else
        {
            currentEmp=0;
        }
      
        if([NSString stringIsEmpty:landscapeEmpAddress.text]==NO)
        {
            empAdd=landscapeEmpAddress.text;
        }
        else
        {
         empAdd=@"";
        }
        
        if([physician11 isEqualToString:@"select one"])
        {
            physicianValue=@"";
        }
        else
        {
            int x=[physicianTypeName indexOfObject:physician11];
            physicianValue=[physicianID objectAtIndex:x];
        }
        
        if([job isEqualToString:@"select one"])
        {
            jobtype=@"";
        }
        else
        {
            int x=[jobTypeName indexOfObject:job];
            jobtype=[jobTypeID objectAtIndex:x];
        }
        
        
        if([NSString stringIsEmpty:landscapeOtherInfo.text]==NO)
        {
           otherInfo=landscapeOtherInfo.text;
        }
        else
        {
            otherInfo=@"";
        }
        
        
        
      //  PCPrelese=[self PCPrelease:[landscapePCPRelease titleForState:UIControlStateNormal]];
        
       
        
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
      //  status=2;
        if([NSString stringIsEmpty:NickName.text]==NO)
        {
            nickName=NickName.text;
        }
        else
        {
            nickName=@"";
        }
        if([NSString stringIsEmpty:SsnNo.text]==NO)
        {
            ssnno=SsnNo.text;
        }
        else
        {
            ssnno=@"";
        }
        
        
        if ([[Status titleForState:UIControlStateNormal] isEqualToString:@"ACTIVE"]) {
            status=1;
        }
        else if([[Status titleForState:UIControlStateNormal] isEqualToString:@"INACTIVE"])
        {
            status=2;
        }
        else
        {
            status=0;
        }
        
        if([[Student titleForState:UIControlStateNormal] isEqualToString:@"select one"])
        {
            student=@"0";
        }
        else
        {
            int x=[studentTypeName indexOfObject:[Student titleForState:UIControlStateNormal]];
            student=[studentTypeID objectAtIndex:x];
        }
        
        
       // student=[self studentID:[Student titleForState:UIControlStateNormal]];
        
        
       // paymentStatus=@"error";
        if ([[PaymentStatus titleForState:UIControlStateNormal] isEqualToString:@"Self Paid"]) {
            paymentStatus=@"SELF";
        }
        else if ([[PaymentStatus titleForState:UIControlStateNormal] isEqualToString:@"Insurance Paid"])
        {
            paymentStatus=@"INSURANCE";
        }
        else
        {
            paymentStatus=@"error";
        }
        
        if ([[RaceOrigin titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
            
            raceOrigin=@"0";
        }
        else
        {
            int index=[EthnicOriginName indexOfObject:[RaceOrigin titleForState:UIControlStateNormal]];
            raceOrigin=[EthnicOriginID objectAtIndex:index];
            
        }
        
        
        
        job=[JobType titleForState:UIControlStateNormal];
        physicianValue=[physician titleForState:UIControlStateNormal];
        
        
        if ([job isEqualToString:@"select one"]) {
            jobtype=@"";
        }
        else
        {
            int x=[jobTypeName indexOfObject:job];
            jobtype=[jobTypeID objectAtIndex:x];
        }
        if ([physicianValue isEqualToString:@"select one"]) {
            physicianValue=@"";
        }
        else
        {
            int x=[physicianTypeName indexOfObject:physicianValue];
            physicianValue=[physicianID objectAtIndex:x];
            
        }

        
        if([NSString stringIsEmpty:empName.text]==NO)
        {
            empname=empName.text;
        }
        else
        {
            empname=@"";
        }
        

        if ([CurrentEmp isOn]) {
            currentEmp=1;
        }
        else
        {
            currentEmp=0;
        }
        
        if([NSString stringIsEmpty:EmpAddress.text]==NO)
        {
            empAdd=EmpAddress.text;
        }
        else
        {
            empAdd=@"";
        }
        if([NSString stringIsEmpty:OtherInfo.text]==NO)
        {
            otherInfo=OtherInfo.text;
        }
        else
        {
            otherInfo=@"";
        }
        
                    
	}
    
       
    if(status==0)
    {
        portstatus.textColor=[UIColor redColor];
        landstatus.textColor=[UIColor redColor];
      //  [self setError:YES];
        
    }
    else
    {
     //   [self setError:NO];
        portstatus.textColor=[UIColor blackColor];
        landstatus.textColor=[UIColor blackColor];
    }
    if ([paymentStatus isEqualToString:@"error"]) {
      //  [self setError:YES];
        portPaymentStatus.textColor=[UIColor redColor];
        landPaymentStatus.textColor=[UIColor redColor];
    }
    else
    {
       // [self setError:NO];
        portPaymentStatus.textColor=[UIColor blackColor];
        landPaymentStatus.textColor=[UIColor blackColor];

    }
    
    
    if ((status==0)||[paymentStatus isEqualToString:@"error"]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        age=[self agefinder:date];
       
        if (age.intValue>=18) {
           
            [landscapeisMinor setOn:NO];
            [isMinor setOn:NO];
        }
        else
        {
            isminor=0;
            [landscapeisMinor setOn:YES];
            [isMinor setOn:YES];
        }

        [portraitView addSubview:portrait_step3];
        [landscapeView addSubview:landscape_step3];
    }
    
    
    
}

- (IBAction)GoBackStep2:(id)sender {
    [portrait_step3 removeFromSuperview];
    [landscape_step3 removeFromSuperview];
}

- (IBAction)Submit:(id)sender {
    [self setError:NO];
    [self setPhone_error:NO];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
        if ([landscapeisMinor isOn]) {
            isminor=1;
           
        }
        else
        {
            isminor=0;
            
        }

        if ([NSString stringIsEmpty:landscapeminor_fatherName.text]==NO) {
            minor_fname=landscapeminor_fatherName.text;
        }
        else
        {
            minor_fname=@"";
        }

        if ([NSString stringIsEmpty:landscapeminor_motherName.text]==NO) {
             minor_mname=landscapeminor_motherName.text;
        }
        else
        {
            minor_mname=@"";
        }

        if ([NSString stringIsEmpty:landscapegurantor_firstName.text]==NO) {
           Gurantor_fname=landscapegurantor_firstName.text;
        }
        else
        {
            Gurantor_fname=@"";
        }
        if ([NSString stringIsEmpty:landscapegurantor_middleName.text]==NO) {
             Gurantor_mname=landscapegurantor_middleName.text;
        }
        else
        {
            Gurantor_mname=@"";
        }
        if ([NSString stringIsEmpty:landscapegurantor_lastName.text]==NO) {
            Gurantor_lname=landscapegurantor_lastName.text;
        }
        else
        {
            Gurantor_lname=@"";
        }
        if ([NSString stringIsEmpty:landscapegurantor_address.text]==NO) {
            Gurantor_add=landscapegurantor_address.text;
        }
        else
        {
            Gurantor_add=@"";
        }
        if ([NSString stringIsEmpty:landscapegurantor_city.text]==NO) {
            Gurantor_City=landscapegurantor_city.text;
        }
        else
        {
            Gurantor_City=@"";
        }
        if ([NSString stringIsEmpty:landscapegurantor_state.text]==NO) {
            Gurantor_State=landscapegurantor_state.text;
        }
        else
        {
            Gurantor_State=@"";
        }
        if ([NSString stringIsEmpty:landscapegurantor_zip.text]==NO) {
            Gurantor_Zip=landscapegurantor_zip.text;
        }
        else
        {
            Gurantor_Zip=@"";
        }
        if ([NSString stringIsEmpty:landscapegurantor_phone.text]==NO) {
             Gurantor_Phone=landscapegurantor_phone.text;        }
        else
        {
            Gurantor_Phone=@"";
        }

           
    } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
       
        if ([isMinor isOn]) {
            isminor=1;
            
        }
        else
        {
            isminor=0;
           
        }

        
        if ([NSString stringIsEmpty:minor_fatherName.text]==NO) {
            minor_fname=minor_fatherName.text;
        }
        else
        {
            minor_fname=@"";
        }
        
        if ([NSString stringIsEmpty:minor_motherName.text]==NO) {
            minor_mname=minor_motherName.text;
        }
        else
        {
            minor_mname=@"";
        }
        
        if ([NSString stringIsEmpty:gurantor_firstName.text]==NO) {
            Gurantor_fname=gurantor_firstName.text;
        }
        else
        {
            Gurantor_fname=@"";
        }
        if ([NSString stringIsEmpty:gurantor_middleName.text]==NO) {
            Gurantor_mname=gurantor_middleName.text;
        }
        else
        {
            Gurantor_mname=@"";
        }
        if ([NSString stringIsEmpty:gurantor_lastName.text]==NO) {
            Gurantor_lname=gurantor_lastName.text;
        }
        else
        {
            Gurantor_lname=@"";
        }
        if ([NSString stringIsEmpty:gurantor_address.text]==NO) {
            Gurantor_add=gurantor_address.text;
        }
        else
        {
            Gurantor_add=@"";
        }
        if ([NSString stringIsEmpty:gurantor_city.text]==NO) {
            Gurantor_City=gurantor_city.text;
        }
        else
        {
            Gurantor_City=@"";
        }
        if ([NSString stringIsEmpty:gurantor_state.text]==NO) {
            Gurantor_State=gurantor_state.text;
        }
        else
        {
            Gurantor_State=@"";
        }
        if ([NSString stringIsEmpty:gurantor_zip.text]==NO) {
            Gurantor_Zip=gurantor_zip.text;
        }
        else
        {
            Gurantor_Zip=@"";
        }
        if ([NSString stringIsEmpty:gurantor_phone.text]==NO) {
            Gurantor_Phone=gurantor_phone.text;        }
        else
        {
            Gurantor_Phone=@"";
        }

        
    }
   
    if (isminor==1) {
        
        if([NSString stringIsEmpty:minor_fname])
        {
            
            portminor_FatherName.textColor=[UIColor redColor];
            landminor_FatherName.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portminor_FatherName.textColor=[UIColor blackColor];
            landminor_FatherName.textColor=[UIColor blackColor];
        }
        if([NSString stringIsEmpty:minor_mname])
        {
            portminor_MotherName.textColor=[UIColor redColor];
            landminor_MotherName.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            
            portminor_MotherName.textColor=[UIColor blackColor];
            landminor_MotherName.textColor=[UIColor blackColor];
        }
        if([NSString stringIsEmpty:Gurantor_fname])
        {
            portgurantor_firstName.textColor=[UIColor redColor];
            landgurantor_firstName.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portgurantor_firstName.textColor=[UIColor blackColor];
            landgurantor_firstName.textColor=[UIColor blackColor];
        }
        
        if([NSString stringIsEmpty:Gurantor_lname])
        {
            
            portgurantor_LastName.textColor=[UIColor redColor];
            landgurantor_LastName.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portgurantor_LastName.textColor=[UIColor blackColor];
            landgurantor_LastName.textColor=[UIColor blackColor];
        }
        if([NSString stringIsEmpty:Gurantor_add])
        {
            portgurantor_add.textColor=[UIColor redColor];
            landgurantor_add.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portgurantor_add.textColor=[UIColor blackColor];
            landgurantor_add.textColor=[UIColor blackColor];
        }
        if([NSString stringIsEmpty:Gurantor_City])
        {
            portgurantor_city.textColor=[UIColor redColor];
            landgurantor_city.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portgurantor_city.textColor=[UIColor blackColor];
            landgurantor_city.textColor=[UIColor blackColor];
        }
        if([NSString stringIsEmpty:Gurantor_State])
        {
            portgurantor_state.textColor=[UIColor redColor];
            landgurantor_state.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portgurantor_state.textColor=[UIColor blackColor];
            landgurantor_state.textColor=[UIColor blackColor];
        }
        if([NSString stringIsEmpty:Gurantor_Zip])
        {
            portgurantor_zip.textColor=[UIColor redColor];
            landgurantor_zip.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portgurantor_zip.textColor=[UIColor blackColor];
            landgurantor_zip.textColor=[UIColor blackColor];
        }
        if([NSString stringIsEmpty:Gurantor_Phone])
        {
            portgurantor_phone.textColor=[UIColor redColor];
            landgurantor_phone.textColor=[UIColor redColor];
            [self setError:YES];
        }
        else
        {
            portgurantor_phone.textColor=[UIColor blackColor];
            landgurantor_phone.textColor=[UIColor blackColor];
        }
        
        int length = [self getLength:Gurantor_Phone];
        
        if (length!=10) {
            gurantor_phone.textColor=[UIColor redColor];
            [self setPhone_error:YES];
            
        }
        else
        {
            gurantor_phone.textColor=[UIColor blackColor];

        }
        

    }
   
    
    if (((isminor==1)&&([self error]==NO)&&([self phone_error]==NO))||(isminor==0)) {
        
       
         IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];

     
        
        NSURL *url = [NSURL URLWithString:appdalegate.addPatientUrl];
        NSString *xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?> <Patient><registrationID>%@</registrationID><firstName>%@</firstName><lastName>%@</lastName><middleName>%@</middleName><titleID>%@</titleID><gender>%@</gender><email>%@</email><employerName>%@</employerName><nickName>%@</nickName><otherInformation>%@</otherInformation><studentTypeID>%i</studentTypeID><ssn>%@</ssn><smokingStatusID>%@</smokingStatusID><jobTypeID>%@</jobTypeID><isWorking>%i</isWorking><employerAddress>%@</employerAddress><dateOfBirth>%@</dateOfBirth><maritalStatusID>%@</maritalStatusID><ethnicOriginID>%@</ethnicOriginID><referralPhysicianID>%@</referralPhysicianID><statusId>%i</statusId><paymentStatus>%@</paymentStatus><isMinor>%i</isMinor><minor_mother_name>%@</minor_mother_name><minor_father_name>%@</minor_father_name><guarantorFirstName>%@</guarantorFirstName><guarantorLastName>%@</guarantorLastName><guarantorMiddleName>%@</guarantorMiddleName><guarantorAddress>%@</guarantorAddress><guarantorCity>%@</guarantorCity><guarantorState>%@</guarantorState><guarantorZip>%@</guarantorZip><guarantorPhone>%@</guarantorPhone><userName>%@</userName><password>%@</password></Patient>",appdalegate.registationID,fname,lname,Mname,title,gender,Emailid,empname,nickName,otherInfo,student.intValue,ssnno,Sstatus,jobtype,currentEmp, empAdd,date,mstatus,raceOrigin,physicianValue,status,paymentStatus,isminor,minor_mname, minor_fname,Gurantor_fname,Gurantor_lname,Gurantor_mname,Gurantor_add,Gurantor_City,Gurantor_State,Gurantor_Zip,Gurantor_Phone,uname,Pass];
       
        
        NSLog(@"%@",xmlText);
       
        NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
       
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        [request setURL:url];
        
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLResponse* response;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
     
    
        NSArray *temp=[responseString componentsSeparatedByString:@" "];
       
        patientID=[temp objectAtIndex:0];
        [self redirctToDemographics];
    
        }
                  
 else if (((isminor==1)&&([self error]==NO)&&([self phone_error]==YES)))
 {
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Contact Number must be 10 digits" message:@"The required information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [alert show];
     
 }
    
    
else
{
   
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}
  
    
    
}
-(NSString *)agefinder:(NSString *)dateofbirth
{
    NSDateFormatter *dateFormatObj = [[NSDateFormatter alloc]init];
    [dateFormatObj setDateFormat:@"yyyy-MM-dd"];
    NSDate *birthday = [dateFormatObj dateFromString:dateofbirth];
    
    NSDate* now = [NSDate date];
    
    
    
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age1 = [ageComponents year];
    NSString *str=[NSString stringWithFormat:@"%ld",(long)age1];
    return str;
}

-(NSString *)ValidationUserName:(NSString *)username withEmail:(NSString *)emailid
{
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    NSURL *url = [NSURL URLWithString:appdalegate.emailIdCheckUrl];
    
    NSString *xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?> <Patient><registrationID>%@</registrationID><userName>%@</userName><email>%@</email></Patient>",appdalegate.registationID,username,emailid];
    
   
    NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
   
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse* response;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
  
    return responseString;
}

-(void)redirctToDemographics
{
    fullname=[NSString stringWithFormat:@"%@ %@",fname,lname];
//    if ([NSString stringIsEmpty:age]) {
//        age=@"";
//    }
//    else
//    {
//    NSString *inStr = [NSString stringWithFormat:@" (%@ yr)", age];
//        fullname = [fullname stringByAppendingString:inStr];
//    }
     
  
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setValue:fullname forKey:@"FullName"];
    [dict setValue:date forKey:@"birth"];
    [dict setValue:Emailid forKey:@"email"];
    [dict setValue:fname forKey:@"firstName"];
    [dict setValue:lname forKey:@"lastName"];
    [dict setValue:Mname forKey:@"middleName"];
    [dict setValue:gender forKey:@"gender"];
    [dict setValue:ssnno forKey:@"ssn"];
    [dict setValue:patientID forKey:@"patientID"];
   
    NSLog(@"%@",dict);
    TabBarView *tabBarView=[[TabBarView alloc]initwithArray:dict];
    [self.navigationController pushViewController:tabBarView animated:YES];
    
}
- (IBAction)selectClicked:(id)sender {
    UIButton *btr=(UIButton *)sender;
    if (btr.tag==1) {
        
        if(dropDown == nil) {
            CGFloat f;
            if ([titleTypeName count]>6) {
                f=180;
            }
            else
            {
               f=[titleTypeName count]*30;
            }
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:titleTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==2)
    {
        if(dropDown == nil) {
            CGFloat f = 60;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:genderTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    else if(btr.tag==3)
    {
        if(dropDown == nil) {
            CGFloat f;
            if ([maritalStatusName count]>6) {
                f=180;
            }
            else{
               f=[maritalStatusName count]*30;
            }
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:maritalStatusName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    else if(btr.tag==4)
    {
       
        
        if(dropDown == nil) {
            CGFloat f;
            if ([smokingName count]>7) {
                f=210;
            }
            else
            {
               f=[smokingName count]*30;
            }
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:smokingName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    else if(btr.tag==5)
    {
        NSArray * arry = [[NSArray alloc] initWithObjects:@"ACTIVE",@"INACTIVE",nil];
        
        if(dropDown == nil) {
            CGFloat f = 60;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:arry IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    else if(btr.tag==6)
    {
        
        if(dropDown == nil) {
            CGFloat f = 90;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:studentTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    
    else if(btr.tag==7)
    {
        
        if(dropDown == nil) {
            CGFloat f;
            if ([EthnicOriginName count]>7) {
                f=210;
            }
            else
            {
                f=[EthnicOriginName count]*30;
            }
            
           dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:EthnicOriginName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }

    else if(btr.tag==8)
    {
       
        if(dropDown == nil) {
            CGFloat f;
            if ([jobTypeName count]>6) {
                f=180;
            }
            else
            {
                f=[jobTypeName count]*30;
            }
            
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:jobTypeName IA:nil D:@"down"];            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    else if(btr.tag==9)
    {
        NSArray * arry = [[NSArray alloc] initWithObjects:@"Yes",@"No",nil];
        if(dropDown == nil) {
            CGFloat f = 60;
           dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:arry IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==10)
    {
        NSArray * arry = [[NSArray alloc] initWithObjects:@"Self Paid",@"Insurance Paid",nil];
        if(dropDown == nil) {
            CGFloat f = 60;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:arry IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==11)
    {
       
        if(dropDown == nil) {
            CGFloat f;
            if ([physicianTypeName count]>6) {
                f=180;
            }
            else
            {
                f=[physicianTypeName count]*30;
            }
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:physicianTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
}



- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    BOOL a;
    if (textField.tag==10) {
        
        
        UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
        
        UIView *popoverView = [[UIView alloc] init];   //view
        popoverView.backgroundColor = [UIColor blackColor];
        
        /*
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setMinuteInterval:5];
        [datePicker setTag:10];
        
      
        [datePicker addTarget:self action:@selector(Result) forControlEvents:UIControlEventValueChanged];
         */
         
        [popoverView addSubview:datePicker];
        
        popoverContent.view = popoverView;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.delegate=self;
     
        
        
        
        self.popoverController = [[UIPopoverController alloc]
                        initWithContentViewController:navigationController];
      
        popoverController.delegate=self;
        
        
        [popoverController setPopoverContentSize:CGSizeMake(320, 250) animated:YES];
        
        if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
            
            [popoverController presentPopoverFromRect:textField.frame inView:landscapeView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            
        } else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
            
            [popoverController presentPopoverFromRect:textField.frame inView:portraitView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            
            
        }

        a=NO;
        
    }
    else
    {
        a=YES;
    }
    return a;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if (!doneButton) {
        doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                      style:UIBarButtonItemStylePlain
                                                     target:self action:@selector(done:)];
    }
    
    viewController.navigationItem.rightBarButtonItem = doneButton;
}
-(IBAction)done:(id)sender
{
    [self.popoverController dismissPopoverAnimated:YES];
}
-(void)Result
{
    NSDateFormatter *formDay = [[NSDateFormatter alloc] init];
    formDay.dateFormat=@"MM-dd-yyyy";
    
   // NSDate* serverDate = [formDay date :[datePicker date]];
   // [formDay setDateStyle:NSDateFormatterMediumStyle];
    NSString *day = [formDay stringFromDate:[datePicker date]];
    dob.text = day;
    landscapedob.text=day;
}
//-(int)titleID:(NSString *)str
//{
//    int i=0;
//    if ([str isEqualToString:@"Dr."]) {
//        i=1;
//    }
//    else if ([str isEqualToString:@"Miss."])
//    {
//        i=2;
//    }
//    else if ([str isEqualToString:@"Mr."])
//    {
//        i=3;
//    }
//    else if ([str isEqualToString:@"Mrs."]) {
//        i=4;
//    }
//    else if ([str isEqualToString:@"Ms."])
//    {
//        i=5;
//    }
//   
//    return i;
//    
//}

//-(int)matrialID:(NSString *)str
//{
//    int i=0;
//    if ([str isEqualToString:@"Married"]) {
//        i=1;
//    }
//    else if ([str isEqualToString:@"Single"])
//    {
//        i=2;
//    }
//    else if ([str isEqualToString:@"Unknown"])
//    {
//        i=3;
//    }
//    else if ([str isEqualToString:@"Divorced"])
//    {
//        i=4;
//    }
//    else if ([str isEqualToString:@"Separated"])
//    {
//        i=5;
//    }
//    else if ([str isEqualToString:@"Widowed"])
//    {
//        i=6;
//    }
//    else if ([str isEqualToString:@"Domestic Partnership"])
//    {
//        i=7;
//    }
//    
//    return i;
//    
//}
//
//-(int)studentID:(NSString *)str
//{
//    int i=0;
//    if ([str isEqualToString:@"Full-Time Student"]) {
//        i=1;
//    }
//    else if ([str isEqualToString:@"Not a Student"])
//    {
//        i=2;
//    }
//    else if ([str isEqualToString:@"Part-Time Student"])
//    {
//        i=3;
//    }
//    
//    
//    return i;
//    
//}
//
//-(int)originID:(NSString *)str
//{
//    int i=0;
//    if ([str isEqualToString:@"Affrican/African-American"]) {
//        i=1;
//    }
//    else if ([str isEqualToString:@"Asian"])
//    {
//        i=2;
//    }
//    else if ([str isEqualToString:@"native American"])
//    {
//        i=3;
//    }
//    else if ([str isEqualToString:@"Pacific Islander/Native Hawaiian"]) {
//        i=4;
//    }
//    else if ([str isEqualToString:@"Mixed Race"])
//    {
//        i=5;
//    }
//    else if ([str isEqualToString:@"Other"])
//    {
//        i=6;
//    }
//    else if ([str isEqualToString:@"Caucasian"])
//    {
//        i=7;
//    }
//    
//    return i;
//    
//}


-(void)textValue
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
        
        landscapefirstName.text=firstName.text;
        landscapeMiddleName.text=MiddleName.text;
        landscapeLastName.text=LastName.text;
        landscapeUserName.text=UserName.text;
        landscapePassword.text=Password.text;
        landscapeRePassword.text=RePassword.text;
        landscapeemail.text=email.text;
        landscapedob.text=dob.text;
        [landscapeGender setTitle:[Gender titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [landscapeSmokingStatus setTitle:[SmokingStatus titleForState:UIControlStateNormal]forState:UIControlStateNormal];
        
        [landscapeTitle setTitle:[Title titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        [landscapematrialStatus setTitle:[matrialStatus titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        landscapeNickName.text=NickName.text;
        landscapeSsnNo.text=SsnNo.text;
        [landscapeStatus setTitle:[Status titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        if ([CurrentEmp isOn]) {
            [landscapeCurrentEmp setOn:YES];
        }
        else
        {
            [landscapeCurrentEmp setOn:NO];
        }
       
        [landscapephysician setTitle:[physician titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
//        if ([hippaAgreement isOn]) {
//            [landscapehippaAgreement setOn:YES];
//        }
//        else
//        {
//            [landscapehippaAgreement setOn:NO];
//        }

        
        
        
       [landscapeStudent setTitle:[Student titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [landscapePaymentStatus setTitle:[PaymentStatus titleForState:UIControlStateNormal] forState:UIControlStateNormal];
       [landscapeRaceOrigin setTitle:[RaceOrigin titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [landscapeJobType setTitle:[JobType titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        landscapeEmpName.text=empName.text;
        landscapeEmpAddress.text=EmpAddress.text;
        landscapeOtherInfo.text=OtherInfo.text;
        [landscapePCPRelease setTitle:[PCPRelease titleForState:UIControlStateNormal] forState:UIControlStateNormal];
       
        if ([isMinor isOn]) {
            [landscapeisMinor setOn:YES];
        }
        else
        {
            [landscapeisMinor setOn:NO];
        }
        
        landscapeminor_fatherName.text=minor_fatherName.text;
        landscapeminor_motherName.text=minor_motherName.text;
        landscapegurantor_firstName.text=gurantor_firstName.text;
        landscapegurantor_middleName.text=gurantor_middleName.text;
        landscapegurantor_address.text=gurantor_address.text;
        landscapegurantor_city.text=gurantor_city.text;
        landscapegurantor_state.text=gurantor_state.text;
        landscapegurantor_zip.text=gurantor_zip.text;
        landscapegurantor_phone.text=gurantor_phone.text;
       
        
        
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
                
        
        firstName.text=landscapefirstName.text;
       
        MiddleName.text=landscapeMiddleName.text;
        
        LastName.text=landscapeLastName.text;
       
        UserName.text=landscapeUserName.text;
        
        Password.text=landscapePassword.text;
        
        RePassword.text=landscapeRePassword.text;
      
        email.text=landscapeemail.text;
       
        dob.text=landscapedob.text;
       
        [Gender setTitle:[landscapeGender titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        [SmokingStatus setTitle:[landscapeSmokingStatus titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
       
        [Title setTitle:[landscapeTitle titleForState:UIControlStateNormal] forState:UIControlStateNormal];
               
        [matrialStatus setTitle:[landscapematrialStatus titleForState:UIControlStateNormal] forState:UIControlStateNormal];

        
        
        NickName.text=landscapeNickName.text;
        SsnNo.text=landscapeSsnNo.text;
        [Status setTitle:[landscapeStatus titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        
        [Student setTitle:[landscapeStudent titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [PaymentStatus setTitle:[landscapePaymentStatus titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [RaceOrigin setTitle:[landscapeRaceOrigin titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        [JobType setTitle:[landscapeJobType titleForState:UIControlStateNormal] forState:UIControlStateNormal];
       
        if ([landscapeCurrentEmp isOn]) {
            [CurrentEmp setOn:YES];
        }
        else
        {
            [CurrentEmp setOn:NO];
        }
       
        [physician setTitle:[landscapephysician titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        empName.text=landscapeEmpName.text;
        EmpAddress.text=landscapeEmpAddress.text;
        OtherInfo.text=landscapeOtherInfo.text;
        [PCPRelease setTitle:[landscapePCPRelease titleForState:UIControlStateNormal] forState:UIControlStateNormal];
        
        if ([landscapeisMinor isOn]) {
            [isMinor setOn:YES];
        }
        else
        {
            [isMinor setOn:NO];
        }
        minor_fatherName.text=landscapeminor_fatherName.text;
        minor_motherName.text=landscapeminor_motherName.text;
        gurantor_firstName.text=landscapegurantor_firstName.text;
        gurantor_middleName.text=landscapegurantor_middleName.text;
        gurantor_address.text=landscapegurantor_address.text;
        gurantor_city.text=landscapegurantor_city.text;
        gurantor_state.text=landscapegurantor_state.text;
        gurantor_zip.text=landscapegurantor_zip.text;
        gurantor_phone.text=landscapegurantor_phone.text;
        
	}
    

}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL temp=YES;
    if (textField.tag==1) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48)
            {
                temp=NO; // 48 unichar for 0
            }
                if (character > 57)
                {
                    temp=NO; // 57 unichar for 9
                }
        }
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 5)
        {
            temp=NO;
        }
       
    }
else if (textField.tag==2)  {
    
    
    NSUInteger lengthOfString = string.length;
    for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
        unichar character = [string characterAtIndex:loopIndex];
        if (character < 48) return NO; // 48 unichar for 0
        if (character > 57) return NO; // 57 unichar for 9
    }

    int length = [self getLength:textField.text];
    
    if(length == 10) {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 3) {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"%@-",num];
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 6) {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
}
   else if (textField.tag==11) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48)
            {
                temp=NO; // 48 unichar for 0
            }
            if (character > 57)
            {
                temp=NO; // 57 unichar for 9
            }
        }
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 9)
        {
            temp=NO;
        }
        
    }
    
    else if(textField.tag==0||textField.tag==5||textField.tag==6||textField.tag==7)
    {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if ((character >= 65 && character<=90)||(character>=97 && character<=122)||character==32||character==8||character==39)
            {
                temp=YES; // 48 unichar for 0
            }
            else
            {
                temp=NO;
            }
        }

            }
    
    return temp;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; 
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        
        if (textField.tag==3){
            const int movementDistance = 100; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
           landscapeView.frame=CGRectOffset(landscapeView.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if (textField.tag==5){
            const int movementDistance = 100; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            landscape_step2.frame=CGRectOffset(landscape_step2.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if (textField.tag==1||textField.tag==2){
            const int movementDistance = 250; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            landscape_step3.frame=CGRectOffset(landscape_step3.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if (textField.tag==6){
            const int movementDistance = 200; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            landscape_step3.frame=CGRectOffset(landscape_step3.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if (textField.tag==7){
            const int movementDistance = 100; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            landscape_step3.frame=CGRectOffset(landscape_step3.frame, 0, movement);
            [UIView commitAnimations];
            
        }
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView: textView up: YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView: textView up: NO];
}
- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        
        if (textView.tag==5){
            const int movementDistance = 200; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            landscape_step2.frame=CGRectOffset(landscape_step2.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if (textView.tag==6){
            const int movementDistance = 150; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            landscape_step3.frame=CGRectOffset(landscape_step3.frame, 0, movement);
            [UIView commitAnimations];
            
        }

    }
}

-(NSString *) saveDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSString *result = [formater stringFromDate:date2];
   
    return result;
    
}
-(NSString *)viewDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSString *result = [formater stringFromDate:date2];
   
    return result;
    
}

-(NSString*)formatNumber:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    if(length > 10) {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    return mobileNumber;
}


-(int)getLength:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    return length;
}


-(NSMutableArray*)getPhysician:(NSMutableArray *)Mutarr

{
    NSMutableArray *array=[[NSMutableArray alloc]init];
    for (int i=0;i<=[Mutarr count]-1;++i) {
        NSMutableDictionary *dictionary=[Mutarr objectAtIndex:i];
  
        NSString *str=[dictionary objectForKey:@"physicianName"];
     
        [array addObject:str];
        dictionary=nil;
        str=nil;
    }
   
    return array;
    
}

-(void)addDropDown
{
    smokingStatus=[[NSMutableArray alloc]init];
    SmokingStatusXMLData *smokingstat=[[SmokingStatusXMLData alloc]init];
    smokingStatus=[smokingstat SmokingStatusList];
    smokingName=[smokingStatus valueForKey:@"smokingStatus"];
    smokingID=[smokingStatus valueForKey:@"id"];
    
    EmployerType=[[NSMutableArray alloc]init];
    EmploymentTypeXMLData *jobTypesStatus=[[EmploymentTypeXMLData alloc]init];
    EmployerType=[jobTypesStatus EmployerTypeList];
    jobTypeName=[EmployerType valueForKey:@"employmentType"];
    jobTypeID=[EmployerType valueForKey:@"id"];
  
    
    
    physicianStatus=[[NSMutableArray alloc]init];
    physicianList *pList=[[physicianList alloc]init];
    IndexAppDelegate *appdel=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    physicianStatus=[pList physicianListXML:appdel.registationID];
    physicianTypeName=[physicianStatus valueForKey:@"physicianName"];
    physicianID=[physicianStatus valueForKey:@"id"];
    
    
    titleList=[[NSMutableArray alloc]init];
    TitleTypeList *titleLst=[[TitleTypeList alloc]init];
    titleList=[titleLst titleTypeList];
    titleTypeName=[titleList valueForKey:@"name"];
    titleTypeID=[titleList valueForKey:@"id"];
   
    genderList=[[NSMutableArray alloc]init];
    genderTypeList *genderLst=[[genderTypeList alloc]init];
    genderList=[genderLst genderTypeListData];
    genderTypeName=[genderList valueForKey:@"name"];
   
    
    maritalList=[[NSMutableArray alloc]init];
    maritalStatusList *meritalList1=[[maritalStatusList alloc]init];
    maritalList=[meritalList1 maritalStatusListData];
    maritalStatusName=[maritalList valueForKey:@"name"];
    maritalStatusID=[maritalList valueForKey:@"id"];
    
    EthnicOriginList=[[NSMutableArray alloc]init];
    EthnicOriginXMLData *ethnicoriginList=[[EthnicOriginXMLData alloc]init];
    EthnicOriginList=[ethnicoriginList EthnicOriginStatusListData];
    EthnicOriginName=[EthnicOriginList valueForKey:@"ethnicOriginName"];
    EthnicOriginID=[EthnicOriginList valueForKey:@"id"];
    
    
    studentList=[[NSMutableArray alloc]init];
    studentTypeXMLList *studentType1=[[studentTypeXMLList alloc]init];
    studentList=[studentType1 studentTypeList];
    studentTypeName=[studentList valueForKey:@"name"];
    studentTypeID=[studentList valueForKey:@"id"];
    
}



@end
