//
//  TabBarView.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 24/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DemographicsViewController.h"
#import "AddressViewController.h"
#import "ContactsViewController.h"
#import "InsuranceViewController.h"
#import "IdentitiesViewController.h"
#import "PatientPhotosViewController.h"
@class PatientInformationViewController;
@interface TabBarView : UIViewController<UITabBarControllerDelegate>

{
    PatientInformationViewController *patientInfo;
    UIImage *image;
DemographicsViewController *demographicsView;
AddressViewController *addressView;
ContactsViewController *contactsView;
InsuranceViewController *InsuranceView;
IdentitiesViewController *identitiesView;
PatientPhotosViewController *patientphotosView;
    
UITabBarController *tabBar;
NSMutableDictionary *arr;
}

-(id)initwithArray:(NSMutableDictionary *)array;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
