//
//  PhotoCustomCell.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 20/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "PhotoCustomCell.h"

@implementation PhotoCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
