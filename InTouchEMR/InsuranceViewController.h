//
//  InsuranceViewController.h
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsurenceCustomCell.h"
#import "NIDropDown.h"
@interface InsuranceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,NIDropDownDelegate,UIPopoverControllerDelegate,UISearchBarDelegate,UINavigationControllerDelegate>
{
    NSMutableArray *InsurenceData,*InsurenceInfo,*company;
    NSMutableDictionary *arr;
    InsurenceCustomCell *cell;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    UIImage *image;
    UIView *vi;
    NIDropDown *dropDown;
    UIDatePicker *datePicker1,*datePicker2,*datePicker3;
    UIBarButtonItem *doneButton;
    UIPopoverController *popoverController;
    UITextField *activeField;
    NSString *patientDOB;
    
    NSString * str_insurenceID;
    NSString * str_group;
    NSString * str_subscribeID;
    NSString * str_planName;
    NSString * str_policyNo;
    
    NSString * str_visitAllowed;
    NSString * str_policy_sdate;
    NSString * str_policy_edate;
    NSString * str_coPay;
    NSString * str_patient_MID;
    NSString * str_InsuredParty;
    NSString * str_subscriber_fname;
    NSString * str_subscriber_lname;
    NSString * str_DateOfBirth;
    NSString * str_subscriber_Gender;
    NSString *id_insurence;
    NSString * str_additionalInfo;
    NSString * str_companyID;
    int gender,VisitResetDate;
    NSString *insuredPartyID;
    NSMutableArray *insurePartyTypeArr,*insurenceTypeArr;
    NSArray *insurePartyTypeName, *insurePartyTypeID ,*insurenceTypeName,*insurenceTypeID;
    
    id butt;
    
}

- (IBAction)saveInsurence:(id)sender;


- (IBAction)selectClicked:(id)sender;

-(void)rel;


-(void)clearCurrentView;
- (IBAction)cancelView:(id)sender;
- (IBAction)update:(id)sender;
@property (nonatomic, assign) BOOL working;
@property (nonatomic, assign) BOOL orientchange;
@property (nonatomic, assign) BOOL listVisible;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UIPopoverController *popoverController;
@property (strong, nonatomic) IBOutlet UIScrollView *sc1;
@property (strong, nonatomic) IBOutlet UITextField *edit_pdate;
@property (strong, nonatomic) IBOutlet UIView *addView;
@property (strong, nonatomic) IBOutlet UIImageView *landscapePatientImage;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;
@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UILabel *patientContact;
@property (strong, nonatomic) IBOutlet UILabel *patientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *ssnHeading;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *patientName;
@property (strong, nonatomic) IBOutlet UITableView *insurenceTable;



@property (strong, nonatomic) IBOutlet UILabel *landscapepatientContact;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientOrigin;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnHeading;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientName;
@property (strong, nonatomic) IBOutlet UITableView *landscapeinsurenceTable;
- (IBAction)next:(id)sender;
-(id)initwithArray:(NSMutableDictionary *)array withImage:(UIImage *)img;
@property (strong, nonatomic) IBOutlet UITextView *edit_insurence;
@property (strong, nonatomic) IBOutlet UITextField *edit_group;
@property (strong, nonatomic) IBOutlet UITextField *edit_subscribID;
@property (strong, nonatomic) IBOutlet UITextField *edit_planName;
@property (strong, nonatomic) IBOutlet UITextField *edit_policyNo;
@property (strong, nonatomic) IBOutlet UIButton *edit_insurenceID;
@property (strong, nonatomic) IBOutlet UITextField *edit_visitAllowed;
@property (strong, nonatomic) IBOutlet UITextField *edit_policy_sdate;
@property (strong, nonatomic) IBOutlet UITextField *edit_policy_edate;
@property (strong, nonatomic) IBOutlet UITextField *edit_coPay;
@property (strong, nonatomic) IBOutlet UITextField *edit_patient_MID;
@property (strong, nonatomic) IBOutlet UIButton *edit_InsuredParty;
@property (strong, nonatomic) IBOutlet UITextField *edit_subscriber_fname;
@property (strong, nonatomic) IBOutlet UITextField *edit_subscriber_lname;
@property (strong, nonatomic) IBOutlet UITextField *edit_DateOfBirth;
@property (strong, nonatomic) IBOutlet UITextField *edit_subscriber_Gender;
@property (strong, nonatomic) IBOutlet UIButton *edit_subscriber_genderID;
@property (strong, nonatomic) IBOutlet UITextView *edit_additionalInfo;
@property (strong, nonatomic) IBOutlet UISwitch *edit_visitResetDate;

@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_insurence;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_group;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_subscribID;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_planName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_policyNo;

@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_visitAllowed;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_policy_sdate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_policy_edate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_coPay;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_patient_MID;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_InsuredParty;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_subscriber_fname;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_subscriber_lname;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_DateOfBirth;
@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_subscriber_Gender;

@property (strong, nonatomic) IBOutlet UILabel *lbl_edit_additionalInfo;
@property (strong, nonatomic) IBOutlet UIButton *edit_Save;
@property (strong, nonatomic) IBOutlet UIButton *edit_Cancel;
@property (strong, nonatomic) IBOutlet UIView *InsurenceCompany;
@property (strong, nonatomic) IBOutlet UITableView *companyList;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (nonatomic, assign) bool isFiltered;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_insurence;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_insurence;

@end
