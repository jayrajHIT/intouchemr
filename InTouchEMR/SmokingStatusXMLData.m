//
//  SmokingStatusXMLData.m
//  InTouchEMR
//
//  Created by Ghanshyam on 01/03/14.
//  Copyright (c) 2014 DSC. All rights reserved.
//

#import "SmokingStatusXMLData.h"
#import "IndexAppDelegate.h"

@implementation SmokingStatusXMLData

-(NSMutableArray *)SmokingStatusList
{
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSURL *url = [NSURL URLWithString:globalDele.smokingStatus];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
     //   NSLog(@"success");
    }
    else
    {
      //  NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"id"]||[myelement isEqualToString:@"smokingStatus"])
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"SmokingStatus"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
