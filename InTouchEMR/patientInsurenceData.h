//
//  patientContactData.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 18/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface patientInsurenceData : NSObject<NSXMLParserDelegate>
{
    
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)patientInsurenceDataByXML:(NSString *)registationID;
@end
