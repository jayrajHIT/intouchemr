//
//  physicianList.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 08/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface physicianList : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)physicianListXML:(NSString *)registationID;


@end
