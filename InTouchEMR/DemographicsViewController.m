//
//  DemographicsViewController.m
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "DemographicsViewController.h"
#import "patientDemographicsData.h"
#import "IndexAppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "SHKActivityIndicator.h"
#import "NSString+validation.h"
#import "physicianList.h"

#import "SmokingStatusXMLData.h"
#import "EmploymentTypeXMLData.h"
#import "TitleTypeList.h"
#import "genderTypeList.h"
#import "maritalStatusList.h"
#import "EthnicOriginXMLData.h"
#import "studentTypeXMLList.h"
@interface DemographicsViewController () <UIPickerViewDataSource,UIPickerViewDelegate>

@end

@implementation DemographicsViewController
@synthesize firstName,lastName,middleName,maritalStatus,emailId,dateOfBirth,SmokingStatus,gender,ssnNo,nickName,FullName,originName;
@synthesize landscapeView,landscapedateOfBirth,landscapeemailId,landscapefirstName,landscapeFullName,landscapegender,landscapelastName,landscapemaritalStatus,landscapemiddleName,landscapenickName,landscapeoriginName,landscapeSmokingStatus,landscapessnNo,portraitView;
@synthesize patientImage,landscapePatientImage,addView,working,btnSelect;
@synthesize edit_dob,edit_email,edit_title,edit_fname,edit_lname,edit_mname,edit_nickname,edit_ssnno,edit_btn_mstatus,edit_btn_smokingStatus,step2view,step3view,edit_username,edit_btn_gender,edit_EmpName;
@synthesize jobType,RaceOriginName,PayStatus,EmpAdd,HippaAgreement,Student,Status,currentEmp,OtherInfo,PCPRelease,EmpName;
@synthesize fatherName,MotherName,guarantorFName,guarantorMName,guarantorLName,Address,City,State,Zip,guarantorPNo;
@synthesize patientTitle,userName;
@synthesize landscapejobType,landscapeRaceOriginName,landscapePayStatus,landscapeEmpAdd,landscapeHippaAgreement,landscapeStudent,landscapeStatus,landscapecurrentEmp,landscapeOtherInfo,landscapePCPRelease,landscapeEmpName;
@synthesize landscapefatherName,landscapeMotherName,landscapeguarantorFName,landscapeguarantorMName,landscapeguarantorLName,landscapeAddress,landscapeCity,landscapeState,landscapeZip,landscapeguarantorPNo;
@synthesize landscapeusername;
@synthesize landscapetitle,landscapestep2view,landscapestep3view;
@synthesize edit_step2,edit_step3;
@synthesize edit_CurrentEmp,edit_EmpAddress,edit_gurantor_address,edit_gurantor_city,edit_gurantor_firstName,edit_gurantor_lastName,edit_gurantor_middleName,edit_gurantor_state,edit_gurantor_zip,edit_isMinor,edit_JobType,edit_minor_fatherName,edit_minor_motherName,edit_OtherInfo,edit_PaymentStatus,edit_PCPRelease,edit_RaceOrigin,edit_Status,edit_Student,edit_gurantor_phone;

@synthesize profile_firstName,profile_lastName,profile_dateOfBirth,profile_emailId,patient_fatherName,patient_MotherName,patient_guarantorFName,patient_guarantorLName,patient_Address,patient_City,patient_State,patient_Zip,patient_guarantorPNo,profile_UserName;
@synthesize btn_edit_profile,btn_edit_Additional,btn_edit_patient,btn_edit_land_profile,btn_edit_land_Additional,btn_edit_land_patient,orientchange,btnemail,landscapebtnemail,arr;
@synthesize port_btn_additional,port_btn_minor,port_btn_profile,land_btn_additional,land_btn_minor,land_btn_profile;
@synthesize patientEmail,ssnValue,landscapepatientEmail,landscapessnValue,landscapeReferalPhysician,referalPhysician,physician,edit_status,edit_paymentmode,profile_gender,profile_mstatus;
-(id)initwithImageName:(NSString *)str
{
    name_image=str;
    return self;
}


-(id)initwithArray:(NSMutableDictionary *)array 
{
    arr=array;
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.view.autoresizesSubviews = YES;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self setWorking:NO];
    [self setOrientchange:NO];
    btn_img_tag=5;
   
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
    IndexAppDelegate *appDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
       
    if ([appDele.isExternal isEqualToString:@"1"]) {
        [btn_edit_profile setHidden:YES];
        [btn_edit_Additional setHidden:YES];
        [btn_edit_patient setHidden:YES];
        [btn_edit_land_profile setHidden:YES];
        [btn_edit_land_Additional setHidden:YES];
        [btn_edit_land_patient setHidden:YES];
        
    }
    else if ([appDele.isExternal isEqualToString:@"0"])
    {
        [btn_edit_profile setHidden:NO];
        [btn_edit_Additional setHidden:NO];
        [btn_edit_patient setHidden:NO];
        [btn_edit_land_profile setHidden:NO];
        [btn_edit_land_Additional setHidden:NO];
        [btn_edit_land_patient setHidden:NO];

    }
   
    UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
    
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];

    form=[[NSDateFormatter alloc]init];
    [form setDateFormat:@"MM-dd-yyyy"];
    datePicker=[[UIDatePicker alloc]init];//Date picker
    datePicker.frame=CGRectMake(0,0,320, 216);
    datePicker.backgroundColor = [UIColor whiteColor];
    
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMinuteInterval:5];
    [datePicker setTag:10];
    
    
    
    [datePicker addTarget:self action:@selector(Result) forControlEvents:UIControlEventValueChanged];
    
    
    [edit_isMinor setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [edit_isMinor setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    
    [edit_CurrentEmp setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [edit_CurrentEmp setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    
    [self myOrientation];
 
}

-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
       
}

-(void)myOrientation
{
    
    [self performSelectorInBackground:@selector(mythread)
                           withObject:nil];

    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
		
        [self clearCurrentView];
        
        
		[self.view insertSubview:landscapeView atIndex:0];
        
        [self buttonImage:btn_img_tag];
        [self patientInfoDisplay];

       
        
        NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [landscapePatientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        
        if (dict)
            {
                [self afterthread];
            }
        
        if (self.working==YES)
            {
                [self editView];
            }
 	}
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
	
        [self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        [self buttonImage:btn_img_tag];
    [self patientInfoDisplay];

       
        
        NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        
        
        if (self.working==YES)
            {
                [self editView];
            }
        
    }

}

-(void)mythread
{
       IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    patientDemographicsData *patientData=[[patientDemographicsData alloc]init];
    
    DemographicsData=[patientData patientDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
    dict=[DemographicsData objectAtIndex:0];
    
    [self addDropDown];
      [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
    
}
-(void)afterthread
{
    NSString *hipa = [dict objectForKey:@"hippaaAgreementSigned"];
    NSLog(@"%@",hipa);
   
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
        
        NSString *hipa = [dict objectForKey:@"hippaaAgreementSigned"];
        NSLog(@"%@",hipa);
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:hipa forKey:@"hippaaAgreementSigned"];
        
        [defaults synchronize];
        
        
        NSString *StatusValue = [defaults stringForKey:@"hippaaAgreementSigned"];
        NSLog(@"Task: %@",StatusValue);
        

        
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"firstName"]]) {
            landscapefirstName.text=@"";
        }
        else
        {
            landscapefirstName.text=[dict objectForKey:@"firstName"];
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"middleName"]]) {
            landscapemiddleName.text=@"";
        }
        else
        {
            
            landscapemiddleName.text=[dict objectForKey:@"middleName"];
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"lastName"]]) {
            landscapelastName.text=@"";
        }
        else
        {
            
            landscapelastName.text=[dict objectForKey:@"lastName"];
        }

        if ([NSString stringIsEmpty:[dict objectForKey:@"birth"]]) {
          landscapedateOfBirth.text=@"";
        }
        else
        {
            
            landscapedateOfBirth.text=[self viewDate:[dict objectForKey:@"birth"]];
            datePicker.date=[form dateFromString:landscapedateOfBirth.text];
            
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"email"]]) {
             landscapeemailId.text=@"";
        }
        else
        {
           
            landscapeemailId.text=[dict objectForKey:@"email"];
        }

        if ([NSString stringIsEmpty:[dict objectForKey:@"gender"]]) {
             landscapegender.text=@"";
        }
        else
        {
            landscapegender.text=[dict objectForKey:@"gender"];
        }
     
        if ([NSString stringIsEmpty:[dict objectForKey:@"ssn"]]) {
            landscapessnNo.text=@"";
        }
        else
        {
            
            landscapessnNo.text=[dict objectForKey:@"ssn"];
        }

        if ([NSString stringIsEmpty:[dict objectForKey:@"maritalStatusID"]]) {
            landscapemaritalStatus.text=@"";
        }
        else
        {
            if ([maritalStatusID indexOfObject:[dict objectForKey:@"maritalStatusID"]] != NSNotFound) {
            int z=[maritalStatusID indexOfObject:[dict objectForKey:@"maritalStatusID"]];
            
            landscapemaritalStatus.text=[maritalStatusName objectAtIndex:z];
            }
            else
            {
                landscapemaritalStatus.text=@"";
            }
        }
       
        if ([NSString stringIsEmpty:[dict objectForKey:@"smokingStatusID"]]) {
            landscapeSmokingStatus.text=@"";
        }
        else
        {
            NSString *smokid=[dict objectForKey:@"smokingStatusID"];
            
            if ([smokingID indexOfObject:smokid] != NSNotFound) {

            int index=[smokingID indexOfObject:smokid];
            landscapeSmokingStatus.text=[smokingName objectAtIndex:index];
        }else
        {
            
            landscapeSmokingStatus.text=@"";
            
        }
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"referralPhysicianID"]]) {
             landscapeReferalPhysician.text=@"";
        }
        else
        {
             if ([physicianID indexOfObject:[dict objectForKey:@"referralPhysicianID"]] != NSNotFound) {
            int x=[physicianID indexOfObject:[dict objectForKey:@"referralPhysicianID"]];
    
            landscapeReferalPhysician.text=[physicianTypeName objectAtIndex:x];
             }
            else
            {
                 landscapeReferalPhysician.text=@"";
            }
        }
       
        if ([NSString stringIsEmpty:[dict objectForKey:@"nickName"]]) {
            landscapenickName.text=@"";

        }
        else
        {
             landscapenickName.text=[dict objectForKey:@"nickName"];
        }
  
        if ([NSString stringIsEmpty:[dict objectForKey:@"ethnicOriginName"]]) {
            
            landscapeoriginName.text=@"";
        }
        else
        {
          //  int index=[EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]];
            
            landscapeoriginName.text=[dict objectForKey:@"ethnicOriginName"];
            
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"titleID"]]) {
            landscapetitle.text=@"";
            
        }
        else
        {
            if ([titleTypeID indexOfObject:[dict objectForKey:@"titleID"]] != NSNotFound) {
            int index=[titleTypeID indexOfObject:[dict objectForKey:@"titleID"]];
            landscapetitle.text=[titleTypeName objectAtIndex:index];
        }else
        {
         landscapetitle.text=@"";
        }
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"userName"]]) {
            landscapeusername.text=@"";
            
        }
        else
        {
            landscapeusername.text=[dict objectForKey:@"userName"];
        }
     
        if ([NSString stringIsEmpty:[dict objectForKey:@"jobTypeID"]]) {
            landscapejobType.text=@"";
            
        }
        else
        {
            NSString *jobid=[dict objectForKey:@"jobTypeID"];
         
             if ([jobTypeID indexOfObject:jobid] != NSNotFound) {
            int index=[jobTypeID indexOfObject:jobid];
            landscapejobType.text=[jobTypeName objectAtIndex:index];
             }
            else
            {
                landscapejobType.text=@"";
            }
          //  landscapejobType.text=[dict objectForKey:@"jobType"];
        }
    
        if ([NSString stringIsEmpty:[dict objectForKey:@"ethnicOriginID"]]) {
            landscapeRaceOriginName.text=@"";
            
        }
        else
        {
            if ([EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]] != NSNotFound) {
                
                int index=[EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]];
                
                landscapeRaceOriginName.text=[EthnicOriginName objectAtIndex:index];
            }
            else {
               landscapeRaceOriginName.text=@"";
            }
            
//            int index=[EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]];
//            
//            landscapeRaceOriginName.text=[EthnicOriginName objectAtIndex:index];
        }
        
   
        if ([NSString stringIsEmpty:[dict objectForKey:@"paymentStatus"]]) {
            landscapePayStatus.text=@"";
            
        }
        else
        {
                   if ([[dict objectForKey:@"paymentStatus"] isEqualToString:@"SELF"]) {
            landscapePayStatus.text=@"Self";
        }
        else if ([[dict objectForKey:@"paymentStatus"] isEqualToString:@"INSURANCE"])
        {
            landscapePayStatus.text=@"Insurance";
        }
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"employerAddress"]]) {
            landscapeEmpAdd.text=@"";
            
        }
        else
        {
            landscapeEmpAdd.text=[dict objectForKey:@"employerAddress"];
        }
        
        
       
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"hippaaAgreementSigned"]]) {
            landscapeHippaAgreement.text=@"";
            
        }
        else
        {
                 if ([[dict objectForKey:@"hippaaAgreementSigned"] isEqualToString:@"true"]) {
                     
                     landscapeHippaAgreement.text=@"Yes";
                 }
                 else
                {
                    landscapeHippaAgreement.text=@"No";
                }
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"studentTypeID"]]) {
            landscapeStudent.text=@"";
            
        }
        else
        {
            if ([studentTypeID indexOfObject:[dict objectForKey:@"studentTypeID"]] != NSNotFound) {
            
            int a=[studentTypeID indexOfObject:[dict objectForKey:@"studentTypeID"]];
            
            landscapeStudent.text=[studentTypeName objectAtIndex:a];
            }
            else
            {
                 landscapeStudent.text=@"";
            }
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"statusName"]]) {
            landscapeStatus.text=@"";
            
        }
        else
        {
           
                if ([[dict objectForKey:@"statusName"] isEqualToString:@"ACTIVE"]) {
                    landscapeStatus.text=@"Active";
                }
                else{
                    landscapeStatus.text=@"Inactive";
                }
        }
       
        if ([NSString stringIsEmpty:[dict objectForKey:@"isWorking"]]) {
            landscapecurrentEmp.text=@"";
            
        }
        else
        {
        
                if ([[dict objectForKey:@"isWorking"]isEqualToString:@"1"]) {
                    landscapecurrentEmp.text=@"Yes";
                }
                else
                {
                    landscapecurrentEmp.text=@"No";
                }
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"otherInformation"]]) {
            landscapeOtherInfo.text=@"";
            
        }
        else
        {
            landscapeOtherInfo.text=[dict objectForKey:@"otherInformation"];
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"pcpRelease"]]) {
            landscapePCPRelease.text=@"";
            
        }
        else
        {
            landscapePCPRelease.text=[dict objectForKey:@"pcpRelease"];
        }

        if ([NSString stringIsEmpty:[dict objectForKey:@"employerName"]]) {
            landscapeEmpName.text=@"";
            
        }
        else
        {
            landscapeEmpName.text=[dict objectForKey:@"employerName"];
        }
  
        age=[self agefinder:[dict objectForKey:@"birth"]];
        if (age.intValue>=18) {
            
            [land_btn_minor setHidden:YES];
//            [land_btn_minor setImage:[UIImage imageNamed:@"patintnot-btn.png"] forState:UIControlStateNormal];
//            [land_btn_minor setUserInteractionEnabled:NO];
         
        }
        else
        {
            [land_btn_minor setHidden:YES];
          //  [land_btn_minor setUserInteractionEnabled:YES];
            
        }

        if ([NSString stringIsEmpty:[dict objectForKey:@"minor_father_name"]]) {
            landscapefatherName.text=@"";
            
        }
        else
        {
            landscapefatherName.text=[dict objectForKey:@"minor_father_name"];
        }
        
        if ([NSString stringIsEmpty:[dict objectForKey:@"minor_mother_name"]]) {
            landscapeMotherName.text=@"";
            
        }
        else
        {
            landscapeMotherName.text=[dict objectForKey:@"minor_mother_name"];
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorFirstName"]]) {
            landscapeguarantorFName.text=@"";
            
        }
        else
        {
            landscapeguarantorFName.text=[dict objectForKey:@"guarantorFirstName"];
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorLastName"]]) {
            landscapeguarantorLName.text=@"";
            
        }
        else
        {
            landscapeguarantorLName.text=[dict objectForKey:@"guarantorLastName"];
        }
   
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorMiddleName"]]) {
            landscapeguarantorMName.text=@"";
            
        }
        else
        {
            landscapeguarantorMName.text=[dict objectForKey:@"guarantorMiddleName"];
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorAddress"]]) {
            landscapeAddress.text=@"";
            
        }
        else
        {
            landscapeAddress.text=[dict objectForKey:@"guarantorAddress"];
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorCity"]]) {
            landscapeCity.text=@"";
            
        }
        else
        {
            landscapeCity.text=[dict objectForKey:@"guarantorCity"];
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorState"]]) {
            landscapeState.text=@"";
            
        }
        else
        {
            landscapeState.text=[dict objectForKey:@"guarantorState"];
        }
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorZip"]]) {
            landscapeZip.text=@"";
            
        }
        else
        {
            landscapeZip.text=[dict objectForKey:@"guarantorZip"];
        }
   
        if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorPhone"]]) {
            landscapeguarantorPNo.text=@"";
            
        }
        else
        {
            landscapeguarantorPNo.text=[dict objectForKey:@"guarantorPhone"];
        }

   
       }
        else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
        {
        
            
            NSString *hipa = [dict objectForKey:@"hippaaAgreementSigned"];
            NSLog(@"%@",hipa);
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            [defaults setObject:hipa forKey:@"hippaaAgreementSigned"];
            
            [defaults synchronize];
            
            
            NSString *StatusValue = [defaults stringForKey:@"hippaaAgreementSigned"];
            NSLog(@"Task: %@",StatusValue);
            
            
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"firstName"]]) {
                firstName.text=@"";
                
            }
            else
            {
                firstName.text=[dict objectForKey:@"firstName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"middleName"]]) {
                middleName.text=@"";
                
            }
            else
            {
                middleName.text=[dict objectForKey:@"middleName"];
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"lastName"]]) {
                lastName.text=@"";
                
            }
            else
            {
                lastName.text=[dict objectForKey:@"lastName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"birth"]]) {
                dateOfBirth.text=@"";
                
            }
            else
            {
                dateOfBirth.text=[self viewDate:[dict objectForKey:@"birth"]];
                datePicker.date=[form dateFromString:dateOfBirth.text];
            }
            
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"email"]]) {
                emailId.text=@"";
                
            }
            else
            {
                emailId.text=[dict objectForKey:@"email"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"gender"]]) {
                gender.text=@"";
                
            }
            else
            {
                gender.text=[dict objectForKey:@"gender"];
            }

            if ([NSString stringIsEmpty:[dict objectForKey:@"ssn"]]) {
                ssnNo.text=@"";
                
            }
            else
            {
                ssnNo.text=[dict objectForKey:@"ssn"];
            }

            if ([NSString stringIsEmpty:[dict objectForKey:@"maritalStatusID"]]) {
                maritalStatus.text=@"";
                
            }
            else
            {
                 if ([maritalStatusID indexOfObject:[dict objectForKey:@"maritalStatusID"]] != NSNotFound) {
                int z=[maritalStatusID indexOfObject:[dict objectForKey:@"maritalStatusID"]];
                
                maritalStatus.text=[maritalStatusName objectAtIndex:z];
                 }
                else
                {
                    maritalStatus.text=@"";
                }
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"smokingStatusID"]]) {
                SmokingStatus.text=@"";
                
            }
            else
            {
                
                NSString *smokid=[dict objectForKey:@"smokingStatusID"];
                if ([smokingID indexOfObject:smokid] != NSNotFound) {
                int index=[smokingID indexOfObject:smokid];
                SmokingStatus.text=[smokingName objectAtIndex:index];
                }
                else
                {
                    SmokingStatus.text=@"";
                }
                
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"referralPhysicianID"]]) {
                referalPhysician.text=@"";
                
            }
            else
            {
                if ([physicianID indexOfObject:[dict objectForKey:@"referralPhysicianID"]] != NSNotFound) {
                int x=[physicianID indexOfObject:[dict objectForKey:@"referralPhysicianID"]];
               
                referalPhysician.text=[physicianTypeName objectAtIndex:x];
                }
                else
                {
                    referalPhysician.text=@"";
                }
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"nickName"]]) {
                nickName.text=@"";
                
            }
            else
            {
                nickName.text=[dict objectForKey:@"nickName"];
            }
            
          
            if ([NSString stringIsEmpty:[dict objectForKey:@"ethnicOriginName"]]) {
                originName.text=@"";
                
            }
            else
            {
                
                originName.text=[dict objectForKey:@"ethnicOriginName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"titleID"]]) {
                patientTitle.text=@"";
                
            }
            else
            {
                if ([titleTypeID indexOfObject:[dict objectForKey:@"titleID"]] != NSNotFound)
                {
                int index=[titleTypeID indexOfObject:[dict objectForKey:@"titleID"]];
                patientTitle.text=[titleTypeName objectAtIndex:index];
                }
                else
                {
                     patientTitle.text=@"";
                }
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"userName"]]) {
                userName.text=@"";
                
            }
            else
            {
                userName.text=[dict objectForKey:@"userName"];
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"jobTypeID"]]) {
                jobType.text=@"";
                
            }
            else
            {
                NSString *jobid=[dict objectForKey:@"jobTypeID"];
                if ([jobTypeID indexOfObject:jobid] != NSNotFound) {
                int index=[jobTypeID indexOfObject:jobid];
                jobType.text=[jobTypeName objectAtIndex:index];
                }
                else
                {
                    jobType.text=@"";
                }
           
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"ethnicOriginID"]]) {
                RaceOriginName.text=@"";
                
            }
            else
            {
                
                if ([EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]] != NSNotFound) {
                    
                    int index=[EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]];
                    
                    RaceOriginName.text=[EthnicOriginName objectAtIndex:index];
                }
                else {
                    RaceOriginName.text=@"";
                }
                
                
//                int index=[EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]];
//                
//                RaceOriginName.text=[EthnicOriginName objectAtIndex:index];
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"paymentStatus"]]) {
                PayStatus.text=@"";
                
            }
            else
            {
            
                if ([[dict objectForKey:@"paymentStatus"] isEqualToString:@"SELF"]) {
                    PayStatus.text=@"Self";
                }
                else if ([[dict objectForKey:@"paymentStatus"] isEqualToString:@"INSURANCE"])
                {
                    PayStatus.text=@"Insurance";
                }
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"employerAddress"]]) {
                EmpAdd.text=@"";
                
            }
            else
            {
                EmpAdd.text=[dict objectForKey:@"employerAddress"];
            }
          
                       
            if ([NSString stringIsEmpty:[dict objectForKey:@"hippaaAgreementSigned"]]) {
                HippaAgreement.text=@"";
                
            }
            else
            {
            
                    if ([[dict objectForKey:@"hippaaAgreementSigned"] isEqualToString:@"true"]) {
                            HippaAgreement.text=@"Yes";
                    }
                    else
                    {
                            HippaAgreement.text=@"No";
                    }
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"studentTypeID"]]) {
                Student.text=@"";
                
            }
            else
            {
                int a=[studentTypeID indexOfObject:[dict objectForKey:@"studentTypeID"]];
                
                Student.text=[studentTypeName objectAtIndex:a];
            }
         
            if ([NSString stringIsEmpty:[dict objectForKey:@"statusName"]]) {
                Status.text=@"";
                
            }
            else
            {
            
                if ([[dict objectForKey:@"statusName"] isEqualToString:@"ACTIVE"]) {
                    Status.text=@"Active";
                }
                else{
                    Status.text=@"Inactive";
                }
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"isWorking"]]) {
                currentEmp.text=@"";
                
            }
            else
            {
         
                if ([[dict objectForKey:@"isWorking"]isEqualToString:@"1"]) {
                        currentEmp.text=@"Yes";
                }
                else
                {
                        currentEmp.text=@"No";
                }
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"otherInformation"]]) {
                OtherInfo.text=@"";
                
            }
            else
            {
                OtherInfo.text=[dict objectForKey:@"otherInformation"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"pcpRelease"]]) {
                PCPRelease.text=@"";
                
            }
            else
            {
                PCPRelease.text=[dict objectForKey:@"pcpRelease"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"employerName"]]) {
                EmpName.text=@"";
                
            }
            else
            {
                EmpName.text=[dict objectForKey:@"employerName"];
            }
   
            age=[self agefinder:[dict objectForKey:@"birth"]];
            
            if (age.intValue>=18) {

                [port_btn_minor setHidden:YES];
//                [port_btn_minor setImage:[UIImage imageNamed:@"patintnot-btn.png"] forState:UIControlStateNormal];
//                [port_btn_minor setUserInteractionEnabled:NO];
               
            }
            else
            {
                [port_btn_minor setHidden:NO];
               // [port_btn_minor setUserInteractionEnabled:YES];
                
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"minor_father_name"]]) {
                fatherName.text=@"";
                
            }
            else
            {
               fatherName.text=[dict objectForKey:@"minor_father_name"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"minor_mother_name"]]) {
                MotherName.text=@"";
                
            }
            else
            {
                MotherName.text=[dict objectForKey:@"minor_mother_name"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorFirstName"]]) {
                guarantorFName.text=@"";
                
            }
            else
            {
                guarantorFName.text=[dict objectForKey:@"guarantorFirstName"];
            }
          
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorLastName"]]) {
                guarantorLName.text=@"";
                
            }
            else
            {
                guarantorLName.text=[dict objectForKey:@"guarantorLastName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorMiddleName"]]) {
                guarantorMName.text=@"";
                
            }
            else
            {
                guarantorMName.text=[dict objectForKey:@"guarantorMiddleName"];
            }
          
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorAddress"]]) {
                Address.text=@"";
                
            }
            else
            {
                Address.text=[dict objectForKey:@"guarantorAddress"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorCity"]]) {
                City.text=@"";
                
            }
            else
            {
                City.text=[dict objectForKey:@"guarantorCity"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorState"]]) {
                State.text=@"";
                
            }
            else
            {
                State.text=[dict objectForKey:@"guarantorState"];
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorZip"]]) {
                Zip.text=@"";
                
            }
            else
            {
                Zip.text=[dict objectForKey:@"guarantorZip"];
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorPhone"]]) {
                guarantorPNo.text=@"";
                
            }
            else
            {
                guarantorPNo.text=[dict objectForKey:@"guarantorPhone"];
            }
    
        }
    
        
[[SHKActivityIndicator currentIndicator]hideAfterDelay];
   
}


-(void)patientUpdate
{
    age=[self agefinder:[dict objectForKey:@"birth"]];
  
    NSString *fname=[NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"firstName"],[dict objectForKey:@"lastName"]];
    NSString *firstletter = [[fname substringToIndex:1] capitalizedString];
    fname = [fname stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstletter];
 
    [arr setObject:fname forKey:@"FullName"];
    [arr setObject:[dict objectForKey:@"firstName"] forKey:@"firstName"];
    [arr setObject:[dict objectForKey:@"lastName"] forKey:@"lastName"];
    
    if ([dict objectForKey:@"birth"]!=NULL) {
        [arr setObject:[dict objectForKey:@"birth"] forKey:@"birth"];
    }
    
    if ([dict objectForKey:@"email"]!=NULL) {
        [arr setObject:[dict objectForKey:@"email"] forKey:@"email"];
    }
    
    if ([dict objectForKey:@"ssn"]!=NULL) {
        [arr setObject:[dict objectForKey:@"ssn"] forKey:@"ssn"];
    }
    if ([dict objectForKey:@"ethnicOriginID"]!=NULL) {
        
        if ([EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]] != NSNotFound) {
        
        int index=[EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]];
        
        [arr setObject:[EthnicOriginName objectAtIndex:index] forKey:@"ethnicOriginName"];
       
        [arr setObject:[dict objectForKey:@"ethnicOriginID"] forKey:@"ethnicOriginID"];
    }
    }
    if ([dict objectForKey:@"hippaaAgreementSigned"]!=NULL) {
        [arr setObject:[dict objectForKey:@"hippaaAgreementSigned"] forKey:@"hippaaAgreementSigned"];
    }
    
    [self patientInfoDisplay];
}
-(void)patientInfoDisplay
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {

       
        
        
        
        if ([arr objectForKey:@"FullName"]) {
            landscapeFullName.text=[arr objectForKey:@"FullName"];
        }
        if ([arr objectForKey:@"ethnicOriginName"]) {
//          
//              if ([EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]] != NSNotFound) {
//            int a=[EthnicOriginID indexOfObject:[arr objectForKey:@"ethnicOriginID"]];
            landscapeoriginName.text=[arr objectForKey:@"ethnicOriginName"];
              }
            else
            {
                landscapeoriginName.text=@"";
            }
        
        if ([arr objectForKey:@"email"]) {
            landscapepatientEmail.text=[arr objectForKey:@"email"];
        }
        if ([arr objectForKey:@"ssn"]) {
            landscapessnValue.text=[arr objectForKey:@"ssn"];
        }
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {

       
        
        if ([arr objectForKey:@"FullName"])
        {
            FullName.text=[arr objectForKey:@"FullName"];
        }
        if ([arr objectForKey:@"ethnicOriginName"])
        {
//             if ([EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]] != NSNotFound) {
//            int a=[EthnicOriginID indexOfObject:[arr objectForKey:@"ethnicOriginID"]];
            originName.text=[arr objectForKey:@"ethnicOriginName"];
             }
            else
            {
                 originName.text=@"";
            }
       
        if ([arr objectForKey:@"email"])
        {
            patientEmail.text=[arr objectForKey:@"email"];
        }
        if ([arr objectForKey:@"ssn"])
        {
            ssnValue.text=[arr objectForKey:@"ssn"];
        }
    }
    
  
}

-(void)editView
{
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
    
    if (btnTag==1) {
        
        
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024,670)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [landscapeView addSubview:vi];
        self.addView.frame=CGRectMake(180, 100, 675, 490);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [landscapeView addSubview:self.addView];
        
        }
        
    else if (btnTag==2)
    {
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024,670)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [landscapeView addSubview:vi];
        
        self.edit_step2.frame=CGRectMake(150, 50, 720, 540);
        self.edit_step2.layer.borderWidth=2.0;
        self.edit_step2.layer.cornerRadius=15.0;
        self.edit_step2.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [landscapeView addSubview:self.edit_step2];
         
        edit_EmpAddress.layer.cornerRadius=5.0;
        edit_EmpAddress.layer.borderWidth=1.0;
        edit_EmpAddress.layer.borderColor=[UIColor lightGrayColor].CGColor;
        edit_OtherInfo.layer.cornerRadius=5.0;
        edit_OtherInfo.layer.borderWidth=1.0;
        edit_OtherInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
     }
    else if (btnTag==3)
    {
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024,670)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [landscapeView addSubview:vi];
        
        self.edit_step3.frame=CGRectMake(150, 50, 720, 557);
        self.edit_step3.layer.borderWidth=2.0;
        self.edit_step3.layer.cornerRadius=15.0;
        self.edit_step3.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [landscapeView addSubview:self.edit_step3];
        edit_gurantor_address.layer.cornerRadius=5.0;
        edit_gurantor_address.layer.borderWidth=1.0;
        edit_gurantor_address.layer.borderColor=[UIColor lightGrayColor].CGColor;
    }
    
    }
   
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
 
    if (btnTag==1) {
        
          vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 930)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [portraitView addSubview:vi];
        self.addView.frame=CGRectMake(50, 200, 675, 490);
        self.addView.layer.borderWidth=2.0;
        self.addView.layer.cornerRadius=15.0;
        self.addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [portraitView addSubview:self.addView];
        
    }
    else if (btnTag==2)
    {
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 930)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [portraitView addSubview:vi];
        self.edit_step2.frame=CGRectMake(20, 200, 720, 540);
        self.edit_step2.layer.borderWidth=2.0;
        self.edit_step2.layer.cornerRadius=15.0;
        self.edit_step2.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [portraitView addSubview:self.edit_step2];
        edit_EmpAddress.layer.cornerRadius=5.0;
        edit_EmpAddress.layer.borderWidth=1.0;
        edit_EmpAddress.layer.borderColor=[UIColor lightGrayColor].CGColor;
        edit_OtherInfo.layer.cornerRadius=5.0;
        edit_OtherInfo.layer.borderWidth=1.0;
        edit_OtherInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
    }
    else if (btnTag==3)
    {
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 930)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [portraitView addSubview:vi];
        self.edit_step3.frame=CGRectMake(20, 200, 720, 557);
        self.edit_step3.layer.borderWidth=2.0;
        self.edit_step3.layer.cornerRadius=15.0;
        self.edit_step3.layer.borderColor=[UIColor lightGrayColor].CGColor;
        [portraitView addSubview:self.edit_step3];
        edit_gurantor_address.layer.cornerRadius=5.0;
        edit_gurantor_address.layer.borderWidth=1.0;
        edit_gurantor_address.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
         
    }
     
    }
    
    if ([self orientchange]) {
        
        if (self.working==YES) {
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"firstName"]]) {
                edit_fname.text=@"";
                
            }
            else
            {
                edit_fname.text=[dict objectForKey:@"firstName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"middleName"]]) {
                edit_mname.text=@"";
                
            }
            else
            {
                edit_mname.text=[dict objectForKey:@"middleName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"lastName"]]) {
                edit_lname.text=@"";
                
            }
            else
            {
                edit_lname.text=[dict objectForKey:@"lastName"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"birth"]]) {
                edit_dob.text=@"";
                
            }
            else
            {
                edit_dob.text=[self viewDate:[dict objectForKey:@"birth"]];
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"email"]]) {
                edit_email.text=@"";
                
            }
            else
            {
                edit_email.text=[dict objectForKey:@"email"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"referralPhysicianID"]]) {
                [physician setTitle:@"select one" forState:UIControlStateNormal];
            }
            else
            {
                if ([physicianID indexOfObject:[dict objectForKey:@"referralPhysicianID"]] != NSNotFound) {

                int x=[physicianID indexOfObject:[dict objectForKey:@"referralPhysicianID"]];
                
               [physician setTitle:[physicianTypeName objectAtIndex:x] forState:UIControlStateNormal];
            }else
            {
                [physician setTitle:@"select one" forState:UIControlStateNormal];
            }
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"ssn"]]) {
                edit_ssnno.text=@"";
                
            }
            else
            {
                edit_ssnno.text=[dict objectForKey:@"ssn"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"smokingStatusID"]]) {
                [edit_btn_smokingStatus setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                if ([smokingID indexOfObject:[dict objectForKey:@"smokingStatusID"]] != NSNotFound) {
                
                int index=[smokingID indexOfObject:[dict objectForKey:@"smokingStatusID"]];
                
                [edit_btn_smokingStatus setTitle:[smokingName objectAtIndex:index] forState:UIControlStateNormal];
            }else
            {
                [edit_btn_smokingStatus setTitle:@"select one" forState:UIControlStateNormal];
            }
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"maritalStatusID"]]) {
                
                
                [edit_btn_mstatus setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                 if ([maritalStatusID indexOfObject:[dict objectForKey:@"maritalStatusID"]] != NSNotFound) {
                int index=[maritalStatusID indexOfObject:[dict objectForKey:@"maritalStatusID"]];
                
                [edit_btn_mstatus setTitle:[maritalStatusName objectAtIndex:index] forState:UIControlStateNormal];
                 }
                else
                {
                     [edit_btn_mstatus setTitle:@"select one" forState:UIControlStateNormal];
                }
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"gender"]]) {
                [edit_btn_gender setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                [edit_btn_gender setTitle:[dict objectForKey:@"gender"] forState:UIControlStateNormal];
            }

            if ([NSString stringIsEmpty:[dict objectForKey:@"titleID"]]) {
                [edit_title setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                 if ([titleTypeID indexOfObject:[dict objectForKey:@"titleID"]] != NSNotFound) {
                
                int index=[titleTypeID indexOfObject:[dict objectForKey:@"titleID"]];
                
                [edit_title setTitle:[titleTypeName objectAtIndex:index] forState:UIControlStateNormal];
                 }
                else
                {
                     [edit_title setTitle:@"select one" forState:UIControlStateNormal];
                }
               
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"nickName"]]) {
                edit_nickname.text=@"";
                
            }
            else
            {
                edit_nickname.text=[dict objectForKey:@"nickName"];
            }
           
            if ([NSString stringIsEmpty:[dict objectForKey:@"userName"]]) {
                edit_username.text=@"";
                
            }
            else
            {
                edit_username.text=[dict objectForKey:@"userName"];
            }
          
            if ([NSString stringIsEmpty:[dict objectForKey:@"jobTypeID"]]) {
                [edit_JobType setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                 if ([jobTypeID indexOfObject:[dict objectForKey:@"jobTypeID"]] != NSNotFound) {
                int index=[jobTypeID indexOfObject:[dict objectForKey:@"jobTypeID"]];
                [edit_JobType setTitle:[jobTypeName objectAtIndex:index] forState:UIControlStateNormal];
            }else
            {
                [edit_JobType setTitle:@"select one" forState:UIControlStateNormal];
            }
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"ethnicOriginID"]]) {
                [edit_RaceOrigin setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                 if ([EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]] != NSNotFound) {
                
                int index=[EthnicOriginID indexOfObject:[dict objectForKey:@"ethnicOriginID"]];
                
                [edit_RaceOrigin setTitle:[EthnicOriginName objectAtIndex:index] forState:UIControlStateNormal];
            }
                else
                {
                    [edit_RaceOrigin setTitle:@"select one" forState:UIControlStateNormal];
                }
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"paymentStatus"]]) {
                [edit_PaymentStatus setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
          
                if ([[dict objectForKey:@"paymentStatus"]isEqualToString:@"SELF"]) {
                    [edit_PaymentStatus setTitle:@"Self Paid" forState:UIControlStateNormal];
                }
                else
                {
                    [edit_PaymentStatus setTitle:@"Insurance Paid" forState:UIControlStateNormal];
                }
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"employerAddress"]]) {
                edit_EmpAddress.text=@"";
                
            }
            else
            {
                edit_EmpAddress.text=[dict objectForKey:@"employerAddress"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"isWorking"]]) {
              [edit_CurrentEmp setOn:NO];
                
            }
            else
            {
            
                if ([[dict objectForKey:@"isWorking"]isEqualToString:@"1"]) {
                    [edit_CurrentEmp setOn:YES];
                }
                else
                {
                    [edit_CurrentEmp setOn:NO];
                }
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"studentTypeID"]]) {
                [edit_Student setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                 if ([studentTypeID indexOfObject:[dict objectForKey:@"studentTypeID"]] != NSNotFound) {
                
                    int a=[studentTypeID indexOfObject:[dict objectForKey:@"studentTypeID"]];
                
                [edit_Student setTitle:[studentTypeName objectAtIndex:a] forState:UIControlStateNormal];
            }
                else
                {
                    [edit_Student setTitle:@"select one" forState:UIControlStateNormal];
                }
            }

            if ([NSString stringIsEmpty:[dict objectForKey:@"statusName"]]) {
                [edit_Status setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                [edit_Status setTitle:[dict objectForKey:@"statusName"] forState:UIControlStateNormal];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"pcpRelease"]]) {
                [edit_PCPRelease setTitle:@"select one" forState:UIControlStateNormal];
                
            }
            else
            {
                [edit_PCPRelease setTitle:[dict objectForKey:@"pcpRelease"] forState:UIControlStateNormal];
            }

         
            if ([NSString stringIsEmpty:[dict objectForKey:@"otherInformation"]]) {
                edit_OtherInfo.text=@"";
                
            }
            else
            {
                edit_OtherInfo.text=[dict objectForKey:@"otherInformation"];
            }
            
            if ([NSString stringIsEmpty:[dict objectForKey:@"employerName"]]) {
                edit_EmpName.text=@"";
                
            }
            else
            {
                edit_EmpName.text=[dict objectForKey:@"employerName"];
            }
          
            
                       
            age=[self agefinder:[dict objectForKey:@"birth"]];
            
            if (age.intValue>=18) {
                [edit_isMinor setOn:NO];
            }
            else
            {
                [edit_isMinor setOn:YES];
            }
   
            if ([NSString stringIsEmpty:[dict objectForKey:@"minor_mother_name"]]) {
                edit_minor_motherName.text=@"";
                
            }
            else
            {
                edit_minor_motherName.text=[dict objectForKey:@"minor_mother_name"];
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"minor_father_name"]]) {
                edit_minor_fatherName.text=@"";
                
            }
            else
            {
                edit_minor_fatherName.text=[dict objectForKey:@"minor_father_name"];
            }

            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorFirstName"]]) {
                edit_gurantor_firstName.text=@"";
                
            }
            else
            {
                edit_gurantor_firstName.text=[dict objectForKey:@"guarantorFirstName"];
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorMiddleName"]]) {
                edit_gurantor_middleName.text=@"";
                
            }
            else
            {
                edit_gurantor_middleName.text=[dict objectForKey:@"guarantorMiddleName"];
            }
          
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorLastName"]]) {
                edit_gurantor_lastName.text=@"";
                
            }
            else
            {
                edit_gurantor_lastName.text=[dict objectForKey:@"guarantorLastName"];
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorAddress"]]) {
                edit_gurantor_address.text=@"";
                
            }
            else
            {
                edit_gurantor_address.text=[dict objectForKey:@"guarantorAddress"];
            }
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorCity"]]) {
                edit_gurantor_city.text=@"";
                
            }
            else
            {
                edit_gurantor_city.text=[dict objectForKey:@"guarantorCity"];
            }

            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorState"]]) {
                edit_gurantor_state.text=@"";
                
            }
            else
            {
                edit_gurantor_state.text=[dict objectForKey:@"guarantorState"];
            }
          
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorZip"]]) {
                edit_gurantor_zip.text=@"";
                
            }
            else
            {
                edit_gurantor_zip.text=[dict objectForKey:@"guarantorZip"];
            }
          
            if ([NSString stringIsEmpty:[dict objectForKey:@"guarantorPhone"]]) {
                edit_gurantor_phone.text=@"";
                
            }
            else
            {
                edit_gurantor_phone.text=[dict objectForKey:@"guarantorPhone"];
            }
           
            [self setOrientchange:NO];
        }
        
    }

}

-(void) clearCurrentView {
    
    [self.addView removeFromSuperview];
    [vi removeFromSuperview];
    
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
       
        
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
        
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)emailSend:(id)sender {
   
    NSArray *toRecipents;
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
		if (landscapeemailId.text != nil && landscapeemailId.text.length > 0) {
             toRecipents =[NSArray arrayWithObject:landscapeemailId.text];
		}
        else
        {
            toRecipents=nil;
        }

	}
    else
    {
		if (emailId.text != nil && emailId.text.length > 0) {
        toRecipents =[NSArray arrayWithObject:emailId.text];
		}
        else
        { 
            toRecipents=nil;
        }
	}

    if ([MFMailComposeViewController canSendMail]) {
        if ([toRecipents count]>0) {
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            NSString *emailTitle = @"";
            NSString *messageBody = @"";
          //  NSArray *toRecipents =[NSArray arrayWithObject:emailId.text];
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [self presentViewController:mc animated:YES completion:nil];
        }
        else{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Patient email id not found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Before sending email, please configure your default email account under settings." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
         
            
            break;
        case MFMailComposeResultSaved:
           
            [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail has been saved successfully!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            break;
        case MFMailComposeResultSent:
         
            [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail has been sent successfully!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
            break;
            
        case MFMailComposeResultFailed:
      [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail sent failure" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}

       
- (IBAction)cancelView:(id)sender {
    [vi removeFromSuperview];
    [addView removeFromSuperview];
    [edit_step2 removeFromSuperview];
    [edit_step3 removeFromSuperview];
     self.working=NO;
    [self setOrientchange:NO];
    [profile_firstName setTextColor:[UIColor blackColor]];
    [profile_lastName setTextColor:[UIColor blackColor]];
    [profile_emailId setTextColor:[UIColor blackColor]];
    [profile_UserName setTextColor:[UIColor blackColor]];
    [patient_fatherName setTextColor:[UIColor blackColor]];
    [patient_MotherName setTextColor:[UIColor blackColor]];
    [patient_guarantorFName setTextColor:[UIColor blackColor]];
    [patient_guarantorLName setTextColor:[UIColor blackColor]];
    [patient_Address setTextColor:[UIColor blackColor]];
    [patient_City setTextColor:[UIColor blackColor]];
    [patient_State setTextColor:[UIColor blackColor]];
    [patient_Zip setTextColor:[UIColor blackColor]];
    [patient_guarantorPNo setTextColor:[UIColor blackColor]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
     if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
    if (textField.tag==5){
        const int movementDistance = 200; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.addView.frame=CGRectOffset(self.addView.frame, 0, movement);
        [UIView commitAnimations];
        
    }
         if (textField.tag==1||textField.tag==2){
        const int movementDistance = 200; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? -movementDistance : movementDistance);
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.edit_step3.frame=CGRectOffset(self.edit_step3.frame, 0, movement);
        [UIView commitAnimations];
        
    }
         if (textField.tag==6){
             const int movementDistance = 150; // tweak as needed
             const float movementDuration = 0.3f; // tweak as needed
             
             int movement = (up ? -movementDistance : movementDistance);
             
             [UIView beginAnimations: @"anim" context: nil];
             [UIView setAnimationBeginsFromCurrentState: YES];
             [UIView setAnimationDuration: movementDuration];
             self.edit_step3.frame=CGRectOffset(self.edit_step3.frame, 0, movement);
             [UIView commitAnimations];
             
         }
     }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView: textView up: YES];
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:textView up:NO];
    
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
        if (textView.tag==5){
            const int movementDistance = 100; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            self.edit_step2.frame=CGRectOffset(self.edit_step2.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if (textView.tag==6){
            const int movementDistance = 100; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            self.edit_step3.frame=CGRectOffset(self.edit_step3.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    BOOL a;
    if (textField.tag==10) {
        
    
    UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
    
    UIView *popoverView = [[UIView alloc] init];   //view
    popoverView.backgroundColor = [UIColor blackColor];
    /*
   
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMinuteInterval:5];
    [datePicker setTag:10];
     
    [datePicker addTarget:self action:@selector(Result) forControlEvents:UIControlEventValueChanged];
     
     */
    [popoverView addSubview:datePicker];
    
    popoverContent.view = popoverView;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:popoverContent];
        navigationController.delegate=self;
                
        
        popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:navigationController];
    popoverController.delegate=self;
    
    
    [popoverController setPopoverContentSize:CGSizeMake(320, 250) animated:NO];
       
       
    [popoverController presentPopoverFromRect:textField.frame inView:self.addView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    a=NO;
  
}
    else
    {
        a=YES;
    }
    return a;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if (!doneButton) {
        doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                      style:UIBarButtonItemStylePlain
                                                     target:self action:@selector(done:)];
    }
    
    viewController.navigationItem.rightBarButtonItem = doneButton;
}
-(IBAction)done:(id)sender
{
    [popoverController dismissPopoverAnimated:YES];
}
-(void)Result
{
    NSDateFormatter *formDay = [[NSDateFormatter alloc] init];
    formDay.dateFormat=@"MM-dd-yyyy";
    NSString *day = [formDay stringFromDate:[datePicker date]];
    self.edit_dob.text = day;
  }


- (IBAction)step1:(id)sender {

    btn_img_tag=5;
    [self buttonImage:btn_img_tag];
    [step2view removeFromSuperview];
    [landscapestep2view removeFromSuperview];
    [step3view removeFromSuperview];
    [landscapestep3view removeFromSuperview];
}

- (IBAction)step2:(id)sender {
    
    btn_img_tag=6;
    [self buttonImage:btn_img_tag];
    [step3view removeFromSuperview];
    [landscapestep3view removeFromSuperview];
    
    
    step2view.frame=CGRectMake(44, 329, 678, 246);
    landscapestep2view.frame=CGRectMake(61, 307, 887, 248);
        [portraitView addSubview:step2view];
    [landscapeView addSubview:landscapestep2view];
    
}
- (IBAction)step3:(id)sender
{

    btn_img_tag=7;
    [self buttonImage:btn_img_tag];
    step3view.frame=CGRectMake(44, 329, 680, 248);
    landscapestep3view.frame=CGRectMake(61, 307, 887, 248);
    [portraitView addSubview:step3view];
    [landscapeView addSubview:landscapestep3view];
}



-(void)buttonImage:(int)tag
{
    [port_btn_profile setImage:[UIImage imageNamed:@"profile_unselected.png"] forState:UIControlStateNormal];
    [port_btn_additional setImage:[UIImage imageNamed:@"addttional_unselected.png"] forState:UIControlStateNormal];
//    [port_btn_minor setImage:[UIImage imageNamed:@"minor_unselected.png"] forState:UIControlStateNormal];
    
    [land_btn_profile setImage:[UIImage imageNamed:@"profile_unselected.png"] forState:UIControlStateNormal];
    [land_btn_additional setImage:[UIImage imageNamed:@"addttional_unselected.png"] forState:UIControlStateNormal];
 //  [land_btn_minor setImage:[UIImage imageNamed:@"minor_unselected.png"] forState:UIControlStateNormal];
    if (age.intValue<18) {
        
//        [port_btn_minor setHidden:YES];
//        [land_btn_minor setHidden:YES]
        [port_btn_minor setImage:[UIImage imageNamed:@"minor_unselected.png"] forState:UIControlStateNormal];
        [land_btn_minor setImage:[UIImage imageNamed:@"minor_unselected.png"] forState:UIControlStateNormal];
        
        
    }
    
    
    switch (tag) {
        case 5:
            [port_btn_profile setImage:[UIImage imageNamed:@"profile_selected.png"] forState:UIControlStateNormal];
            [land_btn_profile setImage:[UIImage imageNamed:@"profile_selected.png"] forState:UIControlStateNormal];

            break;
         case 6:
            [port_btn_additional setImage:[UIImage imageNamed:@"additonal_selected.png"] forState:UIControlStateNormal];
            [land_btn_additional setImage:[UIImage imageNamed:@"additonal_selected.png"] forState:UIControlStateNormal];
            break;
            case 7:
            [port_btn_minor setImage:[UIImage imageNamed:@"minor_selected.png"] forState:UIControlStateNormal];
            [land_btn_minor setImage:[UIImage imageNamed:@"minor_selected.png"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

- (IBAction)next:(id)sender {
    
    self.tabBarController.selectedIndex=1;
}


- (IBAction)update:(id)sender {
    UIButton *btn=(UIButton *)sender;
    [self setWorking:YES];
    [self setOrientchange:YES];
    
  //  IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
//    physicianList *physicianListXML=[[physicianList alloc]init];
//    physicianData=[physicianListXML physicianListXML:appdalegate.registationID];
//  
//    physicianData1=[[NSMutableArray alloc]initWithArray:[self getPhysician:physicianData]];
    
    
    if (btn.tag==1) {
       
    
        btnTag=1;
    
    [self editView];
}
    else if (btn.tag==2)
    {
       //[self setWorking:YES];
        btnTag=2;
        
        [self editView];
        
    }
    else if (btn.tag==3)
    {
      //  [self setWorking:YES];
        btnTag=3;
        
        [self editView];
        
    }

}


- (IBAction)profile_saved:(id)sender {
    updateFlag=1;
    BOOL flag=NO;
    NSString *gender1,*SmokingState,*smokingValue;
    NSString *title,*mstatus;
    str_emailid=edit_email.text;
    str_username=edit_username.text;
    str_firstname=edit_fname.text;
    str_middlename=edit_mname.text;
    str_lastname=edit_lname.text;
    str_nickname=edit_nickname.text;
    str_ssn=edit_ssnno.text;
    
    dob=[self saveDate:edit_dob.text];
  
    if([NSString stringIsEmpty:str_firstname]==YES)
    {
        flag=YES;
        [profile_firstName setTextColor:[UIColor redColor]];
    }
    else
    {
        [profile_firstName setTextColor:[UIColor blackColor]];
    }
    if([NSString stringIsEmpty:str_middlename]==YES)
    {
       str_middlename=@"";
        
    }
    if([NSString stringIsEmpty:str_username]==YES)
    {
        str_username=str_firstname;
        
    }
  
    if([NSString stringIsEmpty:str_lastname]==YES)
    {
        flag=YES;
        [profile_lastName setTextColor:[UIColor redColor]];
    }
    else
    {
        [profile_lastName setTextColor:[UIColor blackColor]];
    }
   
    if([NSString stringIsEmpty:dob]==YES)
    {
        flag=YES;
        [profile_dateOfBirth setTextColor:[UIColor redColor]];
    }
    else
    {
        [profile_dateOfBirth setTextColor:[UIColor blackColor]];
    }
    
    if([NSString stringIsEmpty:str_emailid]==YES)
    {
        flag=YES;
         [profile_emailId setTextColor:[UIColor redColor]];
    }
    else
    {
        [profile_emailId setTextColor:[UIColor blackColor]];
        
    }
    
    gender1=[edit_btn_gender titleForState:UIControlStateNormal];
    if ([gender1 isEqualToString:@"select one"])
    {
        [profile_gender setTextColor:[UIColor redColor]];
        gender1=@"";
    }
    else
    {
        [profile_gender setTextColor:[UIColor blackColor]];
    }
    
    
    SmokingState=[edit_btn_smokingStatus titleForState:UIControlStateNormal];
    if ([SmokingState isEqualToString:@"select one"])
    {
        smokingValue=@"0";
        
    }
    else
    {
         if ([smokingName indexOfObject:SmokingState] != NSNotFound) {
        int z=[smokingName indexOfObject:SmokingState];
        smokingValue=[smokingID objectAtIndex:z];
         }
        else
        {
            smokingValue=@"0";
        }
    }
    
    
    if ([[edit_title titleForState:UIControlStateNormal] isEqualToString:@"select one"])
    {
        title=@"0";
        
    }
    else
    {
         if ([titleTypeName indexOfObject:[edit_title titleForState:UIControlStateNormal]] != NSNotFound) {
    int a=[titleTypeName indexOfObject:[edit_title titleForState:UIControlStateNormal]];
    title=[titleTypeID objectAtIndex:a];
    }else
    {
        title=@"0";
    }
    }
    
    
    if ([[edit_btn_mstatus titleForState:UIControlStateNormal] isEqualToString:@"select one"])
    {
        mstatus=@"0";
        
    }
    else
    {
        if ([maritalStatusName indexOfObject:[edit_btn_mstatus titleForState:UIControlStateNormal]] != NSNotFound) {
        int a=[maritalStatusName indexOfObject:[edit_btn_mstatus titleForState:UIControlStateNormal]];
        mstatus=[maritalStatusID objectAtIndex:a];
    }
        else
        {
             mstatus=@"0";
        }
    }
    
    if ([NSString stringIsEmpty:mstatus]==YES) {
        [profile_mstatus setTextColor:[UIColor redColor]];
    }
    else
    {
        [profile_mstatus setTextColor:[UIColor blackColor]];
    }
    
    if([NSString stringIsEmpty:str_ssn]==YES)
    {
        str_ssn=@"";
        
    }
    if([NSString stringIsEmpty:str_nickname]==YES)
    {
        str_nickname=@"";
    }
        
    if (flag==YES) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if (![self NSStringIsValidEmail:str_emailid]) {
            [profile_emailId setTextColor:[UIColor redColor]];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message." message:@"Emailid is not valid format" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
              [profile_emailId setTextColor:[UIColor blackColor]];
        IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
     
        NSURL *url = [NSURL URLWithString:appdalegate.demogrphicsUpdateUrl];
             
        NSString *xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?> <Patient><registrationID>%@</registrationID><patientID>%@</patientID><updateFlag>%i</updateFlag><firstName>%@</firstName><lastName>%@</lastName><middleName>%@</middleName><titleID>%@</titleID><gender>%@</gender><email>%@</email><nickName>%@</nickName><ssn>%@</ssn><smokingStatusID>%@</smokingStatusID><dateOfBirth>%@</dateOfBirth><maritalStatusID>%@</maritalStatusID><userName>%@</userName></Patient>",appdalegate.registationID,appdalegate.patientId,1,str_firstname,str_lastname,str_middlename,title,gender1,str_emailid,str_nickname,str_ssn,smokingValue,dob,mstatus,str_username];
         
     
        NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
       
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        [request setURL:url];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLResponse* response;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
          
        
        if ([responseString isEqualToString:@"success"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Profile information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self updateall];
        }
        else if([responseString isEqualToString:@"email already exists"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Email Id already exist" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error! Profile information has not update" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
       
      
        }
        }
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

    
- (IBAction)Additional_saved:(id)sender {
    
    NSString *jobType11,*reffPhysician;
    stuID=@"";
    orgin=@"";
     
    if ([[edit_Status titleForState:UIControlStateNormal] isEqualToString:@"ACTIVE"]) {
        status=1;
    }
    else if ([[edit_Status titleForState:UIControlStateNormal] isEqualToString:@"INACTIVE"])
    {
        status=2;
    }
    else
    {
        status=0;
    }
    
    if([[edit_Student titleForState:UIControlStateNormal] isEqualToString:@"select one"])
    {
        studentID=@"0";
    }
    else
    {
         if ([studentTypeName indexOfObject:[edit_Student titleForState:UIControlStateNormal]] != NSNotFound) {
        int x=[studentTypeName indexOfObject:[edit_Student titleForState:UIControlStateNormal]];
        studentID=[studentTypeID objectAtIndex:x];
    }
        else
        {
            studentID=@"0";
        }
    }
    
        if ([[edit_PaymentStatus titleForState:UIControlStateNormal] isEqualToString:@"Self Paid"]) {
        paymentStr=@"SELF";
    }
    else if ([[edit_PaymentStatus titleForState:UIControlStateNormal] isEqualToString:@"Insurance Paid"])
    {
        paymentStr=@"INSURANCE";
    }
    else
    {
        paymentStr=@"";
    }
   
    
    if([NSString stringIsEmpty:[edit_RaceOrigin titleForState:UIControlStateNormal]]==YES)
    {
        raceOrigin=@"0";
        
    }
    else
    {
         if ([EthnicOriginName indexOfObject:[edit_RaceOrigin titleForState:UIControlStateNormal]] != NSNotFound) {
        
    int index=[EthnicOriginName indexOfObject:[edit_RaceOrigin titleForState:UIControlStateNormal]];
    
    raceOrigin=[EthnicOriginID objectAtIndex:index];
    }
        else
        {
             raceOrigin=@"0";
        }
            
    }
    
    job=[edit_JobType titleForState:UIControlStateNormal];
    
    if ([job isEqualToString:@"select one"]) {
        
        jobType11=@"0";
    }
    else
    {
         if ([jobTypeName indexOfObject:job] != NSNotFound) {
            int z=[jobTypeName indexOfObject:job];
            jobType11=[jobTypeID objectAtIndex:z];
         }
        else
        {
             jobType11=@"0";
        }
        
    }

    
    if ([edit_CurrentEmp isOn]) {
        curEmp=1;
    }
    else
    {
        curEmp=0;
    }
    if([NSString stringIsEmpty:edit_EmpName.text]==YES)
    {
        edit_EmpName.text=@"";
        
    }
 
//    pcpState=[edit_PCPRelease titleForState:UIControlStateNormal];
//    
//    if ([pcpState isEqualToString:@"select one"]) {
//        pcpState=@"";
//    }

    str_physician=[physician titleForState:UIControlStateNormal];
    if ([str_physician isEqualToString:@"select one"]) {
        str_physician=@"";
    }
    else
    {
        int x=[physicianTypeName indexOfObject:str_physician];
        
        
        reffPhysician=[physicianID objectAtIndex:x];
        
        
    }
    
    
    if ([paymentStr isEqualToString:@""]) {
        [edit_paymentmode setTextColor:[UIColor redColor]];
    }
    else
    {
    [edit_paymentmode setTextColor:[UIColor blackColor]];
    }
    
    if (status==0) {
        [edit_status setTextColor:[UIColor redColor]];
    }
    else
    {
        [edit_status setTextColor:[UIColor blackColor]];
    }

    if ([paymentStr isEqualToString:@""]||status==0) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
    
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
 
    NSURL *url = [NSURL URLWithString:appdalegate.demogrphicsUpdateUrl];
    
     NSString *xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?> <Patient><registrationID>%@</registrationID><patientID>%@</patientID><updateFlag>%i</updateFlag><otherInformation>%@</otherInformation><studentTypeID>%@</studentTypeID><jobTypeID>%@</jobTypeID><isWorking>%i</isWorking><employerAddress>%@</employerAddress><ethnicOriginID>%@</ethnicOriginID><employerName>%@</employerName><statusId>%i</statusId><paymentStatus>%@</paymentStatus><referralPhysicianID>%@</referralPhysicianID></Patient>",appdalegate.registationID,appdalegate.patientId,2,edit_OtherInfo.text,studentID,jobType11,curEmp,edit_EmpAddress.text,raceOrigin,edit_EmpName.text,status,paymentStr,reffPhysician];
    
 
    NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
  
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse* response;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
   
    
    if ([responseString isEqualToString:@"success"]) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Additional information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [self updateall];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error! Additional information has not update" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    }
    
}

- (IBAction)Minor_saved:(id)sender
{
    BOOL iserror=NO;
    BOOL isminor=NO;
   
    
    if ([edit_isMinor isOn]) {
        
        isminor=YES;
        
        if([NSString stringIsEmpty:edit_minor_fatherName.text])
        {
            
            iserror=YES;
            [patient_fatherName setTextColor:[UIColor redColor]];
            
        }
        else
        {
            [patient_fatherName setTextColor:[UIColor blackColor]];

        }
       
        if([NSString stringIsEmpty:edit_minor_motherName.text])
        {
            
            iserror=YES;
            [patient_MotherName setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_MotherName setTextColor:[UIColor blackColor]];
            
        }
       
        if([NSString stringIsEmpty:edit_gurantor_firstName.text])
        {
            iserror=YES;
            
            [patient_guarantorFName setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_guarantorFName setTextColor:[UIColor blackColor]];
            
        }
        
        
        if([NSString stringIsEmpty:edit_gurantor_lastName.text])
        {
            iserror=YES;
           
            [patient_guarantorLName setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_guarantorLName setTextColor:[UIColor blackColor]];
            
        }

       
        if([NSString stringIsEmpty:edit_gurantor_address.text])
        {
            iserror=YES;
            [patient_Address setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_Address setTextColor:[UIColor blackColor]];
            
        }

        
        if([NSString stringIsEmpty:edit_gurantor_city.text])
        {
           
            iserror=YES;
            [patient_City setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_City setTextColor:[UIColor blackColor]];
            
        }

        
        if([NSString stringIsEmpty:edit_gurantor_state.text])
        {
            iserror=YES;
            [patient_State setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_State setTextColor:[UIColor blackColor]];
            
        }

            
        if([NSString stringIsEmpty:edit_gurantor_zip.text])
        {
            iserror=YES;
            [patient_Zip setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_Zip setTextColor:[UIColor blackColor]];
            
        }
        
        if([NSString stringIsEmpty:edit_gurantor_phone.text])
        {
            iserror=YES;
            [patient_guarantorPNo setTextColor:[UIColor redColor]];
        }
        else
        {
            [patient_guarantorPNo setTextColor:[UIColor blackColor]];
            
        }
        }
    
    
     if (((isminor==YES)&&(iserror==NO))||(isminor==NO)) {
         IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
   
         NSURL *url = [NSURL URLWithString:appdalegate.demogrphicsUpdateUrl];
         
         NSString *xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?> <Patient><registrationID>%@</registrationID><patientID>%@</patientID><updateFlag>%i</updateFlag><isMinor>%i</isMinor><minor_mother_name>%@</minor_mother_name><minor_father_name>%@</minor_father_name><guarantorFirstName>%@</guarantorFirstName><guarantorLastName>%@</guarantorLastName><guarantorMiddleName>%@</guarantorMiddleName><guarantorAddress>%@</guarantorAddress><guarantorCity>%@</guarantorCity><guarantorState>%@</guarantorState><guarantorZip>%@</guarantorZip><guarantorPhone>%@</guarantorPhone></Patient>",appdalegate.registationID,appdalegate.patientId,3,isminor,edit_minor_motherName.text,edit_minor_fatherName.text,edit_gurantor_firstName.text,edit_gurantor_lastName.text,edit_gurantor_middleName.text,edit_gurantor_address.text,edit_gurantor_city.text,edit_gurantor_state.text,edit_gurantor_zip.text,edit_gurantor_phone.text];
         
       
         NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
         NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
         
         NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
         [request setURL:url];
         
         
         [request setHTTPMethod:@"POST"];
         [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
         [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
         [request setHTTPBody:postData];
         NSURLResponse* response;
         NSError *requestError = NULL;
         NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
         NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       
         
         if ([responseString isEqualToString:@"success"]) {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Patient is minor information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [self updateall];
         }
         else
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error! Patient is Minor information has not Update" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }
            
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}


-(void)updateall
{
     [self patientUpdate];
    [vi removeFromSuperview];
    [addView removeFromSuperview];
    [edit_step2 removeFromSuperview];
    [edit_step3 removeFromSuperview];
    
    self.working=NO;
    
    [self myOrientation];
}



-(NSString *)agefinder:(NSString *)dateofbirth
{
    NSDateFormatter *dateFormatObj = [[NSDateFormatter alloc]init];
    [dateFormatObj setDateFormat:@"yyyy-MM-dd"];
    NSDate *birthday = [dateFormatObj dateFromString:dateofbirth];
    
    NSDate* now = [NSDate date];
    
    
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    NSInteger age1 = [ageComponents year];
    NSString *str=[NSString stringWithFormat:@"%ld",(long)age1];
    return str;
}

- (IBAction)selectClicked:(id)sender {
    UIButton *btr=(UIButton *)sender;
    if (btr.tag==1) {
     
        
        if(dropDown == nil) {
            CGFloat f = 150;
             dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:titleTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==2)
    {
     //   NSArray * arry = [[NSArray alloc] initWithObjects:@"Male",@"Female",nil];
        
        if(dropDown == nil) {
            CGFloat f = 60;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:genderTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==3)
    {
        
        if(dropDown == nil) {
            CGFloat f =120;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:maritalStatusName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==4)
    {
//        [self addDropDown];
        
        if(dropDown == nil) {
            CGFloat f = 120;
           dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:smokingName IA:nil D:@"down"];            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    else if(btr.tag==5)
    {
        NSArray * arry  = [[NSArray alloc] initWithObjects:@"ACTIVE",@"INACTIVE",nil];
        
        if(dropDown == nil) {
            CGFloat f = 60;
           dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:arry IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==6)
    {
       
        
        
        if(dropDown == nil) {
            CGFloat f = 90;
           dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:studentTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==7)
    {
//        NSArray * arry = [[NSArray alloc] initWithObjects:@"Affrican/African-American",@"Asian",@"Native American",@"Pacific Islander/Native Hawaiian",@"Mixed Race",@"Other",@"Caucasian",nil];
        if(dropDown == nil) {
            
            CGFloat f;
            if ([EthnicOriginName count]>6) {
                f=180;
            }
            else
            {
              f= [EthnicOriginName count]*30;
            }
        
          dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:EthnicOriginName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    
    else if(btr.tag==8)
    {
        if(dropDown == nil) {
            CGFloat f ;
            if ([jobTypeName count]>7) {
                f=210;
            }
            else
            {
               f=[jobTypeName count]*30;
            }
            
        dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:jobTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
        
    }
    else if(btr.tag==9)
    {
        NSArray * arry = [[NSArray alloc] initWithObjects:@"Yes",@"No",nil];
        if(dropDown == nil) {
            CGFloat f = 60;
      dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:arry IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==10)
    {
        NSArray * arry = [[NSArray alloc] initWithObjects:@"Self Paid",@"Insurance Paid",nil];
        if(dropDown == nil) {
            CGFloat f = 60;
     dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:arry IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    else if(btr.tag==11)
    {
        
        if(dropDown == nil) {
            CGFloat f = 90;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:physicianTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }

}

-(NSString *) saveDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSString *result = [formater stringFromDate:date2];
   
    return result;
    
}
-(NSString *)viewDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSString *result = [formater stringFromDate:date2];
 
    return result;
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL temp=YES;
    if (textField==edit_gurantor_zip) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48)
            {
                temp=NO; // 48 unichar for 0
            }
            if (character > 57)
            {
                temp=NO; // 57 unichar for 9
            }
        }
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 5)
        {
            temp=NO;
        }
       
    }
    else if (textField==edit_gurantor_phone) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48)
            {
                temp= NO; // 48 unichar for 0
            }
            if (character > 57)
            {
                temp=NO; // 57 unichar for 9
            }
        }
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 10)
        {
            temp=NO;
        }
    }
    else if(textField==edit_fname||textField==edit_mname||textField==edit_lname||textField==edit_EmpName||textField==edit_minor_fatherName||textField==edit_minor_motherName||textField==edit_gurantor_firstName||textField==edit_gurantor_middleName||textField==edit_gurantor_lastName||textField==edit_gurantor_city)
    {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if ((character >= 65 && character<=90)||(character>=97 && character<=122)||character==32||character==8||character==39)
            {
                temp=YES; 
            }
            else
            {
                temp=NO;
            }
        }
    }
    else if (textField==edit_ssnno) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48)
            {
                temp= NO; // 48 unichar for 0
            }
            if (character > 57)
            {
                temp=NO; // 57 unichar for 9
            }
        }
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength > 9)
        {
            temp=NO;
        }
    }
    return temp;
}



//-(NSMutableArray*)getPhysician:(NSMutableArray *)Mutarr
//
//{
//    NSMutableArray *array=[[NSMutableArray alloc]init];
//    for (int i=0;i<=[Mutarr count]-1;++i) {
//        NSMutableDictionary *dictionary=[Mutarr objectAtIndex:i];
//     
//        NSString *str=[dictionary objectForKey:@"physicianName"];
//        
//        [array addObject:str];
//        dictionary=nil;
//        str=nil;
//    }
//       return array;
//}

-(NSString*)formatNumber:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    if(length >9) {
        mobileNumber = [mobileNumber substringFromIndex: length-9];
    }
    return mobileNumber;
}

-(int)getLength:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    return length;
}


-(void)addDropDown
{
    smokingStatus=[[NSMutableArray alloc]init];
    SmokingStatusXMLData *smokingstat=[[SmokingStatusXMLData alloc]init];
    smokingStatus=[smokingstat SmokingStatusList];
    
    smokingName=[smokingStatus valueForKey:@"smokingStatus"];
    smokingID=[smokingStatus valueForKey:@"id"];
    
    EmployerType=[[NSMutableArray alloc]init];
    EmploymentTypeXMLData *jobTypesStatus=[[EmploymentTypeXMLData alloc]init];
    EmployerType=[jobTypesStatus EmployerTypeList];
    
    jobTypeName=[EmployerType valueForKey:@"employmentType"];
    jobTypeID=[EmployerType valueForKey:@"id"];
   
    
    physicianStatus=[[NSMutableArray alloc]init];
    physicianList *pList=[[physicianList alloc]init];
    IndexAppDelegate *appdel=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    physicianStatus=[pList physicianListXML:appdel.registationID];
    physicianTypeName=[physicianStatus valueForKey:@"physicianName"];
    physicianID=[physicianStatus valueForKey:@"id"];
    
    
    
    titleList=[[NSMutableArray alloc]init];
    TitleTypeList *titleLst=[[TitleTypeList alloc]init];
    titleList=[titleLst titleTypeList];
    titleTypeName=[titleList valueForKey:@"name"];
    titleTypeID=[titleList valueForKey:@"id"];
    
    genderList=[[NSMutableArray alloc]init];
    genderTypeList *genderLst=[[genderTypeList alloc]init];
    genderList=[genderLst genderTypeListData];
    genderTypeName=[genderList valueForKey:@"name"];
    
    
    maritalList=[[NSMutableArray alloc]init];
    maritalStatusList *meritalList1=[[maritalStatusList alloc]init];
    maritalList=[meritalList1 maritalStatusListData];
    maritalStatusName=[maritalList valueForKey:@"name"];
    maritalStatusID=[maritalList valueForKey:@"id"];

    
    EthnicOriginList=[[NSMutableArray alloc]init];
    EthnicOriginXMLData *ethnicoriginList=[[EthnicOriginXMLData alloc]init];
    EthnicOriginList=[ethnicoriginList EthnicOriginStatusListData];
    EthnicOriginName=[EthnicOriginList valueForKey:@"ethnicOriginName"];
    EthnicOriginID=[EthnicOriginList valueForKey:@"id"];
    
    studentList=[[NSMutableArray alloc]init];
    studentTypeXMLList *studentType1=[[studentTypeXMLList alloc]init];
    studentList=[studentType1 studentTypeList];
    studentTypeName=[studentList valueForKey:@"name"];
    studentTypeID=[studentList valueForKey:@"id"];

    
   
}

@end
