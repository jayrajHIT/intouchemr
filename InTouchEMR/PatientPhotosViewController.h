//
//  PatientPhotosViewController.h
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import "PhotoCustomCell.h"
#import "IndexAppDelegate.h"
#import "NIDropDown.h"

@interface PatientPhotosViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,NIDropDownDelegate>
{
    UIPopoverController *popover;
    UIBarButtonItem *doneButton;
    BOOL newMedia;
    NSData *imageData;
    NSMutableArray *marr;
    
    UIImagePickerController *pickerImg;
    NSString *ACCESS_KEY_ID;
    NSString *SECRET_KEY;
    NSString *PICTURE_BUCKET;
     NSMutableArray *PhotoData;
     NSString *imgname_database,*saveImgname;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    IndexAppDelegate *appdalegate;
    NSString *image_Name,*documenttype;
    NSMutableDictionary *tempdict,*arr;
    NIDropDown *dropDown;
    
    NSMutableArray *DocumentTypeData;
    NSArray *documentTypeName,*DocumentTypeID;
    
}
-(void)clearCurrentView;
-(id)initwithArray:(NSMutableDictionary *)array;
- (IBAction)upload:(id)sender;
- (IBAction)submit:(id)sender;
- (IBAction)selectClicked:(id)sender;
- (IBAction)ImageSend:(id)sender;



@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;
@property (strong, nonatomic) IBOutlet UIButton *DocumentName;
@property (strong, nonatomic) IBOutlet UITextField *imgName;


@property (strong, nonatomic) IBOutlet UIImageView *myImage;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, retain) AWSS3 *s3;
@property (strong, nonatomic) IBOutlet UITableView *PhotoTable;

@property (strong, nonatomic) IBOutlet UIButton *landscapeDocumentName;
@property (strong, nonatomic) IBOutlet UITextField *landscapeimgName;


@property (strong, nonatomic) IBOutlet UIImageView *landscapemyImage;
@property (nonatomic, strong) UIPopoverController *landscapepopover;
@property (strong, nonatomic) IBOutlet UITableView *landscapePhotoTable;


@end
