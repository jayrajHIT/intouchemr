//
//  AddressViewController.m
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.

#import "AddressViewController.h"
#import "patientAddressData.h"
#import "IndexAppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "SHKActivityIndicator.h"
#import "addressCell.h"
#import "stateList.h"
#import "NSMutableString+xmlForm.h"
#import "AddressTypeList.h"
@interface AddressViewController ()

@end

@implementation AddressViewController
@synthesize patientContact,patientEmail,patientName,patientOrigin,ssnHeading,ssnValue,portraitView,landscapeView;

@synthesize landscapepatientContact,landscapepatientEmail,landscapepatientName,landscapepatientOrigin,landscapessnHeading,landscapessnValue;
@synthesize patientImage,landscapePatientImage,addressTable,landscapeaddressTable;
@synthesize edit_city,edit_pin,edit_state,edit_address,index,edit_addresstype,edit_country,edit_address2,Isprimary,edit_addInfo;
@synthesize addressType,Address1,state,city,Country,pincode,error,edit_Country;
@synthesize addView,orientchange,Save_update,btn_edit_Adress,btn_edit_land_Address,statebtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initwithArray:(NSMutableDictionary *)array 
{
      arr=array;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setWorking:NO];
    [self setError:NO];
    [self setOrientchange:NO];
    [statebtn setHidden:YES];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;

    
       if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    [Isprimary setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [Isprimary setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
      
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
    [self myOrientation];
  
  }


-(void)orientationChanged:(NSNotification *)notification{
    
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    // Do your orientation logic here
	
}

-(void)myOrientation
{
    [self performSelectorInBackground:@selector(mythread) withObject:nil];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		// Clear the current view and insert the orientation specific view.
		
        [self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        
        if ([arr objectForKey:@"FullName"]) {
            landscapepatientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            landscapepatientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            landscapepatientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            landscapepatientEmail.text=@"";
        }
        
        if ([arr objectForKey:@"ssn"]) {
            landscapessnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            landscapessnValue.text=@"";
        }
        
        if ([arr objectForKey:@"ethnicOriginName"]) {
            landscapepatientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            landscapepatientOrigin.text=@"";
        }

        if ([arr objectForKey:@"contactNo"]) {
              
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            landscapepatientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                     landscapepatientContact.text=nil;
        }
        }
        else
        {
            landscapepatientContact.text=@"";
        }
        
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [landscapePatientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
                
        [landscapeaddressTable reloadData];
         
        if (AddressData!=nil) {
            [self afterthread];
        }
        if (self.working==YES) {

            [self editView];            
        }
               
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
		[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        
        if ([arr objectForKey:@"FullName"]) {
            patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            patientName.text=@"";
        }
        if ([arr objectForKey:@"email"]) {
            patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            patientEmail.text=@"";
        }
        
        if ([arr objectForKey:@"ssn"]) {
            ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            ssnValue.text=@"";
        }
        
        if ([arr objectForKey:@"ethnicOriginName"]) {
            patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            patientOrigin.text=@"";
        }

        
      if ([arr objectForKey:@"contactNo"]) {
      
        NSString *string = [arr objectForKey:@"contactNo"];
        if ([string rangeOfString:@"@"].location == NSNotFound) {
            patientContact.text=[arr objectForKey:@"contactNo"];
        } else {
                       patientContact.text=nil;
        }
      }
        else
        {
             patientContact.text=@"";
        }
        
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        [addressTable reloadData];
        
        if (AddressData!=nil) {
            [self afterthread];
        }
        
        if (self.working==YES) {
            [self editView];
            
        }
       	}
}

-(void)mythread

{
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    patientAddressData *patientData=[[patientAddressData alloc]init];
    AddressData=[patientData patientAddressDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];

    [self addDropDown];
    [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
    
}

-(void)afterthread
{
    [addressTable reloadData];
    [landscapeaddressTable reloadData];
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}
-(void)editView 
{
      
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
       
        vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024,670)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [landscapeView addSubview:vi];
        addView.frame=CGRectMake(250, 15, 525, 620);
    [landscapeView addSubview:addView];
        addView.layer.borderWidth=2.0;
        addView.layer.cornerRadius=15.0;
        addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        edit_address.layer.cornerRadius=5.0;
        edit_address.layer.borderWidth=1.0;
        edit_address.layer.borderColor=[UIColor lightGrayColor].CGColor;
                
        edit_address2.layer.cornerRadius=5.0;
        edit_address2.layer.borderWidth=1.0;
        edit_address2.layer.borderColor=[UIColor lightGrayColor].CGColor;
        edit_addInfo.layer.cornerRadius=5.0;
        edit_addInfo.layer.borderWidth=1.0;
        edit_addInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
    vi=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 930)];
        vi.backgroundColor=[UIColor blackColor];
        vi.alpha=.8;
        [portraitView addSubview:vi];
    addView.frame=CGRectMake(125, 80, 525, 620);
        [portraitView addSubview:addView];
        addView.layer.borderWidth=2.0;
        addView.layer.cornerRadius=15.0;
        addView.layer.borderColor=[UIColor lightGrayColor].CGColor;
        edit_address.layer.cornerRadius=5.0;
        edit_address.layer.borderWidth=1.0;
        edit_address.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        edit_address2.layer.cornerRadius=5.0;
        edit_address2.layer.borderWidth=1.0;
        edit_address2.layer.borderColor=[UIColor lightGrayColor].CGColor;
        edit_addInfo.layer.cornerRadius=5.0;
        edit_addInfo.layer.borderWidth=1.0;
        edit_addInfo.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
}
    
    if ([self orientchange]) {
        [edit_Country setTitle:@"USA" forState:UIControlStateNormal];
        [self selectUSA];
        
    
    if ([self index]==1001) {
        [Save_update setTitle:@"Save" forState:UIControlStateNormal];
        
        [edit_addresstype setTitle:@"select one" forState:UIControlStateNormal];
        edit_address2.text=@"Address 2";
        edit_city.text=@"";
        [statebtn setTitle:@"select one" forState:UIControlStateNormal];
//        edit_state.text=@"";
        edit_pin.text=@"";
        edit_address.text=@"Address 1";
        edit_addInfo.text=@"Additional Information";
        isHealthFusion=@"0";
        [self setOrientchange:NO];
    }
    else
    {
        
        
        [Save_update setTitle:@"Update" forState:UIControlStateNormal];
    NSDictionary *editdict=[AddressData objectAtIndex:index];
        
        isHealthFusion=[editdict objectForKey:@"isHealthFusion"];
        
                
        address_id=[editdict objectForKey:@"id"];
    edit_city.text=[editdict objectForKey:@"city"];
    
    edit_pin.text=[editdict objectForKey:@"postalCode"];
    [edit_Country setTitle:[editdict objectForKey:@"countryName"] forState:UIControlStateNormal];
        if ([[edit_Country titleForState:UIControlStateNormal]isEqualToString:@"USA"]) {
            [statebtn setHidden:NO];
            [statebtn setTitle:[editdict objectForKey:@"state"] forState:UIControlStateNormal];
        }
        else
        {
            edit_state.text=[editdict objectForKey:@"state"];
        }
    edit_address.text=[editdict objectForKey:@"addressLine1"];
        if ([editdict objectForKey:@"addressLine2"]) {
            edit_address2.text=[editdict objectForKey:@"addressLine2"];
        }
        else
        {
            edit_address2.text=@"Address 2";
        }
        
        if ([editdict objectForKey:@"addressTypeID"]) {
            
             if ([addressTypeID indexOfObject:[editdict objectForKey:@"addressTypeID"]] != NSNotFound) {
            int a=  [addressTypeID indexOfObject:[editdict objectForKey:@"addressTypeID"]];
            [edit_addresstype setTitle:[addressTypeName objectAtIndex:a] forState:UIControlStateNormal];
             }
            else
            {
                [edit_addresstype setTitle:@"select one" forState:UIControlStateNormal];
            }
        }
        else
        {
            [edit_addresstype setTitle:@"select one" forState:UIControlStateNormal];
        }
        
             if ([[editdict objectForKey:@"isPrimary"] isEqualToString:@"1"]) {
            [Isprimary setOn:YES];
        }
        else
        {
            [Isprimary setOn:NO];
        }
        [self setOrientchange:NO];
    }

        
        if ([isHealthFusion isEqualToString:@"1"]) {
            [edit_address setUserInteractionEnabled:NO];
            [edit_addresstype setUserInteractionEnabled:NO];
            [edit_city setUserInteractionEnabled:NO];
            [edit_Country setUserInteractionEnabled:NO];
            [edit_pin setUserInteractionEnabled:NO];
            [edit_address2 setUserInteractionEnabled:NO];
            [statebtn setUserInteractionEnabled:NO];
            [edit_country setUserInteractionEnabled:NO];
            
        }
        else
        {
            [edit_address setUserInteractionEnabled:YES];
            [edit_addresstype setUserInteractionEnabled:YES];
            [edit_city setUserInteractionEnabled:YES];
            [edit_Country setUserInteractionEnabled:YES];
            [edit_pin setUserInteractionEnabled:YES];
            [edit_address2 setUserInteractionEnabled:YES];
            [statebtn setUserInteractionEnabled:YES];
            [edit_country setUserInteractionEnabled:NO];
        }
     
           }
    
}

-(void) clearCurrentView {
  
    [vi removeFromSuperview];
	
    if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}

- (IBAction)cancelView:(id)sender
{
    addressType.textColor=[UIColor blackColor];
    Address1.textColor=[UIColor blackColor];
    city.textColor=[UIColor blackColor];
    state.textColor=[UIColor blackColor];
    Country.textColor=[UIColor blackColor];
    pincode.textColor=[UIColor blackColor];
    [addView removeFromSuperview];
    [vi removeFromSuperview];
    self.working=NO;
    [self setOrientchange:NO];
}
- (IBAction)update:(id)sender {
    [self setWorking:YES];
    [self setIndex:1001];
    [self setOrientchange:YES];
       [self editView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [AddressData count];
    // return [arr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==addressTable) {
        
        static NSString *CellIdentifierPortrait = @"portrait";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"addressCell" owner:self options:nil];
        
        
        cell=(addressCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
        
        if (cell==nil) {
            cell=[[ addressCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierPortrait];
            cell=[nib objectAtIndex:0];
        }
        
        
        NSDictionary *tempdict=[AddressData objectAtIndex:indexPath.row];
      
        if ([tempdict objectForKey:@"addressTypeID"]) {
            int a=[addressTypeID indexOfObject:[tempdict objectForKey:@"addressTypeID"]];
            
            cell.addressType.text=[addressTypeName objectAtIndex:a];
        }
        else
        {
            cell.addressType.text=@"";
        }
        if ([tempdict objectForKey:@"city"]) {
            cell.city.text=[tempdict objectForKey:@"city"];
        }
        else
        {
            cell.city.text=@"";
        }
       
        if ([tempdict objectForKey:@"state"]) {
            cell.State.text=[tempdict objectForKey:@"state"];
        }
        else
        {
            cell.State.text=@"";
        }
       
        if ([tempdict objectForKey:@"postalCode"]) {
            cell.pincode.text=[tempdict objectForKey:@"postalCode"];
        }
        else
        {
            cell.pincode.text=@"";
        }
      
        if ([tempdict objectForKey:@"addressLine1"]) {
            cell.address.text=[tempdict objectForKey:@"addressLine1"];
        }
        else
        {
            cell.address.text=@"";
        }
        if ([tempdict objectForKey:@"countryName"]) {
            cell.country.text=[tempdict objectForKey:@"countryName"];
        }
        else
        {
            cell.country.text=@"";
        }
      
        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (tableView==landscapeaddressTable) {
        
        static NSString *CellIdentifier = @"landscape";
        
        
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"addressCell" owner:self options:nil];
        
        cell=(addressCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil) {
            cell=[[ addressCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell=[nib objectAtIndex:1];
        }
        
        
        NSDictionary *tempdict=[AddressData objectAtIndex:indexPath.row];
        
        if ([tempdict objectForKey:@"addressTypeID"]) {
            if ([addressTypeID indexOfObject:[tempdict objectForKey:@"addressTypeID"]] != NSNotFound) {

            
            int a=[addressTypeID indexOfObject:[tempdict objectForKey:@"addressTypeID"]];
           
            cell.addressType.text=[addressTypeName objectAtIndex:a];
            }
            else
            {
                cell.addressType.text=@"";
            }
        }
        else
        {
            cell.addressType.text=@"";
        }
        if ([tempdict objectForKey:@"city"]) {
            cell.city.text=[tempdict objectForKey:@"city"];
        }
        else
        {
            cell.city.text=@"";
        }
        
        if ([tempdict objectForKey:@"state"]) {
            cell.State.text=[tempdict objectForKey:@"state"];
        }
        else
        {
            cell.State.text=@"";
        }
        
        if ([tempdict objectForKey:@"postalCode"]) {
            cell.pincode.text=[tempdict objectForKey:@"postalCode"];
        }
        else
        {
            cell.pincode.text=@"";
        }
        
        if ([tempdict objectForKey:@"addressLine1"]) {
            cell.address.text=[tempdict objectForKey:@"addressLine1"];
        }
        else
        {
            cell.address.text=@"";
        }
        if ([tempdict objectForKey:@"countryName"]) {
            cell.country.text=[tempdict objectForKey:@"countryName"];
        }
        else
        {
            cell.country.text=@"";
        }

        [cell.edit addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)accessoryButtonTapped:(id)sender event:(id)event
{
   
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition;
    NSIndexPath *indexPath;
	if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        currentTouchPosition = [touch locationInView:landscapeaddressTable];
        indexPath = [landscapeaddressTable indexPathForRowAtPoint: currentTouchPosition];
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        currentTouchPosition = [touch locationInView:addressTable];
        indexPath = [addressTable indexPathForRowAtPoint: currentTouchPosition];
	}
    
    
	if (indexPath != nil)
	{
        [self tableView: addressTable accessoryButtonTappedForRowWithIndexPath: indexPath];
        
	}
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    [self setIndex:indexPath.row];
       self.working=YES;
    [self setOrientchange:YES];
    
    [self editView];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.working=YES;
   
    [self setIndex:indexPath.row];
    [self setOrientchange:YES];
    [self editView];
        
    
}
- (IBAction)next:(id)sender {
    self.tabBarController.selectedIndex=2;
}

-(IBAction)saveAddress:(id)sender
{
      add1=edit_address.text;
   add2=edit_address2.text;
    temp_city=edit_city.text;
    if ([[edit_Country titleForState:UIControlStateNormal]isEqualToString:@"USA"]) {
        temp_state=[statebtn titleForState:UIControlStateNormal];
    }
        
    
 countryID=[self countryId:[edit_Country titleForState:UIControlStateNormal]];
    
   
    if ([[edit_addresstype titleForState:UIControlStateNormal] isEqualToString:@"select one"]) {
         addressType.textColor=[UIColor redColor];
    }
    else
    {
    int aa=[addressTypeName indexOfObject:[edit_addresstype titleForState:UIControlStateNormal]];
    
     AddressId=[addressTypeID objectAtIndex:aa];
        addressType.textColor=[UIColor blackColor];
    }
        temp_pinCode=edit_pin.text;
    addinfo=edit_addInfo.text;
   
    if ([Isprimary isOn]) {
        isPrimay=@"1";
    }
    else
    {
        isPrimay=@"0";
    }
    
    
     if ([isHealthFusion isEqualToString:@"0"]) {
    
   
         
         if([add1 isEqualToString:@"Address 1"])
    {
        Address1.textColor=[UIColor redColor];
     //   [self setError:YES];
    }
    else{
        Address1.textColor=[UIColor blackColor];
     //   [self setError:NO];
    }
    if([add2 isEqualToString:@"Address 2"])
    {
        add2=@"";
    }
    
    if([temp_city isEqualToString:@""])
    {
        city.textColor=[UIColor redColor];
      //  [self setError:YES];
    }
    else
    {
        city.textColor=[UIColor blackColor];
     //   [self setError:NO];
    }
    
    if([temp_state isEqualToString:@"Select One"])
    {
        state.textColor=[UIColor redColor];
      //  [self setError:YES];
    }
    else
    {
        state.textColor=[UIColor blackColor];
  //  [self setError:NO];
    }
    if(countryID==0)
    {
        Country.textColor=[UIColor redColor];
     //   [self setError:YES];
    }
    else
    {
        Country.textColor=[UIColor blackColor];
    //    [self setError:NO];
    }
    if([temp_pinCode isEqualToString:@""])
    {
        pincode.textColor=[UIColor redColor];
       // [self setError:YES];
    }
    else
    {
        pincode.textColor=[UIColor blackColor];
   //    [self setError:NO];
    }
    
    }
    
    if (((AddressId==0)||[add1 isEqualToString:@"Address 1"]||[temp_city isEqualToString:@""]||[temp_state isEqualToString:@"Select One"]||countryID==0||[temp_pinCode isEqualToString:@""])&&[isHealthFusion isEqualToString:@"0"]) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Required fields have been left blank." message:@"The missing information is highlighted in red." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
     
    }
    else
    {
        NSMutableString *newAddress=[NSMutableString stringWithString:add1];
        [newAddress xmlSimpleEscape];
        
        
        
        IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
        NSString *xmlText;
        
           NSURL *url = [NSURL URLWithString:appdalegate.addressSavedUrl];
        
        if ([self index]==1001) {
            xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Address><addressLine1>%@</addressLine1><addressLine2>%@</addressLine2><postalCode>%@</postalCode><city>%@</city> <state>%@</state><isPrimary>%@</isPrimary><additionalInformation>%@</additionalInformation><addressTypeID>%@</addressTypeID> <countryID>%d</countryID><patientID>%@</patientID><isHealthFusion>%@</isHealthFusion></Address>",newAddress,add2,temp_pinCode,temp_city,temp_state,isPrimay,addinfo,AddressId,countryID,appdalegate.patientId,isHealthFusion];
        }
        else{
            xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Address><id>%d</id><addressLine1>%@</addressLine1><addressLine2>%@</addressLine2><postalCode>%@</postalCode><city>%@</city> <state>%@</state><isPrimary>%@</isPrimary><additionalInformation>%@</additionalInformation><addressTypeID>%@</addressTypeID> <countryID>%d</countryID><patientID>%@</patientID><isHealthFusion>%@</isHealthFusion></Address>",address_id.intValue,newAddress,add2,temp_pinCode,temp_city,temp_state,isPrimay,addinfo,AddressId,countryID,appdalegate.patientId,isHealthFusion];
            
        }
       
            NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
           NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        [request setURL:url];
        
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLResponse* response;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        if ([self index]==1001) {
            if ([responseString isEqualToString:@"success"]) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your address information has been successfully saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];

            }
            else
            {
             
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your address information has not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
             }

        else{
            if ([responseString isEqualToString:@"success"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your address information has been successfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            }
            else
            {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your address information has not updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
   
     [self reloadTable];
    }
      
    
}





- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	
	[textField resignFirstResponder];
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag==1 && [textView.text isEqualToString:@"Address 1"]) {
        textView.text=@"";
    }
    if (textView.tag==2 && [textView.text isEqualToString:@"Address 2"]) {
        textView.text=@"";
    }
    if (textView.tag==5 && [textView.text isEqualToString:@"Additional Information"]) {
        textView.text=@"";
    }
    [self animateTextView: textView up: YES];
    
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView: textView up: NO];
    
    if (textView.tag==1 && textView.text.length==0 ) {
        
        textView.text=@"Address 1";
    }
    if (textView.tag==2 && textView.text.length==0 ) {
        
        textView.text=@"Address 2";
    }
    if (textView.tag==5 && textView.text.length==0 ) {
        
        textView.text=@"Additional Information";
    }
    
}
- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        
        if (textView.tag==5){
            const int movementDistance = 200; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            addView.frame=CGRectOffset(addView.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        
    }
}


- (IBAction)selectClicked:(id)sender {
    UIButton *btr=(UIButton *)sender;
    if (btr.tag==1) {
       //  NSArray *arr1 = [[NSArray alloc]initWithObjects:@"Home",@"Billing Address",@"Current or Temporary",@"Legal Address",@"Mailing",@"Office / Business",@"Shipping Address",@"Vacation",nil];
        
        if(dropDown == nil) {
            CGFloat f = 90;
             dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:addressTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    
    if (btr.tag==3) {
     
        if(dropDown == nil) {
            CGFloat f = 240;
            dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:state1 IA:nil D:@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
    }
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    
    }
-(void)rel{
    
    dropDown = nil;
  //  str_addressType=edit_addresstype.titleLabel.text;
}

-(void)selectUSA
{
  
    stateList *statelist=[[stateList alloc]init];
    states=[statelist StateList];
    state1=[self convertString:states];
    
    edit_state.text=@"";
    [statebtn setHidden:NO];
    [edit_state setEnabled:NO];
    [edit_state setUserInteractionEnabled:NO];
    
 }

-(NSMutableArray*)convertString:(NSMutableArray *)Mutarr

{
    NSMutableArray *array=[[NSMutableArray alloc]init];
    for (int i=0;i<=[Mutarr count]-1;++i) {
        NSMutableDictionary *dictionary=[Mutarr objectAtIndex:i];
       
        NSString *str=[dictionary objectForKey:@"states"];
     
        [array addObject:str];
        dictionary=nil;
        str=nil;
           }
    
    return array;
    
}


-(void)reloadTable
{
    [addView removeFromSuperview];
    [vi removeFromSuperview];
    self.working=NO;
    [self setOrientchange:NO];
    [self myOrientation];
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        
        if (textField.tag==5){
            const int movementDistance = 200; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            addView.frame=CGRectOffset(addView.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        if (textField.tag==4){
            const int movementDistance = 100; // tweak as needed
            const float movementDuration = 0.3f; // tweak as needed
            
            int movement = (up ? -movementDistance : movementDistance);
            
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            addView.frame=CGRectOffset(addView.frame, 0, movement);
            [UIView commitAnimations];
            
        }
        
    }
}

-(int)countryId:(NSString *)str
{
    int i=0;
    if ([str isEqualToString:@"USA"]) {
        i=1;
    }
    else if ([str isEqualToString:@"Canada"])
    {
        i=2;
    }
    else if ([str isEqualToString:@"Mexico"])
    {
        i=3;
    }
    return i;
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    BOOL flag=YES;
    if (textField.tag==5) {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48) flag=NO; // 48 unichar for 0
            if (character > 57) flag=NO; // 57 unichar for 9
        }
        
        // Check for total length
        NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
        
        if (proposedNewLength >5) flag=NO;
        
    }
    else if(textField.tag==4)
    {
        NSUInteger lengthOfString = string.length;
        for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if ((character >= 65 && character<=90)||(character>=97 && character<=122)||character==32||character==8)
            {
                flag=YES; // 48 unichar for 0
            }
            else
            {
                flag= NO;
            }
        }
    }
    return flag;
}

-(void)addDropDown
{
    addressTypeArr=[[NSMutableArray alloc]init];
    AddressTypeList *addressList=[[AddressTypeList alloc]init];
    addressTypeArr=[addressList AddressTypeXMLList];
    
    addressTypeName=[addressTypeArr valueForKey:@"name"];
    addressTypeID=[addressTypeArr valueForKey:@"id"];
    
    
}

@end
