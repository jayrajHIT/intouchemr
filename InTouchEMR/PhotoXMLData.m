//
//  PhotoXMLData.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 20/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "PhotoXMLData.h"
#import "IndexAppDelegate.h"
@implementation PhotoXMLData

-(NSMutableArray *)patientPhotoDataByXML:(NSString *)patientID registationNO:(NSString *)registationID
{
   
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString *postString = [[NSString alloc]initWithFormat:@"%@/%@/%@",globalDele.uploadUrl,patientID,registationID];
    
   // NSString *postString = [[NSString alloc]initWithFormat:@"http://54.235.202.25/InTouchEmrWebService/services/intouchEMR/getPatientDocuments/%@/%@",patientID,registationID];
  
  //  NSString *postString = [[NSString alloc]initWithFormat:@"http://10.0.0.115:8080/InTouchEmrWebService/services/intouchEMR/getPatientDocuments/%@/%@",patientID,registationID];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
     //   NSLog(@"success");
    }
    else
    {
       // NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"documentName"]||[myelement isEqualToString:@"documentTypeID"]||[myelement isEqualToString:@"receiptDate"]||[myelement isEqualToString:@"documentUrl"]||[myelement isEqualToString:@"contentType"])
        { 
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Document"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
