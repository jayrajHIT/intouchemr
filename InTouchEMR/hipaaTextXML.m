//
//  hipaaTextXML.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 09/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "hipaaTextXML.h"
#import "IndexAppDelegate.h"

@implementation hipaaTextXML
-(NSMutableString *)getHipaaTextXML
{
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSURL *url = [NSURL URLWithString:globalDele.hipaaText];
    
    NSString *xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><HIPPAAgreement><id>1</id></HIPPAAgreement>"];
    
    
   
    NSData *postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
   
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse* response;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    
//    NSString *myString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
    
   if (!arr) {
        arr=[[NSMutableString alloc]init];
    }
    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:responseData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
    //    NSLog(@"success");
    }
    else
    {
     //   NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([myelement isEqualToString:@"HippaAggrement"]) {
         [arr appendString:string];
    }

           
    
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"HippaAggrement"]) {
        
        
    }
    
}


@end
