//
//  eligibilityServiceCell.h
//  InTouchEMR
//
//  Created by Pradeep Singhal on 04/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eligibilityServiceCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *serviceType;
@property (strong, nonatomic) IBOutlet UILabel *serviceValue;
@end
