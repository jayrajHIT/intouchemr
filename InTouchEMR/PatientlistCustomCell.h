//
//  PatientlistCustomCell.h
//  InTouchEMR
//
//  Created by DSC on 12/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatientlistCustomCell : UITableViewCell

    @property (strong, nonatomic) IBOutlet UILabel *patientName;
    @property (strong, nonatomic) IBOutlet UILabel *emailId;
    @property (strong, nonatomic) IBOutlet UILabel *SsnNo;
    @property (strong, nonatomic) IBOutlet UILabel *gender;
    @property (strong, nonatomic) IBOutlet UIButton *accessoryButton;
@property (strong, nonatomic) IBOutlet UIImageView *patientImgView;
@property (strong, nonatomic) IBOutlet UILabel *ssnNoHeading;

@end
