/*

 filename: PatientlistCustomCell.h

 Dated: 12 April 2013

 Purpose: File used for handle the response data from server in xml form.
 
 */

#import <Foundation/Foundation.h>

@interface patientlistXMLData : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)patientListByXML:(NSString *)registationID;
@end
