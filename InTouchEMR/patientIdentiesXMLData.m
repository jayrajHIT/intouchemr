//
//  patientIdentiesXMLData.m
//  InTouchEMR
//
//  Created by Apple on 15/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "patientIdentiesXMLData.h"
#import "IndexAppDelegate.h"

@implementation patientIdentiesXMLData

-(NSMutableArray *)patientIdentityDataByXML:(NSString *)patientID registationNO:(NSString *)registationID
{
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
   
    NSString *postString = [[NSString alloc]initWithFormat:@"%@/%@/%@",globalDele.identifierUrl,patientID,registationID];
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
   
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
      //  NSLog(@"success");
    }
    else
    {
       // NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"identityTypeName"]||[myelement isEqualToString:@"idNumber"]||[myelement isEqualToString:@"issuingSource"]||[myelement isEqualToString:@"startDate"]||[myelement isEqualToString:@"endDate"]||[myelement isEqualToString:@"isPrimary"]||[myelement isEqualToString:@"id"]||[myelement isEqualToString:@"contactNo"]||[myelement isEqualToString:@"identityTypeID"])
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"PatientIdentity"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
