//
//  patientContactData.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 18/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "patientInsurenceData.h"
#import "IndexAppDelegate.h"
@implementation patientInsurenceData

-(NSMutableArray *)patientInsurenceDataByXML:(NSString *)registationID
{
    
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString *postString = [[NSString alloc]initWithFormat:@"%@/%@",globalDele.insuranceCompanyUrl,registationID];
    
    NSLog(@"%@",postString);

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString:postString]];
    
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
    
   
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:returnData];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
     //   NSLog(@"success--%@",arr);
    }
    else
    {
      //  NSLog(@"error");
    }
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        if ([myelement isEqualToString:@"id"]||[myelement isEqualToString:@"name"])
        {
            if(!dict)
            {
                dict=[[NSMutableDictionary alloc]init];
            }
            [dict setObject:string forKey:myelement];
        }
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Insurance"]) {
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
