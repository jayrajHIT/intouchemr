//
//  eligiblePayers.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 28/07/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "eligiblePayers.h"
#import "IndexAppDelegate.h"

@implementation eligiblePayers
-(NSMutableArray *)PayerList
{
   
    IndexAppDelegate *globalDele=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSURL *url = [NSURL URLWithString:globalDele.eligiblepayersURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&error];
    
//    NSString *myString = [[NSString alloc] initWithData:returnData encoding:NSASCIIStringEncoding];
//    NSLog(@"Response Data %@",myString);
    
    
    NSMutableString *resultstr=[[NSMutableString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    payerId=@"";
    NSData *resultdata=[resultstr dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSXMLParser *xmlparsing=[[NSXMLParser alloc]initWithData:resultdata];
    xmlparsing.delegate=self;
    if([xmlparsing parse]){
        
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"payerName" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        [arr sortUsingDescriptors:sortDescriptors];
     //   NSLog(@"Success");
        
    }
    else
    {
      //  NSLog(@"error");
    }
 
    return arr;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    myelement=elementName;
    
    currentNodeContent=[NSMutableString stringWithString:@""];
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    NSRange renge=[string rangeOfString:@"\n" options:NSCaseInsensitiveSearch];
    if (renge.location==NSNotFound) {
        
        
        if ([myelement isEqualToString:@"payer-id"])
        {
            
            payerId=string;
            
        }
        else if ([myelement isEqualToString:@"name"])
        {
            [currentNodeContent appendString:string];
        }
        
    }
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"name"]){
        
        if(!dict)
        {
            dict=[[NSMutableDictionary alloc]init];
        }
        
        [dict setObject:currentNodeContent forKey:@"payerName"];
        [dict setObject:payerId forKey:@"payerID"];
        if (!arr) {
            arr=[[NSMutableArray alloc]init];
        }
        
        [arr addObject:dict];
        dict=nil;
    }
    
}

@end
