//
//  patientIdentiesXMLData.h
//  InTouchEMR
//
//  Created by Apple on 15/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface patientIdentiesXMLData : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)patientIdentityDataByXML:(NSString *)patientID registationNO:(NSString *)registationID;

@end
