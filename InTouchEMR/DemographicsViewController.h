//
//  DemographicsViewController.h
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "NIDropDown.h"

@class TabBarView;

@interface DemographicsViewController : UIViewController<MFMailComposeViewControllerDelegate,UITextFieldDelegate,NIDropDownDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate>
{
    
    UIBarButtonItem *doneButton;
    NSMutableArray *DemographicsData;
    NSMutableDictionary *dict;
    TabBarView *tabview;
    UIImageView *img_bg;
    UIDeviceOrientation orientation;
    IBOutlet UIView *portraitView;
    IBOutlet UIView *landscapeView;
    NSString *name_image;
    int btnTag,btn_img_tag;
    BOOL Is;
    UIDatePicker *datePicker;
    UIView *vi;
    NSMutableDictionary *arr;
    UIImage *image;
    IBOutlet UIButton *btnSelect;
    NIDropDown *dropDown;
    UIPopoverController *popoverController;
    int updateFlag;
    NSString *str_emailid;
    NSString *str_username;
    NSString *str_firstname;
    NSString *str_middlename;
    NSString *str_lastname;
    NSString *str_nickname;
    NSString *str_ssn;
    NSString *str_physician;
    NSString *dob,*age;
    NSDateFormatter *form;
    int status,curEmp ;
    NSString *studentID;
    NSString *raceOrigin;
   // NSString *pcpState;
    NSString *paymentStr,*job,*stuID,*orgin;
    
    NSMutableArray *smokingStatus,*EmployerType,*physicianStatus,*titleList,*genderList,*maritalList,*EthnicOriginList,*studentList;
    
    NSArray* smokingID,*smokingName,*jobTypeID,*jobTypeName,*physicianID,*physicianTypeName ,*titleTypeName,*titleTypeID,*genderTypeName,*genderTypeID,*maritalStatusName,*maritalStatusID,*EthnicOriginName,*EthnicOriginID,*studentTypeName,*studentTypeID;
    
 //   UIActionSheet *pickerViewPopup;
   
}
- (IBAction)step1:(id)sender;
- (IBAction)step2:(id)sender;
- (IBAction)step3:(id)sender;
- (IBAction)next:(id)sender;

- (IBAction)profile_saved:(id)sender;
- (IBAction)selectClicked:(id)sender;
-(void)rel;
- (IBAction)cancelView:(id)sender;
- (IBAction)update:(id)sender;
-(void)clearCurrentView;
- (IBAction)emailSend:(id)sender;
-(id)initwithImageName:(NSString *)str;
-(id)initwithArray:(NSMutableDictionary *)array;

- (IBAction)Additional_saved:(id)sender;
- (IBAction)Minor_saved:(id)sender;


 @property (retain, nonatomic) NSMutableDictionary *arr;
@property (retain, nonatomic) IBOutlet UIButton *btnemail;
@property (retain, nonatomic) IBOutlet UIButton *landscapebtnemail;

@property (nonatomic, assign) BOOL orientchange;
@property (retain, nonatomic) IBOutlet UIButton *btnSelect;
@property (retain, nonatomic) IBOutlet UIView *step2view;
@property (retain, nonatomic) IBOutlet UIView *step3view;
@property (retain, nonatomic) IBOutlet UIView *landscapestep2view;
@property (retain, nonatomic) IBOutlet UIView *landscapestep3view;
@property (strong, nonatomic) IBOutlet UIView *addView;
@property (retain, nonatomic) IBOutlet UIView *edit_step2;
@property (retain, nonatomic) IBOutlet UIView *edit_step3;

@property (strong, nonatomic) IBOutlet UIButton *physician;

@property (strong, nonatomic) IBOutlet UIButton *edit_btn_mstatus;
@property (strong, nonatomic) IBOutlet UIButton *edit_btn_smokingStatus;
@property (strong, nonatomic) IBOutlet UIButton *edit_btn_gender;
@property (strong, nonatomic) IBOutlet UITextField *edit_fname;
@property (strong, nonatomic) IBOutlet UITextField *edit_mname;
@property (strong, nonatomic) IBOutlet UITextField *edit_lname;
@property (strong, nonatomic) IBOutlet UITextField *edit_ssnno;
@property (strong, nonatomic) IBOutlet UIButton *edit_title;
@property (strong, nonatomic) IBOutlet UITextField *edit_username;
@property (strong, nonatomic) IBOutlet UITextField *edit_nickname;
@property (strong, nonatomic) IBOutlet UITextField *edit_email;


@property (strong, nonatomic) IBOutlet UITextField *edit_dob;


@property (nonatomic, assign) BOOL working;
@property (strong, nonatomic) IBOutlet UIImageView *landscapePatientImage;
@property (strong, nonatomic) IBOutlet UIImageView *patientImage;

@property (strong, nonatomic) IBOutlet UIView *landscapeView;
@property (strong, nonatomic) IBOutlet UIView *portraitView;



@property (strong, nonatomic) IBOutlet UILabel *originName;
@property (strong, nonatomic) IBOutlet UILabel *firstName;
@property (strong, nonatomic) IBOutlet UILabel *lastName;
@property (strong, nonatomic) IBOutlet UILabel *middleName;
@property (strong, nonatomic) IBOutlet UILabel *dateOfBirth;
@property (strong, nonatomic) IBOutlet UILabel *SmokingStatus;

@property (strong, nonatomic) IBOutlet UILabel *emailId;
@property (strong, nonatomic) IBOutlet UILabel *ssnNo;
@property (strong, nonatomic) IBOutlet UILabel *gender;
@property (strong, nonatomic) IBOutlet UILabel *maritalStatus;
@property (strong, nonatomic) IBOutlet UILabel *nickName;
@property (strong, nonatomic) IBOutlet UILabel *referalPhysician;
@property (strong, nonatomic) IBOutlet UILabel *FullName;
@property (strong, nonatomic) IBOutlet UILabel *patientTitle;

@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeReferalPhysician;

@property (strong, nonatomic) IBOutlet UILabel *ssnValue;
@property (strong, nonatomic) IBOutlet UILabel *patientEmail;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnValue;
@property (strong, nonatomic) IBOutlet UILabel *landscapepatientEmail;


@property (strong, nonatomic) IBOutlet UILabel *landscapeoriginName;
@property (strong, nonatomic) IBOutlet UILabel *landscapefirstName;
@property (strong, nonatomic) IBOutlet UILabel *landscapelastName;
@property (strong, nonatomic) IBOutlet UILabel *landscapemiddleName;
@property (strong, nonatomic) IBOutlet UILabel *landscapedateOfBirth;
@property (strong, nonatomic) IBOutlet UILabel *landscapeSmokingStatus;
@property (strong, nonatomic) IBOutlet UILabel *landscapeemailId;
@property (strong, nonatomic) IBOutlet UILabel *landscapessnNo;
@property (strong, nonatomic) IBOutlet UILabel *landscapegender;
@property (strong, nonatomic) IBOutlet UILabel *landscapemaritalStatus;
@property (strong, nonatomic) IBOutlet UILabel *landscapenickName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeFullName;
@property (strong, nonatomic) IBOutlet UILabel *landscapetitle;
@property (strong, nonatomic) IBOutlet UILabel *landscapeusername;


@property (strong, nonatomic) IBOutlet UILabel *jobType;
@property (strong, nonatomic) IBOutlet UILabel *RaceOriginName;
@property (strong, nonatomic) IBOutlet UILabel *PayStatus;


@property (strong, nonatomic) IBOutlet UILabel *HippaAgreement;
@property (strong, nonatomic) IBOutlet UILabel *Student;
@property (strong, nonatomic) IBOutlet UILabel *Status;
@property (strong, nonatomic) IBOutlet UILabel *currentEmp;
@property (strong, nonatomic) IBOutlet UILabel *EmpName;

@property (strong, nonatomic) IBOutlet UITextView *OtherInfo;
@property (strong, nonatomic) IBOutlet UITextView *EmpAdd;
@property (strong, nonatomic) IBOutlet UILabel *PCPRelease;


@property (strong, nonatomic) IBOutlet UILabel *landscapejobType;
@property (strong, nonatomic) IBOutlet UILabel *landscapeRaceOriginName;
@property (strong, nonatomic) IBOutlet UILabel *landscapePayStatus;
@property (strong, nonatomic) IBOutlet UITextView *landscapeEmpAdd;

@property (strong, nonatomic) IBOutlet UILabel *landscapeHippaAgreement;
@property (strong, nonatomic) IBOutlet UILabel *landscapeStudent;
@property (strong, nonatomic) IBOutlet UILabel *landscapeStatus;
@property (strong, nonatomic) IBOutlet UILabel *landscapecurrentEmp;
@property (strong, nonatomic) IBOutlet UILabel *landscapeEmpName;
@property (strong, nonatomic) IBOutlet UITextView *landscapeOtherInfo;
@property (strong, nonatomic) IBOutlet UILabel *landscapePCPRelease;


@property (strong, nonatomic) IBOutlet UILabel *fatherName;
@property (strong, nonatomic) IBOutlet UILabel *MotherName;
@property (strong, nonatomic) IBOutlet UILabel *guarantorFName;
@property (strong, nonatomic) IBOutlet UILabel *guarantorMName;
@property (strong, nonatomic) IBOutlet UILabel *guarantorLName;
@property (strong, nonatomic) IBOutlet UILabel *Address;
@property (strong, nonatomic) IBOutlet UILabel *City;
@property (strong, nonatomic) IBOutlet UILabel *State;
@property (strong, nonatomic) IBOutlet UILabel *Zip;
@property (strong, nonatomic) IBOutlet UILabel *guarantorPNo;


@property (strong, nonatomic) IBOutlet UILabel *landscapefatherName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeMotherName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeguarantorFName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeguarantorMName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeguarantorLName;
@property (strong, nonatomic) IBOutlet UILabel *landscapeAddress;
@property (strong, nonatomic) IBOutlet UILabel *landscapeCity;
@property (strong, nonatomic) IBOutlet UILabel *landscapeState;
@property (strong, nonatomic) IBOutlet UILabel *landscapeZip;
@property (strong, nonatomic) IBOutlet UILabel *landscapeguarantorPNo;




@property (strong, nonatomic) IBOutlet UIButton *edit_Status;
@property (strong, nonatomic) IBOutlet UIButton *edit_Student;
@property (strong, nonatomic) IBOutlet UIButton *edit_PaymentStatus;
@property (strong, nonatomic) IBOutlet UIButton *edit_RaceOrigin;
@property (strong, nonatomic) IBOutlet UIButton *edit_JobType;
@property (strong, nonatomic) IBOutlet UISwitch *edit_CurrentEmp;
@property (strong, nonatomic) IBOutlet UITextView *edit_EmpAddress;
@property (strong, nonatomic) IBOutlet UITextView *edit_OtherInfo;
@property (strong, nonatomic) IBOutlet UITextField *edit_EmpName;

@property (strong, nonatomic) IBOutlet UIButton *edit_PCPRelease;

@property (strong, nonatomic) IBOutlet UILabel *edit_status;
@property (strong, nonatomic) IBOutlet UILabel *edit_paymentmode;

@property (strong, nonatomic) IBOutlet UISwitch *edit_isMinor;
@property (strong, nonatomic) IBOutlet UITextField *edit_minor_fatherName;
@property (strong, nonatomic) IBOutlet UITextField *edit_minor_motherName;
@property (strong, nonatomic) IBOutlet UITextField *edit_gurantor_firstName;
@property (strong, nonatomic) IBOutlet UITextField *edit_gurantor_middleName;
@property (strong, nonatomic) IBOutlet UITextField *edit_gurantor_lastName;
@property (strong, nonatomic) IBOutlet UITextView *edit_gurantor_address;
@property (strong, nonatomic) IBOutlet UITextField *edit_gurantor_city;
@property (strong, nonatomic) IBOutlet UITextField *edit_gurantor_state;
@property (strong, nonatomic) IBOutlet UITextField *edit_gurantor_zip;
@property (strong, nonatomic) IBOutlet UITextField *edit_gurantor_phone;


@property (strong, nonatomic) IBOutlet UILabel *profile_firstName;
@property (strong, nonatomic) IBOutlet UILabel *profile_lastName;
@property (strong, nonatomic) IBOutlet UILabel *profile_UserName;
@property (strong, nonatomic) IBOutlet UILabel *profile_dateOfBirth;
@property (strong, nonatomic) IBOutlet UILabel *profile_emailId;
@property (strong, nonatomic) IBOutlet UILabel *patient_fatherName;
@property (strong, nonatomic) IBOutlet UILabel *patient_MotherName;
@property (strong, nonatomic) IBOutlet UILabel *patient_guarantorFName;

@property (strong, nonatomic) IBOutlet UILabel *patient_guarantorLName;
@property (strong, nonatomic) IBOutlet UILabel *patient_Address;
@property (strong, nonatomic) IBOutlet UILabel *patient_City;
@property (strong, nonatomic) IBOutlet UILabel *patient_State;
@property (strong, nonatomic) IBOutlet UILabel *patient_Zip;
@property (strong, nonatomic) IBOutlet UILabel *patient_guarantorPNo;

@property (strong, nonatomic) IBOutlet UIButton *btn_edit_profile;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_Additional;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_patient;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_profile;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_Additional;
@property (strong, nonatomic) IBOutlet UIButton *btn_edit_land_patient;

@property (strong, nonatomic) IBOutlet UIButton *port_btn_profile;
@property (strong, nonatomic) IBOutlet UIButton *port_btn_additional;
@property (strong, nonatomic) IBOutlet UIButton *port_btn_minor;
@property (strong, nonatomic) IBOutlet UIButton *land_btn_profile;
@property (strong, nonatomic) IBOutlet UIButton *land_btn_additional;
@property (strong, nonatomic) IBOutlet UILabel *profile_gender;
@property (strong, nonatomic) IBOutlet UILabel *profile_mstatus;
@property (strong, nonatomic) IBOutlet UIButton *land_btn_minor;
@end
