//
//  PatientPhotosViewController.m
//  InTouchEMR
//
//  Created by Apple on 13/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//


//#define CREDENTIALS_ERROR_TITLE    @"Missing Credentials"
//#define CREDENTIALS_ERROR_MESSAGE  @"AWS Credentials not configured correctly.  Please review the README file."


#import "PatientPhotosViewController.h"
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import <AWSCognitoAuth/AWSCognitoAuth.h>
#import "PhotoViewController.h"
#import "PhotoXMLData.h"
#import "SHKActivityIndicator.h"
#import "HippaViewController.h"
#import "CompleteViewController.h"
#import "documentTypeList.h"

@interface PatientPhotosViewController ()

@end

@implementation PatientPhotosViewController
@synthesize popover,myImage,imgName,s3,DocumentName,PhotoTable,portraitView;
@synthesize landscapeDocumentName,landscapeimgName,landscapemyImage,landscapePhotoTable,landscapepopover,landscapeView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initwithArray:(NSMutableDictionary *)array
{
    
    arr=array;
    return self;
    
}
-(void)orientationChanged:(NSNotification *)notification{
    
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown)  {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
    // Do your orientation logic here
	
}



-(void)myOrientation
{
    [self performSelectorInBackground:@selector(mythread)
                           withObject:nil];
    
  
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		
		[self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        landscapeimgName.text=imgName.text;
    
            
        UIImageView *sectionView=[[UIImageView alloc]initWithFrame:CGRectMake(85, 345, 820, 40)];
        [sectionView setImage:[UIImage imageNamed:@"landscape_myhead.png"]];
        [landscapeView insertSubview:sectionView atIndex:1];
        
       
        
     
        
        
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		
		[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        imgName.text=landscapeimgName.text;
            
            UIImageView *sectionView=[[UIImageView alloc]initWithFrame:CGRectMake(62, 390, 630, 40)];
            [sectionView setImage:[UIImage imageNamed:@"myhead.png"]];
        [portraitView insertSubview:sectionView atIndex:1];
 
	}
}

-(void)mythread
{
    appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    PhotoXMLData *patientData=[[PhotoXMLData alloc]init];
    PhotoData=[patientData patientPhotoDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
    
   
    [self addDropDown];
    [self performSelectorOnMainThread:@selector(afterthread)
                           withObject:nil
                        waitUntilDone:NO];
    
    
}
-(void)afterthread
{
    [PhotoTable reloadData];
    [landscapePhotoTable reloadData];
     [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}


-(void) clearCurrentView {
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  
        
    [[SHKActivityIndicator currentIndicator]hidden];
    
   
  //  marr=[[NSMutableArray alloc]initWithObjects:@"Profile Image",@"Other",@"Script",@"XRay",@"MRI",@"Physician's Notes",@"Medicine Listing",@"Blood Work Results/Labs", nil];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	orientation = [[UIDevice currentDevice] orientation];
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
       if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    documenttype=@"Select One";
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
    [self myOrientation];
    [NSThread detachNewThreadSelector:@selector(clientCreate) toTarget:self withObject:nil];
}
-(void)clientCreate
{
    ACCESS_KEY_ID = [[NSBundle mainBundle]objectForInfoDictionaryKey:@"ACCESS_KEY_ID"];
    SECRET_KEY = [[NSBundle mainBundle]objectForInfoDictionaryKey:@"SECRET_KEY"];
    PICTURE_BUCKET = [[NSBundle mainBundle]objectForInfoDictionaryKey:@"PICTURE_BUCKET"];
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSWest2 identityPoolId:@"YourIdentityPoolId"];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSWest2 credentialsProvider:credentialsProvider];
    
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ImageSend:(id)sender {
    pickerImg= [[UIImagePickerController alloc]init];
    pickerImg.delegate = self;
    NSString *massage=@"Where would you like to get your photo from?";
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:massage delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Photo Library",@"Camera",nil];
    alert.delegate=self;
    [alert show];
    
}

- (void)processBackgroundThreadUpload:(NSData *)imagepatientData
{
    [self performSelectorInBackground:@selector(processBackgroundThreadUploadInBackground:)
                           withObject:imagepatientData];
}

- (void)processBackgroundThreadUploadInBackground:(NSData *)imagepatientData
{
    image_Name = [NSString stringWithFormat:@"%@.png",imgname_database];
    image_Name = [image_Name stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = PICTURE_BUCKET;
    uploadRequest.key = image_Name;
    uploadRequest.contentType = @"image/png";
    uploadRequest.serverSideEncryption = AWSS3ServerSideEncryptionAES256;
    uploadRequest.body = [NSURL URLWithString:image_Name];
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        [self showCheckErrorMessage:task.error];
        return nil;
    }];
}

- (void)showCheckErrorMessage:(NSError *)error
{
    if(error != nil)
    {
    
        [self showAlertMessage:[error.userInfo objectForKey:@"message"] withTitle:@"Upload Error"];
    }
    else
    {
        [self imageSave:image_Name];
        myImage.image=nil;
        landscapemyImage.image=nil;
        documenttype=@"Select One";
        imageData=nil;
        [DocumentName setTitle:@"Select One" forState:UIControlStateNormal];
        [landscapeDocumentName setTitle:@"Select One" forState:UIControlStateNormal];
        [imgName setPlaceholder:@"Document Name"];
        [landscapeimgName setPlaceholder:@"Document Name"];
       // [self showAlertMessage:@"The image was successfully uploaded." withTitle:@"Upload Completed"];
        
        PhotoXMLData *patientData=[[PhotoXMLData alloc]init];
        PhotoData=[patientData patientPhotoDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
        [PhotoTable reloadData];
        [landscapePhotoTable reloadData];
    }
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
   
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if([mediaType isEqualToString:@"public.image"])
    {
        
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        myImage.image=image;
        landscapemyImage.image=image;
        imageData = UIImageJPEGRepresentation(image, 1.0);
        if (newMedia)
            UIImageWriteToSavedPhotosAlbum(image,self,@selector(image:finishedSavingWithError:contextInfo:),nil);
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image on amazon"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL ];
}

#pragma mark - Helper Method

- (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alertView show];
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if (!doneButton) {
        doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                      style:UIBarButtonItemStylePlain
                                                     target:self action:@selector(done:)];
    }
    
    viewController.navigationItem.rightBarButtonItem = doneButton;
}
-(IBAction)done:(id)sender
{
   [self.popover dismissPopoverAnimated:YES];
}
- (IBAction)upload:(id)sender {
    
    
        NSDate *date=[NSDate date];
    NSDateFormatter *form=[[NSDateFormatter alloc]init];
    [form setDateFormat:@"ddMMHHmmss"];
    NSString *currentDate = [form stringFromDate:date];

    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
    
        imgname_database=landscapeimgName.text;
                
	}
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		
            imgname_database=imgName.text;
        
	}
    if (imgname_database == (id)[NSNull null] || imgname_database.length == 0) {
        imgname_database=@"patient_default";
    }
    saveImgname=[NSString stringWithFormat:@"%@",imgname_database];
    
    imgname_database=[imgname_database stringByAppendingString:currentDate];
  
      if (imageData==nil) {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:nil message:@"Please select Image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
   
    
      else if([documenttype isEqualToString:@"Select One"])
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:nil message:@"Please select Ducument Type" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
    }
    else
    {
        [self processBackgroundThreadUpload:imageData];
        NSThread *actThread=[[NSThread alloc]initWithTarget:self selector:@selector(threadMethod) object:nil];
        [actThread start];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    }
}


-(void)imageSave:(NSString *)imageName
{
    
    NSURL *url;
    NSData *postData;
    NSString *xmlText;
   
    
    NSDate *date=[NSDate date];
    NSDateFormatter *form=[[NSDateFormatter alloc]init];
    [form setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDate = [form stringFromDate:date];

    
    if (![documenttype isEqualToString:@"Select One"]) {
        
    
    
    if([documenttype isEqualToString:@"Profile Image"])
    {
       url = [NSURL URLWithString:appdalegate.profileImageSavedUrl];
    xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><PatientImage><patientID>%@</patientID><imageName>%@</imageName><dateOfReceipt>%@</dateOfReceipt><contentType>%@</contentType></PatientImage>",appdalegate.patientId,image_Name,currentDate,@"image/png"];
    postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
          NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        [request setURL:url];
       
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLResponse* response;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
              NSArray *respon=[responseString componentsSeparatedByString:@"\t"];
            NSString *profileImg=[respon objectAtIndex:0];
        NSString *responce=[respon objectAtIndex:1];
        if ([responce isEqualToString:@"success"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your image has been successfully uploaded" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [arr setObject:profileImg forKey:@"imageName"];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Your image has not uploaded" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        int a=[documentTypeName indexOfObject:documenttype];
        NSString *x=[DocumentTypeID objectAtIndex:a];
        
        url = [NSURL URLWithString:appdalegate.documentSavedUrl];
        
        xmlText = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Document><registrationID>%@</registrationID><patientID>%@</patientID><documentName>%@</documentName><documentTypeID>%i</documentTypeID><dateOfReceipt>%@</dateOfReceipt><documentUrl>%@</documentUrl><contentType>%@</contentType></Document>",appdalegate.registationID,appdalegate.patientId,saveImgname,x.intValue,currentDate,image_Name,@"image/png"];
           postData = [xmlText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
       
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
   
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse* response;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
          if ([responseString isEqualToString:@"success"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you" message:@"Your document has been successfully uploaded" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your document has not saved" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
 
    }
    }
    else
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:nil message:@"Please select Ducument Type" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        
    }
    
  }


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
       return [PhotoData count];
    }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   PhotoCustomCell * cell;
       if(tableView==PhotoTable)
    {
        
        static NSString *CellIdentifierPortrait = @"portrait";
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PhotoCustomCell" owner:self options:nil];
        
        cell=(PhotoCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
        
        if (cell==nil) {
            cell=[[ PhotoCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifierPortrait];
            cell=[nib objectAtIndex:0];
        }
        
        
        tempdict = [PhotoData objectAtIndex:indexPath.row];
    cell.Name.text=[tempdict objectForKey:@"documentName"];
  
        if ([tempdict objectForKey:@"documentTypeID"]) {
            
            if ([DocumentTypeID indexOfObject:[tempdict objectForKey:@"documentTypeID"]] != NSNotFound) {

            int a=[DocumentTypeID indexOfObject:[tempdict objectForKey:@"documentTypeID"]];
            cell.Type.text=[documentTypeName objectAtIndex:a];
        }
            else
            {
                 cell.Type.text=@"";
            }
        }
        else
        {
             cell.Type.text=@"";
        }
        
       
        
    cell.Date.text=[self viewDate:[tempdict objectForKey:@"receiptDate"]];
    [cell.accessoryButton addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside]; 
        
}
   else if(tableView==landscapePhotoTable)
   {
       
       static NSString *CellIdentifier = @"landscape";
       
       NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PhotoCustomCell" owner:self options:nil];
       
       cell=(PhotoCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       
       if (cell==nil) {
           cell=[[ PhotoCustomCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
           cell=[nib objectAtIndex:1];
       }
       
       
       tempdict = [PhotoData objectAtIndex:indexPath.row];
       
       if ([tempdict objectForKey:@"documentName"]) {
            cell.Name.text=[tempdict objectForKey:@"documentName"];
       }
      else
      {
           cell.Name.text=@"";
      }
       if ([tempdict objectForKey:@"documentTypeID"]) {
         
             if ([DocumentTypeID indexOfObject:[tempdict objectForKey:@"documentTypeID"]] != NSNotFound) {
           int a=[DocumentTypeID indexOfObject:[tempdict objectForKey:@"documentTypeID"]];
           
           cell.Type.text=[documentTypeName objectAtIndex:a];
       }
           else
           {
               cell.Type.text=@"";
           }
       }
       
       else
       {
           cell.Type.text=@"";
       }
       
       if ([tempdict objectForKey:@"receiptDate"]) {
          cell.Date.text=[self viewDate:[tempdict objectForKey:@"receiptDate"]];
       }
       
       [cell.accessoryButton addTarget:self action:@selector(accessoryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
       
      
   }
    return cell;
}

- (void)accessoryButtonTapped:(id)sender event:(id)event
{
    NSThread *actThread=[[NSThread alloc]initWithTarget:self selector:@selector(threadMethod) object:nil];
    [actThread start];
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition;
    NSIndexPath *indexPath;
	if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
       currentTouchPosition = [touch locationInView:self.landscapePhotoTable];
        indexPath = [self.landscapePhotoTable indexPathForRowAtPoint: currentTouchPosition];
	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        currentTouchPosition = [touch locationInView:self.PhotoTable];
        indexPath = [self.PhotoTable indexPathForRowAtPoint: currentTouchPosition];
	}
    
   
 
	if (indexPath != nil)
	{
        [self tableView: self.PhotoTable accessoryButtonTappedForRowWithIndexPath: indexPath];
        
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
    PhotoViewController *patientPhoto= [[PhotoViewController alloc]initwithImageName:[PhotoData objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:patientPhoto animated:YES];
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
    
}
-(void)threadMethod
{
    
    [[SHKActivityIndicator currentIndicator]displayActivity:@"Loading.."];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (tableView==documentTypeTable) {
//       
//    [DocumentName setTitle:[marr objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//        documenttype=[marr objectAtIndex:indexPath.row];
//       
//    documentTypeTable.hidden=YES;
//    }
//    else if (tableView==landscapedocumentTypeTable) {
//        [landscapeDocumentName setTitle:[marr objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//        
//         documenttype=[marr objectAtIndex:indexPath.row];
//        landscapedocumentTypeTable.hidden=YES;
//    }
   if(tableView==PhotoTable||tableView==landscapePhotoTable)
    {

        PhotoViewController *patientPhoto= [[PhotoViewController alloc]initwithImageName:[PhotoData objectAtIndex:indexPath.row]];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
             [self.navigationController pushViewController:patientPhoto animated:YES];
        
    
                
        
    }
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
                      if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                
                pickerImg.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                
                self.popover = [[UIPopoverController alloc]
                                initWithContentViewController:pickerImg];
                popover.delegate = self;
                if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
                [popover presentPopoverFromRect:CGRectMake(500 ,130, 100, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES] ;
                
                }
                else
                {
                    [popover presentPopoverFromRect:CGRectMake(330 ,100, 200, 200) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES] ;
                   
                }
                newMedia=NO;
            }
            break;
        case 2:
           
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                
                [pickerImg setDelegate:self];
                [pickerImg setSourceType:UIImagePickerControllerSourceTypeCamera];
                pickerImg.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
                pickerImg.showsCameraControls = YES;
                [self presentViewController:pickerImg animated:YES completion:nil];
                
                newMedia=YES;

                
            }
            else
            {
                              UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: @"Message"
                                      message: @"Camera Not Support"
                                      delegate: nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
                [alert show];
                return;
            }
            break;
        default:
            
            break;
    }
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}


/*
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

 */

- (IBAction)submit:(id)sender{
    [[SHKActivityIndicator currentIndicator]hide];
    NSLog(@"%@",arr);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *StatusValue = [defaults stringForKey:@"hippaaAgreementSigned"];
    NSLog(@"Task: %@",StatusValue);

//    [[arr objectForKey:@"hippaaAgreementSigned"]isEqualToString:@"false"]
    
    if ([StatusValue isEqualToString:@"false"]) {
        HippaViewController *hippa=[[HippaViewController alloc]init];
        [self.navigationController pushViewController:hippa animated:YES];
    }
    else
    {
        
        CompleteViewController *lastView=[[CompleteViewController alloc]init];
        [self.navigationController pushViewController:lastView animated:YES];
    }
    
   
  
}

- (IBAction)selectClicked:(id)sender {
    UIButton *btr=(UIButton *)sender;
    if (btr.tag==1) {
        
    //    NSArray * arr1 = [[NSArray alloc] initWithObjects:@"Profile Image",@"Other",@"Script",@"XRay",@"MRI",@"Physician's Notes",@"Medicine Listing",@"Blood Work Results/Labs", nil];
        
        if(dropDown == nil) {
            CGFloat f = 240;
             dropDown = [[NIDropDown alloc]showDropDown:sender H:&f A:documentTypeName IA:nil D:@"down"];
            dropDown.delegate = self;
            
            
           
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
            
        }
       
    }
    
}
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
     
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight))
    {
        documenttype=[landscapeDocumentName titleForState:UIControlStateNormal];
    }
    else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    
    {
        documenttype=[DocumentName titleForState:UIControlStateNormal];
    }
 
 
}



-(NSString *)viewDate:(NSString *)strdate
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [formater dateFromString:strdate];
    [formater setDateFormat:@"MM-dd-yyyy"];
    NSString *result = [formater stringFromDate:date2];
  
    return result;
    
}

-(void)addDropDown
{
    DocumentTypeData=[[NSMutableArray alloc]init];
    documentTypeList *documentList=[[documentTypeList alloc]init];
    DocumentTypeData=[documentList DocumentTypeListData];
    
    NSDictionary *abc=[[NSDictionary alloc]initWithObjectsAndKeys:@"1",@"id",@"Profile Image",@"name", nil];
    [DocumentTypeData insertObject:abc atIndex:0];
    
  
    documentTypeName=[DocumentTypeData valueForKey:@"name"];
    DocumentTypeID=[DocumentTypeData valueForKey:@"id"];
   
    
}




@end
