//
//  patientDemographicsData.h
//  InTouchEMR
//
//  Created by Apple on 15/04/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface patientDemographicsData : NSObject<NSXMLParserDelegate>
{
    NSString *myelement;
    NSMutableDictionary *dict;
    NSMutableArray *arr;
}
-(NSMutableArray *)patientDataByXML:(NSString *)patientID registationNO:(NSString *)registationID;
@end
