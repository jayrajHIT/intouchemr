//
//  InTouchViewController.m
//  InTouchEMR
//
//  Created by Pradeep Singhal on 22/05/13.
//  Copyright (c) 2013 DSC. All rights reserved.
//

#import "InTouchViewController.h"
#import "IndexAppDelegate.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QuartzCore/QuartzCore.h"
#import "inTouchXML.h"
#import "patientAddressData.h"
#import "patientContactXMLData.h"
#import "SHKActivityIndicator.h"
#import "LoginViewController.h"

@interface InTouchViewController ()

@end

@implementation InTouchViewController
@synthesize patientContact,patientEmail,patientName,patientOrigin,ssnHeading,ssnValue,patientImage;
@synthesize landscape_patientContact,landscape_patientEmail,landscape_patientImage,landscape_patientName,landscape_patientOrigin,landscape_ssnHeading,landscape_ssnValue,landscapeView,portraitView,landscapebackImg;
@synthesize port_appointment,port_newsletter,land_appointment,land_newsletter,backImg,port_callremaining,port_smsremaining,land_callremaining,land_smsremaining;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initwithArray:(NSMutableDictionary *)array
{
    arr=array;
    return self;
    
}
-(void)orientationChanged:(NSNotification *)notification{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    UIDeviceOrientation newOrientation = [[UIDevice currentDevice] orientation];
	
    if (newOrientation != UIDeviceOrientationUnknown && newOrientation != UIDeviceOrientationFaceUp && newOrientation != UIDeviceOrientationFaceDown) {
		if (newOrientation==orientation) {
            
        }
        else
        {
            orientation=newOrientation;
            
            [self myOrientation];
        }
	}
    else if (newOrientation == UIDeviceOrientationFaceUp || newOrientation ==UIDeviceOrientationFaceDown)
    {
        
    }
}

-(void)Intouch
{
    IndexAppDelegate *appdalegate=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if (!addressData) {
     
    patientAddressData *AddressXML=[[patientAddressData alloc]init];
    addressData=[AddressXML patientAddressDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
      
    }
    
    if (!contactData) {
        
        patientContactXMLData *contactXML=[[patientContactXMLData alloc]init];
        contactData=[contactXML patientContactDataByXML:appdalegate.patientId registationNO:appdalegate.registationID];
    }
        inTouchXML *intouchXMLData=[[inTouchXML alloc]init];
        
        requestData=[intouchXMLData getnewsletterXML:appdalegate.registationID patientID:appdalegate.patientId];
    
   
    NSDictionary *temp=[requestData objectAtIndex:0];
  
    if ([addressData count]>0) {
        
        if ([[temp objectForKey:@"isTherapySubscription"]isEqualToString:@"true"])
        {
            [port_newsletter setOn:YES];
            [land_newsletter setOn:YES];
        }
        else
        {
            [port_newsletter setOn:NO];
            [land_newsletter setOn:NO];
        }
    }
    else
    {
        [port_newsletter setOn:NO];
        [land_newsletter setOn:NO];
        
    }
    
    if ([temp objectForKey:@"callRemaining"]) {
        port_callremaining.text=[temp objectForKey:@"callRemaining"];
        land_callremaining.text=[temp objectForKey:@"callRemaining"];
    }
    else
    {
        port_callremaining.text=@"";
        land_callremaining.text=@"";
    }
    if ([temp objectForKey:@"smsRemaining"]) {
        port_smsremaining.text=[temp objectForKey:@"smsRemaining"];
        land_smsremaining.text=[temp objectForKey:@"smsRemaining"];
    }
    else
    {
        port_smsremaining.text=@"";
        land_smsremaining.text=@"";
    }
    
           if ([[temp objectForKey:@"appointmentReminderStatus"]isEqualToString:@"0"]) {
            [port_appointment setOn:NO];
            [land_appointment setOn:NO];
        }
        else
        {
            [port_appointment setOn:YES];
            [land_appointment setOn:YES];
        }
    }


-(void)myOrientation
{
    [self Intouch];
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
		// Clear the current view and insert the orientation specific view.
		[self clearCurrentView];
		[self.view insertSubview:landscapeView atIndex:0];
        
        if ([arr objectForKey:@"FullName"]) {
            landscape_patientName.text=[arr objectForKey:@"FullName"];
        }
        else
        {
            landscape_patientName.text=@"";
        }
        
        if ([arr objectForKey:@"email"]) {
            landscape_patientEmail.text=[arr objectForKey:@"email"];
        }
        else
        {
            landscape_patientEmail.text=@"";
        }

        if ([arr objectForKey:@"ssn"]) {
            landscape_ssnValue.text=[arr objectForKey:@"ssn"];
        }
        else
        {
            landscape_ssnValue.text=@"";
        }

        if ([arr objectForKey:@"ethnicOriginName"]) {
            landscape_patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
        }
        else
        {
            landscape_patientOrigin.text=@"";
        }
        
        if ([arr objectForKey:@"imageName"]!=nil) {
            NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
            [landscape_patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
        }
        else
        {
            landscape_patientImage.image=[UIImage imageNamed:@"defaultpatient.jpeg"];
        }
        
        footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 660, 1024, 50)];
        [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
        [self.landscapeView addSubview:footerView];
        
       	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
		
		[self clearCurrentView];
		[self.view insertSubview:portraitView atIndex:0];
        
            if ([arr objectForKey:@"FullName"]) {
                patientName.text=[arr objectForKey:@"FullName"];
            }
            else
            {
              patientName.text=@"";
            }
            if ([arr objectForKey:@"email"]) {
                patientEmail.text=[arr objectForKey:@"email"];
            }
            else
            {
                patientEmail.text=@"";
            }
            if ([arr objectForKey:@"ssn"]) {
                ssnValue.text=[arr objectForKey:@"ssn"];
            }
            else
            {
                ssnValue.text=@"";
            }
            if ([arr objectForKey:@"ethnicOriginName"]) {
                patientOrigin.text=[arr objectForKey:@"ethnicOriginName"];
            }
            else
            {
                patientOrigin.text=@"";
            }
    
        NSString *docName=[NSString stringWithFormat:@"%@",[arr objectForKey:@"imageName"]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/patientimage/%@",docName]];
        [patientImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultpatient.jpeg"]];
            footerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 910, 770, 50)];
            [footerView setImage:[UIImage imageNamed:@"bg_footer.png"]];
            [self.portraitView addSubview:footerView];
        }
}
-(void) clearCurrentView {
  
	if (landscapeView.superview) {
		[landscapeView removeFromSuperview];
	} else if (portraitView.superview) {
		[portraitView removeFromSuperview];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.navigationItem.title=@"In Touch";
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc]
                                initWithTitle:@"Logout"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:@selector(logout:)];
    self.navigationItem.rightBarButtonItem=btnSave;
    self.backImg.layer.borderWidth=2.0;
    self.backImg.layer.cornerRadius=15.0;
    self.backImg.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.landscapebackImg.layer.borderWidth=2.0;
    self.landscapebackImg.layer.cornerRadius=15.0;
    self.landscapebackImg.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [port_newsletter setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [port_newsletter setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    [land_newsletter setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [land_newsletter setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    [port_appointment setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [port_appointment setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    [land_appointment setOnImage:[UIImage imageNamed:@"yesImage.png"]];
    [land_appointment setOffImage:[UIImage imageNamed:@"noImage.jpeg"]];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification"  object:nil];
	orientation = [[UIDevice currentDevice] orientation];
	UIApplication *app = [UIApplication sharedApplication];
    UIInterfaceOrientation currentOrientation = app.statusBarOrientation;
 
    if(UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        orientation = UIDeviceOrientationPortrait;
        
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
   // [self Intouch];
    [self myOrientation];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)logout:(id)sender {
    
    UIAlertView *logoutAlert=[[UIAlertView alloc]initWithTitle:@"Do you really want to log out?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [logoutAlert show];
    logoutAlert.tag=1;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1)
    {
    switch (buttonIndex) {
        case 1:
        {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
        }
            
        default:
            break;
    }
    }
    else if(alertView.tag==2)
    {
        switch (buttonIndex) {
            case 1:
            {
                [self updateNewsletters:@"1"];
                break;
            }
            case 0:
            {
                [port_newsletter setOn:NO];
                [land_newsletter setOn:NO];
                break;
            }
                
            default:
                break;
        }
    }
    else if(alertView.tag==3)
    {
        switch (buttonIndex) {
            case 1:
            {
                [self updateNewsletters:@"0"];
                break;
            }
            case 0:
            {
                [port_newsletter setOn:YES];
                [land_newsletter setOn:YES];
                break;
            }
                
            default:
                break;
        }
    }
    else if(alertView.tag==4)
    {
        switch (buttonIndex) {
            case 1:
            {
                [self appointmentReinder:@"1"];
                break;
            }
            case 0:
            {
                [port_appointment setOn:NO];
                [land_appointment setOn:NO];
                break;
            }
                
            default:
                break;
        }
    }
    else if(alertView.tag==5)
    {
        switch (buttonIndex) {
            case 1:
            {
                [self appointmentReinder:@"0"];
                break;
            }
            case 0:
            {
                [port_appointment setOn:YES];
                [land_appointment setOn:YES];
                break;
            }
                
            default:
                break;
        }
    }
       
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)updatenewsletter:(id)sender
{
    IndexAppDelegate *globleData=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
   
  
    if ([addressData count]>0) {
        
           if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
               
                if ([globleData.newsletterStatus isEqualToString:@"1"]) {
               
        if ([land_newsletter isOn]) {
          log=[[UIAlertView alloc]initWithTitle:@"Would you like to enable newsletters?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            [log show];
            log.tag=2;
            
        }
        else
        {
           log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable newsletters?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            [log show];
            log.tag=3;
        }
                }
               else
               {
                   if ([land_newsletter isOn]) {
                //      log=[[UIAlertView alloc]initWithTitle:@"Please enable your Therapy Newsletter account " message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                log=[[UIAlertView alloc]initWithTitle:@"Would you like to enable appointment reminders? " message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                       [log show];
                       log.tag=2;
                       
                   }
                   else
                   {
                       log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable newsletters?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
                       [log show];
                       log.tag=3;
                   }
               }
        
 	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        if ([globleData.newsletterStatus isEqualToString:@"1"]) {
                
        if ([port_newsletter isOn]) {
            
            log=[[UIAlertView alloc]initWithTitle:@"Would you like to enable newsletters?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            [log show];
            log.tag=2;
            
        }
        else
        {
           log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable newsletters?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            [log show];
            log.tag=3;
        }
        }
        else
        {
            if ([port_newsletter isOn]) {
               log=[[UIAlertView alloc]initWithTitle:@"Please enable your Therapy Newsletter account " message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                [log show];
                log.tag=2;
                
            }
            else
            {
                log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable newsletters?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
                [log show];
                log.tag=3;
            }
        }
    }
        
}
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your address information does not exist" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [port_newsletter setOn:NO];
        [land_newsletter setOn:NO];
        
    }
}


-(void)updateNewsletters:(NSString *)newslet
{
    NSThread *actThread=[[NSThread alloc]initWithTarget:self selector:@selector(threadtest) object:nil];
    [actThread start];
    
    IndexAppDelegate *globleValue=(IndexAppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString *postStr = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",globleValue.newsletterUpdateURL,[arr objectForKey:@"patientID"],newslet,globleValue.newsletterStatus,globleValue.username];
    NSLog(@"%@",postStr);
    NSURLRequest *request =[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:postStr]];
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *temp = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    
    if ([temp isEqualToString:@"true"]) {
        [port_newsletter setOn:YES];
        [land_newsletter setOn:YES];
    }
    else if ([temp isEqualToString:@"false"])
    {
        [port_newsletter setOn:NO];
        [land_newsletter setOn:NO];
    }
    else
    {
        if ([newslet isEqualToString:@"0"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            [port_newsletter setOn:YES];
            [land_newsletter setOn:YES];
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [port_newsletter setOn:NO];
            [land_newsletter setOn:NO];
        }
        
    }
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
    
}

-(IBAction)updateappointment:(id)sender
{
    IndexAppDelegate *globleData=(IndexAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if ([contactData count]>0) {
   
    if ((orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        
        if ([globleData.appointmentStatus isEqualToString:@"1"]) {
               
        if ([land_appointment isOn]) {
            
            log=[[UIAlertView alloc]initWithTitle:@"Would you like to enable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [log show];
            log.tag=4;
           
        }
        else
        {
            log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [log show];
            log.tag=5;
        }
        }
        else
        {
            if ([land_appointment isOn]) {
               // log=[[UIAlertView alloc]initWithTitle:@" Please enable your appointment reminders" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                 log=[[UIAlertView alloc]initWithTitle:@"Would you like to enable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                [log show];
                log.tag=4;
                
            }
            else
            {
                log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
                [log show];
                log.tag=5;
            }
        }
    
        
 	} else if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {
        
        if ([globleData.appointmentStatus isEqualToString:@"1"])
        {
        
        if ([port_appointment isOn]) {
            
           log=[[UIAlertView alloc]initWithTitle:@"Would you like to enable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [log show];
            log.tag=4;
        }
        else
        {
           log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [log show];
            log.tag=5;
        }
    }
    
    else
    {
        if ([port_appointment isOn]) {
          //  log=[[UIAlertView alloc]initWithTitle:@" Please enable your appointment reminders" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
              log=[[UIAlertView alloc]initWithTitle:@"Would you like to enable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
            [log show];
            log.tag=4;
            
        }
        else
        {
           log=[[UIAlertView alloc]initWithTitle:@"Would you like to disable appointment reminders?" message:nil delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            [log show];
            log.tag=5;
           
        }
    }
    }
        
    }

    
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your contact information does not exist" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [port_appointment setOn:NO];
        [land_appointment setOn:NO];
        
    }
        
}

-(void)appointmentReinder:(NSString *)reminderStatus
{
   
    NSThread *abcd=[[NSThread alloc]initWithTarget:self selector:@selector(threadtest) object:nil];
    [abcd start];

    IndexAppDelegate *globleValue=(IndexAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString *postStr = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",globleValue.appointmentUpdateURL,[arr objectForKey:@"patientID"],reminderStatus,globleValue.appointmentStatus,globleValue.username];
    
    NSURLRequest *request =[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:postStr]];
    NSURLResponse *response;
    NSError *error;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *temp = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];

    if ([temp isEqualToString:@"Reminder activated"]) {
        [port_appointment setOn:YES];
        [land_appointment setOn:YES];
    }
    else if ([temp isEqualToString:@"Reminder deactivated"])
    {
        [port_appointment setOn:NO];
        [land_appointment setOn:NO];
    }
    else
    {
        if ([reminderStatus isEqualToString:@"0"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            [port_appointment setOn:YES];
            [land_appointment setOn:YES];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [port_appointment setOn:NO];
            [land_appointment setOn:NO];
        }
    }
    [[SHKActivityIndicator currentIndicator]hideAfterDelay];
}

-(void)threadtest
{
    [[SHKActivityIndicator currentIndicator]displayActivity:@""];
}

@end
